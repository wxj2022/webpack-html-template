var path = require('path')
var utils = require('./utils')
var config = require('../config')

function resolve(dir) {
    return path.join(__dirname, '..', dir)
}

module.exports = {
    entry: {
        header: './src/static/js/header',
        index: './src/static/index',
        pageA: './src/static/pageA',

    },
    output: {
        path: config.build.assetsRoot,
        filename: '[name].js',
        publicPath: process.env.NODE_ENV === 'production'
            ? config.build.assetsPublicPath
            : config.dev.assetsPublicPath
    },

    module: {
        rules: [

            /*
             {
             test: /\.(js|vue)$/,
             loader: 'eslint-loader',
             enforce: "pre",
             include: [resolve('src'), resolve('test')],
             options: {
             formatter: require('eslint-friendly-formatter')
             }
             },*/ {
                test: /\.html$/,
                loader: 'ejs-loader',
                query: {
                    variable: 'data',
                    interpolate: '\\{\\{(.+?)\\}\\}',
                    evaluate: '\\[\\[(.+?)\\]\\]'
                }
            }, {
                test: /\.html/,
                loader: 'ejs-html-loader',
                options: {
                    title: 'The Ant: An Introduction',
                    season: 1,
                    episode: 9,
                    production: process.env.ENV === 'production'
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: [resolve('src'), resolve('test')]
            },

            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                loader: 'url-loader',
                query: {
                    limit: 10000,
                    name: utils.assetsPath('img/[name].[hash:7].[ext]')
                }
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'url-loader',
                query: {
                    limit: 10000,
                    name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
                }
            },
            {
                test: /vue-scroller.src.*?js$/,
                loader: 'babel'
            }
        ]
    }
}
