function SN(a) {
    this.settings = arguments[0];
}
var deleteFolder = function (path) {
    var fs = require('fs');
    var files = [];
    if (fs.existsSync(path)) {
        files = fs.readdirSync(path);
        files.forEach(function (file, index) {
            var curPath = path + "/" + file;
            if (fs.statSync(curPath).isDirectory()) { // recurse
                deleteFolder(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
}
var logout = function (text) {
    console.log("***********************************************************");
    console.log(text);
    console.log("***********************************************************");
}
SN.prototype.apply = function (compiler) {
    var that = this;
    compiler.plugin("done", function (params) {
        console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

        console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
        /*数据描述*/
        var desc = {
            path: '/index.html',
            size: '8',
            msg: '12345',
            v_version: '1.0.1',
            code: 'md5(1.0.1+8+msg)'
        };
        var fs = require('fs');
        /*获取文件大小*/
        var size = fs.statSync(that.settings.path + '/index.html').size;
        desc.size = size;
        /*版本号*/
        var data = fs.readFileSync(this.options.context + '/package.json');
        var package = JSON.parse(data);
        desc.v_version = package.version;
        /*生成随机消息*/
        desc.msg = package.updateDes;
        /*计算md5*/

        var crypto = require('crypto');
        var h = crypto.createHash('md5');
        var html = fs.readFileSync(that.settings.path + '/index.html', 'utf-8').substring(0, size);
        h.update(`${desc.v_version}${html}${desc.msg}`);
        var ret = h.digest('hex');
        desc.code = ret;
        fs.writeFileSync(that.settings.path + '//sn.txt', JSON.stringify(desc));
        /*自动跟新版本号*/
        /*let version = package.version;
         let temps = version.split('.');
         temps[2] = temps[2] * 1 + 1;
         package.version = temps.join('.');

         fs.writeFile(this.options.context + '/package.json', JSON.stringify(package), function (err) {
         if (err) throw err;
         console.log("version updated!");
         });*/


        /**
         *文件路径替换
         */
        var pathStr = this.options.plugins[4]["assetJson"];
        var paths = JSON.parse(pathStr);


        logout("生成文件路径集：");
        logout("path:" + paths);


        var path = that.settings.path + paths[1].substring(1);

        logout("文件路径：");
        logout("path:" + path);

        var data = fs.readFileSync(path, 'utf-8');
        data = data.replace(/static\//g, "../");


        logout("文件路径替换开始：");

        fs.writeFileSync(path, data);

        /*替换 js中的写死的路径*/
        var argv = require('minimist')(process.argv.slice(2));
        var server = argv['server'];
        if (server) {
            path=that.settings.path + paths[2].substring(1);
            logout("path:" + path);
            var data = fs.readFileSync(path, 'utf-8');
            data = data.replace(/https:..lo-test.yingu.com/g, server);
            fs.writeFileSync(path, data);
        }
        // var cssData = fs.readFileSync(this.options.plugins[4]["assetJson"]; + '/package.json');

        /*清理不需要的文件*/
        deleteFolder(that.settings.path + '/static/js/data');
        deleteFolder(that.settings.path + '/static/image');
        deleteFolder(that.settings.path + '/static/font');
        deleteFolder(that.settings.path + '/static/plug-in');
        deleteFolder(that.settings.path + '/static/scss');
        try {
            fs.unlinkSync(that.settings.path + '/dist.zip');
        } catch (e) {
            logout("清理文件不存在：" + that.settings.path);
        }

        logout("文件清理完成");
        /*打包文件*/
        var zipper = require("zip-local");
        zipper.sync.zip(that.settings.path).compress().save(that.settings.path + '/dist.zip');
    });
};

module.exports = SN;
