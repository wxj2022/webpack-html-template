export default [{
    'areaCode' : '110000',
    'areaName' : '北京市',
    'py' : 'beijing',
    'spy' : 'bj',
    'datas' : [{
        'areaCode' : '110100',
        'areaName' : '北京市',
        'py' : 'beijing',
        'spy' : 'bj',
        'datas' : [{
            'areaCode' : '110101',
            'areaName' : '东城区',
            'py' : 'dongcheng',
            'spy' : 'dc'
        }, {
            'areaCode' : '110102',
            'areaName' : '西城区',
            'py' : 'xicheng',
            'spy' : 'xc'
        }, {
            'areaCode' : '110103',
            'areaName' : '崇文区',
            'py' : 'chongwen',
            'spy' : 'cw'
        }, {
            'areaCode' : '110104',
            'areaName' : '宣武区',
            'py' : 'xuanwu',
            'spy' : 'xw'
        }, {
            'areaCode' : '110105',
            'areaName' : '朝阳区',
            'py' : 'chaoyang',
            'spy' : 'cy'
        }, {
            'areaCode' : '110106',
            'areaName' : '丰台区',
            'py' : 'fengtai',
            'spy' : 'ft'
        }, {
            'areaCode' : '110107',
            'areaName' : '石景山区',
            'py' : 'shijingshan',
            'spy' : 'sjs'
        }, {
            'areaCode' : '110108',
            'areaName' : '海淀区',
            'py' : 'haidian',
            'spy' : 'hd'
        }, {
        'areaCode' : '110109',
        'areaName' : '门头沟区',
        'py' : 'mentougou',
        'spy' : 'mtg'
    }, {
    'areaCode' : '110111',
    'areaName' : '房山区',
    'py' : 'fangshan',
    'spy' : 'fs'
}, {
    'areaCode' : '110112',
    'areaName' : '通州区',
    'py' : 'tongzhou',
    'spy' : 'tz'
}, {
    'areaCode' : '110113',
    'areaName' : '顺义区',
    'py' : 'shunyi',
    'spy' : 'sy'
}, {
    'areaCode' : '110114',
    'areaName' : '昌平区',
    'py' : 'changping',
    'spy' : 'cp'
}, {
    'areaCode' : '110115',
    'areaName' : '大兴区',
    'py' : 'daxing',
    'spy' : 'dx'
}, {
    'areaCode' : '110116',
    'areaName' : '怀柔区',
    'py' : 'huairou',
    'spy' : 'hr'
}, {
    'areaCode' : '110117',
    'areaName' : '平谷区',
    'py' : 'pinggu',
    'spy' : 'pg'
}, {
    'areaCode' : '110228',
    'areaName' : '密云县',
    'py' : 'miyun',
    'spy' : 'my'
}, {
    'areaCode' : '110229',
    'areaName' : '延庆县',
    'py' : 'yanqing',
    'spy' : 'yq'
}]
    }]
}, {
    'areaCode' : '120000',
    'areaName' : '天津市',
    'py' : 'tianjin',
    'spy' : 'tj',
    'datas' : [{
        'areaCode' : '120100',
        'areaName' : '天津市',
        'py' : 'tianjin',
        'spy' : 'tj',
        'datas' : [{
            'areaCode' : '120101',
            'areaName' : '和平区',
            'py' : 'heping',
            'spy' : 'hp'
        }, {
            'areaCode' : '120102',
            'areaName' : '河东区',
            'py' : 'hedong',
            'spy' : 'hd'
        }, {
            'areaCode' : '120103',
            'areaName' : '河西区',
            'py' : 'hexi',
            'spy' : 'hx'
        }, {
            'areaCode' : '120104',
            'areaName' : '南开区',
            'py' : 'nankai',
            'spy' : 'nk'
        }, {
            'areaCode' : '120105',
            'areaName' : '河北区',
            'py' : 'hebei',
            'spy' : 'hb'
        }, {
            'areaCode' : '120106',
            'areaName' : '红桥区',
            'py' : 'hongqiao',
            'spy' : 'hq'
        }, {
            'areaCode' : '120107',
            'areaName' : '塘沽区',
            'py' : 'tanggu',
            'spy' : 'tg'
        }, {
            'areaCode' : '120108',
            'areaName' : '汉沽区',
            'py' : 'hangu',
            'spy' : 'hg'
        }, {
        'areaCode' : '120109',
        'areaName' : '大港区',
        'py' : 'dagang',
        'spy' : 'dg'
    }, {
    'areaCode' : '120110',
    'areaName' : '东丽区',
    'py' : 'dongli',
    'spy' : 'dl'
}, {
    'areaCode' : '120111',
    'areaName' : '西青区',
    'py' : 'xiqing',
    'spy' : 'xq'
}, {
    'areaCode' : '120112',
    'areaName' : '津南区',
    'py' : 'jinnan',
    'spy' : 'jn'
}, {
    'areaCode' : '120113',
    'areaName' : '北辰区',
    'py' : 'beichen',
    'spy' : 'bc'
}, {
    'areaCode' : '120114',
    'areaName' : '武清区',
    'py' : 'wuqing',
    'spy' : 'wq'
}, {
    'areaCode' : '120115',
    'areaName' : '宝坻区',
    'py' : 'baodi',
    'spy' : 'bd'
}, {
    'areaCode' : '120116',
    'areaName' : '滨海新区',
    'py' : 'binghaixinqu',
    'spy' : 'bhxq'
}, {
    'areaCode' : '120221',
    'areaName' : '宁河县',
    'py' : 'ninghe',
    'spy' : 'nh'
}, {
    'areaCode' : '120223',
    'areaName' : '静海县',
    'py' : 'jinghai',
    'spy' : 'jh'
}, {
    'areaCode' : '120225',
    'areaName' : '蓟县',
    'py' : 'jixian',
    'spy' : 'jx'
}]
    }]
}, {
    'areaCode' : '130000',
    'areaName' : '河北省',
    'py' : 'hebei',
    'spy' : 'hb',
    'datas' : [{
        'areaCode' : '130100',
        'areaName' : '石家庄市',
        'py' : 'shijiazhuang',
        'spy' : 'sjz',
        'datas' : [{
            'areaCode' : '130102',
            'areaName' : '长安区',
            'py' : 'changan',
            'spy' : 'ca'
        }, {
            'areaCode' : '130103',
            'areaName' : '桥东区',
            'py' : 'qiaodong',
            'spy' : 'qd'
        }, {
            'areaCode' : '130104',
            'areaName' : '桥西区',
            'py' : 'qiaoxi',
            'spy' : 'qx'
        }, {
            'areaCode' : '130105',
            'areaName' : '新华区',
            'py' : 'xinhua',
            'spy' : 'xh'
        }, {
            'areaCode' : '130107',
            'areaName' : '井陉矿区',
            'py' : 'jingxingkuang',
            'spy' : 'jxk'
        }, {
            'areaCode' : '130108',
            'areaName' : '裕华区',
            'py' : 'yuhua',
            'spy' : 'yh'
        }, {
            'areaCode' : '130121',
            'areaName' : '井陉县',
            'py' : 'jingxing',
            'spy' : 'jx'
        }, {
            'areaCode' : '130123',
            'areaName' : '正定县',
            'py' : 'zhengding',
            'spy' : 'zd'
        }, {
        'areaCode' : '130124',
        'areaName' : '栾城县',
        'py' : 'luancheng',
        'spy' : 'lc'
    }, {
    'areaCode' : '130125',
    'areaName' : '行唐县',
    'py' : 'xingtang',
    'spy' : 'xt'
}, {
    'areaCode' : '130126',
    'areaName' : '灵寿县',
    'py' : 'lingshou',
    'spy' : 'ls'
}, {
    'areaCode' : '130127',
    'areaName' : '高邑县',
    'py' : 'gaoyi',
    'spy' : 'gy'
}, {
    'areaCode' : '130128',
    'areaName' : '深泽县',
    'py' : 'shenze',
    'spy' : 'sz'
}, {
    'areaCode' : '130129',
    'areaName' : '赞皇县',
    'py' : 'zanhuang',
    'spy' : 'zh'
}, {
    'areaCode' : '130130',
    'areaName' : '无极县',
    'py' : 'wuji',
    'spy' : 'wj'
}, {
    'areaCode' : '130131',
    'areaName' : '平山县',
    'py' : 'pingdingshan',
    'spy' : 'pds'
}, {
    'areaCode' : '130132',
    'areaName' : '元氏县',
    'py' : 'yuanshi',
    'spy' : 'ys'
}, {
    'areaCode' : '130133',
    'areaName' : '赵县',
    'py' : 'zhaoxian',
    'spy' : 'zx'
}, {
    'areaCode' : '130181',
    'areaName' : '辛集市',
    'py' : 'xinji',
    'spy' : 'xj'
}, {
    'areaCode' : '130182',
    'areaName' : '藁城市',
    'py' : 'gaocheng',
    'spy' : 'gc'
}, {
    'areaCode' : '130183',
    'areaName' : '晋州市',
    'py' : 'jinzhou',
    'spy' : 'jz'
}, {
    'areaCode' : '130184',
    'areaName' : '新乐市',
    'py' : 'xinle',
    'spy' : 'xl'
}, {
    'areaCode' : '130185',
    'areaName' : '鹿泉市',
    'py' : 'luquan',
    'spy' : 'lq'
}]
    }, {
        'areaCode' : '130200',
        'areaName' : '唐山市',
        'py' : 'tangshan',
        'spy' : 'ts',
        'datas' : [{
            'areaCode' : '130202',
            'areaName' : '路南区',
            'py' : 'lunan',
            'spy' : 'ln'
        }, {
            'areaCode' : '130203',
            'areaName' : '路北区',
            'py' : 'lubei',
            'spy' : 'lb'
        }, {
            'areaCode' : '130204',
            'areaName' : '古冶区',
            'py' : 'guzhi',
            'spy' : 'gz'
        }, {
            'areaCode' : '130205',
            'areaName' : '开平区',
            'py' : 'kaiping',
            'spy' : 'kp'
        }, {
            'areaCode' : '130207',
            'areaName' : '丰南区',
            'py' : 'fengnan',
            'spy' : 'fn'
        }, {
            'areaCode' : '130208',
            'areaName' : '丰润区',
            'py' : 'fengrun',
            'spy' : 'fr'
        }, {
            'areaCode' : '130223',
            'areaName' : '滦县',
            'py' : 'luanxian',
            'spy' : 'lx'
        }, {
        'areaCode' : '130224',
        'areaName' : '滦南县',
        'py' : 'luannan',
        'spy' : 'ln'
    }, {
        'areaCode' : '130225',
        'areaName' : '乐亭县',
        'py' : 'laoting',
        'spy' : 'lt'
    }, {
    'areaCode' : '130227',
    'areaName' : '迁西县',
    'py' : 'qianxi',
    'spy' : 'qx'
}, {
    'areaCode' : '130229',
    'areaName' : '玉田县',
    'py' : 'yutian',
    'spy' : 'yt'
}, {
    'areaCode' : '130230',
    'areaName' : '唐海县',
    'py' : 'tanghai',
    'spy' : 'th'
}, {
    'areaCode' : '130281',
    'areaName' : '遵化市',
    'py' : 'zunhua',
    'spy' : 'zh'
}, {
    'areaCode' : '130283',
    'areaName' : '迁安市',
    'py' : 'qianan',
    'spy' : 'qa'
}]
    }, {
        'areaCode' : '130300',
        'areaName' : '秦皇岛市',
        'py' : 'qinhuangdao',
        'spy' : 'qhd',
        'datas' : [{
            'areaCode' : '130302',
            'areaName' : '海港区',
            'py' : 'haigang',
            'spy' : 'hg'
        }, {
            'areaCode' : '130303',
            'areaName' : '山海关区',
            'py' : 'shanhaiguan',
            'spy' : 'shg'
        }, {
            'areaCode' : '130304',
            'areaName' : '北戴河区',
            'py' : 'beidaihe',
            'spy' : 'bdh'
        }, {
            'areaCode' : '130321',
            'areaName' : '青龙满族自治县',
            'py' : 'qinglongmanzuzizhixian',
            'spy' : 'qlmzzzx'
        }, {
            'areaCode' : '130322',
            'areaName' : '昌黎县',
            'py' : 'changli',
            'spy' : 'cl'
        }, {
            'areaCode' : '130323',
            'areaName' : '抚宁县',
            'py' : 'funing',
            'spy' : 'fn'
        }, {
        'areaCode' : '130324',
        'areaName' : '卢龙县',
        'py' : 'lulong',
        'spy' : 'll'
    }]
    }, {
        'areaCode' : '130400',
        'areaName' : '邯郸市',
        'py' : 'handan',
        'spy' : 'hd',
        'datas' : [{
            'areaCode' : '130402',
            'areaName' : '邯山区',
            'py' : 'hanshan',
            'spy' : 'hs'
        }, {
            'areaCode' : '130403',
            'areaName' : '丛台区',
            'py' : 'congtai',
            'spy' : 'ct'
        }, {
            'areaCode' : '130404',
            'areaName' : '复兴区',
            'py' : 'fuxing',
            'spy' : 'fx'
        }, {
            'areaCode' : '130406',
            'areaName' : '峰峰矿区',
            'py' : 'fengfeng',
            'spy' : 'ff'
        }, {
            'areaCode' : '130421',
            'areaName' : '邯郸县',
            'py' : 'handan',
            'spy' : 'hd'
        }, {
        'areaCode' : '130423',
        'areaName' : '临漳县',
        'py' : 'linzhang',
        'spy' : 'lz'
    }, {
        'areaCode' : '130424',
        'areaName' : '成安县',
        'py' : 'chengan',
        'spy' : 'ca'
    }, {
        'areaCode' : '130425',
        'areaName' : '大名县',
        'py' : 'daming',
        'spy' : 'dm'
    }, {
        'areaCode' : '130426',
        'areaName' : '涉县',
        'py' : 'shexian',
        'spy' : 'sx'
    }, {
    'areaCode' : '130427',
    'areaName' : '磁县',
    'py' : 'ciju',
    'spy' : 'cj'
}, {
    'areaCode' : '130428',
    'areaName' : '肥乡县',
    'py' : 'feixiang',
    'spy' : 'fx'
}, {
    'areaCode' : '130429',
    'areaName' : '永年县',
    'py' : 'yongnian',
    'spy' : 'yn'
}, {
    'areaCode' : '130430',
    'areaName' : '邱县',
    'py' : 'qiuxian',
    'spy' : 'qx'
}, {
    'areaCode' : '130431',
    'areaName' : '鸡泽县',
    'py' : 'jize',
    'spy' : 'jz'
}, {
    'areaCode' : '130432',
    'areaName' : '广平县',
    'py' : 'guangping',
    'spy' : 'gp'
}, {
    'areaCode' : '130433',
    'areaName' : '馆陶县',
    'py' : 'guantao',
    'spy' : 'gt'
}, {
    'areaCode' : '130434',
    'areaName' : '魏县',
    'py' : 'weixian',
    'spy' : 'wx'
}, {
    'areaCode' : '130435',
    'areaName' : '曲周县',
    'py' : 'quzhou',
    'spy' : 'qz'
}, {
    'areaCode' : '130481',
    'areaName' : '武安市',
    'py' : 'wuan',
    'spy' : 'wa'
}]
    }, {
        'areaCode' : '130500',
        'areaName' : '邢台市',
        'py' : 'xingtai',
        'spy' : 'xt',
        'datas' : [{
            'areaCode' : '130502',
            'areaName' : '桥东区',
            'py' : 'qiaodong',
            'spy' : 'qd'
        }, {
            'areaCode' : '130503',
            'areaName' : '桥西区',
            'py' : 'qiaoxi',
            'spy' : 'qx'
        }, {
            'areaCode' : '130521',
            'areaName' : '邢台县',
            'py' : 'xingtai',
            'spy' : 'xt'
        }, {
            'areaCode' : '130522',
            'areaName' : '临城县',
            'py' : 'lincheng',
            'spy' : 'lc'
        }, {
        'areaCode' : '130523',
        'areaName' : '内丘县',
        'py' : 'neiqiu',
        'spy' : 'nq'
    }, {
        'areaCode' : '130524',
        'areaName' : '柏乡县',
        'py' : 'baixiang',
        'spy' : 'bx'
    }, {
        'areaCode' : '130525',
        'areaName' : '隆尧县',
        'py' : 'longyao',
        'spy' : 'ly'
    }, {
        'areaCode' : '130526',
        'areaName' : '任县',
        'py' : 'renxian',
        'spy' : 'rx'
    }, {
        'areaCode' : '130527',
        'areaName' : '南和县',
        'py' : 'nanhe',
        'spy' : 'nh'
    }, {
    'areaCode' : '130528',
    'areaName' : '宁晋县',
    'py' : 'ningjin',
    'spy' : 'nj'
}, {
    'areaCode' : '130529',
    'areaName' : '巨鹿县',
    'py' : 'julu',
    'spy' : 'jl'
}, {
    'areaCode' : '130530',
    'areaName' : '新河县',
    'py' : 'xinhe',
    'spy' : 'xh'
}, {
    'areaCode' : '130531',
    'areaName' : '广宗县',
    'py' : 'guangzong',
    'spy' : 'gz'
}, {
    'areaCode' : '130532',
    'areaName' : '平乡县',
    'py' : 'pingxiang',
    'spy' : 'px'
}, {
    'areaCode' : '130533',
    'areaName' : '威县',
    'py' : 'weixian',
    'spy' : 'wx'
}, {
    'areaCode' : '130534',
    'areaName' : '清河县',
    'py' : 'qinghe',
    'spy' : 'qh'
}, {
    'areaCode' : '130535',
    'areaName' : '临西县',
    'py' : 'linxi',
    'spy' : 'lx'
}, {
    'areaCode' : '130581',
    'areaName' : '南宫市',
    'py' : 'nangong',
    'spy' : 'ng'
}, {
    'areaCode' : '130582',
    'areaName' : '沙河市',
    'py' : 'shahe',
    'spy' : 'sh'
}]
    }, {
        'areaCode' : '130600',
        'areaName' : '保定市',
        'py' : 'baoding',
        'spy' : 'bd',
        'datas' : [{
            'areaCode' : '130602',
            'areaName' : '新市区',
            'py' : 'xinshi',
            'spy' : 'xs'
        }, {
            'areaCode' : '130603',
            'areaName' : '北市区',
            'py' : 'beishi',
            'spy' : 'bs'
        }, {
            'areaCode' : '130604',
            'areaName' : '南市区',
            'py' : 'nanshi',
            'spy' : 'ns'
        }, {
        'areaCode' : '130621',
        'areaName' : '满城县',
        'py' : 'mancheng',
        'spy' : 'mc'
    }, {
        'areaCode' : '130622',
        'areaName' : '清苑县',
        'py' : 'qingyuan',
        'spy' : 'qy'
    }, {
        'areaCode' : '130623',
        'areaName' : '涞水县',
        'py' : 'laishui',
        'spy' : 'ls'
    }, {
        'areaCode' : '130624',
        'areaName' : '阜平县',
        'py' : 'fuping',
        'spy' : 'fp'
    }, {
        'areaCode' : '130625',
        'areaName' : '徐水县',
        'py' : 'xushui',
        'spy' : 'xs'
    }, {
        'areaCode' : '130626',
        'areaName' : '定兴县',
        'py' : 'dingxing',
        'spy' : 'dx'
    }, {
    'areaCode' : '130627',
    'areaName' : '唐县',
    'py' : 'tangxian',
    'spy' : 'tx'
}, {
    'areaCode' : '130628',
    'areaName' : '高阳县',
    'py' : 'gaoyang',
    'spy' : 'gy'
}, {
    'areaCode' : '130629',
    'areaName' : '容城县',
    'py' : 'rongcheng',
    'spy' : 'rc'
}, {
    'areaCode' : '130630',
    'areaName' : '涞源县',
    'py' : 'laiyuan',
    'spy' : 'ly'
}, {
    'areaCode' : '130631',
    'areaName' : '望都县',
    'py' : 'wangdu',
    'spy' : 'wd'
}, {
    'areaCode' : '130632',
    'areaName' : '安新县',
    'py' : 'anxin',
    'spy' : 'ax'
}, {
    'areaCode' : '130633',
    'areaName' : '易县',
    'py' : 'yixian',
    'spy' : 'yx'
}, {
    'areaCode' : '130634',
    'areaName' : '曲阳县',
    'py' : 'quyang',
    'spy' : 'qy'
}, {
    'areaCode' : '130635',
    'areaName' : '蠡县',
    'py' : 'luoxian',
    'spy' : 'lx'
}, {
    'areaCode' : '130636',
    'areaName' : '顺平县',
    'py' : 'shunping',
    'spy' : 'sp'
}, {
    'areaCode' : '130637',
    'areaName' : '博野县',
    'py' : 'boye',
    'spy' : 'by'
}, {
    'areaCode' : '130638',
    'areaName' : '雄县',
    'py' : 'xiongxian',
    'spy' : 'xx'
}, {
    'areaCode' : '130681',
    'areaName' : '涿州市',
    'py' : 'zhuozhou',
    'spy' : 'zz'
}, {
    'areaCode' : '130682',
    'areaName' : '定州市',
    'py' : 'dingzhou',
    'spy' : 'dz'
}, {
    'areaCode' : '130683',
    'areaName' : '安国市',
    'py' : 'anguo',
    'spy' : 'ag'
}, {
    'areaCode' : '130684',
    'areaName' : '高碑店市',
    'py' : 'gaobeidian',
    'spy' : 'gbd'
}]
    }, {
        'areaCode' : '130700',
        'areaName' : '张家口市',
        'py' : 'zhangjiakou',
        'spy' : 'zjk',
        'datas' : [{
            'areaCode' : '130702',
            'areaName' : '桥东区',
            'py' : 'qiaodong',
            'spy' : 'qd'
        }, {
            'areaCode' : '130703',
            'areaName' : '桥西区',
            'py' : 'qiaoxi',
            'spy' : 'qx'
        }, {
        'areaCode' : '130705',
        'areaName' : '宣化区',
        'py' : 'xuanhua',
        'spy' : 'xh'
    }, {
        'areaCode' : '130706',
        'areaName' : '下花园区',
        'py' : 'xiahuayuan',
        'spy' : 'xhy'
    }, {
        'areaCode' : '130721',
        'areaName' : '宣化县',
        'py' : 'xuanhua',
        'spy' : 'xh'
    }, {
        'areaCode' : '130722',
        'areaName' : '张北县',
        'py' : 'zhangbei',
        'spy' : 'zb'
    }, {
        'areaCode' : '130723',
        'areaName' : '康保县',
        'py' : 'kangbao',
        'spy' : 'kb'
    }, {
        'areaCode' : '130724',
        'areaName' : '沽源县',
        'py' : 'guyuan',
        'spy' : 'gy'
    }, {
        'areaCode' : '130725',
        'areaName' : '尚义县',
        'py' : 'shangyi',
        'spy' : 'sy'
    }, {
    'areaCode' : '130726',
    'areaName' : '蔚县',
    'py' : 'weixian',
    'spy' : 'wx'
}, {
    'areaCode' : '130727',
    'areaName' : '阳原县',
    'py' : 'yangyuan',
    'spy' : 'yy'
}, {
    'areaCode' : '130728',
    'areaName' : '怀安县',
    'py' : 'huaian',
    'spy' : 'ha'
}, {
    'areaCode' : '130729',
    'areaName' : '万全县',
    'py' : 'wanquan',
    'spy' : 'wq'
}, {
    'areaCode' : '130730',
    'areaName' : '怀来县',
    'py' : 'huailai',
    'spy' : 'hl'
}, {
    'areaCode' : '130731',
    'areaName' : '涿鹿县',
    'py' : 'zhuolu',
    'spy' : 'zl'
}, {
    'areaCode' : '130732',
    'areaName' : '赤城县',
    'py' : 'chicheng',
    'spy' : 'cc'
}, {
    'areaCode' : '130733',
    'areaName' : '崇礼县',
    'py' : 'chongli',
    'spy' : 'cl'
}]
    }, {
        'areaCode' : '130800',
        'areaName' : '承德市',
        'py' : 'chengdu',
        'spy' : 'cd',
        'datas' : [{
            'areaCode' : '130802',
            'areaName' : '双桥区',
            'py' : 'shuangqiao',
            'spy' : 'sq'
        }, {
        'areaCode' : '130803',
        'areaName' : '双滦区',
        'py' : 'shuangluan',
        'spy' : 'sl'
    }, {
        'areaCode' : '130804',
        'areaName' : '鹰手营子矿区',
        'py' : 'yingshouyingzikuang',
        'spy' : 'ysyzk'
    }, {
        'areaCode' : '130821',
        'areaName' : '承德县',
        'py' : 'chengde',
        'spy' : 'cd'
    }, {
        'areaCode' : '130822',
        'areaName' : '兴隆县',
        'py' : 'xinglong',
        'spy' : 'xl'
    }, {
        'areaCode' : '130823',
        'areaName' : '平泉县',
        'py' : 'pingquan',
        'spy' : 'pq'
    }, {
        'areaCode' : '130824',
        'areaName' : '滦平县',
        'py' : 'luanping',
        'spy' : 'lp'
    }, {
        'areaCode' : '130825',
        'areaName' : '隆化县',
        'py' : 'longhua',
        'spy' : 'lh'
    }, {
        'areaCode' : '130826',
        'areaName' : '丰宁满族自治县',
        'py' : 'fengningmanzuzizhixian',
        'spy' : 'fnmzzzx'
    }, {
    'areaCode' : '130827',
    'areaName' : '宽城满族自治县',
    'py' : 'kuanchengmanzuzizhixian',
    'spy' : 'kcmzzzx'
}, {
    'areaCode' : '130828',
    'areaName' : '围场满族蒙古族自治县',
    'py' : 'weichangmanzumengguzuzizhixian',
    'spy' : 'wcmzmgzzzx'
}]
    }, {
        'areaCode' : '130900',
        'areaName' : '沧州市',
        'py' : 'cangzhou',
        'spy' : 'cz',
        'datas' : [{
        'areaCode' : '130902',
        'areaName' : '新华区',
        'py' : 'xinhua',
        'spy' : 'xh'
    }, {
        'areaCode' : '130903',
        'areaName' : '运河区',
        'py' : 'yunhe',
        'spy' : 'yh'
    }, {
        'areaCode' : '130921',
        'areaName' : '沧县',
        'py' : 'cangxian',
        'spy' : 'cx'
    }, {
        'areaCode' : '130922',
        'areaName' : '青县',
        'py' : 'qingxian',
        'spy' : 'qx'
    }, {
        'areaCode' : '130923',
        'areaName' : '东光县',
        'py' : 'dongguang',
        'spy' : 'dg'
    }, {
        'areaCode' : '130924',
        'areaName' : '海兴县',
        'py' : 'haixing',
        'spy' : 'hx'
    }, {
        'areaCode' : '130925',
        'areaName' : '盐山县',
        'py' : 'yanshan',
        'spy' : 'ys'
    }, {
        'areaCode' : '130926',
        'areaName' : '肃宁县',
        'py' : 'suning',
        'spy' : 'sn'
    }, {
        'areaCode' : '130927',
        'areaName' : '南皮县',
        'py' : 'nanpi',
        'spy' : 'np'
    }, {
    'areaCode' : '130928',
    'areaName' : '吴桥县',
    'py' : 'wuqiao',
    'spy' : 'wq'
}, {
    'areaCode' : '130929',
    'areaName' : '献县',
    'py' : 'xianxian',
    'spy' : 'xx'
}, {
    'areaCode' : '130930',
    'areaName' : '孟村回族自治县',
    'py' : 'mengcunhuizuzizhixian',
    'spy' : 'mchzzzx'
}, {
    'areaCode' : '130981',
    'areaName' : '泊头市',
    'py' : 'botou',
    'spy' : 'bt'
}, {
    'areaCode' : '130982',
    'areaName' : '任丘市',
    'py' : 'renqiu',
    'spy' : 'rq'
}, {
    'areaCode' : '130983',
    'areaName' : '黄骅市',
    'py' : 'huanghua',
    'spy' : 'hh'
}, {
    'areaCode' : '130984',
    'areaName' : '河间市',
    'py' : 'hejian',
    'spy' : 'hj'
}]
    }, {
    'areaCode' : '131000',
    'areaName' : '廊坊市',
    'py' : 'langfang',
    'spy' : 'lf',
    'datas' : [{
        'areaCode' : '131002',
        'areaName' : '安次区',
        'py' : 'anci',
        'spy' : 'ac'
    }, {
        'areaCode' : '131003',
        'areaName' : '广阳区',
        'py' : 'guangyang',
        'spy' : 'gy'
    }, {
        'areaCode' : '131022',
        'areaName' : '固安县',
        'py' : 'guan',
        'spy' : 'ga'
    }, {
        'areaCode' : '131023',
        'areaName' : '永清县',
        'py' : 'yongqing',
        'spy' : 'yq'
    }, {
        'areaCode' : '131024',
        'areaName' : '香河县',
        'py' : 'xianghe',
        'spy' : 'xh'
    }, {
        'areaCode' : '131025',
        'areaName' : '大城县',
        'py' : 'dacheng',
        'spy' : 'dc'
    }, {
        'areaCode' : '131026',
        'areaName' : '文安县',
        'py' : 'wenan',
        'spy' : 'wa'
    }, {
        'areaCode' : '131028',
        'areaName' : '大厂回族自治县',
        'py' : 'dachanghuizuzizhixian',
        'spy' : 'dchzzzx'
    }, {
        'areaCode' : '131081',
        'areaName' : '霸州市',
        'py' : 'bazhou',
        'spy' : 'bz'
    }, {
    'areaCode' : '131082',
    'areaName' : '三河市',
    'py' : 'sanhe',
    'spy' : 'sh'
}]
}, {
    'areaCode' : '131100',
    'areaName' : '衡水市',
    'py' : 'hengshui',
    'spy' : 'hs',
    'datas' : [{
        'areaCode' : '131102',
        'areaName' : '桃城区',
        'py' : 'taocheng',
        'spy' : 'tc'
    }, {
        'areaCode' : '131121',
        'areaName' : '枣强县',
        'py' : 'zaoqiang',
        'spy' : 'zq'
    }, {
        'areaCode' : '131122',
        'areaName' : '武邑县',
        'py' : 'wuyi',
        'spy' : 'wy'
    }, {
        'areaCode' : '131123',
        'areaName' : '武强县',
        'py' : 'wuqiang',
        'spy' : 'wq'
    }, {
        'areaCode' : '131124',
        'areaName' : '饶阳县',
        'py' : 'raoyang',
        'spy' : 'ry'
    }, {
        'areaCode' : '131125',
        'areaName' : '安平县',
        'py' : 'anping',
        'spy' : 'ap'
    }, {
        'areaCode' : '131126',
        'areaName' : '故城县',
        'py' : 'gucheng',
        'spy' : 'gc'
    }, {
        'areaCode' : '131127',
        'areaName' : '景县',
        'py' : 'jingxian',
        'spy' : 'jx'
    }, {
        'areaCode' : '131128',
        'areaName' : '阜城县',
        'py' : 'fucheng',
        'spy' : 'fc'
    }, {
    'areaCode' : '131181',
    'areaName' : '冀州市',
    'py' : 'jizhou',
    'spy' : 'jz'
}, {
    'areaCode' : '131182',
    'areaName' : '深州市',
    'py' : 'shenzhou',
    'spy' : 'sz'
}]
}]
}, {
    'areaCode' : '140000',
    'areaName' : '山西省',
    'py' : 'shanxi',
    'spy' : 'sx',
    'datas' : [{
        'areaCode' : '140100',
        'areaName' : '太原市',
        'py' : 'taiyuan',
        'spy' : 'ty',
        'datas' : [{
            'areaCode' : '140105',
            'areaName' : '小店区',
            'py' : 'xiaodian',
            'spy' : 'xd'
        }, {
            'areaCode' : '140106',
            'areaName' : '迎泽区',
            'py' : 'yingze',
            'spy' : 'yz'
        }, {
            'areaCode' : '140107',
            'areaName' : '杏花岭区',
            'py' : 'xinghualing',
            'spy' : 'xhl'
        }, {
            'areaCode' : '140108',
            'areaName' : '尖草坪区',
            'py' : 'jiancaoping',
            'spy' : 'jcp'
        }, {
            'areaCode' : '140109',
            'areaName' : '万柏林区',
            'py' : 'wanbailinqu',
            'spy' : 'wblq'
        }, {
            'areaCode' : '140110',
            'areaName' : '晋源区',
            'py' : 'jinyuan',
            'spy' : 'jy'
        }, {
            'areaCode' : '140121',
            'areaName' : '清徐县',
            'py' : 'qingxu',
            'spy' : 'qx'
        }, {
            'areaCode' : '140122',
            'areaName' : '阳曲县',
            'py' : 'yangqu',
            'spy' : 'yq'
        }, {
        'areaCode' : '140123',
        'areaName' : '娄烦县',
        'py' : 'loufan',
        'spy' : 'lf'
    }, {
    'areaCode' : '140181',
    'areaName' : '古交市',
    'py' : 'gujiao',
    'spy' : 'gj'
}]
    }, {
        'areaCode' : '140200',
        'areaName' : '大同市',
        'py' : 'datong',
        'spy' : 'dt',
        'datas' : [{
            'areaCode' : '140202',
            'areaName' : '城区',
            'py' : 'chengqu',
            'spy' : 'cq'
        }, {
            'areaCode' : '140203',
            'areaName' : '矿区',
            'py' : 'kuangqu',
            'spy' : 'kq'
        }, {
            'areaCode' : '140211',
            'areaName' : '南郊区',
            'py' : 'nanjiao',
            'spy' : 'nj'
        }, {
            'areaCode' : '140212',
            'areaName' : '新荣区',
            'py' : 'xinrong',
            'spy' : 'xr'
        }, {
            'areaCode' : '140221',
            'areaName' : '阳高县',
            'py' : 'yanggao',
            'spy' : 'yg'
        }, {
            'areaCode' : '140222',
            'areaName' : '天镇县',
            'py' : 'tianzhen',
            'spy' : 'tz'
        }, {
            'areaCode' : '140223',
            'areaName' : '广灵县',
            'py' : 'guangling',
            'spy' : 'gl'
        }, {
        'areaCode' : '140224',
        'areaName' : '灵丘县',
        'py' : 'lingqiu',
        'spy' : 'lq'
    }, {
        'areaCode' : '140225',
        'areaName' : '浑源县',
        'py' : 'hunyuan',
        'spy' : 'hy'
    }, {
    'areaCode' : '140226',
    'areaName' : '左云县',
    'py' : 'zuoyun',
    'spy' : 'zy'
}, {
    'areaCode' : '140227',
    'areaName' : '大同县',
    'py' : 'datong',
    'spy' : 'dt'
}]
    }, {
        'areaCode' : '140300',
        'areaName' : '阳泉市',
        'py' : 'yangquan',
        'spy' : 'yq',
        'datas' : [{
            'areaCode' : '140302',
            'areaName' : '城区',
            'py' : 'chengqu',
            'spy' : 'cq'
        }, {
            'areaCode' : '140303',
            'areaName' : '矿区',
            'py' : 'kuangqu',
            'spy' : 'kq'
        }, {
            'areaCode' : '140311',
            'areaName' : '郊区',
            'py' : 'jiaoqu',
            'spy' : 'jq'
        }, {
            'areaCode' : '140321',
            'areaName' : '平定县',
            'py' : 'pingding',
            'spy' : 'pd'
        }, {
            'areaCode' : '140322',
            'areaName' : '盂县',
            'py' : 'mengxian',
            'spy' : 'mx'
        }]
    }, {
        'areaCode' : '140400',
        'areaName' : '长治市',
        'py' : 'changzhi',
        'spy' : 'cz',
        'datas' : [{
            'areaCode' : '140402',
            'areaName' : '城区',
            'py' : 'chengqu',
            'spy' : 'cq'
        }, {
            'areaCode' : '140411',
            'areaName' : '郊区',
            'py' : 'jiaoqu',
            'spy' : 'jq'
        }, {
            'areaCode' : '140421',
            'areaName' : '长治县',
            'py' : 'changzhi',
            'spy' : 'cz'
        }, {
            'areaCode' : '140423',
            'areaName' : '襄垣县',
            'py' : 'xiangyuan',
            'spy' : 'xy'
        }, {
            'areaCode' : '140424',
            'areaName' : '屯留县',
            'py' : 'tunliu',
            'spy' : 'tl'
        }, {
        'areaCode' : '140425',
        'areaName' : '平顺县',
        'py' : 'pingshun',
        'spy' : 'ps'
    }, {
        'areaCode' : '140426',
        'areaName' : '黎城县',
        'py' : 'licheng',
        'spy' : 'lc'
    }, {
        'areaCode' : '140427',
        'areaName' : '壶关县',
        'py' : 'huguan',
        'spy' : 'hg'
    }, {
        'areaCode' : '140428',
        'areaName' : '长子县',
        'py' : 'changzi',
        'spy' : 'cz'
    }, {
    'areaCode' : '140429',
    'areaName' : '武乡县',
    'py' : 'wuxiang',
    'spy' : 'wx'
}, {
    'areaCode' : '140430',
    'areaName' : '沁县',
    'py' : 'qinxian',
    'spy' : 'qx'
}, {
    'areaCode' : '140431',
    'areaName' : '沁源县',
    'py' : 'qinyuan',
    'spy' : 'qy'
}, {
    'areaCode' : '140481',
    'areaName' : '潞城市',
    'py' : 'lucheng',
    'spy' : 'lc'
}]
    }, {
        'areaCode' : '140500',
        'areaName' : '晋城市',
        'py' : 'jincheng',
        'spy' : 'jc',
        'datas' : [{
            'areaCode' : '140502',
            'areaName' : '城区',
            'py' : 'chengqu',
            'spy' : 'cq'
        }, {
            'areaCode' : '140521',
            'areaName' : '沁水县',
            'py' : 'qinshui',
            'spy' : 'qs'
        }, {
            'areaCode' : '140522',
            'areaName' : '阳城县',
            'py' : 'yangcheng',
            'spy' : 'yc'
        }, {
            'areaCode' : '140524',
            'areaName' : '陵川县',
            'py' : 'linchuan',
            'spy' : 'lc'
        }, {
        'areaCode' : '140525',
        'areaName' : '泽州县',
        'py' : 'zezhou',
        'spy' : 'zz'
    }, {
        'areaCode' : '140581',
        'areaName' : '高平市',
        'py' : 'gaoping',
        'spy' : 'gp'
    }]
    }, {
        'areaCode' : '140600',
        'areaName' : '朔州市',
        'py' : 'shuozhou',
        'spy' : 'sz',
        'datas' : [{
            'areaCode' : '140602',
            'areaName' : '朔城区',
            'py' : 'shuocheng',
            'spy' : 'sc'
        }, {
            'areaCode' : '140603',
            'areaName' : '平鲁区',
            'py' : 'pinglu',
            'spy' : 'pl'
        }, {
            'areaCode' : '140621',
            'areaName' : '山阴县',
            'py' : 'shanyin',
            'spy' : 'sy'
        }, {
        'areaCode' : '140622',
        'areaName' : '应县',
        'py' : 'yingxian',
        'spy' : 'yx'
    }, {
        'areaCode' : '140623',
        'areaName' : '右玉县',
        'py' : 'youyu',
        'spy' : 'yy'
    }, {
        'areaCode' : '140624',
        'areaName' : '怀仁县',
        'py' : 'huairen',
        'spy' : 'hr'
    }]
    }, {
        'areaCode' : '140700',
        'areaName' : '晋中市',
        'py' : 'jinzhong',
        'spy' : 'jz',
        'datas' : [{
            'areaCode' : '140702',
            'areaName' : '榆次区',
            'py' : 'yuci',
            'spy' : 'yc'
        }, {
            'areaCode' : '140721',
            'areaName' : '榆社县',
            'py' : 'yushe',
            'spy' : 'ys'
        }, {
        'areaCode' : '140722',
        'areaName' : '左权县',
        'py' : 'zuoquan',
        'spy' : 'zq'
    }, {
        'areaCode' : '140723',
        'areaName' : '和顺县',
        'py' : 'heshun',
        'spy' : 'hs'
    }, {
        'areaCode' : '140724',
        'areaName' : '昔阳县',
        'py' : 'jinyang',
        'spy' : 'jy'
    }, {
        'areaCode' : '140725',
        'areaName' : '寿阳县',
        'py' : 'shouyang',
        'spy' : 'sy'
    }, {
        'areaCode' : '140726',
        'areaName' : '太谷县',
        'py' : 'taigu',
        'spy' : 'tg'
    }, {
        'areaCode' : '140727',
        'areaName' : '祁县',
        'py' : 'qixian',
        'spy' : 'qx'
    }, {
        'areaCode' : '140728',
        'areaName' : '平遥县',
        'py' : 'pingyao',
        'spy' : 'py'
    }, {
    'areaCode' : '140729',
    'areaName' : '灵石县',
    'py' : 'lingshi',
    'spy' : 'ls'
}, {
    'areaCode' : '140781',
    'areaName' : '介休市',
    'py' : 'jiexiu',
    'spy' : 'jx'
}]
    }, {
        'areaCode' : '140800',
        'areaName' : '运城市',
        'py' : 'yuncheng',
        'spy' : 'yc',
        'datas' : [{
            'areaCode' : '140802',
            'areaName' : '盐湖区',
            'py' : 'yanhu',
            'spy' : 'yh'
        }, {
        'areaCode' : '140821',
        'areaName' : '临猗县',
        'py' : 'linyi',
        'spy' : 'ly'
    }, {
        'areaCode' : '140822',
        'areaName' : '万荣县',
        'py' : 'wanrong',
        'spy' : 'wr'
    }, {
        'areaCode' : '140823',
        'areaName' : '闻喜县',
        'py' : 'wenxi',
        'spy' : 'wx'
    }, {
        'areaCode' : '140824',
        'areaName' : '稷山县',
        'py' : 'jishan',
        'spy' : 'js'
    }, {
        'areaCode' : '140825',
        'areaName' : '新绛县',
        'py' : 'xinjiang',
        'spy' : 'xj'
    }, {
        'areaCode' : '140826',
        'areaName' : '绛县',
        'py' : 'jiangxian',
        'spy' : 'jx'
    }, {
        'areaCode' : '140827',
        'areaName' : '垣曲县',
        'py' : 'yuanqu',
        'spy' : 'yq'
    }, {
        'areaCode' : '140828',
        'areaName' : '夏县',
        'py' : 'xiaxian',
        'spy' : 'xx'
    }, {
    'areaCode' : '140829',
    'areaName' : '平陆县',
    'py' : 'pinglu',
    'spy' : 'pl'
}, {
    'areaCode' : '140830',
    'areaName' : '芮城县',
    'py' : 'ruicheng',
    'spy' : 'rc'
}, {
    'areaCode' : '140881',
    'areaName' : '永济市',
    'py' : 'yongji',
    'spy' : 'yj'
}, {
    'areaCode' : '140882',
    'areaName' : '河津市',
    'py' : 'hejin',
    'spy' : 'hj'
}]
    }, {
        'areaCode' : '140900',
        'areaName' : '忻州市',
        'py' : 'xinzhou',
        'spy' : 'xz',
        'datas' : [{
        'areaCode' : '140902',
        'areaName' : '忻府区',
        'py' : 'xinfu',
        'spy' : 'xf'
    }, {
        'areaCode' : '140921',
        'areaName' : '定襄县',
        'py' : 'dingxiang',
        'spy' : 'dx'
    }, {
        'areaCode' : '140922',
        'areaName' : '五台县',
        'py' : 'wutai',
        'spy' : 'wt'
    }, {
        'areaCode' : '140923',
        'areaName' : '代县',
        'py' : 'daixian',
        'spy' : 'dx'
    }, {
        'areaCode' : '140924',
        'areaName' : '繁峙县',
        'py' : 'fanshi',
        'spy' : 'fs'
    }, {
        'areaCode' : '140925',
        'areaName' : '宁武县',
        'py' : 'ningwu',
        'spy' : 'nw'
    }, {
        'areaCode' : '140926',
        'areaName' : '静乐县',
        'py' : 'jingle',
        'spy' : 'jl'
    }, {
        'areaCode' : '140927',
        'areaName' : '神池县',
        'py' : 'shenchi',
        'spy' : 'sc'
    }, {
        'areaCode' : '140928',
        'areaName' : '五寨县',
        'py' : 'wuzhai',
        'spy' : 'wz'
    }, {
    'areaCode' : '140929',
    'areaName' : '岢岚县',
    'py' : 'kelan',
    'spy' : 'kl'
}, {
    'areaCode' : '140930',
    'areaName' : '河曲县',
    'py' : 'hequ',
    'spy' : 'hq'
}, {
    'areaCode' : '140931',
    'areaName' : '保德县',
    'py' : 'baode',
    'spy' : 'bd'
}, {
    'areaCode' : '140932',
    'areaName' : '偏关县',
    'py' : 'pianguan',
    'spy' : 'pg'
}, {
    'areaCode' : '140981',
    'areaName' : '原平市',
    'py' : 'yuanping',
    'spy' : 'yp'
}]
    }, {
    'areaCode' : '141000',
    'areaName' : '临汾市',
    'py' : 'linfen',
    'spy' : 'lf',
    'datas' : [{
        'areaCode' : '141002',
        'areaName' : '尧都区',
        'py' : 'raodu',
        'spy' : 'rd'
    }, {
        'areaCode' : '141021',
        'areaName' : '曲沃县',
        'py' : 'quwo',
        'spy' : 'qw'
    }, {
        'areaCode' : '141022',
        'areaName' : '翼城县',
        'py' : 'yicheng',
        'spy' : 'yc'
    }, {
        'areaCode' : '141023',
        'areaName' : '襄汾县',
        'py' : 'xiangfen',
        'spy' : 'xf'
    }, {
        'areaCode' : '141024',
        'areaName' : '洪洞县',
        'py' : 'hongtong',
        'spy' : 'ht'
    }, {
        'areaCode' : '141025',
        'areaName' : '古县',
        'py' : 'guxiang',
        'spy' : 'gx'
    }, {
        'areaCode' : '141026',
        'areaName' : '安泽县',
        'py' : 'anze',
        'spy' : 'az'
    }, {
        'areaCode' : '141027',
        'areaName' : '浮山县',
        'py' : 'fushan',
        'spy' : 'fs'
    }, {
        'areaCode' : '141028',
        'areaName' : '吉县',
        'py' : 'jixian',
        'spy' : 'jx'
    }, {
    'areaCode' : '141029',
    'areaName' : '乡宁县',
    'py' : 'xiangning',
    'spy' : 'xn'
}, {
    'areaCode' : '141030',
    'areaName' : '大宁县',
    'py' : 'daning',
    'spy' : 'dn'
}, {
    'areaCode' : '141031',
    'areaName' : '隰县',
    'py' : 'xixian',
    'spy' : 'xx'
}, {
    'areaCode' : '141032',
    'areaName' : '永和县',
    'py' : 'yonghe',
    'spy' : 'yh'
}, {
    'areaCode' : '141033',
    'areaName' : '蒲县',
    'py' : 'puxian',
    'spy' : 'px'
}, {
    'areaCode' : '141034',
    'areaName' : '汾西县',
    'py' : 'fenxi',
    'spy' : 'fx'
}, {
    'areaCode' : '141081',
    'areaName' : '侯马市',
    'py' : 'houma',
    'spy' : 'hm'
}, {
    'areaCode' : '141082',
    'areaName' : '霍州市',
    'py' : 'huozhou',
    'spy' : 'hz'
}]
}, {
    'areaCode' : '141100',
    'areaName' : '吕梁市',
    'py' : 'lvliang',
    'spy' : 'll',
    'datas' : [{
        'areaCode' : '141102',
        'areaName' : '离石区',
        'py' : 'lishi',
        'spy' : 'ls'
    }, {
        'areaCode' : '141121',
        'areaName' : '文水县',
        'py' : 'wenshui',
        'spy' : 'ws'
    }, {
        'areaCode' : '141122',
        'areaName' : '交城县',
        'py' : 'jiaocheng',
        'spy' : 'jc'
    }, {
        'areaCode' : '141123',
        'areaName' : '兴县',
        'py' : 'xingxian',
        'spy' : 'xx'
    }, {
        'areaCode' : '141124',
        'areaName' : '临县',
        'py' : 'linxian',
        'spy' : 'lx'
    }, {
        'areaCode' : '141125',
        'areaName' : '柳林县',
        'py' : 'liulin',
        'spy' : 'll'
    }, {
        'areaCode' : '141126',
        'areaName' : '石楼县',
        'py' : 'shilou',
        'spy' : 'sl'
    }, {
        'areaCode' : '141127',
        'areaName' : '岚县',
        'py' : 'lanxian',
        'spy' : 'lx'
    }, {
        'areaCode' : '141128',
        'areaName' : '方山县',
        'py' : 'fangshan',
        'spy' : 'fs'
    }, {
    'areaCode' : '141129',
    'areaName' : '中阳县',
    'py' : 'zhongyang',
    'spy' : 'zy'
}, {
    'areaCode' : '141130',
    'areaName' : '交口县',
    'py' : 'jiaokou',
    'spy' : 'jk'
}, {
    'areaCode' : '141181',
    'areaName' : '孝义市',
    'py' : 'xiaoyi',
    'spy' : 'xy'
}, {
    'areaCode' : '141182',
    'areaName' : '汾阳市',
    'py' : 'fenyang',
    'spy' : 'fy'
}]
}]
}, {
    'areaCode' : '150000',
    'areaName' : '内蒙古自治区',
    'py' : 'neimengguzizhiqu',
    'spy' : 'nmgzzq',
    'datas' : [{
        'areaCode' : '150100',
        'areaName' : '呼和浩特市',
        'py' : 'huhehaote',
        'spy' : 'hhht',
        'datas' : [{
            'areaCode' : '150102',
            'areaName' : '新城区',
            'py' : 'xincheng',
            'spy' : 'xc'
        }, {
            'areaCode' : '150103',
            'areaName' : '回民区',
            'py' : 'huimin',
            'spy' : 'hm'
        }, {
            'areaCode' : '150104',
            'areaName' : '玉泉区',
            'py' : 'yuquan',
            'spy' : 'yq'
        }, {
            'areaCode' : '150105',
            'areaName' : '赛罕区',
            'py' : 'saihan',
            'spy' : 'sh'
        }, {
            'areaCode' : '150121',
            'areaName' : '土默特左旗',
            'py' : 'tumotezuoqi',
            'spy' : 'tmtzq'
        }, {
            'areaCode' : '150122',
            'areaName' : '托克托县',
            'py' : 'tuoketuo',
            'spy' : 'tkt'
        }, {
            'areaCode' : '150123',
            'areaName' : '和林格尔县',
            'py' : 'helingeer',
            'spy' : 'hlge'
        }, {
            'areaCode' : '150124',
            'areaName' : '清水河县',
            'py' : 'qingshuihe',
            'spy' : 'qsh'
        }, {
        'areaCode' : '150125',
        'areaName' : '武川县',
        'py' : 'wuchuan',
        'spy' : 'wc'
    }]
    }, {
        'areaCode' : '150200',
        'areaName' : '包头市',
        'py' : 'baotou',
        'spy' : 'bt',
        'datas' : [{
            'areaCode' : '150202',
            'areaName' : '东河区',
            'py' : 'donghe',
            'spy' : 'dh'
        }, {
            'areaCode' : '150203',
            'areaName' : '昆都仑区',
            'py' : 'kundulun',
            'spy' : 'kdl'
        }, {
            'areaCode' : '150204',
            'areaName' : '青山区',
            'py' : 'qingshan',
            'spy' : 'qs'
        }, {
            'areaCode' : '150205',
            'areaName' : '石拐区',
            'py' : 'shiguai',
            'spy' : 'sg'
        }, {
            'areaCode' : '150206',
            'areaName' : '白云鄂博矿区',
            'py' : 'baiyunebo',
            'spy' : 'byeb'
        }, {
            'areaCode' : '150207',
            'areaName' : '九原区',
            'py' : 'jiuyuan',
            'spy' : 'jy'
        }, {
            'areaCode' : '150208',
            'areaName' : '白云矿区',
            'py' : 'baiyunkuang',
            'spy' : 'byk'
        }, {
        'areaCode' : '150221',
        'areaName' : '土默特右旗',
        'py' : 'tumoteyouqi',
        'spy' : 'tmtyq'
    }, {
        'areaCode' : '150222',
        'areaName' : '固阳县',
        'py' : 'guyang',
        'spy' : 'gy'
    }, {
    'areaCode' : '150223',
    'areaName' : '达尔罕茂明安联合旗',
    'py' : 'daerhanmaominganlianheqi',
    'spy' : 'dehmmalhq'
}]
    }, {
        'areaCode' : '150300',
        'areaName' : '乌海市',
        'py' : 'wuhai',
        'spy' : 'wh',
        'datas' : [{
            'areaCode' : '150302',
            'areaName' : '海勃湾区',
            'py' : 'haibowan',
            'spy' : 'hbw'
        }, {
            'areaCode' : '150303',
            'areaName' : '海南区',
            'py' : 'hainan',
            'spy' : 'hn'
        }, {
            'areaCode' : '150304',
            'areaName' : '乌达区',
            'py' : 'wuda',
            'spy' : 'wd'
        }]
    }, {
        'areaCode' : '150400',
        'areaName' : '赤峰市',
        'py' : 'chifeng',
        'spy' : 'cf',
        'datas' : [{
            'areaCode' : '150402',
            'areaName' : '红山区',
            'py' : 'hongshan',
            'spy' : 'hs'
        }, {
            'areaCode' : '150403',
            'areaName' : '元宝山区',
            'py' : 'yuanbaoshan',
            'spy' : 'ybs'
        }, {
            'areaCode' : '150404',
            'areaName' : '松山区',
            'py' : 'songshan',
            'spy' : 'ss'
        }, {
            'areaCode' : '150421',
            'areaName' : '阿鲁科尔沁旗',
            'py' : 'alukeerqinqi',
            'spy' : 'alkeqq'
        }, {
            'areaCode' : '150422',
            'areaName' : '巴林左旗',
            'py' : 'balinzuoqi',
            'spy' : 'blzq'
        }, {
        'areaCode' : '150423',
        'areaName' : '巴林右旗',
        'py' : 'balinyouqi',
        'spy' : 'blyq'
    }, {
        'areaCode' : '150424',
        'areaName' : '林西县',
        'py' : 'linxi',
        'spy' : 'lx'
    }, {
        'areaCode' : '150425',
        'areaName' : '克什克腾旗',
        'py' : 'keshiketengqi',
        'spy' : 'ksktq'
    }, {
        'areaCode' : '150426',
        'areaName' : '翁牛特旗',
        'py' : 'wengniuteqi',
        'spy' : 'wntq'
    }, {
    'areaCode' : '150428',
    'areaName' : '喀喇沁旗',
    'py' : 'kalaqinqi',
    'spy' : 'klqq'
}, {
    'areaCode' : '150429',
    'areaName' : '宁城县',
    'py' : 'ningcheng',
    'spy' : 'nc'
}, {
    'areaCode' : '150430',
    'areaName' : '敖汉旗',
    'py' : 'aohanqi',
    'spy' : 'ahq'
}]
    }, {
        'areaCode' : '150500',
        'areaName' : '通辽市',
        'py' : 'tongliao',
        'spy' : 'tl',
        'datas' : [{
            'areaCode' : '150502',
            'areaName' : '科尔沁区',
            'py' : 'keerqin',
            'spy' : 'keq'
        }, {
            'areaCode' : '150521',
            'areaName' : '科尔沁左翼中旗',
            'py' : 'keerqinzuoyizhongqi',
            'spy' : 'keqzyzq'
        }, {
            'areaCode' : '150522',
            'areaName' : '科尔沁左翼后旗',
            'py' : 'keerqinzuoyihouqi',
            'spy' : 'keqzyhq'
        }, {
            'areaCode' : '150523',
            'areaName' : '开鲁县',
            'py' : 'kailu',
            'spy' : 'kl'
        }, {
        'areaCode' : '150524',
        'areaName' : '库伦旗',
        'py' : 'kulun',
        'spy' : 'kl'
    }, {
        'areaCode' : '150525',
        'areaName' : '奈曼旗',
        'py' : 'naimanqi',
        'spy' : 'nmq'
    }, {
        'areaCode' : '150526',
        'areaName' : '扎鲁特旗',
        'py' : 'zhaluteqi',
        'spy' : 'zltq'
    }, {
        'areaCode' : '150581',
        'areaName' : '霍林郭勒市',
        'py' : 'huolinguole',
        'spy' : 'hlgl'
    }]
    }, {
        'areaCode' : '150600',
        'areaName' : '鄂尔多斯市',
        'py' : 'eerduosi',
        'spy' : 'eeds',
        'datas' : [{
            'areaCode' : '150602',
            'areaName' : '东胜区',
            'py' : 'dongsheng',
            'spy' : 'ds'
        }, {
            'areaCode' : '150621',
            'areaName' : '达拉特旗',
            'py' : 'dalateqi',
            'spy' : 'dltq'
        }, {
            'areaCode' : '150622',
            'areaName' : '准格尔旗',
            'py' : 'zhungeerqi',
            'spy' : 'zgeq'
        }, {
        'areaCode' : '150623',
        'areaName' : '鄂托克前旗',
        'py' : 'etuokeqianqi',
        'spy' : 'etkqq'
    }, {
        'areaCode' : '150624',
        'areaName' : '鄂托克旗',
        'py' : 'etuokeqi',
        'spy' : 'etkq'
    }, {
        'areaCode' : '150625',
        'areaName' : '杭锦旗',
        'py' : 'hangjinqi',
        'spy' : 'hjq'
    }, {
        'areaCode' : '150626',
        'areaName' : '乌审旗',
        'py' : 'wushenqi',
        'spy' : 'wsq'
    }, {
        'areaCode' : '150627',
        'areaName' : '伊金霍洛旗',
        'py' : 'yijinhuoluoqi',
        'spy' : 'yjhlq'
    }]
    }, {
        'areaCode' : '150700',
        'areaName' : '呼伦贝尔市',
        'py' : 'hulunbeier',
        'spy' : 'hlbe',
        'datas' : [{
            'areaCode' : '150702',
            'areaName' : '海拉尔区',
            'py' : 'hailaer',
            'spy' : 'hle'
        }, {
            'areaCode' : '150721',
            'areaName' : '阿荣旗',
            'py' : 'arongqi',
            'spy' : 'arq'
        }, {
        'areaCode' : '150722',
        'areaName' : '莫力达瓦达斡尔族自治旗',
        'py' : 'molidawadawoerzuzizhiqi',
        'spy' : 'mldwdwezzzq'
    }, {
        'areaCode' : '150723',
        'areaName' : '鄂伦春自治旗',
        'py' : 'elunchunzizhiqi',
        'spy' : 'elczzq'
    }, {
        'areaCode' : '150724',
        'areaName' : '鄂温克族自治旗',
        'py' : 'ewenkezuzizhiqi',
        'spy' : 'ewkzzzq'
    }, {
        'areaCode' : '150725',
        'areaName' : '陈巴尔虎旗',
        'py' : 'chenbaerheqi',
        'spy' : 'cbehq'
    }, {
        'areaCode' : '150726',
        'areaName' : '新巴尔虎左旗',
        'py' : 'xinbaerhuzuoqi',
        'spy' : 'xbehzq'
    }, {
        'areaCode' : '150727',
        'areaName' : '新巴尔虎右旗',
        'py' : 'xinbaerhuyouqi',
        'spy' : 'xbehyq'
    }, {
        'areaCode' : '150781',
        'areaName' : '满洲里市',
        'py' : 'manzhouli',
        'spy' : 'mzl'
    }, {
    'areaCode' : '150782',
    'areaName' : '牙克石市',
    'py' : 'yakeshi',
    'spy' : 'yks'
}, {
    'areaCode' : '150783',
    'areaName' : '扎兰屯市',
    'py' : 'zhalantun',
    'spy' : 'zlt'
}, {
    'areaCode' : '150784',
    'areaName' : '额尔古纳市',
    'py' : 'eerguna',
    'spy' : 'eegn'
}, {
    'areaCode' : '150785',
    'areaName' : '根河市',
    'py' : 'genhe',
    'spy' : 'gh'
}]
    }, {
        'areaCode' : '150800',
        'areaName' : '巴彦淖尔市',
        'py' : 'bayannaoer',
        'spy' : 'byne',
        'datas' : [{
            'areaCode' : '150802',
            'areaName' : '临河区',
            'py' : 'linhe',
            'spy' : 'lh'
        }, {
        'areaCode' : '150821',
        'areaName' : '五原县',
        'py' : 'wuyuan',
        'spy' : 'wy'
    }, {
        'areaCode' : '150822',
        'areaName' : '磴口县',
        'py' : 'dengkou',
        'spy' : 'dk'
    }, {
        'areaCode' : '150823',
        'areaName' : '乌拉特前旗',
        'py' : 'wulateqianqi',
        'spy' : 'wltqq'
    }, {
        'areaCode' : '150824',
        'areaName' : '乌拉特中旗',
        'py' : 'wulatezhongqi',
        'spy' : 'wltzq'
    }, {
        'areaCode' : '150825',
        'areaName' : '乌拉特后旗',
        'py' : 'wulatehouqi',
        'spy' : 'wlthq'
    }, {
        'areaCode' : '150826',
        'areaName' : '杭锦后旗',
        'py' : 'hangjinhouqi',
        'spy' : 'hjhq'
    }]
    }, {
        'areaCode' : '150900',
        'areaName' : '乌兰察布市',
        'py' : 'wulanchabu',
        'spy' : 'wlcb',
        'datas' : [{
        'areaCode' : '150902',
        'areaName' : '集宁区',
        'py' : 'jining',
        'spy' : 'jn'
    }, {
        'areaCode' : '150921',
        'areaName' : '卓资县',
        'py' : 'zhuozi',
        'spy' : 'zz'
    }, {
        'areaCode' : '150922',
        'areaName' : '化德县',
        'py' : 'huade',
        'spy' : 'hd'
    }, {
        'areaCode' : '150923',
        'areaName' : '商都县',
        'py' : 'shangdu',
        'spy' : 'sd'
    }, {
        'areaCode' : '150924',
        'areaName' : '兴和县',
        'py' : 'xinghe',
        'spy' : 'xh'
    }, {
        'areaCode' : '150925',
        'areaName' : '凉城县',
        'py' : 'liangcheng',
        'spy' : 'lc'
    }, {
        'areaCode' : '150926',
        'areaName' : '察哈尔右翼前旗',
        'py' : 'chahaeryouyiqianqi',
        'spy' : 'cheyyqq'
    }, {
        'areaCode' : '150927',
        'areaName' : '察哈尔右翼中旗',
        'py' : 'chahaeryouyizhongqi',
        'spy' : 'cheyyzq'
    }, {
        'areaCode' : '150928',
        'areaName' : '察哈尔右翼后旗',
        'py' : 'chahaeryouyihouqi',
        'spy' : 'cheyyhq'
    }, {
    'areaCode' : '150929',
    'areaName' : '四子王旗',
    'py' : 'siziwangqi',
    'spy' : 'szwq'
}, {
    'areaCode' : '150981',
    'areaName' : '丰镇市',
    'py' : 'fengzhen',
    'spy' : 'fz'
}]
    }, {
    'areaCode' : '152200',
    'areaName' : '兴安盟',
    'py' : 'xinganmeng',
    'spy' : 'xam',
    'datas' : [{
        'areaCode' : '152201',
        'areaName' : '乌兰浩特市',
        'py' : 'wulanhaote',
        'spy' : 'wlht'
    }, {
        'areaCode' : '152202',
        'areaName' : '阿尔山市',
        'py' : 'aershan',
        'spy' : 'aes'
    }, {
        'areaCode' : '152221',
        'areaName' : '科尔沁右翼前旗',
        'py' : 'keerqinyouyiqianqi',
        'spy' : 'keqyyqq'
    }, {
        'areaCode' : '152222',
        'areaName' : '科尔沁右翼中旗',
        'py' : 'keerqinyouyizhongqi',
        'spy' : 'keqyyzq'
    }, {
        'areaCode' : '152223',
        'areaName' : '扎赉特旗',
        'py' : 'zhalaiteqi',
        'spy' : 'zltq'
    }, {
        'areaCode' : '152224',
        'areaName' : '突泉县',
        'py' : 'tuquan',
        'spy' : 'tq'
    }]
}, {
    'areaCode' : '152500',
    'areaName' : '锡林郭勒盟',
    'py' : 'xilinguolemeng',
    'spy' : 'xlglm',
    'datas' : [{
        'areaCode' : '152501',
        'areaName' : '二连浩特市',
        'py' : 'erlianhaote',
        'spy' : 'elht'
    }, {
        'areaCode' : '152502',
        'areaName' : '锡林浩特市',
        'py' : 'xilinhaote',
        'spy' : 'xlht'
    }, {
        'areaCode' : '152522',
        'areaName' : '阿巴嘎旗',
        'py' : 'abagaqi',
        'spy' : 'abgq'
    }, {
        'areaCode' : '152523',
        'areaName' : '苏尼特左旗',
        'py' : 'sunitezuoqi',
        'spy' : 'sntzq'
    }, {
        'areaCode' : '152524',
        'areaName' : '苏尼特右旗',
        'py' : 'suniteyouqi',
        'spy' : 'sntyq'
    }, {
        'areaCode' : '152525',
        'areaName' : '东乌珠穆沁旗',
        'py' : 'dongwuzhumuqinqi',
        'spy' : 'dwzmqq'
    }, {
        'areaCode' : '152526',
        'areaName' : '西乌珠穆沁旗',
        'py' : 'xiwuzhumuqinqi',
        'spy' : 'xwzmqq'
    }, {
        'areaCode' : '152527',
        'areaName' : '太仆寺旗',
        'py' : 'taipusiqi',
        'spy' : 'tpsq'
    }, {
        'areaCode' : '152528',
        'areaName' : '镶黄旗',
        'py' : 'xianghuangqi',
        'spy' : 'xhq'
    }, {
    'areaCode' : '152529',
    'areaName' : '正镶白旗',
    'py' : 'zhengxiangbaiqi',
    'spy' : 'zxbq'
}, {
    'areaCode' : '152530',
    'areaName' : '正蓝旗',
    'py' : 'zhenglanqi',
    'spy' : 'zlq'
}, {
    'areaCode' : '152531',
    'areaName' : '多伦县',
    'py' : 'duolun',
    'spy' : 'dl'
}]
}, {
    'areaCode' : '152900',
    'areaName' : '阿拉善盟',
    'py' : 'alashanmeng',
    'spy' : 'alsm',
    'datas' : [{
        'areaCode' : '152921',
        'areaName' : '阿拉善左旗',
        'py' : 'alashanzuoqi',
        'spy' : 'alszq'
    }, {
        'areaCode' : '152922',
        'areaName' : '阿拉善右旗',
        'py' : 'alashanyouqi',
        'spy' : 'alsyq'
    }, {
        'areaCode' : '152923',
        'areaName' : '额济纳旗',
        'py' : 'ejinaqi',
        'spy' : 'ejnq'
    }]
}]
}, {
    'areaCode' : '210000',
    'areaName' : '辽宁省',
    'py' : 'liaoning',
    'spy' : 'ln',
    'datas' : [{
        'areaCode' : '210100',
        'areaName' : '沈阳市',
        'py' : 'shenyang',
        'spy' : 'sy',
        'datas' : [{
            'areaCode' : '210102',
            'areaName' : '和平区',
            'py' : 'heping',
            'spy' : 'hp'
        }, {
            'areaCode' : '210103',
            'areaName' : '沈河区',
            'py' : 'shenhe',
            'spy' : 'sh'
        }, {
            'areaCode' : '210104',
            'areaName' : '大东区',
            'py' : 'dadong',
            'spy' : 'dd'
        }, {
            'areaCode' : '210105',
            'areaName' : '皇姑区',
            'py' : 'huanggu',
            'spy' : 'hg'
        }, {
            'areaCode' : '210106',
            'areaName' : '铁西区',
            'py' : 'tiexi',
            'spy' : 'tx'
        }, {
            'areaCode' : '210111',
            'areaName' : '苏家屯区',
            'py' : 'sujiatun',
            'spy' : 'sjt'
        }, {
            'areaCode' : '210112',
            'areaName' : '东陵区',
            'py' : 'dongling',
            'spy' : 'dl'
        }, {
            'areaCode' : '210113',
            'areaName' : '沈北新区',
            'py' : 'shenbeixinqu',
            'spy' : 'sbxq'
        }, {
        'areaCode' : '210114',
        'areaName' : '于洪区',
        'py' : 'yuhong',
        'spy' : 'yh'
    }, {
    'areaCode' : '210122',
    'areaName' : '辽中县',
    'py' : 'liaozhong',
    'spy' : 'lz'
}, {
    'areaCode' : '210123',
    'areaName' : '康平县',
    'py' : 'kangping',
    'spy' : 'kp'
}, {
    'areaCode' : '210124',
    'areaName' : '法库县',
    'py' : 'faku',
    'spy' : 'fk'
}, {
    'areaCode' : '210181',
    'areaName' : '新民市',
    'py' : 'xinmin',
    'spy' : 'xm'
}]
    }, {
        'areaCode' : '210200',
        'areaName' : '大连市',
        'py' : 'dalian',
        'spy' : 'dl',
        'datas' : [{
            'areaCode' : '210202',
            'areaName' : '中山区',
            'py' : 'zhongshan',
            'spy' : 'zs'
        }, {
            'areaCode' : '210203',
            'areaName' : '西岗区',
            'py' : 'xigang',
            'spy' : 'xg'
        }, {
            'areaCode' : '210204',
            'areaName' : '沙河口区',
            'py' : 'shahekou',
            'spy' : 'shk'
        }, {
            'areaCode' : '210211',
            'areaName' : '甘井子区',
            'py' : 'ganjingzi',
            'spy' : 'gjz'
        }, {
            'areaCode' : '210212',
            'areaName' : '旅顺口区',
            'py' : 'lvshunkou',
            'spy' : 'lsk'
        }, {
            'areaCode' : '210213',
            'areaName' : '金州区',
            'py' : 'jinzhou',
            'spy' : 'jz'
        }, {
            'areaCode' : '210224',
            'areaName' : '长海县',
            'py' : 'changhai',
            'spy' : 'ch'
        }, {
        'areaCode' : '210281',
        'areaName' : '瓦房店市',
        'py' : 'wafangdian',
        'spy' : 'wfd'
    }, {
        'areaCode' : '210282',
        'areaName' : '普兰店市',
        'py' : 'pulandian',
        'spy' : 'pld'
    }, {
    'areaCode' : '210283',
    'areaName' : '庄河市',
    'py' : 'zhuanghe',
    'spy' : 'zh'
}, {
    'areaCode' : '210284',
    'areaName' : '高新园区',
    'py' : 'gaoxinyuanqu',
    'spy' : 'gxyq'
}, {
    'areaCode' : '210285',
    'areaName' : '开发区',
    'py' : 'kaifa',
    'spy' : 'kf'
}]
    }, {
        'areaCode' : '210300',
        'areaName' : '鞍山市',
        'py' : 'anshan',
        'spy' : 'as',
        'datas' : [{
            'areaCode' : '210302',
            'areaName' : '铁东区',
            'py' : 'tiedong',
            'spy' : 'td'
        }, {
            'areaCode' : '210303',
            'areaName' : '铁西区',
            'py' : 'tiexi',
            'spy' : 'tx'
        }, {
            'areaCode' : '210304',
            'areaName' : '立山区',
            'py' : 'lishan',
            'spy' : 'ls'
        }, {
            'areaCode' : '210311',
            'areaName' : '千山区',
            'py' : 'qianshan',
            'spy' : 'qs'
        }, {
            'areaCode' : '210321',
            'areaName' : '台安县',
            'py' : 'taian',
            'spy' : 'ta'
        }, {
            'areaCode' : '210323',
            'areaName' : '岫岩满族自治县',
            'py' : 'xiuyanmanzuzizhixian',
            'spy' : 'xymzzzx'
        }, {
        'areaCode' : '210381',
        'areaName' : '海城市',
        'py' : 'haicheng',
        'spy' : 'hc'
    }]
    }, {
        'areaCode' : '210400',
        'areaName' : '抚顺市',
        'py' : 'fushun',
        'spy' : 'fs',
        'datas' : [{
            'areaCode' : '210402',
            'areaName' : '新抚区',
            'py' : 'xinfu',
            'spy' : 'xf'
        }, {
            'areaCode' : '210403',
            'areaName' : '东洲区',
            'py' : 'dongzhou',
            'spy' : 'dz'
        }, {
            'areaCode' : '210404',
            'areaName' : '望花区',
            'py' : 'wanghua',
            'spy' : 'wh'
        }, {
            'areaCode' : '210411',
            'areaName' : '顺城区',
            'py' : 'shuncheng',
            'spy' : 'sc'
        }, {
            'areaCode' : '210421',
            'areaName' : '抚顺县',
            'py' : 'fushun',
            'spy' : 'fs'
        }, {
        'areaCode' : '210422',
        'areaName' : '新宾满族自治县',
        'py' : 'xinbinmanzuzizhixian',
        'spy' : 'xbmzzzx'
    }, {
        'areaCode' : '210423',
        'areaName' : '清原满族自治县',
        'py' : 'qingyuanmanzuzizhixian',
        'spy' : 'qymzzzx'
    }]
    }, {
        'areaCode' : '210500',
        'areaName' : '本溪市',
        'py' : 'benxi',
        'spy' : 'bx',
        'datas' : [{
            'areaCode' : '210502',
            'areaName' : '平山区',
            'py' : 'pingshan',
            'spy' : 'ps'
        }, {
            'areaCode' : '210503',
            'areaName' : '溪湖区',
            'py' : 'xihu',
            'spy' : 'xh'
        }, {
            'areaCode' : '210504',
            'areaName' : '明山区',
            'py' : 'mingshan',
            'spy' : 'ms'
        }, {
            'areaCode' : '210505',
            'areaName' : '南芬区',
            'py' : 'nanfen',
            'spy' : 'nf'
        }, {
        'areaCode' : '210521',
        'areaName' : '本溪满族自治县',
        'py' : 'benximanzuzizhixian',
        'spy' : 'bxmzzzx'
    }, {
        'areaCode' : '210522',
        'areaName' : '桓仁满族自治县',
        'py' : 'huanrenmanzuzizhixian',
        'spy' : 'hrmzzzx'
    }]
    }, {
        'areaCode' : '210600',
        'areaName' : '丹东市',
        'py' : 'dandong',
        'spy' : 'dd',
        'datas' : [{
            'areaCode' : '210602',
            'areaName' : '元宝区',
            'py' : 'yuanbao',
            'spy' : 'yb'
        }, {
            'areaCode' : '210603',
            'areaName' : '振兴区',
            'py' : 'zhenxing',
            'spy' : 'zx'
        }, {
            'areaCode' : '210604',
            'areaName' : '振安区',
            'py' : 'zhenan',
            'spy' : 'za'
        }, {
        'areaCode' : '210624',
        'areaName' : '宽甸满族自治县',
        'py' : 'kuandianmanzuzizhixian',
        'spy' : 'kdmzzzx'
    }, {
        'areaCode' : '210681',
        'areaName' : '东港市',
        'py' : 'donggang',
        'spy' : 'dg'
    }, {
        'areaCode' : '210682',
        'areaName' : '凤城市',
        'py' : 'fengcheng',
        'spy' : 'fc'
    }]
    }, {
        'areaCode' : '210700',
        'areaName' : '锦州市',
        'py' : 'jinzhou',
        'spy' : 'jz',
        'datas' : [{
            'areaCode' : '210702',
            'areaName' : '古塔区',
            'py' : 'guta',
            'spy' : 'gt'
        }, {
            'areaCode' : '210703',
            'areaName' : '凌河区',
            'py' : 'linghe',
            'spy' : 'lh'
        }, {
        'areaCode' : '210711',
        'areaName' : '太和区',
        'py' : 'taihe',
        'spy' : 'th'
    }, {
        'areaCode' : '210726',
        'areaName' : '黑山县',
        'py' : 'heishan',
        'spy' : 'hs'
    }, {
        'areaCode' : '210727',
        'areaName' : '义县',
        'py' : 'yixian',
        'spy' : 'yx'
    }, {
        'areaCode' : '210781',
        'areaName' : '凌海市',
        'py' : 'linghai',
        'spy' : 'lh'
    }, {
        'areaCode' : '210782',
        'areaName' : '北镇市',
        'py' : 'beizhen',
        'spy' : 'bz'
    }]
    }, {
        'areaCode' : '210800',
        'areaName' : '营口市',
        'py' : 'yingkou',
        'spy' : 'yk',
        'datas' : [{
            'areaCode' : '210802',
            'areaName' : '站前区',
            'py' : 'zhanqian',
            'spy' : 'zq'
        }, {
        'areaCode' : '210803',
        'areaName' : '西市区',
        'py' : 'xishi',
        'spy' : 'xs'
    }, {
        'areaCode' : '210804',
        'areaName' : '鲅鱼圈区',
        'py' : 'bayuquan',
        'spy' : 'byq'
    }, {
        'areaCode' : '210811',
        'areaName' : '老边区',
        'py' : 'laobian',
        'spy' : 'lb'
    }, {
        'areaCode' : '210881',
        'areaName' : '盖州市',
        'py' : 'gaizhou',
        'spy' : 'gz'
    }, {
        'areaCode' : '210882',
        'areaName' : '大石桥市',
        'py' : 'dashiqiao',
        'spy' : 'dsq'
    }]
    }, {
        'areaCode' : '210900',
        'areaName' : '阜新市',
        'py' : 'fuxin',
        'spy' : 'fx',
        'datas' : [{
        'areaCode' : '210902',
        'areaName' : '海州区',
        'py' : 'haizhou',
        'spy' : 'hz'
    }, {
        'areaCode' : '210903',
        'areaName' : '新邱区',
        'py' : 'xinqiu',
        'spy' : 'xq'
    }, {
        'areaCode' : '210904',
        'areaName' : '太平区',
        'py' : 'taiping',
        'spy' : 'tp'
    }, {
        'areaCode' : '210905',
        'areaName' : '清河门区',
        'py' : 'qinghemen',
        'spy' : 'qhm'
    }, {
        'areaCode' : '210911',
        'areaName' : '细河区',
        'py' : 'xihe',
        'spy' : 'xh'
    }, {
        'areaCode' : '210921',
        'areaName' : '阜新蒙古族自治县',
        'py' : 'fuxinmengguzuzizhixian',
        'spy' : 'fxmgzzzx'
    }, {
        'areaCode' : '210922',
        'areaName' : '彰武县',
        'py' : 'zhangwu',
        'spy' : 'zw'
    }]
    }, {
    'areaCode' : '211000',
    'areaName' : '辽阳市',
    'py' : 'liaoyang',
    'spy' : 'ly',
    'datas' : [{
        'areaCode' : '211002',
        'areaName' : '白塔区',
        'py' : 'baita',
        'spy' : 'bt'
    }, {
        'areaCode' : '211003',
        'areaName' : '文圣区',
        'py' : 'wensheng',
        'spy' : 'ws'
    }, {
        'areaCode' : '211004',
        'areaName' : '宏伟区',
        'py' : 'hongwei',
        'spy' : 'hw'
    }, {
        'areaCode' : '211005',
        'areaName' : '弓长岭区',
        'py' : 'gongchangling',
        'spy' : 'gcl'
    }, {
        'areaCode' : '211011',
        'areaName' : '太子河区',
        'py' : 'taizihe',
        'spy' : 'tzh'
    }, {
        'areaCode' : '211021',
        'areaName' : '辽阳县',
        'py' : 'liaoyang',
        'spy' : 'ly'
    }, {
        'areaCode' : '211081',
        'areaName' : '灯塔市',
        'py' : 'dengta',
        'spy' : 'dt'
    }]
}, {
    'areaCode' : '211100',
    'areaName' : '盘锦市',
    'py' : 'panjin',
    'spy' : 'pk',
    'datas' : [{
        'areaCode' : '211102',
        'areaName' : '双台子区',
        'py' : 'shuangtaizi',
        'spy' : 'stz'
    }, {
        'areaCode' : '211103',
        'areaName' : '兴隆台区',
        'py' : 'xinglongtai',
        'spy' : 'xlt'
    }, {
        'areaCode' : '211121',
        'areaName' : '大洼县',
        'py' : 'dawa',
        'spy' : 'dw'
    }, {
        'areaCode' : '211122',
        'areaName' : '盘山县',
        'py' : 'panshan',
        'spy' : 'ps'
    }]
}, {
    'areaCode' : '211200',
    'areaName' : '铁岭市',
    'py' : 'tieling',
    'spy' : 'tl',
    'datas' : [{
        'areaCode' : '211202',
        'areaName' : '银州区',
        'py' : 'yinchuan',
        'spy' : 'yc'
    }, {
        'areaCode' : '211204',
        'areaName' : '清河区',
        'py' : 'qinghe',
        'spy' : 'qh'
    }, {
        'areaCode' : '211221',
        'areaName' : '铁岭县',
        'py' : 'tieling',
        'spy' : 'tl'
    }, {
        'areaCode' : '211223',
        'areaName' : '西丰县',
        'py' : 'xifeng',
        'spy' : 'xf'
    }, {
        'areaCode' : '211224',
        'areaName' : '昌图县',
        'py' : 'changtu',
        'spy' : 'ct'
    }, {
        'areaCode' : '211281',
        'areaName' : '调兵山市',
        'py' : 'diaobingshan',
        'spy' : 'dbs'
    }, {
        'areaCode' : '211282',
        'areaName' : '开原市',
        'py' : 'kaiyuan',
        'spy' : 'ky'
    }]
}, {
    'areaCode' : '211300',
    'areaName' : '朝阳市',
    'py' : 'chaoyang',
    'spy' : 'cy',
    'datas' : [{
        'areaCode' : '211302',
        'areaName' : '双塔区',
        'py' : 'shuangta',
        'spy' : 'st'
    }, {
        'areaCode' : '211303',
        'areaName' : '龙城区',
        'py' : 'longcheng',
        'spy' : 'lc'
    }, {
        'areaCode' : '211321',
        'areaName' : '朝阳县',
        'py' : 'chaoyang',
        'spy' : 'cy'
    }, {
        'areaCode' : '211322',
        'areaName' : '建平县',
        'py' : 'jianping',
        'spy' : 'jp'
    }, {
        'areaCode' : '211324',
        'areaName' : '喀喇沁左翼蒙古族自治县',
        'py' : 'kalaqinzuoyimengguzuzizhixian',
        'spy' : 'klqzymgzzzx'
    }, {
        'areaCode' : '211381',
        'areaName' : '北票市',
        'py' : 'beipiao',
        'spy' : 'bp'
    }, {
        'areaCode' : '211382',
        'areaName' : '凌源市',
        'py' : 'lingyuan',
        'spy' : 'ly'
    }]
}, {
    'areaCode' : '211400',
    'areaName' : '葫芦岛市',
    'py' : 'huludao',
    'spy' : 'hld',
    'datas' : [{
        'areaCode' : '211402',
        'areaName' : '连山区',
        'py' : 'lianshan',
        'spy' : 'ls'
    }, {
        'areaCode' : '211403',
        'areaName' : '龙港区',
        'py' : 'longgang',
        'spy' : 'lg'
    }, {
        'areaCode' : '211404',
        'areaName' : '南票区',
        'py' : 'nanpiao',
        'spy' : 'np'
    }, {
        'areaCode' : '211421',
        'areaName' : '绥中县',
        'py' : 'suizhong',
        'spy' : 'sz'
    }, {
        'areaCode' : '211422',
        'areaName' : '建昌县',
        'py' : 'jianchang',
        'spy' : 'jc'
    }, {
        'areaCode' : '211481',
        'areaName' : '兴城市',
        'py' : 'xingcheng',
        'spy' : 'xc'
    }]
}]
}, {
    'areaCode' : '220000',
    'areaName' : '吉林省',
    'py' : 'jilin',
    'spy' : 'jl',
    'datas' : [{
        'areaCode' : '220100',
        'areaName' : '长春市',
        'py' : 'changchun',
        'spy' : 'cc',
        'datas' : [{
            'areaCode' : '220102',
            'areaName' : '南关区',
            'py' : 'nanguan',
            'spy' : 'ng'
        }, {
            'areaCode' : '220103',
            'areaName' : '宽城区',
            'py' : 'kuancheng',
            'spy' : 'kc'
        }, {
            'areaCode' : '220104',
            'areaName' : '朝阳区',
            'py' : 'chaoyang',
            'spy' : 'cy'
        }, {
            'areaCode' : '220105',
            'areaName' : '二道区',
            'py' : 'erdao',
            'spy' : 'ed'
        }, {
            'areaCode' : '220106',
            'areaName' : '绿园区',
            'py' : 'lvyuan',
            'spy' : 'ly'
        }, {
            'areaCode' : '220112',
            'areaName' : '双阳区',
            'py' : 'shuangyang',
            'spy' : 'sy'
        }, {
            'areaCode' : '220122',
            'areaName' : '农安县',
            'py' : 'nongan',
            'spy' : 'na'
        }, {
            'areaCode' : '220181',
            'areaName' : '九台市',
            'py' : 'jiutai',
            'spy' : 'jt'
        }, {
        'areaCode' : '220182',
        'areaName' : '榆树市',
        'py' : 'yushu',
        'spy' : 'ys'
    }, {
    'areaCode' : '220183',
    'areaName' : '德惠市',
    'py' : 'dehui',
    'spy' : 'dh'
}]
    }, {
        'areaCode' : '220200',
        'areaName' : '吉林市',
        'py' : 'jilin',
        'spy' : 'jl',
        'datas' : [{
            'areaCode' : '220202',
            'areaName' : '昌邑区',
            'py' : 'changyi',
            'spy' : 'cy'
        }, {
            'areaCode' : '220203',
            'areaName' : '龙潭区',
            'py' : 'longtan',
            'spy' : 'lt'
        }, {
            'areaCode' : '220204',
            'areaName' : '船营区',
            'py' : 'chuanying',
            'spy' : 'cy'
        }, {
            'areaCode' : '220211',
            'areaName' : '丰满区',
            'py' : 'fengman',
            'spy' : 'fm'
        }, {
            'areaCode' : '220221',
            'areaName' : '永吉县',
            'py' : 'yongji',
            'spy' : 'yj'
        }, {
            'areaCode' : '220281',
            'areaName' : '蛟河市',
            'py' : 'jiaohe',
            'spy' : 'jh'
        }, {
            'areaCode' : '220282',
            'areaName' : '桦甸市',
            'py' : 'huatian',
            'spy' : 'ht'
        }, {
        'areaCode' : '220283',
        'areaName' : '舒兰市',
        'py' : 'shulan',
        'spy' : 'sl'
    }, {
        'areaCode' : '220284',
        'areaName' : '磐石市',
        'py' : 'panshi',
        'spy' : 'ps'
    }]
    }, {
        'areaCode' : '220300',
        'areaName' : '四平市',
        'py' : 'siping',
        'spy' : 'sp',
        'datas' : [{
            'areaCode' : '220302',
            'areaName' : '铁西区',
            'py' : 'tiexi',
            'spy' : 'tx'
        }, {
            'areaCode' : '220303',
            'areaName' : '铁东区',
            'py' : 'tiedong',
            'spy' : 'td'
        }, {
            'areaCode' : '220322',
            'areaName' : '梨树县',
            'py' : 'lishu',
            'spy' : 'ls'
        }, {
            'areaCode' : '220323',
            'areaName' : '伊通满族自治县',
            'py' : 'yitongmanzuzizhixian',
            'spy' : 'ytmzzzx'
        }, {
            'areaCode' : '220381',
            'areaName' : '公主岭市',
            'py' : 'gongzhuling',
            'spy' : 'gzl'
        }, {
            'areaCode' : '220382',
            'areaName' : '双辽市',
            'py' : 'shuangliao',
            'spy' : 'sl'
        }]
    }, {
        'areaCode' : '220400',
        'areaName' : '辽源市',
        'py' : 'liaoyuan',
        'spy' : 'ly',
        'datas' : [{
            'areaCode' : '220402',
            'areaName' : '龙山区',
            'py' : 'longshan',
            'spy' : 'ls'
        }, {
            'areaCode' : '220403',
            'areaName' : '西安区',
            'py' : 'xian',
            'spy' : 'xa'
        }, {
            'areaCode' : '220421',
            'areaName' : '东丰县',
            'py' : 'dongfeng',
            'spy' : 'df'
        }, {
            'areaCode' : '220422',
            'areaName' : '东辽县',
            'py' : 'dongliao',
            'spy' : 'dl'
        }]
    }, {
        'areaCode' : '220500',
        'areaName' : '通化市',
        'py' : 'tonghua',
        'spy' : 'th',
        'datas' : [{
            'areaCode' : '220502',
            'areaName' : '东昌区',
            'py' : 'dongchang',
            'spy' : 'dc'
        }, {
            'areaCode' : '220503',
            'areaName' : '二道江区',
            'py' : 'erdaojiang',
            'spy' : 'edj'
        }, {
            'areaCode' : '220521',
            'areaName' : '通化县',
            'py' : 'tonghua',
            'spy' : 'th'
        }, {
            'areaCode' : '220523',
            'areaName' : '辉南县',
            'py' : 'huinan',
            'spy' : 'hn'
        }, {
        'areaCode' : '220524',
        'areaName' : '柳河县',
        'py' : 'liuhe',
        'spy' : 'lh'
    }, {
        'areaCode' : '220581',
        'areaName' : '梅河口市',
        'py' : 'meihekou',
        'spy' : 'mhk'
    }, {
        'areaCode' : '220582',
        'areaName' : '集安市',
        'py' : 'jian',
        'spy' : 'ja'
    }]
    }, {
        'areaCode' : '220600',
        'areaName' : '白山市',
        'py' : 'baishan',
        'spy' : 'bs',
        'datas' : [{
            'areaCode' : '220602',
            'areaName' : '八道江区',
            'py' : 'badaojiang',
            'spy' : 'bdj'
        }, {
            'areaCode' : '220621',
            'areaName' : '抚松县',
            'py' : 'fusong',
            'spy' : 'fs'
        }, {
            'areaCode' : '220622',
            'areaName' : '靖宇县',
            'py' : 'jingyu',
            'spy' : 'jy'
        }, {
        'areaCode' : '220623',
        'areaName' : '长白朝鲜族自治县',
        'py' : 'changbaichaoxianzuzizhixian',
        'spy' : 'cbcxzzzx'
    }, {
        'areaCode' : '220625',
        'areaName' : '江源区',
        'py' : 'jiangyuan',
        'spy' : 'jy'
    }, {
        'areaCode' : '220681',
        'areaName' : '临江市',
        'py' : 'linjiang',
        'spy' : 'lj'
    }]
    }, {
        'areaCode' : '220700',
        'areaName' : '松原市',
        'py' : 'songyuan',
        'spy' : 'sy',
        'datas' : [{
            'areaCode' : '220702',
            'areaName' : '宁江区',
            'py' : 'ningjiang',
            'spy' : 'nj'
        }, {
            'areaCode' : '220721',
            'areaName' : '前郭尔罗斯蒙古族自治县',
            'py' : 'qianguoerluosimengguzuzizhixian',
            'spy' : 'qgelsmgzzzx'
        }, {
        'areaCode' : '220722',
        'areaName' : '长岭县',
        'py' : 'changling',
        'spy' : 'cl'
    }, {
        'areaCode' : '220723',
        'areaName' : '乾安县',
        'py' : 'qianan',
        'spy' : 'qa'
    }, {
        'areaCode' : '220724',
        'areaName' : '扶余县',
        'py' : 'fuyu',
        'spy' : 'fy'
    }]
    }, {
        'areaCode' : '220800',
        'areaName' : '白城市',
        'py' : 'baicheng',
        'spy' : 'bc',
        'datas' : [{
            'areaCode' : '220802',
            'areaName' : '洮北区',
            'py' : 'taobei',
            'spy' : 'tb'
        }, {
        'areaCode' : '220821',
        'areaName' : '镇赉县',
        'py' : 'zhenlai',
        'spy' : 'zl'
    }, {
        'areaCode' : '220822',
        'areaName' : '通榆县',
        'py' : 'tongyu',
        'spy' : 'ty'
    }, {
        'areaCode' : '220881',
        'areaName' : '洮南市',
        'py' : 'taonan',
        'spy' : 'tn'
    }, {
        'areaCode' : '220882',
        'areaName' : '大安市',
        'py' : 'daan',
        'spy' : 'da'
    }]
    }, {
        'areaCode' : '222400',
        'areaName' : '延边朝鲜族自治州',
        'py' : 'yanbianchaoxianzuzizhizhou',
        'spy' : 'ybcxzzzz',
        'datas' : [{
        'areaCode' : '222401',
        'areaName' : '延吉市',
        'py' : 'yanji',
        'spy' : 'yj'
    }, {
        'areaCode' : '222402',
        'areaName' : '图们市',
        'py' : 'tumen',
        'spy' : 'tm'
    }, {
        'areaCode' : '222403',
        'areaName' : '敦化市',
        'py' : 'dunhua',
        'spy' : 'dh'
    }, {
        'areaCode' : '222404',
        'areaName' : '珲春市',
        'py' : 'hunchun',
        'spy' : 'hc'
    }, {
        'areaCode' : '222405',
        'areaName' : '龙井市',
        'py' : 'longjing',
        'spy' : 'lj'
    }, {
        'areaCode' : '222406',
        'areaName' : '和龙市',
        'py' : 'helong',
        'spy' : 'hl'
    }, {
        'areaCode' : '222424',
        'areaName' : '汪清县',
        'py' : 'wangqing',
        'spy' : 'wq'
    }, {
        'areaCode' : '222426',
        'areaName' : '安图县',
        'py' : 'antu',
        'spy' : 'at'
    }]
    }]
}, {
    'areaCode' : '230000',
    'areaName' : '黑龙江省',
    'py' : 'heilongjiang',
    'spy' : 'hlj',
    'datas' : [{
        'areaCode' : '230100',
        'areaName' : '哈尔滨市',
        'py' : 'haerbin',
        'spy' : 'heb',
        'datas' : [{
            'areaCode' : '230102',
            'areaName' : '道里区',
            'py' : 'daoli',
            'spy' : 'dl'
        }, {
            'areaCode' : '230103',
            'areaName' : '南岗区',
            'py' : 'nangang',
            'spy' : 'ng'
        }, {
            'areaCode' : '230104',
            'areaName' : '道外区',
            'py' : 'daowai',
            'spy' : 'dw'
        }, {
            'areaCode' : '230107',
            'areaName' : '动力区',
            'py' : 'dongli',
            'spy' : 'dl'
        }, {
            'areaCode' : '230108',
            'areaName' : '平房区',
            'py' : 'pingfang',
            'spy' : 'pf'
        }, {
            'areaCode' : '230109',
            'areaName' : '松北区',
            'py' : 'songbei',
            'spy' : 'sb'
        }, {
            'areaCode' : '230110',
            'areaName' : '香坊区',
            'py' : 'xiangfang',
            'spy' : 'xf'
        }, {
            'areaCode' : '230111',
            'areaName' : '呼兰区',
            'py' : 'hulan',
            'spy' : 'hl'
        }, {
        'areaCode' : '230112',
        'areaName' : '阿城区',
        'py' : 'acheng',
        'spy' : 'ac'
    }, {
    'areaCode' : '230123',
    'areaName' : '依兰县',
    'py' : 'yilan',
    'spy' : 'yl'
}, {
    'areaCode' : '230124',
    'areaName' : '方正县',
    'py' : 'fangzheng',
    'spy' : 'fz'
}, {
    'areaCode' : '230125',
    'areaName' : '宾县',
    'py' : 'binxian',
    'spy' : 'bx'
}, {
    'areaCode' : '230126',
    'areaName' : '巴彦县',
    'py' : 'bayan',
    'spy' : 'by'
}, {
    'areaCode' : '230127',
    'areaName' : '木兰县',
    'py' : 'mulan',
    'spy' : 'ml'
}, {
    'areaCode' : '230128',
    'areaName' : '通河县',
    'py' : 'tonghe',
    'spy' : 'th'
}, {
    'areaCode' : '230129',
    'areaName' : '延寿县',
    'py' : 'yanshou',
    'spy' : 'ys'
}, {
    'areaCode' : '230182',
    'areaName' : '双城市',
    'py' : 'shuangcheng',
    'spy' : 'sc'
}, {
    'areaCode' : '230183',
    'areaName' : '尚志市',
    'py' : 'shangzhi',
    'spy' : 'sz'
}, {
    'areaCode' : '230184',
    'areaName' : '五常市',
    'py' : 'wuchang',
    'spy' : 'wc'
}]
    }, {
        'areaCode' : '230200',
        'areaName' : '齐齐哈尔市',
        'py' : 'qiqihaer',
        'spy' : 'qqhe',
        'datas' : [{
            'areaCode' : '230202',
            'areaName' : '龙沙区',
            'py' : 'longsha',
            'spy' : 'ls'
        }, {
            'areaCode' : '230203',
            'areaName' : '建华区',
            'py' : 'jianhua',
            'spy' : 'jh'
        }, {
            'areaCode' : '230204',
            'areaName' : '铁锋区',
            'py' : 'tiefeng',
            'spy' : 'tf'
        }, {
            'areaCode' : '230205',
            'areaName' : '昂昂溪区',
            'py' : 'angangxi',
            'spy' : 'aax'
        }, {
            'areaCode' : '230206',
            'areaName' : '富拉尔基区',
            'py' : 'fulaerji',
            'spy' : 'flej'
        }, {
            'areaCode' : '230207',
            'areaName' : '碾子山区',
            'py' : 'nianzishanqu',
            'spy' : 'nzsq'
        }, {
            'areaCode' : '230208',
            'areaName' : '梅里斯达斡尔族区',
            'py' : 'meilisidawoerzu',
            'spy' : 'mlsdwez'
        }, {
        'areaCode' : '230221',
        'areaName' : '龙江县',
        'py' : 'longjiang',
        'spy' : 'lj'
    }, {
        'areaCode' : '230223',
        'areaName' : '依安县',
        'py' : 'yian',
        'spy' : 'ya'
    }, {
    'areaCode' : '230224',
    'areaName' : '泰来县',
    'py' : 'tailai',
    'spy' : 'tl'
}, {
    'areaCode' : '230225',
    'areaName' : '甘南县',
    'py' : 'gannan',
    'spy' : 'gn'
}, {
    'areaCode' : '230227',
    'areaName' : '富裕县',
    'py' : 'fuyu',
    'spy' : 'fy'
}, {
    'areaCode' : '230229',
    'areaName' : '克山县',
    'py' : 'keshan',
    'spy' : 'ks'
}, {
    'areaCode' : '230230',
    'areaName' : '克东县',
    'py' : 'kedong',
    'spy' : 'kd'
}, {
    'areaCode' : '230231',
    'areaName' : '拜泉县',
    'py' : 'baiquan',
    'spy' : 'bq'
}, {
    'areaCode' : '230281',
    'areaName' : '讷河市',
    'py' : 'nehe',
    'spy' : 'nh'
}]
    }, {
        'areaCode' : '230300',
        'areaName' : '鸡西市',
        'py' : 'jixi',
        'spy' : 'jx',
        'datas' : [{
            'areaCode' : '230302',
            'areaName' : '鸡冠区',
            'py' : 'jiguan',
            'spy' : 'jg'
        }, {
            'areaCode' : '230303',
            'areaName' : '恒山区',
            'py' : 'hengshan',
            'spy' : 'hs'
        }, {
            'areaCode' : '230304',
            'areaName' : '滴道区',
            'py' : 'didao',
            'spy' : 'dd'
        }, {
            'areaCode' : '230305',
            'areaName' : '梨树区',
            'py' : 'lishu',
            'spy' : 'ls'
        }, {
            'areaCode' : '230306',
            'areaName' : '城子河区',
            'py' : 'chengzihe',
            'spy' : 'czh'
        }, {
            'areaCode' : '230307',
            'areaName' : '麻山区',
            'py' : 'mashan',
            'spy' : 'ms'
        }, {
        'areaCode' : '230321',
        'areaName' : '鸡东县',
        'py' : 'jidong',
        'spy' : 'jd'
    }, {
        'areaCode' : '230381',
        'areaName' : '虎林市',
        'py' : 'hl',
        'spy' : 'hl'
    }, {
        'areaCode' : '230382',
        'areaName' : '密山市',
        'py' : 'mishan',
        'spy' : 'ms'
    }]
    }, {
        'areaCode' : '230400',
        'areaName' : '鹤岗市',
        'py' : 'hegang',
        'spy' : 'hg',
        'datas' : [{
            'areaCode' : '230402',
            'areaName' : '向阳区',
            'py' : 'xiangyang',
            'spy' : 'xy'
        }, {
            'areaCode' : '230403',
            'areaName' : '工农区',
            'py' : 'gongnong',
            'spy' : 'gn'
        }, {
            'areaCode' : '230404',
            'areaName' : '南山区',
            'py' : 'nanshan',
            'spy' : 'ns'
        }, {
            'areaCode' : '230405',
            'areaName' : '兴安区',
            'py' : 'xingan',
            'spy' : 'xa'
        }, {
            'areaCode' : '230406',
            'areaName' : '东山区',
            'py' : 'dongshan',
            'spy' : 'ds'
        }, {
        'areaCode' : '230407',
        'areaName' : '兴山区',
        'py' : 'xingshan',
        'spy' : 'xs'
    }, {
        'areaCode' : '230421',
        'areaName' : '萝北县',
        'py' : 'luobei',
        'spy' : 'lb'
    }, {
        'areaCode' : '230422',
        'areaName' : '绥滨县',
        'py' : 'suibin',
        'spy' : 'sb'
    }]
    }, {
        'areaCode' : '230500',
        'areaName' : '双鸭山市',
        'py' : 'shuangyashan',
        'spy' : 'sys',
        'datas' : [{
            'areaCode' : '230502',
            'areaName' : '尖山区',
            'py' : 'jianshan',
            'spy' : 'js'
        }, {
            'areaCode' : '230503',
            'areaName' : '岭东区',
            'py' : 'lingdong',
            'spy' : 'ld'
        }, {
            'areaCode' : '230505',
            'areaName' : '四方台区',
            'py' : 'sifangtai',
            'spy' : 'sft'
        }, {
            'areaCode' : '230506',
            'areaName' : '宝山区',
            'py' : 'baoshan',
            'spy' : 'bs'
        }, {
        'areaCode' : '230521',
        'areaName' : '集贤县',
        'py' : 'jixian',
        'spy' : 'jx'
    }, {
        'areaCode' : '230522',
        'areaName' : '友谊县',
        'py' : 'youyi',
        'spy' : 'yy'
    }, {
        'areaCode' : '230523',
        'areaName' : '宝清县',
        'py' : 'baoqing',
        'spy' : 'bq'
    }, {
        'areaCode' : '230524',
        'areaName' : '饶河县',
        'py' : 'raohe',
        'spy' : 'rh'
    }]
    }, {
        'areaCode' : '230600',
        'areaName' : '大庆市',
        'py' : 'daqing',
        'spy' : 'dq',
        'datas' : [{
            'areaCode' : '230602',
            'areaName' : '萨尔图区',
            'py' : 'saertu',
            'spy' : 'set'
        }, {
            'areaCode' : '230603',
            'areaName' : '龙凤区',
            'py' : 'longfeng',
            'spy' : 'lf'
        }, {
            'areaCode' : '230604',
            'areaName' : '让胡路区',
            'py' : 'ranghulu',
            'spy' : 'rhl'
        }, {
        'areaCode' : '230605',
        'areaName' : '红岗区',
        'py' : 'honggang',
        'spy' : 'hg'
    }, {
        'areaCode' : '230606',
        'areaName' : '大同区',
        'py' : 'datong',
        'spy' : 'dt'
    }, {
        'areaCode' : '230621',
        'areaName' : '肇州县',
        'py' : 'zhaozhou',
        'spy' : 'zz'
    }, {
        'areaCode' : '230622',
        'areaName' : '肇源县',
        'py' : 'zhaoyuan',
        'spy' : 'zy'
    }, {
        'areaCode' : '230623',
        'areaName' : '林甸县',
        'py' : 'lintian',
        'spy' : 'lt'
    }, {
        'areaCode' : '230624',
        'areaName' : '杜尔伯特蒙古族自治县',
        'py' : 'duerbotemengguzuzizhixian',
        'spy' : 'debtmgzzzx'
    }]
    }, {
        'areaCode' : '230700',
        'areaName' : '伊春市',
        'py' : 'yichun',
        'spy' : 'yc',
        'datas' : [{
            'areaCode' : '230702',
            'areaName' : '伊春区',
            'py' : 'yichun',
            'spy' : 'yc'
        }, {
            'areaCode' : '230703',
            'areaName' : '南岔区',
            'py' : 'nancha',
            'spy' : 'nc'
        }, {
        'areaCode' : '230704',
        'areaName' : '友好区',
        'py' : 'youhao',
        'spy' : 'yh'
    }, {
        'areaCode' : '230705',
        'areaName' : '西林区',
        'py' : 'xilin',
        'spy' : 'xl'
    }, {
        'areaCode' : '230706',
        'areaName' : '翠峦区',
        'py' : 'cuiluan',
        'spy' : 'cl'
    }, {
        'areaCode' : '230707',
        'areaName' : '新青区',
        'py' : 'xinqing',
        'spy' : 'xq'
    }, {
        'areaCode' : '230708',
        'areaName' : '美溪区',
        'py' : 'meixi',
        'spy' : 'mx'
    }, {
        'areaCode' : '230709',
        'areaName' : '金山屯区',
        'py' : 'jinshantun',
        'spy' : 'jst'
    }, {
        'areaCode' : '230710',
        'areaName' : '五营区',
        'py' : 'wuying',
        'spy' : 'wy'
    }, {
    'areaCode' : '230711',
    'areaName' : '乌马河区',
    'py' : 'wumahe',
    'spy' : 'wmh'
}, {
    'areaCode' : '230712',
    'areaName' : '汤旺河区',
    'py' : 'tangwanghe',
    'spy' : 'twh'
}, {
    'areaCode' : '230713',
    'areaName' : '带岭区',
    'py' : 'dailing',
    'spy' : 'dl'
}, {
    'areaCode' : '230714',
    'areaName' : '乌伊岭区',
    'py' : 'wuyiling',
    'spy' : 'wyl'
}, {
    'areaCode' : '230715',
    'areaName' : '红星区',
    'py' : 'hongxing',
    'spy' : 'hx'
}, {
    'areaCode' : '230716',
    'areaName' : '上甘岭区',
    'py' : 'shangganling',
    'spy' : 'sgl'
}, {
    'areaCode' : '230722',
    'areaName' : '嘉荫县',
    'py' : 'jiayin',
    'spy' : 'jy'
}, {
    'areaCode' : '230781',
    'areaName' : '铁力市',
    'py' : 'tieli',
    'spy' : ''
}]
    }, {
        'areaCode' : '230800',
        'areaName' : '佳木斯市',
        'py' : 'jiamusi',
        'spy' : 'jms',
        'datas' : [{
            'areaCode' : '230802',
            'areaName' : '永红区',
            'py' : 'yonghong',
            'spy' : 'yh'
        }, {
        'areaCode' : '230803',
        'areaName' : '向阳区',
        'py' : 'xiangyang',
        'spy' : 'xy'
    }, {
        'areaCode' : '230804',
        'areaName' : '前进区',
        'py' : 'qianjin',
        'spy' : 'qj'
    }, {
        'areaCode' : '230805',
        'areaName' : '东风区',
        'py' : 'dongfeng',
        'spy' : 'df'
    }, {
        'areaCode' : '230811',
        'areaName' : '郊区',
        'py' : 'jiaoqu',
        'spy' : 'jq'
    }, {
        'areaCode' : '230822',
        'areaName' : '桦南县',
        'py' : 'huanan',
        'spy' : 'hn'
    }, {
        'areaCode' : '230826',
        'areaName' : '桦川县',
        'py' : 'huachuan',
        'spy' : 'hc'
    }, {
        'areaCode' : '230828',
        'areaName' : '汤原县',
        'py' : 'tangyuan',
        'spy' : 'ty'
    }, {
        'areaCode' : '230833',
        'areaName' : '抚远县',
        'py' : 'fuyuan',
        'spy' : 'fy'
    }, {
    'areaCode' : '230881',
    'areaName' : '同江市',
    'py' : 'tongjiang',
    'spy' : 'tj'
}, {
    'areaCode' : '230882',
    'areaName' : '富锦市',
    'py' : 'fujin',
    'spy' : 'fj'
}]
    }, {
        'areaCode' : '230900',
        'areaName' : '七台河市',
        'py' : 'qitaihe',
        'spy' : 'qth',
        'datas' : [{
        'areaCode' : '230902',
        'areaName' : '新兴区',
        'py' : 'xinxing',
        'spy' : 'xx'
    }, {
        'areaCode' : '230903',
        'areaName' : '桃山区',
        'py' : 'taoshan',
        'spy' : 'ts'
    }, {
        'areaCode' : '230904',
        'areaName' : '茄子河区',
        'py' : 'qiezihe',
        'spy' : 'qzh'
    }, {
        'areaCode' : '230921',
        'areaName' : '勃利县',
        'py' : 'boli',
        'spy' : 'bl'
    }]
    }, {
    'areaCode' : '231000',
    'areaName' : '牡丹江市',
    'py' : 'mudanjiang',
    'spy' : 'mdj',
    'datas' : [{
        'areaCode' : '231002',
        'areaName' : '东安区',
        'py' : 'dongan',
        'spy' : 'da'
    }, {
        'areaCode' : '231003',
        'areaName' : '阳明区',
        'py' : 'yangming',
        'spy' : 'ym'
    }, {
        'areaCode' : '231004',
        'areaName' : '爱民区',
        'py' : 'aimin',
        'spy' : 'am'
    }, {
        'areaCode' : '231005',
        'areaName' : '西安区',
        'py' : 'xian',
        'spy' : 'xa'
    }, {
        'areaCode' : '231024',
        'areaName' : '东宁县',
        'py' : 'dongning',
        'spy' : 'dn'
    }, {
        'areaCode' : '231025',
        'areaName' : '林口县',
        'py' : 'linkou',
        'spy' : 'lk'
    }, {
        'areaCode' : '231081',
        'areaName' : '绥芬河市',
        'py' : 'suifenhe',
        'spy' : 'sfh'
    }, {
        'areaCode' : '231083',
        'areaName' : '海林市',
        'py' : 'hailin',
        'spy' : 'hl'
    }, {
        'areaCode' : '231084',
        'areaName' : '宁安市',
        'py' : 'ningan',
        'spy' : 'na'
    }, {
    'areaCode' : '231085',
    'areaName' : '穆棱市',
    'py' : 'muling',
    'spy' : 'ml'
}]
}, {
    'areaCode' : '231100',
    'areaName' : '黑河市',
    'py' : 'heihe',
    'spy' : 'hh',
    'datas' : [{
        'areaCode' : '231102',
        'areaName' : '爱辉区',
        'py' : 'aihui',
        'spy' : 'ah'
    }, {
        'areaCode' : '231121',
        'areaName' : '嫩江县',
        'py' : 'nenjiang',
        'spy' : 'nj'
    }, {
        'areaCode' : '231123',
        'areaName' : '逊克县',
        'py' : 'xunke',
        'spy' : 'xk'
    }, {
        'areaCode' : '231124',
        'areaName' : '孙吴县',
        'py' : 'sunwu',
        'spy' : 'sw'
    }, {
        'areaCode' : '231181',
        'areaName' : '北安市',
        'py' : 'beian',
        'spy' : 'ba'
    }, {
        'areaCode' : '231182',
        'areaName' : '五大连池市',
        'py' : 'wudalianchi',
        'spy' : 'wdlc'
    }]
}, {
    'areaCode' : '231200',
    'areaName' : '绥化市',
    'py' : 'suihua',
    'spy' : 'sh',
    'datas' : [{
        'areaCode' : '231202',
        'areaName' : '北林区',
        'py' : 'beilin',
        'spy' : 'bl'
    }, {
        'areaCode' : '231221',
        'areaName' : '望奎县',
        'py' : 'wangkui',
        'spy' : 'wk'
    }, {
        'areaCode' : '231222',
        'areaName' : '兰西县',
        'py' : 'lanxi',
        'spy' : 'lx'
    }, {
        'areaCode' : '231223',
        'areaName' : '青冈县',
        'py' : 'qinggang',
        'spy' : 'qg'
    }, {
        'areaCode' : '231224',
        'areaName' : '庆安县',
        'py' : 'qingan',
        'spy' : 'qa'
    }, {
        'areaCode' : '231225',
        'areaName' : '明水县',
        'py' : 'mingshui',
        'spy' : 'ms'
    }, {
        'areaCode' : '231226',
        'areaName' : '绥棱县',
        'py' : 'suiling',
        'spy' : 'sl'
    }, {
        'areaCode' : '231281',
        'areaName' : '安达市',
        'py' : 'anda',
        'spy' : 'ad'
    }, {
        'areaCode' : '231282',
        'areaName' : '肇东市',
        'py' : 'zhaodong',
        'spy' : 'zd'
    }, {
    'areaCode' : '231283',
    'areaName' : '海伦市',
    'py' : 'hailun',
    'spy' : 'hl'
}]
}, {
    'areaCode' : '232700',
    'areaName' : '大兴安岭地区',
    'py' : 'daxinganling',
    'spy' : 'dxal',
    'datas' : [{
        'areaCode' : '232721',
        'areaName' : '呼玛县',
        'py' : 'huma',
        'spy' : 'hm'
    }, {
        'areaCode' : '232722',
        'areaName' : '塔河县',
        'py' : 'tahe',
        'spy' : 'th'
    }, {
        'areaCode' : '232723',
        'areaName' : '漠河县',
        'py' : 'mohe',
        'spy' : 'mh'
    }]
}]
}, {
    'areaCode' : '310000',
    'areaName' : '上海市',
    'py' : 'shanghai',
    'spy' : 'sh',
    'datas' : [{
        'areaCode' : '310100',
        'areaName' : '上海市',
        'py' : 'shanghai',
        'spy' : 'sh',
        'datas' : [{
            'areaCode' : '310101',
            'areaName' : '黄浦区',
            'py' : 'hupu',
            'spy' : 'hp'
        }, {
            'areaCode' : '310103',
            'areaName' : '卢湾区',
            'py' : 'luwan',
            'spy' : 'lw'
        }, {
            'areaCode' : '310104',
            'areaName' : '徐汇区',
            'py' : 'xuhui',
            'spy' : 'xh'
        }, {
            'areaCode' : '310105',
            'areaName' : '长宁区',
            'py' : 'changning',
            'spy' : 'cn'
        }, {
            'areaCode' : '310106',
            'areaName' : '静安区',
            'py' : 'jingan',
            'spy' : 'ja'
        }, {
            'areaCode' : '310107',
            'areaName' : '普陀区',
            'py' : 'putuo',
            'spy' : 'pt'
        }, {
            'areaCode' : '310108',
            'areaName' : '闸北区',
            'py' : 'zhabei',
            'spy' : 'zb'
        }, {
            'areaCode' : '310109',
            'areaName' : '虹口区',
            'py' : 'hongkou',
            'spy' : 'hk'
        }, {
        'areaCode' : '310110',
        'areaName' : '杨浦区',
        'py' : 'yangpu',
        'spy' : 'yp'
    }, {
    'areaCode' : '310112',
    'areaName' : '闵行区',
    'py' : 'minxing',
    'spy' : 'mx'
}, {
    'areaCode' : '310113',
    'areaName' : '宝山区',
    'py' : 'baoshan',
    'spy' : 'bs'
}, {
    'areaCode' : '310114',
    'areaName' : '嘉定区',
    'py' : 'jiading',
    'spy' : 'jd'
}, {
    'areaCode' : '310115',
    'areaName' : '浦东新区',
    'py' : 'pudongxinqu',
    'spy' : 'pdx'
}, {
    'areaCode' : '310116',
    'areaName' : '金山区',
    'py' : 'jinshan',
    'spy' : 'js'
}, {
    'areaCode' : '310117',
    'areaName' : '松江区',
    'py' : 'songjiang',
    'spy' : 'sj'
}, {
    'areaCode' : '310118',
    'areaName' : '青浦区',
    'py' : 'qingpu',
    'spy' : 'qp'
}, {
    'areaCode' : '310119',
    'areaName' : '南汇区',
    'py' : 'nanhui',
    'spy' : 'nh'
}, {
    'areaCode' : '310120',
    'areaName' : '奉贤区',
    'py' : 'fengxian',
    'spy' : 'fx'
}, {
    'areaCode' : '310230',
    'areaName' : '崇明县',
    'py' : 'chongming',
    'spy' : 'cm'
}]
    }]
}, {
    'areaCode' : '320000',
    'areaName' : '江苏省',
    'py' : 'jiangsu',
    'spy' : 'js',
    'datas' : [{
        'areaCode' : '320100',
        'areaName' : '南京市',
        'py' : 'nanjing',
        'spy' : 'nj',
        'datas' : [{
            'areaCode' : '320102',
            'areaName' : '玄武区',
            'py' : 'xuanwu',
            'spy' : 'xw'
        }, {
            'areaCode' : '320103',
            'areaName' : '白下区',
            'py' : 'baixia',
            'spy' : 'bx'
        }, {
            'areaCode' : '320104',
            'areaName' : '秦淮区',
            'py' : 'qinhuai',
            'spy' : 'qh'
        }, {
            'areaCode' : '320105',
            'areaName' : '建邺区',
            'py' : 'jianye',
            'spy' : 'jy'
        }, {
            'areaCode' : '320106',
            'areaName' : '鼓楼区',
            'py' : 'gulou',
            'spy' : 'gl'
        }, {
            'areaCode' : '320107',
            'areaName' : '下关区',
            'py' : 'xiaguan',
            'spy' : 'xg'
        }, {
            'areaCode' : '320111',
            'areaName' : '浦口区',
            'py' : 'pukou',
            'spy' : 'pk'
        }, {
            'areaCode' : '320113',
            'areaName' : '栖霞区',
            'py' : 'qixia',
            'spy' : 'qx'
        }, {
        'areaCode' : '320114',
        'areaName' : '雨花台区',
        'py' : 'yuhuatai',
        'spy' : 'yht'
    }, {
    'areaCode' : '320115',
    'areaName' : '江宁区',
    'py' : 'jiangning',
    'spy' : 'jn'
}, {
    'areaCode' : '320116',
    'areaName' : '六合区',
    'py' : 'liuhe',
    'spy' : 'lh'
}, {
    'areaCode' : '320124',
    'areaName' : '溧水县',
    'py' : 'lishui',
    'spy' : 'ls'
}, {
    'areaCode' : '320125',
    'areaName' : '高淳县',
    'py' : 'gaochun',
    'spy' : 'gc'
}]
    }, {
        'areaCode' : '320200',
        'areaName' : '无锡市',
        'py' : 'wuxi',
        'spy' : 'wx',
        'datas' : [{
            'areaCode' : '320202',
            'areaName' : '崇安区',
            'py' : 'chongan',
            'spy' : 'ca'
        }, {
            'areaCode' : '320203',
            'areaName' : '南长区',
            'py' : 'nanchang',
            'spy' : 'nc'
        }, {
            'areaCode' : '320204',
            'areaName' : '北塘区',
            'py' : 'beitang',
            'spy' : 'bt'
        }, {
            'areaCode' : '320205',
            'areaName' : '锡山区',
            'py' : 'xishan',
            'spy' : 'xs'
        }, {
            'areaCode' : '320206',
            'areaName' : '惠山区',
            'py' : 'huishan',
            'spy' : 'hs'
        }, {
            'areaCode' : '320211',
            'areaName' : '滨湖区',
            'py' : 'binhu',
            'spy' : 'bh'
        }, {
            'areaCode' : '320281',
            'areaName' : '江阴市',
            'py' : 'jiangyin',
            'spy' : 'jy'
        }, {
        'areaCode' : '320282',
        'areaName' : '宜兴市',
        'py' : 'yixing',
        'spy' : 'yx'
    }]
    }, {
        'areaCode' : '320300',
        'areaName' : '徐州市',
        'py' : 'xuzhou',
        'spy' : 'xz',
        'datas' : [{
            'areaCode' : '320302',
            'areaName' : '鼓楼区',
            'py' : 'gulou',
            'spy' : 'gl'
        }, {
            'areaCode' : '320303',
            'areaName' : '云龙区',
            'py' : 'yunlong',
            'spy' : 'yl'
        }, {
            'areaCode' : '320305',
            'areaName' : '贾汪区',
            'py' : 'jiawang',
            'spy' : 'jw'
        }, {
            'areaCode' : '320311',
            'areaName' : '泉山区',
            'py' : 'quanshan',
            'spy' : 'qs'
        }, {
            'areaCode' : '320312',
            'areaName' : '铜山区',
            'py' : 'tongshan',
            'spy' : 'ts'
        }, {
            'areaCode' : '320321',
            'areaName' : '丰县',
            'py' : 'fengxian',
            'spy' : 'fx'
        }, {
        'areaCode' : '320322',
        'areaName' : '沛县',
        'py' : 'peixian',
        'spy' : 'px'
    }, {
        'areaCode' : '320324',
        'areaName' : '睢宁县',
        'py' : 'suining',
        'spy' : 'sn'
    }, {
        'areaCode' : '320381',
        'areaName' : '新沂市',
        'py' : 'xinyi',
        'spy' : 'xy'
    }, {
    'areaCode' : '320382',
    'areaName' : '邳州市',
    'py' : 'pizhou',
    'spy' : 'pz'
}]
    }, {
        'areaCode' : '320400',
        'areaName' : '常州市',
        'py' : 'changzhou',
        'spy' : 'cz',
        'datas' : [{
            'areaCode' : '320402',
            'areaName' : '天宁区',
            'py' : 'tianning',
            'spy' : 'tn'
        }, {
            'areaCode' : '320404',
            'areaName' : '钟楼区',
            'py' : 'gulou',
            'spy' : 'gl'
        }, {
            'areaCode' : '320405',
            'areaName' : '戚墅堰区',
            'py' : 'qishuyan',
            'spy' : 'qsy'
        }, {
            'areaCode' : '320411',
            'areaName' : '新北区',
            'py' : 'xinbei',
            'spy' : 'xb'
        }, {
            'areaCode' : '320412',
            'areaName' : '武进区',
            'py' : 'wujin',
            'spy' : 'wj'
        }, {
        'areaCode' : '320481',
        'areaName' : '溧阳市',
        'py' : 'liyang',
        'spy' : 'ly'
    }, {
        'areaCode' : '320482',
        'areaName' : '金坛市',
        'py' : 'jintan',
        'spy' : 'jt'
    }]
    }, {
        'areaCode' : '320500',
        'areaName' : '苏州市',
        'py' : 'suzhou',
        'spy' : 'sz',
        'datas' : [{
            'areaCode' : '320502',
            'areaName' : '沧浪区',
            'py' : 'canglang',
            'spy' : 'cl'
        }, {
            'areaCode' : '320503',
            'areaName' : '平江区',
            'py' : 'pingjiang',
            'spy' : 'pj'
        }, {
            'areaCode' : '320504',
            'areaName' : '金阊区',
            'py' : 'jinchang',
            'spy' : 'jc'
        }, {
            'areaCode' : '320505',
            'areaName' : '虎丘区',
            'py' : 'huqiu',
            'spy' : 'hq'
        }, {
        'areaCode' : '320506',
        'areaName' : '吴中区',
        'py' : 'wuzhong',
        'spy' : 'wz'
    }, {
        'areaCode' : '320507',
        'areaName' : '相城区',
        'py' : 'xiangcheng',
        'spy' : 'xc'
    }, {
        'areaCode' : '320508',
        'areaName' : '工业园区',
        'py' : 'gongyeyuan',
        'spy' : 'gyy'
    }, {
        'areaCode' : '320509',
        'areaName' : '姑苏区',
        'py' : 'gusu',
        'spy' : 'gs'
    }, {
        'areaCode' : '320581',
        'areaName' : '常熟市',
        'py' : 'changshu',
        'spy' : 'cs'
    }, {
    'areaCode' : '320582',
    'areaName' : '张家港市',
    'py' : 'zhangjiagang',
    'spy' : 'zjg'
}, {
    'areaCode' : '320583',
    'areaName' : '昆山市',
    'py' : 'kunshan',
    'spy' : 'ks'
}, {
    'areaCode' : '320584',
    'areaName' : '吴江市',
    'py' : 'wujiang',
    'spy' : 'wj'
}, {
    'areaCode' : '320585',
    'areaName' : '太仓市',
    'py' : 'taicang',
    'spy' : 'tc'
}]
    }, {
        'areaCode' : '320600',
        'areaName' : '南通市',
        'py' : 'nantong',
        'spy' : 'nt',
        'datas' : [{
            'areaCode' : '320602',
            'areaName' : '崇川区',
            'py' : 'chongchuan',
            'spy' : 'cc'
        }, {
            'areaCode' : '320611',
            'areaName' : '港闸区',
            'py' : 'gangzha',
            'spy' : 'gz'
        }, {
            'areaCode' : '320621',
            'areaName' : '海安县',
            'py' : 'haian',
            'spy' : 'ha'
        }, {
        'areaCode' : '320623',
        'areaName' : '如东县',
        'py' : 'rudong',
        'spy' : 'rd'
    }, {
        'areaCode' : '320681',
        'areaName' : '启东市',
        'py' : 'qidong',
        'spy' : 'qd'
    }, {
        'areaCode' : '320682',
        'areaName' : '如皋市',
        'py' : 'rugao',
        'spy' : 'rg'
    }, {
        'areaCode' : '320683',
        'areaName' : '通州区',
        'py' : 'tongzhou',
        'spy' : 'tz'
    }, {
        'areaCode' : '320684',
        'areaName' : '海门市',
        'py' : 'haimen',
        'spy' : 'hm'
    }]
    }, {
        'areaCode' : '320700',
        'areaName' : '连云港市',
        'py' : 'lianyungang',
        'spy' : 'lyg',
        'datas' : [{
            'areaCode' : '320703',
            'areaName' : '连云区',
            'py' : 'lianyun',
            'spy' : 'ly'
        }, {
            'areaCode' : '320705',
            'areaName' : '新浦区',
            'py' : 'xinpu',
            'spy' : 'xp'
        }, {
        'areaCode' : '320706',
        'areaName' : '海州区',
        'py' : 'haizhou',
        'spy' : 'hz'
    }, {
        'areaCode' : '320721',
        'areaName' : '赣榆县',
        'py' : 'ganyu',
        'spy' : 'gy'
    }, {
        'areaCode' : '320722',
        'areaName' : '东海县',
        'py' : 'donghai',
        'spy' : 'dh'
    }, {
        'areaCode' : '320723',
        'areaName' : '灌云县',
        'py' : 'guanyun',
        'spy' : 'gy'
    }, {
        'areaCode' : '320724',
        'areaName' : '灌南县',
        'py' : 'guannan',
        'spy' : 'gn'
    }]
    }, {
        'areaCode' : '320800',
        'areaName' : '淮安市',
        'py' : 'huaian',
        'spy' : 'ha',
        'datas' : [{
            'areaCode' : '320802',
            'areaName' : '清河区',
            'py' : 'qinghe',
            'spy' : 'qh'
        }, {
        'areaCode' : '320803',
        'areaName' : '楚州区',
        'py' : 'chuzhou',
        'spy' : 'cz'
    }, {
        'areaCode' : '320804',
        'areaName' : '淮阴区',
        'py' : 'huaiyin',
        'spy' : 'hy'
    }, {
        'areaCode' : '320811',
        'areaName' : '清浦区',
        'py' : 'qingpu',
        'spy' : 'qp'
    }, {
        'areaCode' : '320826',
        'areaName' : '涟水县',
        'py' : 'lianshui',
        'spy' : 'ls'
    }, {
        'areaCode' : '320829',
        'areaName' : '洪泽县',
        'py' : 'hongze',
        'spy' : 'hz'
    }, {
        'areaCode' : '320830',
        'areaName' : '盱眙县',
        'py' : 'xuchi',
        'spy' : 'xc'
    }, {
        'areaCode' : '320831',
        'areaName' : '金湖县',
        'py' : 'jinhu',
        'spy' : 'jh'
    }]
    }, {
        'areaCode' : '320900',
        'areaName' : '盐城市',
        'py' : 'yancheng',
        'spy' : 'yc',
        'datas' : [{
        'areaCode' : '320902',
        'areaName' : '亭湖区',
        'py' : 'tinghu',
        'spy' : 'th'
    }, {
        'areaCode' : '320903',
        'areaName' : '盐都区',
        'py' : 'yandu',
        'spy' : 'yd'
    }, {
        'areaCode' : '320921',
        'areaName' : '响水县',
        'py' : 'xiangshui',
        'spy' : 'xs'
    }, {
        'areaCode' : '320922',
        'areaName' : '滨海县',
        'py' : 'binhai',
        'spy' : 'bh'
    }, {
        'areaCode' : '320923',
        'areaName' : '阜宁县',
        'py' : 'funing',
        'spy' : 'fn'
    }, {
        'areaCode' : '320924',
        'areaName' : '射阳县',
        'py' : 'sheyang',
        'spy' : 'sy'
    }, {
        'areaCode' : '320925',
        'areaName' : '建湖县',
        'py' : 'jianhu',
        'spy' : 'jh'
    }, {
        'areaCode' : '320981',
        'areaName' : '东台市',
        'py' : 'dongtai',
        'spy' : 'dt'
    }, {
        'areaCode' : '320982',
        'areaName' : '大丰市',
        'py' : 'dafeng',
        'spy' : 'df'
    }]
    }, {
    'areaCode' : '321000',
    'areaName' : '扬州市',
    'py' : 'yangzhou',
    'spy' : 'yz',
    'datas' : [{
        'areaCode' : '321002',
        'areaName' : '广陵区',
        'py' : 'guangling',
        'spy' : 'gl'
    }, {
        'areaCode' : '321003',
        'areaName' : '邗江区',
        'py' : 'hanjiang',
        'spy' : 'hj'
    }, {
        'areaCode' : '321012',
        'areaName' : '江都区',
        'py' : 'jiangdu',
        'spy' : 'jd'
    }, {
        'areaCode' : '321023',
        'areaName' : '宝应县',
        'py' : 'baoying',
        'spy' : 'by'
    }, {
        'areaCode' : '321081',
        'areaName' : '仪征市',
        'py' : 'yizheng',
        'spy' : 'yz'
    }, {
        'areaCode' : '321084',
        'areaName' : '高邮市',
        'py' : 'gaoyou',
        'spy' : 'gy'
    }]
}, {
    'areaCode' : '321100',
    'areaName' : '镇江市',
    'py' : 'zhenjiang',
    'spy' : 'zj',
    'datas' : [{
        'areaCode' : '321102',
        'areaName' : '京口区',
        'py' : 'jingkou',
        'spy' : 'jk'
    }, {
        'areaCode' : '321111',
        'areaName' : '润州区',
        'py' : 'runzhou',
        'spy' : 'rz'
    }, {
        'areaCode' : '321112',
        'areaName' : '丹徒区',
        'py' : 'dantu',
        'spy' : 'dt'
    }, {
        'areaCode' : '321181',
        'areaName' : '丹阳市',
        'py' : 'danyang',
        'spy' : 'dy'
    }, {
        'areaCode' : '321182',
        'areaName' : '扬中市',
        'py' : 'yangzhong',
        'spy' : 'yz'
    }, {
        'areaCode' : '321183',
        'areaName' : '句容市',
        'py' : 'jurong',
        'spy' : 'jr'
    }]
}, {
    'areaCode' : '321200',
    'areaName' : '泰州市',
    'py' : 'taizhou',
    'spy' : 'tz',
    'datas' : [{
        'areaCode' : '321202',
        'areaName' : '海陵区',
        'py' : 'hailing',
        'spy' : 'hl'
    }, {
        'areaCode' : '321203',
        'areaName' : '高港区',
        'py' : 'gaogang',
        'spy' : 'gg'
    }, {
        'areaCode' : '321281',
        'areaName' : '兴化市',
        'py' : 'xinghua',
        'spy' : 'xh'
    }, {
        'areaCode' : '321282',
        'areaName' : '靖江市',
        'py' : 'jingjiang',
        'spy' : 'jj'
    }, {
        'areaCode' : '321283',
        'areaName' : '泰兴市',
        'py' : 'taixing',
        'spy' : 'tx'
    }, {
        'areaCode' : '321284',
        'areaName' : '姜堰市',
        'py' : 'jiangyan',
        'spy' : 'jy'
    }]
}, {
    'areaCode' : '321300',
    'areaName' : '宿迁市',
    'py' : 'suqian',
    'spy' : 'sq',
    'datas' : [{
        'areaCode' : '321302',
        'areaName' : '宿城区',
        'py' : 'sucheng',
        'spy' : 'sc'
    }, {
        'areaCode' : '321311',
        'areaName' : '宿豫区',
        'py' : 'suyu',
        'spy' : 'sy'
    }, {
        'areaCode' : '321322',
        'areaName' : '沭阳县',
        'py' : 'shuyang',
        'spy' : 'sy'
    }, {
        'areaCode' : '321323',
        'areaName' : '泗阳县',
        'py' : 'siyang',
        'spy' : 'sy'
    }, {
        'areaCode' : '321324',
        'areaName' : '泗洪县',
        'py' : 'sihong',
        'spy' : 'sh'
    }]
}]
}, {
    'areaCode' : '330000',
    'areaName' : '浙江省',
    'py' : 'zhejiang',
    'spy' : 'zj',
    'datas' : [{
        'areaCode' : '330100',
        'areaName' : '杭州市',
        'py' : 'hangzhou',
        'spy' : 'hz',
        'datas' : [{
            'areaCode' : '330102',
            'areaName' : '上城区',
            'py' : 'shangcheng',
            'spy' : 'sc'
        }, {
            'areaCode' : '330103',
            'areaName' : '下城区',
            'py' : 'xiacheng',
            'spy' : 'xc'
        }, {
            'areaCode' : '330104',
            'areaName' : '江干区',
            'py' : 'jianggan',
            'spy' : 'jg'
        }, {
            'areaCode' : '330105',
            'areaName' : '拱墅区',
            'py' : 'gongshu',
            'spy' : 'gs'
        }, {
            'areaCode' : '330106',
            'areaName' : '西湖区',
            'py' : 'xihu',
            'spy' : 'xh'
        }, {
            'areaCode' : '330108',
            'areaName' : '滨江区',
            'py' : 'binjiang',
            'spy' : 'bj'
        }, {
            'areaCode' : '330109',
            'areaName' : '萧山区',
            'py' : 'xiaoshan',
            'spy' : 'xs'
        }, {
            'areaCode' : '330110',
            'areaName' : '余杭区',
            'py' : 'yuhang',
            'spy' : 'yh'
        }, {
        'areaCode' : '330122',
        'areaName' : '桐庐县',
        'py' : 'tonglu',
        'spy' : 'tl'
    }, {
    'areaCode' : '330127',
    'areaName' : '淳安县',
    'py' : 'chunan',
    'spy' : 'ca'
}, {
    'areaCode' : '330182',
    'areaName' : '建德市',
    'py' : 'jiande',
    'spy' : 'jd'
}, {
    'areaCode' : '330183',
    'areaName' : '富阳市',
    'py' : 'fuyang',
    'spy' : 'fy'
}, {
    'areaCode' : '330185',
    'areaName' : '临安市',
    'py' : 'linan',
    'spy' : 'la'
}]
    }, {
        'areaCode' : '330200',
        'areaName' : '宁波市',
        'py' : 'ningbo',
        'spy' : 'nb',
        'datas' : [{
            'areaCode' : '330203',
            'areaName' : '海曙区',
            'py' : 'haishu',
            'spy' : 'hs'
        }, {
            'areaCode' : '330204',
            'areaName' : '江东区',
            'py' : 'jiangdong',
            'spy' : 'jd'
        }, {
            'areaCode' : '330205',
            'areaName' : '江北区',
            'py' : 'jiangbei',
            'spy' : 'jb'
        }, {
            'areaCode' : '330206',
            'areaName' : '北仑区',
            'py' : 'beilun',
            'spy' : 'bl'
        }, {
            'areaCode' : '330211',
            'areaName' : '镇海区',
            'py' : 'zhenhai',
            'spy' : 'zh'
        }, {
            'areaCode' : '330212',
            'areaName' : '鄞州区',
            'py' : 'yinzhou',
            'spy' : 'yz'
        }, {
            'areaCode' : '330225',
            'areaName' : '象山县',
            'py' : 'xiangshan',
            'spy' : 'xs'
        }, {
        'areaCode' : '330226',
        'areaName' : '宁海县',
        'py' : 'ninghai',
        'spy' : 'nh'
    }, {
        'areaCode' : '330281',
        'areaName' : '余姚市',
        'py' : 'yuyao',
        'spy' : 'yy'
    }, {
    'areaCode' : '330282',
    'areaName' : '慈溪市',
    'py' : 'cixi',
    'spy' : 'cx'
}, {
    'areaCode' : '330283',
    'areaName' : '奉化市',
    'py' : 'fenghua',
    'spy' : 'fh'
}]
    }, {
        'areaCode' : '330300',
        'areaName' : '温州市',
        'py' : 'wenzhou',
        'spy' : 'wz',
        'datas' : [{
            'areaCode' : '330302',
            'areaName' : '鹿城区',
            'py' : 'lucheng',
            'spy' : 'lc'
        }, {
            'areaCode' : '330303',
            'areaName' : '龙湾区',
            'py' : 'longwan',
            'spy' : 'lw'
        }, {
            'areaCode' : '330304',
            'areaName' : '瓯海区',
            'py' : 'ouhai',
            'spy' : 'oh'
        }, {
            'areaCode' : '330322',
            'areaName' : '洞头县',
            'py' : 'dongtou',
            'spy' : 'dt'
        }, {
            'areaCode' : '330324',
            'areaName' : '永嘉县',
            'py' : 'yongjia',
            'spy' : 'yj'
        }, {
            'areaCode' : '330326',
            'areaName' : '平阳县',
            'py' : 'pingyang',
            'spy' : 'py'
        }, {
        'areaCode' : '330327',
        'areaName' : '苍南县',
        'py' : 'cangnan',
        'spy' : 'cn'
    }, {
        'areaCode' : '330328',
        'areaName' : '文成县',
        'py' : 'wencheng',
        'spy' : 'wc'
    }, {
        'areaCode' : '330329',
        'areaName' : '泰顺县',
        'py' : 'taishun',
        'spy' : 'ts'
    }, {
    'areaCode' : '330381',
    'areaName' : '瑞安市',
    'py' : 'ruian',
    'spy' : 'ra'
}, {
    'areaCode' : '330382',
    'areaName' : '乐清市',
    'py' : 'leqing',
    'spy' : 'lq'
}]
    }, {
        'areaCode' : '330400',
        'areaName' : '嘉兴市',
        'py' : 'jiaxing',
        'spy' : 'jx',
        'datas' : [{
            'areaCode' : '330402',
            'areaName' : '南湖区',
            'py' : 'nanhu',
            'spy' : 'nh'
        }, {
            'areaCode' : '330411',
            'areaName' : '秀洲区',
            'py' : 'xiuzhou',
            'spy' : 'xz'
        }, {
            'areaCode' : '330421',
            'areaName' : '嘉善县',
            'py' : 'jiashan',
            'spy' : 'js'
        }, {
            'areaCode' : '330424',
            'areaName' : '海盐县',
            'py' : 'haiyan',
            'spy' : 'hy'
        }, {
            'areaCode' : '330481',
            'areaName' : '海宁市',
            'py' : 'haining',
            'spy' : 'hn'
        }, {
        'areaCode' : '330482',
        'areaName' : '平湖市',
        'py' : 'pinghu',
        'spy' : 'ph'
    }, {
        'areaCode' : '330483',
        'areaName' : '桐乡市',
        'py' : 'tongxiang',
        'spy' : 'tx'
    }]
    }, {
        'areaCode' : '330500',
        'areaName' : '湖州市',
        'py' : 'huzhou',
        'spy' : 'hz',
        'datas' : [{
            'areaCode' : '330502',
            'areaName' : '吴兴区',
            'py' : 'wuxing',
            'spy' : 'wx'
        }, {
            'areaCode' : '330503',
            'areaName' : '南浔区',
            'py' : 'nanxun',
            'spy' : 'nx'
        }, {
            'areaCode' : '330521',
            'areaName' : '德清县',
            'py' : 'deqing',
            'spy' : 'dq'
        }, {
            'areaCode' : '330522',
            'areaName' : '长兴县',
            'py' : 'changxing',
            'spy' : 'cx'
        }, {
        'areaCode' : '330523',
        'areaName' : '安吉县',
        'py' : 'anji',
        'spy' : 'aj'
    }]
    }, {
        'areaCode' : '330600',
        'areaName' : '绍兴市',
        'py' : 'shaoxing',
        'spy' : 'sx',
        'datas' : [{
            'areaCode' : '330602',
            'areaName' : '越城区',
            'py' : 'yuecheng',
            'spy' : 'yc'
        }, {
            'areaCode' : '330621',
            'areaName' : '绍兴县',
            'py' : 'shaoxing',
            'spy' : 'sx'
        }, {
            'areaCode' : '330624',
            'areaName' : '新昌县',
            'py' : 'xinchang',
            'spy' : 'xc'
        }, {
        'areaCode' : '330681',
        'areaName' : '诸暨市',
        'py' : 'zhuji',
        'spy' : 'zj'
    }, {
        'areaCode' : '330682',
        'areaName' : '上虞市',
        'py' : 'shangyu',
        'spy' : 'sy'
    }, {
        'areaCode' : '330683',
        'areaName' : '嵊州市',
        'py' : 'shengzhou',
        'spy' : 'sz'
    }]
    }, {
        'areaCode' : '330700',
        'areaName' : '金华市',
        'py' : 'jinhua',
        'spy' : 'jh',
        'datas' : [{
            'areaCode' : '330702',
            'areaName' : '婺城区',
            'py' : 'wucheng',
            'spy' : 'wc'
        }, {
            'areaCode' : '330703',
            'areaName' : '金东区',
            'py' : 'jindong',
            'spy' : 'jd'
        }, {
        'areaCode' : '330723',
        'areaName' : '武义县',
        'py' : 'wuyi',
        'spy' : 'wy'
    }, {
        'areaCode' : '330726',
        'areaName' : '浦江县',
        'py' : 'pujiang',
        'spy' : 'pj'
    }, {
        'areaCode' : '330727',
        'areaName' : '磐安县',
        'py' : 'panan',
        'spy' : 'pa'
    }, {
        'areaCode' : '330781',
        'areaName' : '兰溪市',
        'py' : 'lanxi',
        'spy' : 'lx'
    }, {
        'areaCode' : '330782',
        'areaName' : '义乌市',
        'py' : 'yiwu',
        'spy' : 'yw'
    }, {
        'areaCode' : '330783',
        'areaName' : '东阳市',
        'py' : 'dongyang',
        'spy' : 'dy'
    }, {
        'areaCode' : '330784',
        'areaName' : '永康市',
        'py' : 'yongkang',
        'spy' : 'yk'
    }]
    }, {
        'areaCode' : '330800',
        'areaName' : '衢州市',
        'py' : 'quzhou',
        'spy' : 'qz',
        'datas' : [{
            'areaCode' : '330802',
            'areaName' : '柯城区',
            'py' : 'kecheng',
            'spy' : 'kc'
        }, {
        'areaCode' : '330803',
        'areaName' : '衢江区',
        'py' : 'qujiang',
        'spy' : 'qj'
    }, {
        'areaCode' : '330822',
        'areaName' : '常山县',
        'py' : 'changshan',
        'spy' : 'cs'
    }, {
        'areaCode' : '330824',
        'areaName' : '开化县',
        'py' : 'kaihua',
        'spy' : 'kh'
    }, {
        'areaCode' : '330825',
        'areaName' : '龙游县',
        'py' : 'longyou',
        'spy' : 'ly'
    }, {
        'areaCode' : '330881',
        'areaName' : '江山市',
        'py' : 'jiangshan',
        'spy' : 'js'
    }]
    }, {
        'areaCode' : '330900',
        'areaName' : '舟山市',
        'py' : 'zhoushan',
        'spy' : 'zs',
        'datas' : [{
        'areaCode' : '330902',
        'areaName' : '定海区',
        'py' : 'dinghai',
        'spy' : 'dh'
    }, {
        'areaCode' : '330903',
        'areaName' : '普陀区',
        'py' : 'shantuo',
        'spy' : 'st'
    }, {
        'areaCode' : '330921',
        'areaName' : '岱山县',
        'py' : 'daishan',
        'spy' : 'ds'
    }, {
        'areaCode' : '330922',
        'areaName' : '嵊泗县',
        'py' : 'shengsi',
        'spy' : 'ss'
    }]
    }, {
    'areaCode' : '331000',
    'areaName' : '台州市',
    'py' : 'taizhou',
    'spy' : 'tz',
    'datas' : [{
        'areaCode' : '331002',
        'areaName' : '椒江区',
        'py' : 'jiaojiang',
        'spy' : 'jj'
    }, {
        'areaCode' : '331003',
        'areaName' : '黄岩区',
        'py' : 'huangyan',
        'spy' : 'hy'
    }, {
        'areaCode' : '331004',
        'areaName' : '路桥区',
        'py' : 'luqiao',
        'spy' : 'lq'
    }, {
        'areaCode' : '331021',
        'areaName' : '玉环县',
        'py' : 'yuhuan',
        'spy' : 'yh'
    }, {
        'areaCode' : '331022',
        'areaName' : '三门县',
        'py' : 'sanmen',
        'spy' : 'sm'
    }, {
        'areaCode' : '331023',
        'areaName' : '天台县',
        'py' : 'tiantai',
        'spy' : 'tt'
    }, {
        'areaCode' : '331024',
        'areaName' : '仙居县',
        'py' : 'xianju',
        'spy' : 'xj'
    }, {
        'areaCode' : '331081',
        'areaName' : '温岭市',
        'py' : 'wenling',
        'spy' : 'wl'
    }, {
        'areaCode' : '331082',
        'areaName' : '临海市',
        'py' : 'linhai',
        'spy' : 'lh'
    }]
}, {
    'areaCode' : '331100',
    'areaName' : '丽水市',
    'py' : 'lishui',
    'spy' : 'ls',
    'datas' : [{
        'areaCode' : '331102',
        'areaName' : '莲都区',
        'py' : 'liandu',
        'spy' : 'ld'
    }, {
        'areaCode' : '331121',
        'areaName' : '青田县',
        'py' : 'qingtian',
        'spy' : 'qt'
    }, {
        'areaCode' : '331122',
        'areaName' : '缙云县',
        'py' : 'jinyun',
        'spy' : 'jy'
    }, {
        'areaCode' : '331123',
        'areaName' : '遂昌县',
        'py' : 'suichang',
        'spy' : 'sc'
    }, {
        'areaCode' : '331124',
        'areaName' : '松阳县',
        'py' : 'songyang',
        'spy' : 'sy'
    }, {
        'areaCode' : '331125',
        'areaName' : '云和县',
        'py' : 'yunhe',
        'spy' : 'yh'
    }, {
        'areaCode' : '331126',
        'areaName' : '庆元县',
        'py' : 'qingyuan',
        'spy' : 'qy'
    }, {
        'areaCode' : '331127',
        'areaName' : '景宁畲族自治县',
        'py' : 'jingningshezuzizhixian',
        'spy' : 'jnszzzx'
    }, {
        'areaCode' : '331181',
        'areaName' : '龙泉市',
        'py' : 'longquan',
        'spy' : 'lq'
    }]
}]
}, {
    'areaCode' : '340000',
    'areaName' : '安徽省',
    'py' : 'anhui',
    'spy' : 'ah',
    'datas' : [{
        'areaCode' : '340100',
        'areaName' : '合肥市',
        'py' : 'hefei',
        'spy' : 'hf',
        'datas' : [{
            'areaCode' : '340102',
            'areaName' : '瑶海区',
            'py' : 'yaohai',
            'spy' : 'yh'
        }, {
            'areaCode' : '340103',
            'areaName' : '庐阳区',
            'py' : 'luyang',
            'spy' : 'ly'
        }, {
            'areaCode' : '340104',
            'areaName' : '蜀山区',
            'py' : 'shushan',
            'spy' : 'ss'
        }, {
            'areaCode' : '340111',
            'areaName' : '包河区',
            'py' : 'baohe',
            'spy' : 'bh'
        }, {
            'areaCode' : '340121',
            'areaName' : '长丰县',
            'py' : 'changfeng',
            'spy' : 'cf'
        }, {
            'areaCode' : '340122',
            'areaName' : '肥东县',
            'py' : 'feidong',
            'spy' : 'fd'
        }, {
            'areaCode' : '340123',
            'areaName' : '肥西县',
            'py' : 'feixi',
            'spy' : 'fx'
        }, {
            'areaCode' : '341400',
            'areaName' : '巢湖市',
            'py' : 'chaohu',
            'spy' : 'ch'
        }, {
        'areaCode' : '341421',
        'areaName' : '庐江县',
        'py' : 'lujiang',
        'spy' : 'lj'
    }]
    }, {
        'areaCode' : '340200',
        'areaName' : '芜湖市',
        'py' : 'wuhu',
        'spy' : 'wh',
        'datas' : [{
            'areaCode' : '340202',
            'areaName' : '镜湖区',
            'py' : 'jinghu',
            'spy' : 'jh'
        }, {
            'areaCode' : '340203',
            'areaName' : '弋江区',
            'py' : 'yijiang',
            'spy' : 'yj'
        }, {
            'areaCode' : '340204',
            'areaName' : '新芜区',
            'py' : 'xinwu',
            'spy' : 'xw'
        }, {
            'areaCode' : '340207',
            'areaName' : '鸠江区',
            'py' : 'jiujiang',
            'spy' : 'jj'
        }, {
            'areaCode' : '340208',
            'areaName' : '三山区',
            'py' : 'sanshan',
            'spy' : 'ss'
        }, {
            'areaCode' : '340221',
            'areaName' : '芜湖县',
            'py' : 'wuhu',
            'spy' : 'wh'
        }, {
            'areaCode' : '340222',
            'areaName' : '繁昌县',
            'py' : 'fanchang',
            'spy' : 'fc'
        }, {
        'areaCode' : '340223',
        'areaName' : '南陵县',
        'py' : 'nanling',
        'spy' : 'nl'
    }, {
        'areaCode' : '341422',
        'areaName' : '无为县',
        'py' : 'wuwei',
        'spy' : 'ww'
    }]
    }, {
        'areaCode' : '340300',
        'areaName' : '蚌埠市',
        'py' : 'bangbu',
        'spy' : 'bb',
        'datas' : [{
            'areaCode' : '340302',
            'areaName' : '龙子湖区',
            'py' : 'longzi',
            'spy' : 'lz'
        }, {
            'areaCode' : '340303',
            'areaName' : '蚌山区',
            'py' : 'bangshan',
            'spy' : 'bs'
        }, {
            'areaCode' : '340304',
            'areaName' : '禹会区',
            'py' : 'yuhui',
            'spy' : 'yh'
        }, {
            'areaCode' : '340311',
            'areaName' : '淮上区',
            'py' : 'huaishang',
            'spy' : 'hs'
        }, {
            'areaCode' : '340321',
            'areaName' : '怀远县',
            'py' : 'huaiyuan',
            'spy' : 'hy'
        }, {
            'areaCode' : '340322',
            'areaName' : '五河县',
            'py' : 'wuhe',
            'spy' : 'wh'
        }, {
        'areaCode' : '340323',
        'areaName' : '固镇县',
        'py' : 'guzhen',
        'spy' : 'gz'
    }]
    }, {
        'areaCode' : '340400',
        'areaName' : '淮南市',
        'py' : 'huainan',
        'spy' : 'hn',
        'datas' : [{
            'areaCode' : '340402',
            'areaName' : '大通区',
            'py' : 'datong',
            'spy' : 'dt'
        }, {
            'areaCode' : '340403',
            'areaName' : '田家庵区',
            'py' : 'tianjiaan',
            'spy' : 'tja'
        }, {
            'areaCode' : '340404',
            'areaName' : '谢家集区',
            'py' : 'xiejiaji',
            'spy' : 'xjj'
        }, {
            'areaCode' : '340405',
            'areaName' : '八公山区',
            'py' : 'bagongshan',
            'spy' : 'bgs'
        }, {
            'areaCode' : '340406',
            'areaName' : '潘集区',
            'py' : 'panji',
            'spy' : 'pj'
        }, {
        'areaCode' : '340421',
        'areaName' : '凤台县',
        'py' : 'fengtai',
        'spy' : 'ft'
    }]
    }, {
        'areaCode' : '340500',
        'areaName' : '马鞍山市',
        'py' : 'maanshan',
        'spy' : 'mas',
        'datas' : [{
            'areaCode' : '340502',
            'areaName' : '金家庄区',
            'py' : 'jinjiazhuang',
            'spy' : 'jjz'
        }, {
            'areaCode' : '340503',
            'areaName' : '花山区',
            'py' : 'huashan',
            'spy' : 'hs'
        }, {
            'areaCode' : '340504',
            'areaName' : '雨山区',
            'py' : 'yushan',
            'spy' : 'ys'
        }, {
            'areaCode' : '340521',
            'areaName' : '当涂县',
            'py' : 'dangtu',
            'spy' : 'dt'
        }, {
        'areaCode' : '341423',
        'areaName' : '含山县',
        'py' : 'hanshan',
        'spy' : 'hs'
    }, {
        'areaCode' : '341424',
        'areaName' : '和县',
        'py' : 'hexian',
        'spy' : 'hx'
    }]
    }, {
        'areaCode' : '340600',
        'areaName' : '淮北市',
        'py' : 'huaibei',
        'spy' : 'hb',
        'datas' : [{
            'areaCode' : '340602',
            'areaName' : '杜集区',
            'py' : 'duji',
            'spy' : 'dj'
        }, {
            'areaCode' : '340603',
            'areaName' : '相山区',
            'py' : 'xiangshan',
            'spy' : 'xs'
        }, {
            'areaCode' : '340604',
            'areaName' : '烈山区',
            'py' : 'lieshan',
            'spy' : 'ls'
        }, {
        'areaCode' : '340621',
        'areaName' : '濉溪县',
        'py' : 'suixi',
        'spy' : 'sx'
    }]
    }, {
        'areaCode' : '340700',
        'areaName' : '铜陵市',
        'py' : 'tongling',
        'spy' : 'tl',
        'datas' : [{
            'areaCode' : '340702',
            'areaName' : '铜官山区',
            'py' : 'tongguanshan',
            'spy' : 'tgs'
        }, {
            'areaCode' : '340703',
            'areaName' : '狮子山区',
            'py' : 'shizishan',
            'spy' : 'szs'
        }, {
        'areaCode' : '340711',
        'areaName' : '郊区',
        'py' : 'jiaoqu',
        'spy' : 'jq'
    }, {
        'areaCode' : '340721',
        'areaName' : '铜陵县',
        'py' : 'tongling',
        'spy' : 'tl'
    }]
    }, {
        'areaCode' : '340800',
        'areaName' : '安庆市',
        'py' : 'anqing',
        'spy' : 'aq',
        'datas' : [{
            'areaCode' : '340802',
            'areaName' : '迎江区',
            'py' : 'yingjiang',
            'spy' : 'yj'
        }, {
        'areaCode' : '340803',
        'areaName' : '大观区',
        'py' : 'daguan',
        'spy' : 'dg'
    }, {
        'areaCode' : '340811',
        'areaName' : '宜秀区',
        'py' : 'yixiu',
        'spy' : 'yx'
    }, {
        'areaCode' : '340812',
        'areaName' : '郊区',
        'py' : 'jiaoqu',
        'spy' : 'jq'
    }, {
        'areaCode' : '340822',
        'areaName' : '怀宁县',
        'py' : 'huaining',
        'spy' : 'hn'
    }, {
        'areaCode' : '340823',
        'areaName' : '枞阳县',
        'py' : 'congyang',
        'spy' : 'cy'
    }, {
        'areaCode' : '340824',
        'areaName' : '潜山县',
        'py' : 'qianshan',
        'spy' : 'qs'
    }, {
        'areaCode' : '340825',
        'areaName' : '太湖县',
        'py' : 'taihu',
        'spy' : 'th'
    }, {
        'areaCode' : '340826',
        'areaName' : '宿松县',
        'py' : 'susong',
        'spy' : 'ss'
    }, {
    'areaCode' : '340827',
    'areaName' : '望江县',
    'py' : 'wangjiang',
    'spy' : 'wj'
}, {
    'areaCode' : '340828',
    'areaName' : '岳西县',
    'py' : 'yuexi',
    'spy' : 'yx'
}, {
    'areaCode' : '340881',
    'areaName' : '桐城市',
    'py' : 'tongcheng',
    'spy' : 'tc'
}]
    }, {
        'areaCode' : '341000',
        'areaName' : '黄山市',
        'py' : 'huangshan',
        'spy' : 'hs',
        'datas' : [{
        'areaCode' : '341002',
        'areaName' : '屯溪区',
        'py' : 'tunxi',
        'spy' : 'tx'
    }, {
        'areaCode' : '341003',
        'areaName' : '黄山区',
        'py' : 'huangshan',
        'spy' : 'hs'
    }, {
        'areaCode' : '341004',
        'areaName' : '徽州区',
        'py' : 'weizhou',
        'spy' : 'wz'
    }, {
        'areaCode' : '341021',
        'areaName' : '歙县',
        'py' : 'shexian',
        'spy' : 'sx'
    }, {
        'areaCode' : '341022',
        'areaName' : '休宁县',
        'py' : 'xiuning',
        'spy' : 'xn'
    }, {
        'areaCode' : '341023',
        'areaName' : '黟县',
        'py' : 'yixian',
        'spy' : 'yx'
    }, {
        'areaCode' : '341024',
        'areaName' : '祁门县',
        'py' : 'qimen',
        'spy' : 'qm'
    }]
    }, {
    'areaCode' : '341100',
    'areaName' : '滁州市',
    'py' : 'chuzhou',
    'spy' : 'cz',
    'datas' : [{
        'areaCode' : '341102',
        'areaName' : '琅琊区',
        'py' : 'langya',
        'spy' : 'ly'
    }, {
        'areaCode' : '341103',
        'areaName' : '南谯区',
        'py' : 'nanqiao',
        'spy' : 'nq'
    }, {
        'areaCode' : '341122',
        'areaName' : '来安县',
        'py' : 'laian',
        'spy' : 'la'
    }, {
        'areaCode' : '341124',
        'areaName' : '全椒县',
        'py' : 'quanjiao',
        'spy' : 'qj'
    }, {
        'areaCode' : '341125',
        'areaName' : '定远县',
        'py' : 'dingyuan',
        'spy' : 'dy'
    }, {
        'areaCode' : '341126',
        'areaName' : '凤阳县',
        'py' : 'fengyang',
        'spy' : 'fy'
    }, {
        'areaCode' : '341181',
        'areaName' : '天长市',
        'py' : 'tianchang',
        'spy' : 'tc'
    }, {
        'areaCode' : '341182',
        'areaName' : '明光市',
        'py' : 'mingguang',
        'spy' : 'mg'
    }]
}, {
    'areaCode' : '341200',
    'areaName' : '阜阳市',
    'py' : 'fuyang',
    'spy' : 'fy',
    'datas' : [{
        'areaCode' : '341202',
        'areaName' : '颍州区',
        'py' : 'yingzhou',
        'spy' : 'yz'
    }, {
        'areaCode' : '341203',
        'areaName' : '颍东区',
        'py' : 'yingdong',
        'spy' : 'yd'
    }, {
        'areaCode' : '341204',
        'areaName' : '颍泉区',
        'py' : 'yingquan',
        'spy' : 'yq'
    }, {
        'areaCode' : '341221',
        'areaName' : '临泉县',
        'py' : 'linquan',
        'spy' : 'lq'
    }, {
        'areaCode' : '341222',
        'areaName' : '太和县',
        'py' : 'taihe',
        'spy' : 'th'
    }, {
        'areaCode' : '341225',
        'areaName' : '阜南县',
        'py' : 'funan',
        'spy' : 'fn'
    }, {
        'areaCode' : '341226',
        'areaName' : '颍上县',
        'py' : 'yingshang',
        'spy' : 'ys'
    }, {
        'areaCode' : '341282',
        'areaName' : '界首市',
        'py' : 'jieshou',
        'spy' : 'js'
    }]
}, {
    'areaCode' : '341300',
    'areaName' : '宿州市',
    'py' : 'suzhou',
    'spy' : 'sz',
    'datas' : [{
        'areaCode' : '341302',
        'areaName' : '埇桥区',
        'py' : 'yongqiao',
        'spy' : 'yq'
    }, {
        'areaCode' : '341321',
        'areaName' : '砀山县',
        'py' : 'dangshan',
        'spy' : 'ds'
    }, {
        'areaCode' : '341322',
        'areaName' : '萧县',
        'py' : 'xiaoxian',
        'spy' : 'xx'
    }, {
        'areaCode' : '341323',
        'areaName' : '灵璧县',
        'py' : 'lingbi',
        'spy' : 'lb'
    }, {
        'areaCode' : '341324',
        'areaName' : '泗县',
        'py' : 'sixian',
        'spy' : 'sx'
    }]
}, {
    'areaCode' : '341500',
    'areaName' : '六安市',
    'py' : 'liuan',
    'spy' : 'la',
    'datas' : [{
        'areaCode' : '341502',
        'areaName' : '金安区',
        'py' : 'jinan',
        'spy' : 'ja'
    }, {
        'areaCode' : '341503',
        'areaName' : '裕安区',
        'py' : 'yuan',
        'spy' : 'ya'
    }, {
        'areaCode' : '341521',
        'areaName' : '寿县',
        'py' : 'shouxian',
        'spy' : 'sx'
    }, {
        'areaCode' : '341522',
        'areaName' : '霍邱县',
        'py' : 'huoqiu',
        'spy' : 'hq'
    }, {
        'areaCode' : '341523',
        'areaName' : '舒城县',
        'py' : 'shucheng',
        'spy' : 'sc'
    }, {
        'areaCode' : '341524',
        'areaName' : '金寨县',
        'py' : 'jinzhai',
        'spy' : 'jz'
    }, {
        'areaCode' : '341525',
        'areaName' : '霍山县',
        'py' : 'huoshan',
        'spy' : 'hs'
    }]
}, {
    'areaCode' : '341600',
    'areaName' : '亳州市',
    'py' : 'haozhou',
    'spy' : 'hz',
    'datas' : [{
        'areaCode' : '341602',
        'areaName' : '谯城区',
        'py' : 'qiaocheng',
        'spy' : 'qc'
    }, {
        'areaCode' : '341621',
        'areaName' : '涡阳县',
        'py' : 'guoyang',
        'spy' : 'gy'
    }, {
        'areaCode' : '341622',
        'areaName' : '蒙城县',
        'py' : 'mengcheng',
        'spy' : 'mc'
    }, {
        'areaCode' : '341623',
        'areaName' : '利辛县',
        'py' : 'lixin',
        'spy' : 'lx'
    }]
}, {
    'areaCode' : '341700',
    'areaName' : '池州市',
    'py' : 'chizhou',
    'spy' : 'cz',
    'datas' : [{
        'areaCode' : '341702',
        'areaName' : '贵池区',
        'py' : 'guichi',
        'spy' : 'gc'
    }, {
        'areaCode' : '341721',
        'areaName' : '东至县',
        'py' : 'dongzhi',
        'spy' : 'dz'
    }, {
        'areaCode' : '341722',
        'areaName' : '石台县',
        'py' : 'shitai',
        'spy' : 'st'
    }, {
        'areaCode' : '341723',
        'areaName' : '青阳县',
        'py' : 'qingyang',
        'spy' : 'qy'
    }]
}, {
    'areaCode' : '341800',
    'areaName' : '宣城市',
    'py' : 'xuancheng',
    'spy' : 'xc',
    'datas' : [{
        'areaCode' : '341802',
        'areaName' : '宣州区',
        'py' : 'xuanzhou',
        'spy' : 'xz'
    }, {
        'areaCode' : '341821',
        'areaName' : '郎溪县',
        'py' : 'langxi',
        'spy' : 'lx'
    }, {
        'areaCode' : '341822',
        'areaName' : '广德县',
        'py' : 'guangde',
        'spy' : 'gd'
    }, {
        'areaCode' : '341823',
        'areaName' : '泾县',
        'py' : 'jingxian',
        'spy' : 'jx'
    }, {
        'areaCode' : '341824',
        'areaName' : '绩溪县',
        'py' : 'jixi',
        'spy' : 'jx'
    }, {
        'areaCode' : '341825',
        'areaName' : '旌德县',
        'py' : 'jingde',
        'spy' : 'jd'
    }, {
        'areaCode' : '341881',
        'areaName' : '宁国市',
        'py' : 'ningguo',
        'spy' : 'ng'
    }]
}]
}, {
    'areaCode' : '350000',
    'areaName' : '福建省',
    'py' : 'fujian',
    'spy' : 'fj',
    'datas' : [{
        'areaCode' : '350100',
        'areaName' : '福州市',
        'py' : 'fuzhou',
        'spy' : 'fz',
        'datas' : [{
            'areaCode' : '350102',
            'areaName' : '鼓楼区',
            'py' : 'gulou',
            'spy' : 'gl'
        }, {
            'areaCode' : '350103',
            'areaName' : '台江区',
            'py' : 'taijiang',
            'spy' : 'tj'
        }, {
            'areaCode' : '350104',
            'areaName' : '仓山区',
            'py' : 'cangshan',
            'spy' : 'cs'
        }, {
            'areaCode' : '350105',
            'areaName' : '马尾区',
            'py' : 'mawei',
            'spy' : 'mw'
        }, {
            'areaCode' : '350111',
            'areaName' : '晋安区',
            'py' : 'jinan',
            'spy' : 'ja'
        }, {
            'areaCode' : '350121',
            'areaName' : '闽侯县',
            'py' : 'minhou',
            'spy' : 'mh'
        }, {
            'areaCode' : '350122',
            'areaName' : '连江县',
            'py' : 'lianjiang',
            'spy' : 'lj'
        }, {
            'areaCode' : '350123',
            'areaName' : '罗源县',
            'py' : 'luoyuan',
            'spy' : 'ly'
        }, {
        'areaCode' : '350124',
        'areaName' : '闽清县',
        'py' : 'minqing',
        'spy' : 'mq'
    }, {
    'areaCode' : '350125',
    'areaName' : '永泰县',
    'py' : 'yongtai',
    'spy' : 'yt'
}, {
    'areaCode' : '350128',
    'areaName' : '平潭县',
    'py' : 'pingtan',
    'spy' : 'pt'
}, {
    'areaCode' : '350181',
    'areaName' : '福清市',
    'py' : 'fuqing',
    'spy' : 'fq'
}, {
    'areaCode' : '350182',
    'areaName' : '长乐市',
    'py' : 'changle',
    'spy' : 'cl'
}]
    }, {
        'areaCode' : '350200',
        'areaName' : '厦门市',
        'py' : 'xiamen',
        'spy' : 'xm',
        'datas' : [{
            'areaCode' : '350203',
            'areaName' : '思明区',
            'py' : 'siming',
            'spy' : 'sm'
        }, {
            'areaCode' : '350205',
            'areaName' : '海沧区',
            'py' : 'haicang',
            'spy' : 'hc'
        }, {
            'areaCode' : '350206',
            'areaName' : '湖里区',
            'py' : 'huli',
            'spy' : 'hl'
        }, {
            'areaCode' : '350211',
            'areaName' : '集美区',
            'py' : 'jimei',
            'spy' : 'jm'
        }, {
            'areaCode' : '350212',
            'areaName' : '同安区',
            'py' : 'tongan',
            'spy' : 'tg'
        }, {
            'areaCode' : '350213',
            'areaName' : '翔安区',
            'py' : 'xiangan',
            'spy' : 'xa'
        }]
    }, {
        'areaCode' : '350300',
        'areaName' : '莆田市',
        'py' : 'putian',
        'spy' : 'pt',
        'datas' : [{
            'areaCode' : '350302',
            'areaName' : '城厢区',
            'py' : 'chengxiang',
            'spy' : 'cx'
        }, {
            'areaCode' : '350303',
            'areaName' : '涵江区',
            'py' : 'hanjiang',
            'spy' : 'hj'
        }, {
            'areaCode' : '350304',
            'areaName' : '荔城区',
            'py' : 'licheng',
            'spy' : 'lc'
        }, {
            'areaCode' : '350305',
            'areaName' : '秀屿区',
            'py' : 'xiuyu',
            'spy' : 'xy'
        }, {
            'areaCode' : '350322',
            'areaName' : '仙游县',
            'py' : 'xianyou',
            'spy' : 'xy'
        }]
    }, {
        'areaCode' : '350400',
        'areaName' : '三明市',
        'py' : 'sanming',
        'spy' : 'sm',
        'datas' : [{
            'areaCode' : '350402',
            'areaName' : '梅列区',
            'py' : 'meilie',
            'spy' : 'ml'
        }, {
            'areaCode' : '350403',
            'areaName' : '三元区',
            'py' : 'sanyuan',
            'spy' : 'sy'
        }, {
            'areaCode' : '350421',
            'areaName' : '明溪县',
            'py' : 'mingxi',
            'spy' : 'mx'
        }, {
            'areaCode' : '350423',
            'areaName' : '清流县',
            'py' : 'qingliu',
            'spy' : 'ql'
        }, {
            'areaCode' : '350424',
            'areaName' : '宁化县',
            'py' : 'ninghua',
            'spy' : 'ng'
        }, {
        'areaCode' : '350425',
        'areaName' : '大田县',
        'py' : 'datian',
        'spy' : 'dt'
    }, {
        'areaCode' : '350426',
        'areaName' : '尤溪县',
        'py' : 'youxi',
        'spy' : 'yx'
    }, {
        'areaCode' : '350427',
        'areaName' : '沙县',
        'py' : 'shaxian',
        'spy' : 'sx'
    }, {
        'areaCode' : '350428',
        'areaName' : '将乐县',
        'py' : 'jiangle',
        'spy' : 'jg'
    }, {
    'areaCode' : '350429',
    'areaName' : '泰宁县',
    'py' : 'taining',
    'spy' : 'tn'
}, {
    'areaCode' : '350430',
    'areaName' : '建宁县',
    'py' : 'jianning',
    'spy' : 'jn'
}, {
    'areaCode' : '350481',
    'areaName' : '永安市',
    'py' : 'yongan',
    'spy' : 'ya'
}]
    }, {
        'areaCode' : '350500',
        'areaName' : '泉州市',
        'py' : 'quanzhou',
        'spy' : 'qz',
        'datas' : [{
            'areaCode' : '350502',
            'areaName' : '鲤城区',
            'py' : 'licheng',
            'spy' : 'lc'
        }, {
            'areaCode' : '350503',
            'areaName' : '丰泽区',
            'py' : 'fengze',
            'spy' : 'fg'
        }, {
            'areaCode' : '350504',
            'areaName' : '洛江区',
            'py' : 'luojiang',
            'spy' : 'lj'
        }, {
            'areaCode' : '350505',
            'areaName' : '泉港区',
            'py' : 'quangang',
            'spy' : 'qg'
        }, {
        'areaCode' : '350521',
        'areaName' : '惠安县',
        'py' : 'huian',
        'spy' : 'ha'
    }, {
        'areaCode' : '350524',
        'areaName' : '安溪县',
        'py' : 'anxi',
        'spy' : 'ax'
    }, {
        'areaCode' : '350525',
        'areaName' : '永春县',
        'py' : 'yongchun',
        'spy' : 'yc'
    }, {
        'areaCode' : '350526',
        'areaName' : '德化县',
        'py' : 'dehua',
        'spy' : 'dh'
    }, {
        'areaCode' : '350527',
        'areaName' : '金门县',
        'py' : 'jinmen',
        'spy' : 'jm'
    }, {
    'areaCode' : '350581',
    'areaName' : '石狮市',
    'py' : 'shishi',
    'spy' : 'ss'
}, {
    'areaCode' : '350582',
    'areaName' : '晋江市',
    'py' : 'jinjiang',
    'spy' : 'jj'
}, {
    'areaCode' : '350583',
    'areaName' : '南安市',
    'py' : 'nanan',
    'spy' : 'na'
}]
    }, {
        'areaCode' : '350600',
        'areaName' : '漳州市',
        'py' : 'zhangzhou',
        'spy' : 'zz',
        'datas' : [{
            'areaCode' : '350602',
            'areaName' : '芗城区',
            'py' : 'xiangcheng',
            'spy' : 'xc'
        }, {
            'areaCode' : '350603',
            'areaName' : '龙文区',
            'py' : 'longwen',
            'spy' : 'lw'
        }, {
            'areaCode' : '350622',
            'areaName' : '云霄县',
            'py' : 'yunxiao',
            'spy' : 'yx'
        }, {
        'areaCode' : '350623',
        'areaName' : '漳浦县',
        'py' : 'zhangpu',
        'spy' : 'zp'
    }, {
        'areaCode' : '350624',
        'areaName' : '诏安县',
        'py' : 'zhaoan',
        'spy' : 'za'
    }, {
        'areaCode' : '350625',
        'areaName' : '长泰县',
        'py' : 'changtai',
        'spy' : 'ct'
    }, {
        'areaCode' : '350626',
        'areaName' : '东山县',
        'py' : 'dongshan',
        'spy' : 'ds'
    }, {
        'areaCode' : '350627',
        'areaName' : '南靖县',
        'py' : 'nanjing',
        'spy' : 'nj'
    }, {
        'areaCode' : '350628',
        'areaName' : '平和县',
        'py' : 'pinghe',
        'spy' : 'ph'
    }, {
    'areaCode' : '350629',
    'areaName' : '华安县',
    'py' : 'huaan',
    'spy' : 'ha'
}, {
    'areaCode' : '350681',
    'areaName' : '龙海市',
    'py' : 'longhai',
    'spy' : 'lh'
}]
    }, {
        'areaCode' : '350700',
        'areaName' : '南平市',
        'py' : 'nanping',
        'spy' : 'np',
        'datas' : [{
            'areaCode' : '350702',
            'areaName' : '延平区',
            'py' : 'yanping',
            'spy' : 'yp'
        }, {
            'areaCode' : '350721',
            'areaName' : '顺昌县',
            'py' : 'shunchang',
            'spy' : 'sc'
        }, {
        'areaCode' : '350722',
        'areaName' : '浦城县',
        'py' : 'pucheng',
        'spy' : 'pc'
    }, {
        'areaCode' : '350723',
        'areaName' : '光泽县',
        'py' : 'guangze',
        'spy' : 'gz'
    }, {
        'areaCode' : '350724',
        'areaName' : '松溪县',
        'py' : 'songxi',
        'spy' : 'sx'
    }, {
        'areaCode' : '350725',
        'areaName' : '政和县',
        'py' : 'zhenghe',
        'spy' : 'zh'
    }, {
        'areaCode' : '350781',
        'areaName' : '邵武市',
        'py' : 'shaowu',
        'spy' : 'sw'
    }, {
        'areaCode' : '350782',
        'areaName' : '武夷山市',
        'py' : 'wuyishan',
        'spy' : 'wys'
    }, {
        'areaCode' : '350783',
        'areaName' : '建瓯市',
        'py' : 'jiano',
        'spy' : 'jo'
    }, {
    'areaCode' : '350784',
    'areaName' : '建阳市',
    'py' : 'jianyang',
    'spy' : 'jy'
}]
    }, {
        'areaCode' : '350800',
        'areaName' : '龙岩市',
        'py' : 'longyan',
        'spy' : 'ly',
        'datas' : [{
            'areaCode' : '350802',
            'areaName' : '新罗区',
            'py' : 'xinluo',
            'spy' : 'xl'
        }, {
        'areaCode' : '350821',
        'areaName' : '长汀县',
        'py' : 'changting',
        'spy' : 'ct'
    }, {
        'areaCode' : '350822',
        'areaName' : '永定县',
        'py' : 'yongding',
        'spy' : 'yd'
    }, {
        'areaCode' : '350823',
        'areaName' : '上杭县',
        'py' : 'shanghang',
        'spy' : 'sh'
    }, {
        'areaCode' : '350824',
        'areaName' : '武平县',
        'py' : 'wuping',
        'spy' : 'wp'
    }, {
        'areaCode' : '350825',
        'areaName' : '连城县',
        'py' : 'liancheng',
        'spy' : 'lc'
    }, {
        'areaCode' : '350881',
        'areaName' : '漳平市',
        'py' : 'zhangping',
        'spy' : 'zp'
    }]
    }, {
        'areaCode' : '350900',
        'areaName' : '宁德市',
        'py' : 'ningde',
        'spy' : 'nd',
        'datas' : [{
        'areaCode' : '350902',
        'areaName' : '蕉城区',
        'py' : 'jiaocheng',
        'spy' : 'jc'
    }, {
        'areaCode' : '350921',
        'areaName' : '霞浦县',
        'py' : 'xiapu',
        'spy' : 'xp'
    }, {
        'areaCode' : '350922',
        'areaName' : '古田县',
        'py' : 'jitai',
        'spy' : 'jt'
    }, {
        'areaCode' : '350923',
        'areaName' : '屏南县',
        'py' : 'pingnan',
        'spy' : 'pn'
    }, {
        'areaCode' : '350924',
        'areaName' : '寿宁县',
        'py' : 'shouning',
        'spy' : 'sn'
    }, {
        'areaCode' : '350925',
        'areaName' : '周宁县',
        'py' : 'zhouning',
        'spy' : 'zn'
    }, {
        'areaCode' : '350926',
        'areaName' : '柘荣县',
        'py' : 'zherong',
        'spy' : 'zr'
    }, {
        'areaCode' : '350981',
        'areaName' : '福安市',
        'py' : 'fuan',
        'spy' : 'fa'
    }, {
        'areaCode' : '350982',
        'areaName' : '福鼎市',
        'py' : 'fuding',
        'spy' : 'fd'
    }]
    }]
}, {
    'areaCode' : '360000',
    'areaName' : '江西省',
    'py' : 'jiangxi',
    'spy' : 'jx',
    'datas' : [{
        'areaCode' : '360100',
        'areaName' : '南昌市',
        'py' : 'nanchang',
        'spy' : 'nc',
        'datas' : [{
            'areaCode' : '360102',
            'areaName' : '东湖区',
            'py' : 'donghu',
            'spy' : 'dh'
        }, {
            'areaCode' : '360103',
            'areaName' : '西湖区',
            'py' : 'xihu',
            'spy' : 'xh'
        }, {
            'areaCode' : '360104',
            'areaName' : '青云谱区',
            'py' : 'qingyunpu',
            'spy' : 'qyp'
        }, {
            'areaCode' : '360105',
            'areaName' : '湾里区',
            'py' : 'wnali',
            'spy' : 'wl'
        }, {
            'areaCode' : '360111',
            'areaName' : '青山湖区',
            'py' : 'qingshanhu',
            'spy' : 'qsh'
        }, {
            'areaCode' : '360121',
            'areaName' : '南昌县',
            'py' : 'nanchang',
            'spy' : 'nc'
        }, {
            'areaCode' : '360122',
            'areaName' : '新建县',
            'py' : 'xinjian',
            'spy' : 'xj'
        }, {
            'areaCode' : '360123',
            'areaName' : '安义县',
            'py' : 'anyi',
            'spy' : 'ay'
        }, {
        'areaCode' : '360124',
        'areaName' : '进贤县',
        'py' : 'jinxian',
        'spy' : 'jx'
    }]
    }, {
        'areaCode' : '360200',
        'areaName' : '景德镇市',
        'py' : 'jingdezhen',
        'spy' : 'jdz',
        'datas' : [{
            'areaCode' : '360202',
            'areaName' : '昌江区',
            'py' : 'changjiang',
            'spy' : 'cj'
        }, {
            'areaCode' : '360203',
            'areaName' : '珠山区',
            'py' : 'zhushan',
            'spy' : 'zs'
        }, {
            'areaCode' : '360222',
            'areaName' : '浮梁县',
            'py' : 'fuliang',
            'spy' : 'fl'
        }, {
            'areaCode' : '360281',
            'areaName' : '乐平市',
            'py' : 'leping',
            'spy' : 'lp'
        }]
    }, {
        'areaCode' : '360300',
        'areaName' : '萍乡市',
        'py' : 'pingxiang',
        'spy' : 'px',
        'datas' : [{
            'areaCode' : '360302',
            'areaName' : '安源区',
            'py' : 'anyuan',
            'spy' : 'ay'
        }, {
            'areaCode' : '360313',
            'areaName' : '湘东区',
            'py' : 'xiangdong',
            'spy' : 'xd'
        }, {
            'areaCode' : '360321',
            'areaName' : '莲花县',
            'py' : 'lianhua',
            'spy' : 'lh'
        }, {
            'areaCode' : '360322',
            'areaName' : '上栗县',
            'py' : 'shangli',
            'spy' : 'sl'
        }, {
            'areaCode' : '360323',
            'areaName' : '芦溪县',
            'py' : 'luxi',
            'spy' : 'lx'
        }]
    }, {
        'areaCode' : '360400',
        'areaName' : '九江市',
        'py' : 'jiujiang',
        'spy' : 'jj',
        'datas' : [{
            'areaCode' : '360402',
            'areaName' : '庐山区',
            'py' : 'lushan',
            'spy' : 'ls'
        }, {
            'areaCode' : '360403',
            'areaName' : '浔阳区',
            'py' : 'xunyang',
            'spy' : 'xy'
        }, {
            'areaCode' : '360421',
            'areaName' : '九江县',
            'py' : 'jiujiang',
            'spy' : 'jj'
        }, {
            'areaCode' : '360423',
            'areaName' : '武宁县',
            'py' : 'wuning',
            'spy' : 'wn'
        }, {
            'areaCode' : '360424',
            'areaName' : '修水县',
            'py' : 'xiushui',
            'spy' : 'xs'
        }, {
        'areaCode' : '360425',
        'areaName' : '永修县',
        'py' : 'yongxiu',
        'spy' : 'yx'
    }, {
        'areaCode' : '360426',
        'areaName' : '德安县',
        'py' : 'dean',
        'spy' : 'da'
    }, {
        'areaCode' : '360427',
        'areaName' : '星子县',
        'py' : 'xingzi',
        'spy' : 'xz'
    }, {
        'areaCode' : '360428',
        'areaName' : '都昌县',
        'py' : 'douchang',
        'spy' : 'dc'
    }, {
    'areaCode' : '360429',
    'areaName' : '湖口县',
    'py' : 'hukou',
    'spy' : 'hk'
}, {
    'areaCode' : '360430',
    'areaName' : '彭泽县',
    'py' : 'pengze',
    'spy' : 'pz'
}, {
    'areaCode' : '360481',
    'areaName' : '瑞昌市',
    'py' : 'ruichang',
    'spy' : 'rc'
}, {
    'areaCode' : '360482',
    'areaName' : '共青城市',
    'py' : 'gongqingcheng',
    'spy' : 'gqc'
}, {
    'areaCode' : '360483',
    'areaName' : '经济技术开发区',
    'py' : 'jingjikaifa',
    'spy' : 'jjkf'
}]
    }, {
        'areaCode' : '360500',
        'areaName' : '新余市',
        'py' : 'xinyu',
        'spy' : 'xy',
        'datas' : [{
            'areaCode' : '360502',
            'areaName' : '渝水区',
            'py' : 'yushui',
            'spy' : 'ys'
        }, {
            'areaCode' : '360521',
            'areaName' : '分宜县',
            'py' : 'fenyi',
            'spy' : 'fy'
        }]
    }, {
        'areaCode' : '360600',
        'areaName' : '鹰潭市',
        'py' : 'yingtan',
        'spy' : 'yt',
        'datas' : [{
            'areaCode' : '360602',
            'areaName' : '月湖区',
            'py' : 'yuehu',
            'spy' : 'yh'
        }, {
            'areaCode' : '360622',
            'areaName' : '余江县',
            'py' : 'yujiang',
            'spy' : 'yj'
        }, {
            'areaCode' : '360681',
            'areaName' : '贵溪市',
            'py' : 'guixi',
            'spy' : 'gx'
        }]
    }, {
        'areaCode' : '360700',
        'areaName' : '赣州市',
        'py' : 'ganzhou',
        'spy' : 'gz',
        'datas' : [{
            'areaCode' : '360702',
            'areaName' : '章贡区',
            'py' : 'zhanggong',
            'spy' : 'zg'
        }, {
            'areaCode' : '360721',
            'areaName' : '赣县',
            'py' : 'ganxian',
            'spy' : 'gx'
        }, {
        'areaCode' : '360722',
        'areaName' : '信丰县',
        'py' : 'xinfeng',
        'spy' : 'xf'
    }, {
        'areaCode' : '360723',
        'areaName' : '大余县',
        'py' : 'dayu',
        'spy' : 'dy'
    }, {
        'areaCode' : '360724',
        'areaName' : '上犹县',
        'py' : 'shangyou',
        'spy' : 'sy'
    }, {
        'areaCode' : '360725',
        'areaName' : '崇义县',
        'py' : 'chonyi',
        'spy' : 'cy'
    }, {
        'areaCode' : '360726',
        'areaName' : '安远县',
        'py' : 'anyuan',
        'spy' : 'ay'
    }, {
        'areaCode' : '360727',
        'areaName' : '龙南县',
        'py' : 'longnan',
        'spy' : 'ln'
    }, {
        'areaCode' : '360728',
        'areaName' : '定南县',
        'py' : 'dingnan',
        'spy' : 'dg'
    }, {
    'areaCode' : '360729',
    'areaName' : '全南县',
    'py' : 'quannan',
    'spy' : 'qn'
}, {
    'areaCode' : '360730',
    'areaName' : '宁都县',
    'py' : 'ningdou',
    'spy' : 'nd'
}, {
    'areaCode' : '360731',
    'areaName' : '于都县',
    'py' : 'yudou',
    'spy' : 'yd'
}, {
    'areaCode' : '360732',
    'areaName' : '兴国县',
    'py' : 'xingguo',
    'spy' : 'xg'
}, {
    'areaCode' : '360733',
    'areaName' : '会昌县',
    'py' : 'huichang',
    'spy' : 'hc'
}, {
    'areaCode' : '360734',
    'areaName' : '寻乌县',
    'py' : 'xunwu',
    'spy' : 'xw'
}, {
    'areaCode' : '360735',
    'areaName' : '石城县',
    'py' : 'shicheng',
    'spy' : 'sc'
}, {
    'areaCode' : '360781',
    'areaName' : '瑞金市',
    'py' : 'ruijin',
    'spy' : 'rj'
}, {
    'areaCode' : '360782',
    'areaName' : '南康市',
    'py' : 'nankang',
    'spy' : 'nk'
}]
    }, {
        'areaCode' : '360800',
        'areaName' : '吉安市',
        'py' : 'jian',
        'spy' : 'ja',
        'datas' : [{
            'areaCode' : '360802',
            'areaName' : '吉州区',
            'py' : 'jizhou',
            'spy' : 'jz'
        }, {
        'areaCode' : '360803',
        'areaName' : '青原区',
        'py' : 'qingyuan',
        'spy' : 'qy'
    }, {
        'areaCode' : '360821',
        'areaName' : '吉安县',
        'py' : 'jian',
        'spy' : 'ja'
    }, {
        'areaCode' : '360822',
        'areaName' : '吉水县',
        'py' : 'jishui',
        'spy' : 'js'
    }, {
        'areaCode' : '360823',
        'areaName' : '峡江县',
        'py' : 'xiajiang',
        'spy' : 'xj'
    }, {
        'areaCode' : '360824',
        'areaName' : '新干县',
        'py' : 'xingan',
        'spy' : 'xg'
    }, {
        'areaCode' : '360825',
        'areaName' : '永丰县',
        'py' : 'yongfeng',
        'spy' : 'yf'
    }, {
        'areaCode' : '360826',
        'areaName' : '泰和县',
        'py' : 'taihe',
        'spy' : 'th'
    }, {
        'areaCode' : '360827',
        'areaName' : '遂川县',
        'py' : 'suichuan',
        'spy' : 'sc'
    }, {
    'areaCode' : '360828',
    'areaName' : '万安县',
    'py' : 'wanan',
    'spy' : 'wa'
}, {
    'areaCode' : '360829',
    'areaName' : '安福县',
    'py' : 'anfu',
    'spy' : 'af'
}, {
    'areaCode' : '360830',
    'areaName' : '永新县',
    'py' : 'yongxin',
    'spy' : 'yg'
}, {
    'areaCode' : '360881',
    'areaName' : '井冈山市',
    'py' : 'jinggangshan',
    'spy' : 'jgs'
}]
    }, {
        'areaCode' : '360900',
        'areaName' : '宜春市',
        'py' : 'yichun',
        'spy' : 'yc',
        'datas' : [{
        'areaCode' : '360902',
        'areaName' : '袁州区',
        'py' : 'yuanzhou',
        'spy' : 'yz'
    }, {
        'areaCode' : '360921',
        'areaName' : '奉新县',
        'py' : 'fengxin',
        'spy' : 'fx'
    }, {
        'areaCode' : '360922',
        'areaName' : '万载县',
        'py' : 'wanzai',
        'spy' : 'wz'
    }, {
        'areaCode' : '360923',
        'areaName' : '上高县',
        'py' : 'shanggao',
        'spy' : 'sg'
    }, {
        'areaCode' : '360924',
        'areaName' : '宜丰县',
        'py' : 'yifeng',
        'spy' : 'yf'
    }, {
        'areaCode' : '360925',
        'areaName' : '靖安县',
        'py' : 'jingan',
        'spy' : 'ja'
    }, {
        'areaCode' : '360926',
        'areaName' : '铜鼓县',
        'py' : 'tonggu',
        'spy' : 'tg'
    }, {
        'areaCode' : '360981',
        'areaName' : '丰城市',
        'py' : 'fengcheng',
        'spy' : 'fc'
    }, {
        'areaCode' : '360982',
        'areaName' : '樟树市',
        'py' : 'zhangshu',
        'spy' : 'zs'
    }, {
    'areaCode' : '360983',
    'areaName' : '高安市',
    'py' : 'gaoan',
    'spy' : 'ga'
}]
    }, {
    'areaCode' : '361000',
    'areaName' : '抚州市',
    'py' : 'wuzhou',
    'spy' : 'wz',
    'datas' : [{
        'areaCode' : '361002',
        'areaName' : '临川区',
        'py' : 'linchuan',
        'spy' : 'lc'
    }, {
        'areaCode' : '361021',
        'areaName' : '南城县',
        'py' : 'nancheng',
        'spy' : 'nc'
    }, {
        'areaCode' : '361022',
        'areaName' : '黎川县',
        'py' : 'lichuan',
        'spy' : 'lc'
    }, {
        'areaCode' : '361023',
        'areaName' : '南丰县',
        'py' : 'nanfeng',
        'spy' : 'nf'
    }, {
        'areaCode' : '361024',
        'areaName' : '崇仁县',
        'py' : 'chongren',
        'spy' : 'cr'
    }, {
        'areaCode' : '361025',
        'areaName' : '乐安县',
        'py' : 'lean',
        'spy' : 'la'
    }, {
        'areaCode' : '361026',
        'areaName' : '宜黄县',
        'py' : 'yihuang',
        'spy' : 'yh'
    }, {
        'areaCode' : '361027',
        'areaName' : '金溪县',
        'py' : 'jinxi',
        'spy' : 'jx'
    }, {
        'areaCode' : '361028',
        'areaName' : '资溪县',
        'py' : 'zixi',
        'spy' : 'zx'
    }, {
    'areaCode' : '361029',
    'areaName' : '东乡县',
    'py' : 'dongxiang',
    'spy' : 'dx'
}, {
    'areaCode' : '361030',
    'areaName' : '广昌县',
    'py' : 'guangchang',
    'spy' : 'gc'
}]
}, {
    'areaCode' : '361100',
    'areaName' : '上饶市',
    'py' : 'shangrao',
    'spy' : 'sr',
    'datas' : [{
        'areaCode' : '361102',
        'areaName' : '信州区',
        'py' : 'xinzhou',
        'spy' : 'xz'
    }, {
        'areaCode' : '361121',
        'areaName' : '上饶县',
        'py' : 'shangrao',
        'spy' : 'sr'
    }, {
        'areaCode' : '361122',
        'areaName' : '广丰县',
        'py' : 'guangfeng',
        'spy' : 'gf'
    }, {
        'areaCode' : '361123',
        'areaName' : '玉山县',
        'py' : 'yushan',
        'spy' : 'ys'
    }, {
        'areaCode' : '361124',
        'areaName' : '铅山县',
        'py' : 'qianshan',
        'spy' : 'qs'
    }, {
        'areaCode' : '361125',
        'areaName' : '横峰县',
        'py' : 'hengfeng',
        'spy' : 'hf'
    }, {
        'areaCode' : '361126',
        'areaName' : '弋阳县',
        'py' : 'yiyang',
        'spy' : 'yy'
    }, {
        'areaCode' : '361127',
        'areaName' : '余干县',
        'py' : 'yugan',
        'spy' : 'yg'
    }, {
        'areaCode' : '361128',
        'areaName' : '鄱阳县',
        'py' : 'poyang',
        'spy' : 'py'
    }, {
    'areaCode' : '361129',
    'areaName' : '万年县',
    'py' : 'wannian',
    'spy' : 'wn'
}, {
    'areaCode' : '361130',
    'areaName' : '婺源县',
    'py' : 'wuyuan',
    'spy' : 'wy'
}, {
    'areaCode' : '361181',
    'areaName' : '德兴市',
    'py' : 'dexing',
    'spy' : 'dx'
}]
}]
}, {
    'areaCode' : '370000',
    'areaName' : '山东省',
    'py' : 'shandong',
    'spy' : 'sd',
    'datas' : [{
        'areaCode' : '370100',
        'areaName' : '济南市',
        'py' : 'jinan',
        'spy' : 'jn',
        'datas' : [{
            'areaCode' : '370102',
            'areaName' : '历下区',
            'py' : 'lixia',
            'spy' : 'lx'
        }, {
            'areaCode' : '370103',
            'areaName' : '市中区',
            'py' : 'shizhong',
            'spy' : 'sz'
        }, {
            'areaCode' : '370104',
            'areaName' : '槐荫区',
            'py' : 'huaiyin',
            'spy' : 'hy'
        }, {
            'areaCode' : '370105',
            'areaName' : '天桥区',
            'py' : 'tianqiao',
            'spy' : 'tq'
        }, {
            'areaCode' : '370112',
            'areaName' : '历城区',
            'py' : 'licheng',
            'spy' : 'lc'
        }, {
            'areaCode' : '370113',
            'areaName' : '长清区',
            'py' : 'changqing',
            'spy' : 'cq'
        }, {
            'areaCode' : '370124',
            'areaName' : '平阴县',
            'py' : 'pingyin',
            'spy' : 'py'
        }, {
            'areaCode' : '370125',
            'areaName' : '济阳县',
            'py' : 'jiyang',
            'spy' : 'jy'
        }, {
        'areaCode' : '370126',
        'areaName' : '商河县',
        'py' : 'shanghe',
        'spy' : 'sg'
    }, {
    'areaCode' : '370181',
    'areaName' : '章丘市',
    'py' : 'zhangqiu',
    'spy' : 'zq'
}]
    }, {
        'areaCode' : '370200',
        'areaName' : '青岛市',
        'py' : 'qingdao',
        'spy' : 'qd',
        'datas' : [{
            'areaCode' : '370202',
            'areaName' : '市南区',
            'py' : 'shinan',
            'spy' : 'sn'
        }, {
            'areaCode' : '370203',
            'areaName' : '市北区',
            'py' : 'shibei',
            'spy' : 'sb'
        }, {
            'areaCode' : '370205',
            'areaName' : '四方区',
            'py' : 'sifang',
            'spy' : 'sf'
        }, {
            'areaCode' : '370211',
            'areaName' : '黄岛区',
            'py' : 'huangdao',
            'spy' : 'hd'
        }, {
            'areaCode' : '370212',
            'areaName' : '崂山区',
            'py' : 'laoshan',
            'spy' : 'ls'
        }, {
            'areaCode' : '370213',
            'areaName' : '李沧区',
            'py' : 'licang',
            'spy' : 'lc'
        }, {
            'areaCode' : '370214',
            'areaName' : '城阳区',
            'py' : 'chengyang',
            'spy' : 'cy'
        }, {
        'areaCode' : '370281',
        'areaName' : '胶州市',
        'py' : 'jiaozhou',
        'spy' : 'jz'
    }, {
        'areaCode' : '370282',
        'areaName' : '即墨市',
        'py' : 'jimo',
        'spy' : 'jm'
    }, {
    'areaCode' : '370283',
    'areaName' : '平度市',
    'py' : 'pingdu',
    'spy' : 'pd'
}, {
    'areaCode' : '370284',
    'areaName' : '胶南市',
    'py' : 'jiaonan',
    'spy' : 'jn'
}, {
    'areaCode' : '370285',
    'areaName' : '莱西市',
    'py' : 'laixi',
    'spy' : 'lx'
}]
    }, {
        'areaCode' : '370300',
        'areaName' : '淄博市',
        'py' : 'zibo',
        'spy' : 'zb',
        'datas' : [{
            'areaCode' : '370302',
            'areaName' : '淄川区',
            'py' : 'zichuan',
            'spy' : 'zc'
        }, {
            'areaCode' : '370303',
            'areaName' : '张店区',
            'py' : 'zhangdian',
            'spy' : 'zd'
        }, {
            'areaCode' : '370304',
            'areaName' : '博山区',
            'py' : 'boshan',
            'spy' : 'bs'
        }, {
            'areaCode' : '370305',
            'areaName' : '临淄区',
            'py' : 'linzi',
            'spy' : 'lz'
        }, {
            'areaCode' : '370306',
            'areaName' : '周村区',
            'py' : 'zhoucun',
            'spy' : 'zc'
        }, {
            'areaCode' : '370321',
            'areaName' : '桓台县',
            'py' : 'huantai',
            'spy' : 'ht'
        }, {
        'areaCode' : '370322',
        'areaName' : '高青县',
        'py' : 'gaoqing',
        'spy' : 'gq'
    }, {
        'areaCode' : '370323',
        'areaName' : '沂源县',
        'py' : 'qinyuan',
        'spy' : 'qy'
    }]
    }, {
        'areaCode' : '370400',
        'areaName' : '枣庄市',
        'py' : 'zaozhuang',
        'spy' : 'zz',
        'datas' : [{
            'areaCode' : '370402',
            'areaName' : '市中区',
            'py' : 'shizhong',
            'spy' : 'sz'
        }, {
            'areaCode' : '370403',
            'areaName' : '薛城区',
            'py' : 'xuecheng',
            'spy' : 'xc'
        }, {
            'areaCode' : '370404',
            'areaName' : '峄城区',
            'py' : 'yicheng',
            'spy' : 'yc'
        }, {
            'areaCode' : '370405',
            'areaName' : '台儿庄区',
            'py' : 'taierzhuang',
            'spy' : 'tez'
        }, {
            'areaCode' : '370406',
            'areaName' : '山亭区',
            'py' : 'shanting',
            'spy' : 'st'
        }, {
        'areaCode' : '370481',
        'areaName' : '滕州市',
        'py' : 'tengzhou',
        'spy' : 'tz'
    }]
    }, {
        'areaCode' : '370500',
        'areaName' : '东营市',
        'py' : 'dongying',
        'spy' : 'dy',
        'datas' : [{
            'areaCode' : '370502',
            'areaName' : '东营区',
            'py' : 'dongying',
            'spy' : 'dy'
        }, {
            'areaCode' : '370503',
            'areaName' : '河口区',
            'py' : 'hekou',
            'spy' : 'hk'
        }, {
            'areaCode' : '370521',
            'areaName' : '垦利县',
            'py' : 'kenli',
            'spy' : 'kl'
        }, {
            'areaCode' : '370522',
            'areaName' : '利津县',
            'py' : 'lijin',
            'spy' : 'lj'
        }, {
        'areaCode' : '370523',
        'areaName' : '广饶县',
        'py' : 'guangrao',
        'spy' : 'gr'
    }]
    }, {
        'areaCode' : '370600',
        'areaName' : '烟台市',
        'py' : 'yantai',
        'spy' : 'yt',
        'datas' : [{
            'areaCode' : '370602',
            'areaName' : '芝罘区',
            'py' : 'zhifu',
            'spy' : 'zf'
        }, {
            'areaCode' : '370611',
            'areaName' : '福山区',
            'py' : 'fushan',
            'spy' : 'fs'
        }, {
            'areaCode' : '370612',
            'areaName' : '牟平区',
            'py' : 'muping',
            'spy' : 'mp'
        }, {
        'areaCode' : '370613',
        'areaName' : '莱山区',
        'py' : 'laishan',
        'spy' : 'ls'
    }, {
        'areaCode' : '370634',
        'areaName' : '长岛县',
        'py' : 'changdao',
        'spy' : 'cd'
    }, {
        'areaCode' : '370681',
        'areaName' : '龙口市',
        'py' : 'longkou',
        'spy' : 'lk'
    }, {
        'areaCode' : '370682',
        'areaName' : '莱阳市',
        'py' : 'laiyang',
        'spy' : 'ly'
    }, {
        'areaCode' : '370683',
        'areaName' : '莱州市',
        'py' : 'laizhou',
        'spy' : 'lz'
    }, {
        'areaCode' : '370684',
        'areaName' : '蓬莱市',
        'py' : 'penglai',
        'spy' : 'pl'
    }, {
    'areaCode' : '370685',
    'areaName' : '招远市',
    'py' : 'zhaoyuan',
    'spy' : 'zy'
}, {
    'areaCode' : '370686',
    'areaName' : '栖霞市',
    'py' : 'qixia',
    'spy' : 'qx'
}, {
    'areaCode' : '370687',
    'areaName' : '海阳市',
    'py' : 'haiyang',
    'spy' : 'hy'
}]
    }, {
        'areaCode' : '370700',
        'areaName' : '潍坊市',
        'py' : 'weifang',
        'spy' : 'wf',
        'datas' : [{
            'areaCode' : '370702',
            'areaName' : '潍城区',
            'py' : 'weicheng',
            'spy' : 'wc'
        }, {
            'areaCode' : '370703',
            'areaName' : '寒亭区',
            'py' : 'hanting',
            'spy' : 'ht'
        }, {
        'areaCode' : '370704',
        'areaName' : '坊子区',
        'py' : 'fangzi',
        'spy' : 'fz'
    }, {
        'areaCode' : '370705',
        'areaName' : '奎文区',
        'py' : 'kuiwen',
        'spy' : 'kw'
    }, {
        'areaCode' : '370724',
        'areaName' : '临朐县',
        'py' : 'linqu',
        'spy' : 'lq'
    }, {
        'areaCode' : '370725',
        'areaName' : '昌乐县',
        'py' : 'changle',
        'spy' : 'cg'
    }, {
        'areaCode' : '370781',
        'areaName' : '青州市',
        'py' : 'qingzhou',
        'spy' : 'qz'
    }, {
        'areaCode' : '370782',
        'areaName' : '诸城市',
        'py' : 'zhucheng',
        'spy' : 'zc'
    }, {
        'areaCode' : '370783',
        'areaName' : '寿光市',
        'py' : 'shouguang',
        'spy' : 'sg'
    }, {
    'areaCode' : '370784',
    'areaName' : '安丘市',
    'py' : 'anqiu',
    'spy' : 'aq'
}, {
    'areaCode' : '370785',
    'areaName' : '高密市',
    'py' : 'gaomi',
    'spy' : 'gm'
}, {
    'areaCode' : '370786',
    'areaName' : '昌邑市',
    'py' : 'changyi',
    'spy' : 'cy'
}]
    }, {
        'areaCode' : '370800',
        'areaName' : '济宁市',
        'py' : 'jining',
        'spy' : 'jn',
        'datas' : [{
            'areaCode' : '370802',
            'areaName' : '市中区',
            'py' : 'shizhong',
            'spy' : 'sz'
        }, {
        'areaCode' : '370811',
        'areaName' : '任城区',
        'py' : 'rencheng',
        'spy' : 'rc'
    }, {
        'areaCode' : '370826',
        'areaName' : '微山县',
        'py' : 'weishan',
        'spy' : 'ws'
    }, {
        'areaCode' : '370827',
        'areaName' : '鱼台县',
        'py' : 'yutai',
        'spy' : 'yt'
    }, {
        'areaCode' : '370828',
        'areaName' : '金乡县',
        'py' : 'jinxiang',
        'spy' : 'jx'
    }, {
        'areaCode' : '370829',
        'areaName' : '嘉祥县',
        'py' : 'jiaxiang',
        'spy' : 'jx'
    }, {
        'areaCode' : '370830',
        'areaName' : '汶上县',
        'py' : 'wenshang',
        'spy' : 'ws'
    }, {
        'areaCode' : '370831',
        'areaName' : '泗水县',
        'py' : 'sishui',
        'spy' : 'ss'
    }, {
        'areaCode' : '370832',
        'areaName' : '梁山县',
        'py' : 'liangshan',
        'spy' : 'ls'
    }, {
    'areaCode' : '370881',
    'areaName' : '曲阜市',
    'py' : 'qufu',
    'spy' : 'qf'
}, {
    'areaCode' : '370882',
    'areaName' : '兖州市',
    'py' : 'yanzhou',
    'spy' : 'yz'
}, {
    'areaCode' : '370883',
    'areaName' : '邹城市',
    'py' : 'zoucheng',
    'spy' : 'zc'
}]
    }, {
        'areaCode' : '370900',
        'areaName' : '泰安市',
        'py' : 'taian',
        'spy' : 'ta',
        'datas' : [{
        'areaCode' : '370902',
        'areaName' : '泰山区',
        'py' : 'taishan',
        'spy' : 'ts'
    }, {
        'areaCode' : '370903',
        'areaName' : '岱岳区',
        'py' : 'daiyue',
        'spy' : 'dy'
    }, {
        'areaCode' : '370921',
        'areaName' : '宁阳县',
        'py' : 'ningyang',
        'spy' : 'ny'
    }, {
        'areaCode' : '370923',
        'areaName' : '东平县',
        'py' : 'dongping',
        'spy' : 'dp'
    }, {
        'areaCode' : '370982',
        'areaName' : '新泰市',
        'py' : 'xintai',
        'spy' : 'xt'
    }, {
        'areaCode' : '370983',
        'areaName' : '肥城市',
        'py' : 'feicheng',
        'spy' : 'fc'
    }]
    }, {
    'areaCode' : '371000',
    'areaName' : '威海市',
    'py' : 'weihai',
    'spy' : 'wh',
    'datas' : [{
        'areaCode' : '371002',
        'areaName' : '环翠区',
        'py' : 'huancui',
        'spy' : 'hc'
    }, {
        'areaCode' : '371081',
        'areaName' : '文登市',
        'py' : 'wendeng',
        'spy' : 'wd'
    }, {
        'areaCode' : '371082',
        'areaName' : '荣成市',
        'py' : 'rongcheng',
        'spy' : 'rc'
    }, {
        'areaCode' : '371083',
        'areaName' : '乳山市',
        'py' : 'rushan',
        'spy' : 'rs'
    }]
}, {
    'areaCode' : '371100',
    'areaName' : '日照市',
    'py' : 'rizhao',
    'spy' : 'rz',
    'datas' : [{
        'areaCode' : '371102',
        'areaName' : '东港区',
        'py' : 'donggang',
        'spy' : 'dg'
    }, {
        'areaCode' : '371103',
        'areaName' : '岚山区',
        'py' : 'lanshan',
        'spy' : 'ls'
    }, {
        'areaCode' : '371121',
        'areaName' : '五莲县',
        'py' : 'wulian',
        'spy' : 'wl'
    }, {
        'areaCode' : '371122',
        'areaName' : '莒县',
        'py' : 'juxian',
        'spy' : 'jx'
    }]
}, {
    'areaCode' : '371200',
    'areaName' : '莱芜市',
    'py' : 'laiwu',
    'spy' : 'lw',
    'datas' : [{
        'areaCode' : '371202',
        'areaName' : '莱城区',
        'py' : 'laicheng',
        'spy' : 'lc'
    }, {
        'areaCode' : '371203',
        'areaName' : '钢城区',
        'py' : 'gangcheng',
        'spy' : 'gc'
    }]
}, {
    'areaCode' : '371300',
    'areaName' : '临沂市',
    'py' : 'linyi',
    'spy' : 'ly',
    'datas' : [{
        'areaCode' : '371302',
        'areaName' : '兰山区',
        'py' : 'lanshan',
        'spy' : 'ls'
    }, {
        'areaCode' : '371311',
        'areaName' : '罗庄区',
        'py' : 'luozhuang',
        'spy' : 'lz'
    }, {
        'areaCode' : '371312',
        'areaName' : '河东区',
        'py' : 'hedong',
        'spy' : 'hd'
    }, {
        'areaCode' : '371321',
        'areaName' : '沂南县',
        'py' : 'yinan',
        'spy' : 'yn'
    }, {
        'areaCode' : '371322',
        'areaName' : '郯城县',
        'py' : 'tancheng',
        'spy' : 'tc'
    }, {
        'areaCode' : '371323',
        'areaName' : '沂水县',
        'py' : 'yishui',
        'spy' : 'yh'
    }, {
        'areaCode' : '371324',
        'areaName' : '苍山县',
        'py' : 'cangshan',
        'spy' : 'cs'
    }, {
        'areaCode' : '371325',
        'areaName' : '费县',
        'py' : 'feixian',
        'spy' : 'fx'
    }, {
        'areaCode' : '371326',
        'areaName' : '平邑县',
        'py' : 'pingyi',
        'spy' : 'py'
    }, {
    'areaCode' : '371327',
    'areaName' : '莒南县',
    'py' : 'junan',
    'spy' : 'jn'
}, {
    'areaCode' : '371328',
    'areaName' : '蒙阴县',
    'py' : 'mengyin',
    'spy' : 'my'
}, {
    'areaCode' : '371329',
    'areaName' : '临沭县',
    'py' : 'linmu',
    'spy' : 'lm'
}]
}, {
    'areaCode' : '371400',
    'areaName' : '德州市',
    'py' : 'dezhou',
    'spy' : 'dz',
    'datas' : [{
        'areaCode' : '371402',
        'areaName' : '德城区',
        'py' : 'decheng',
        'spy' : 'dc'
    }, {
        'areaCode' : '371421',
        'areaName' : '陵县',
        'py' : 'lingxian',
        'spy' : 'lx'
    }, {
        'areaCode' : '371422',
        'areaName' : '宁津县',
        'py' : 'ningjin',
        'spy' : 'ng'
    }, {
        'areaCode' : '371423',
        'areaName' : '庆云县',
        'py' : 'qingyun',
        'spy' : 'qg'
    }, {
        'areaCode' : '371424',
        'areaName' : '临邑县',
        'py' : 'linyi',
        'spy' : 'ly'
    }, {
        'areaCode' : '371425',
        'areaName' : '齐河县',
        'py' : 'qihe',
        'spy' : 'qh'
    }, {
        'areaCode' : '371426',
        'areaName' : '平原县',
        'py' : 'pingyuan',
        'spy' : 'py'
    }, {
        'areaCode' : '371427',
        'areaName' : '夏津县',
        'py' : 'xiajin',
        'spy' : 'xj'
    }, {
        'areaCode' : '371428',
        'areaName' : '武城县',
        'py' : 'wucheng',
        'spy' : 'wc'
    }, {
    'areaCode' : '371481',
    'areaName' : '乐陵市',
    'py' : 'leling',
    'spy' : 'll'
}, {
    'areaCode' : '371482',
    'areaName' : '禹城市',
    'py' : 'yucheng',
    'spy' : 'yc'
}]
}, {
    'areaCode' : '371500',
    'areaName' : '聊城市',
    'py' : 'liaocheng',
    'spy' : 'lc',
    'datas' : [{
        'areaCode' : '371502',
        'areaName' : '东昌府区',
        'py' : 'dongchangfu',
        'spy' : 'dcf'
    }, {
        'areaCode' : '371521',
        'areaName' : '阳谷县',
        'py' : 'yanggu',
        'spy' : 'yg'
    }, {
        'areaCode' : '371522',
        'areaName' : '莘县',
        'py' : 'shenxian',
        'spy' : 'sx'
    }, {
        'areaCode' : '371523',
        'areaName' : '茌平县',
        'py' : 'chiping',
        'spy' : 'cp'
    }, {
        'areaCode' : '371524',
        'areaName' : '东阿县',
        'py' : 'donga',
        'spy' : 'da'
    }, {
        'areaCode' : '371525',
        'areaName' : '冠县',
        'py' : 'guanxian',
        'spy' : 'gx'
    }, {
        'areaCode' : '371526',
        'areaName' : '高唐县',
        'py' : 'gaotang',
        'spy' : 'gt'
    }, {
        'areaCode' : '371581',
        'areaName' : '临清市',
        'py' : 'linqing',
        'spy' : 'lq'
    }]
}, {
    'areaCode' : '371600',
    'areaName' : '滨州市',
    'py' : 'binzhou',
    'spy' : 'bz',
    'datas' : [{
        'areaCode' : '371602',
        'areaName' : '滨城区',
        'py' : 'bincheng',
        'spy' : 'bc'
    }, {
        'areaCode' : '371621',
        'areaName' : '惠民县',
        'py' : 'huimin',
        'spy' : 'hm'
    }, {
        'areaCode' : '371622',
        'areaName' : '阳信县',
        'py' : 'yangxin',
        'spy' : 'yx'
    }, {
        'areaCode' : '371623',
        'areaName' : '无棣县',
        'py' : 'wudi',
        'spy' : 'wd'
    }, {
        'areaCode' : '371624',
        'areaName' : '沾化县',
        'py' : 'zhanhua',
        'spy' : 'zh'
    }, {
        'areaCode' : '371625',
        'areaName' : '博兴县',
        'py' : 'boxing',
        'spy' : 'bx'
    }, {
        'areaCode' : '371626',
        'areaName' : '邹平县',
        'py' : 'zhouping',
        'spy' : 'zp'
    }]
}, {
    'areaCode' : '371700',
    'areaName' : '菏泽市',
    'py' : 'heze',
    'spy' : 'hz',
    'datas' : [{
        'areaCode' : '371702',
        'areaName' : '牡丹区',
        'py' : 'mudan',
        'spy' : 'md'
    }, {
        'areaCode' : '371721',
        'areaName' : '曹县',
        'py' : 'caoxian',
        'spy' : 'cx'
    }, {
        'areaCode' : '371722',
        'areaName' : '单县',
        'py' : 'danxianshanxian',
        'spy' : 'dx'
    }, {
        'areaCode' : '371723',
        'areaName' : '成武县',
        'py' : 'chengwu',
        'spy' : 'cgx'
    }, {
        'areaCode' : '371724',
        'areaName' : '巨野县',
        'py' : 'juye',
        'spy' : 'jyx'
    }, {
        'areaCode' : '371725',
        'areaName' : '郓城县',
        'py' : 'yuncheng',
        'spy' : 'ycx'
    }, {
        'areaCode' : '371726',
        'areaName' : '鄄城县',
        'py' : 'juancheng',
        'spy' : 'jcx'
    }, {
        'areaCode' : '371727',
        'areaName' : '定陶县',
        'py' : 'dingtao',
        'spy' : 'dtx'
    }, {
        'areaCode' : '371728',
        'areaName' : '东明县',
        'py' : 'dongming',
        'spy' : 'dm'
    }]
}]
}, {
    'areaCode' : '410000',
    'areaName' : '河南省',
    'py' : 'henan',
    'spy' : 'hn',
    'datas' : [{
        'areaCode' : '410100',
        'areaName' : '郑州市',
        'py' : 'zhengzhou',
        'spy' : 'zz',
        'datas' : [{
            'areaCode' : '410102',
            'areaName' : '中原区',
            'py' : 'zhongyuan',
            'spy' : 'zy'
        }, {
            'areaCode' : '410103',
            'areaName' : '二七区',
            'py' : 'erqi',
            'spy' : 'eq'
        }, {
            'areaCode' : '410104',
            'areaName' : '管城回族区',
            'py' : 'guanchenghuizu',
            'spy' : 'gchz'
        }, {
            'areaCode' : '410105',
            'areaName' : '金水区',
            'py' : 'jinshui',
            'spy' : 'js'
        }, {
            'areaCode' : '410106',
            'areaName' : '上街区',
            'py' : 'shangjie',
            'spy' : 'sj'
        }, {
            'areaCode' : '410108',
            'areaName' : '惠济区',
            'py' : 'huiji',
            'spy' : 'hj'
        }, {
            'areaCode' : '410122',
            'areaName' : '中牟县',
            'py' : 'zhongmu',
            'spy' : 'zm'
        }, {
            'areaCode' : '410181',
            'areaName' : '巩义市',
            'py' : 'gongyi',
            'spy' : 'gy'
        }, {
        'areaCode' : '410182',
        'areaName' : '荥阳市',
        'py' : 'xingyang',
        'spy' : 'xy'
    }, {
    'areaCode' : '410183',
    'areaName' : '新密市',
    'py' : 'xinmi',
    'spy' : 'xm'
}, {
    'areaCode' : '410184',
    'areaName' : '新郑市',
    'py' : 'xinzheng',
    'spy' : 'xz'
}, {
    'areaCode' : '410185',
    'areaName' : '登封市',
    'py' : 'dengfeng',
    'spy' : 'df'
}]
    }, {
        'areaCode' : '410200',
        'areaName' : '开封市',
        'py' : 'kaifeng',
        'spy' : 'kf',
        'datas' : [{
            'areaCode' : '410202',
            'areaName' : '龙亭区',
            'py' : 'longting',
            'spy' : 'lt'
        }, {
            'areaCode' : '410203',
            'areaName' : '顺河回族区',
            'py' : 'shunhehuizu',
            'spy' : 'shhz'
        }, {
            'areaCode' : '410204',
            'areaName' : '鼓楼区',
            'py' : 'gulou',
            'spy' : 'gl'
        }, {
            'areaCode' : '410205',
            'areaName' : '禹王台区',
            'py' : 'yuwangtai',
            'spy' : 'ywt'
        }, {
            'areaCode' : '410211',
            'areaName' : '金明区',
            'py' : 'jinming',
            'spy' : 'jm'
        }, {
            'areaCode' : '410221',
            'areaName' : '杞县',
            'py' : 'qixian',
            'spy' : 'qx'
        }, {
            'areaCode' : '410222',
            'areaName' : '通许县',
            'py' : 'tongxu',
            'spy' : 'tg'
        }, {
        'areaCode' : '410223',
        'areaName' : '尉氏县',
        'py' : 'weishi',
        'spy' : 'ws'
    }, {
        'areaCode' : '410224',
        'areaName' : '开封县',
        'py' : 'kaifeng',
        'spy' : 'kf'
    }, {
    'areaCode' : '410225',
    'areaName' : '兰考县',
    'py' : 'lankao',
    'spy' : 'lk'
}]
    }, {
        'areaCode' : '410300',
        'areaName' : '洛阳市',
        'py' : 'luoyang',
        'spy' : 'ly',
        'datas' : [{
            'areaCode' : '410302',
            'areaName' : '老城区',
            'py' : 'laocheng',
            'spy' : 'lc'
        }, {
            'areaCode' : '410303',
            'areaName' : '西工区',
            'py' : 'xigong',
            'spy' : 'xg'
        }, {
            'areaCode' : '410304',
            'areaName' : '瀍河回族区',
            'py' : 'chanhehuizu',
            'spy' : 'chhz'
        }, {
            'areaCode' : '410305',
            'areaName' : '涧西区',
            'py' : 'jianxi',
            'spy' : 'jx'
        }, {
            'areaCode' : '410306',
            'areaName' : '吉利区',
            'py' : 'jili',
            'spy' : 'jl'
        }, {
            'areaCode' : '410307',
            'areaName' : '洛龙区',
            'py' : 'luolong',
            'spy' : 'll'
        }, {
        'areaCode' : '410322',
        'areaName' : '孟津县',
        'py' : 'mengjin',
        'spy' : 'mj'
    }, {
        'areaCode' : '410323',
        'areaName' : '新安县',
        'py' : 'xinan',
        'spy' : 'xa'
    }, {
        'areaCode' : '410324',
        'areaName' : '栾川县',
        'py' : 'luanchuan',
        'spy' : 'lc'
    }, {
    'areaCode' : '410325',
    'areaName' : '嵩县',
    'py' : 'songxian',
    'spy' : 'sx'
}, {
    'areaCode' : '410326',
    'areaName' : '汝阳县',
    'py' : 'ruyang',
    'spy' : 'ry'
}, {
    'areaCode' : '410327',
    'areaName' : '宜阳县',
    'py' : 'yiyang',
    'spy' : 'yy'
}, {
    'areaCode' : '410328',
    'areaName' : '洛宁县',
    'py' : 'luoning',
    'spy' : 'ln'
}, {
    'areaCode' : '410329',
    'areaName' : '伊川县',
    'py' : 'yichuan',
    'spy' : 'yc'
}, {
    'areaCode' : '410381',
    'areaName' : '偃师市',
    'py' : 'yanshi',
    'spy' : 'ys'
}]
    }, {
        'areaCode' : '410400',
        'areaName' : '平顶山市',
        'py' : 'pingdingshan',
        'spy' : 'pds',
        'datas' : [{
            'areaCode' : '410402',
            'areaName' : '新华区',
            'py' : 'xinhua',
            'spy' : 'xh'
        }, {
            'areaCode' : '410403',
            'areaName' : '卫东区',
            'py' : 'weidong',
            'spy' : 'wd'
        }, {
            'areaCode' : '410404',
            'areaName' : '石龙区',
            'py' : 'shilong',
            'spy' : 'sl'
        }, {
            'areaCode' : '410411',
            'areaName' : '湛河区',
            'py' : 'zhanhe',
            'spy' : 'zh'
        }, {
            'areaCode' : '410421',
            'areaName' : '宝丰县',
            'py' : 'baofeng',
            'spy' : 'bf'
        }, {
        'areaCode' : '410422',
        'areaName' : '叶县',
        'py' : 'yexian',
        'spy' : 'yx'
    }, {
        'areaCode' : '410423',
        'areaName' : '鲁山县',
        'py' : 'lushan',
        'spy' : 'ls'
    }, {
        'areaCode' : '410425',
        'areaName' : '郏县',
        'py' : 'jianxian',
        'spy' : 'jx'
    }, {
        'areaCode' : '410481',
        'areaName' : '舞钢市',
        'py' : 'wugang',
        'spy' : 'wg'
    }, {
    'areaCode' : '410482',
    'areaName' : '汝州市',
    'py' : 'ruzhou',
    'spy' : 'rz'
}]
    }, {
        'areaCode' : '410500',
        'areaName' : '安阳市',
        'py' : 'anyang',
        'spy' : 'ay',
        'datas' : [{
            'areaCode' : '410502',
            'areaName' : '文峰区',
            'py' : 'wenfeng',
            'spy' : 'wf'
        }, {
            'areaCode' : '410503',
            'areaName' : '北关区',
            'py' : 'beiguan',
            'spy' : 'bg'
        }, {
            'areaCode' : '410505',
            'areaName' : '殷都区',
            'py' : 'yindou',
            'spy' : 'yd'
        }, {
            'areaCode' : '410506',
            'areaName' : '龙安区',
            'py' : 'longan',
            'spy' : 'la'
        }, {
        'areaCode' : '410522',
        'areaName' : '安阳县',
        'py' : 'anyang',
        'spy' : 'ay'
    }, {
        'areaCode' : '410523',
        'areaName' : '汤阴县',
        'py' : 'ruyang',
        'spy' : 'ry'
    }, {
        'areaCode' : '410526',
        'areaName' : '滑县',
        'py' : 'huaxian',
        'spy' : 'hx'
    }, {
        'areaCode' : '410527',
        'areaName' : '内黄县',
        'py' : 'neihuang',
        'spy' : 'nh'
    }, {
        'areaCode' : '410581',
        'areaName' : '林州市',
        'py' : 'linzhou',
        'spy' : 'lz'
    }]
    }, {
        'areaCode' : '410600',
        'areaName' : '鹤壁市',
        'py' : 'hebi',
        'spy' : 'hb',
        'datas' : [{
            'areaCode' : '410602',
            'areaName' : '鹤山区',
            'py' : 'heshan',
            'spy' : 'hh'
        }, {
            'areaCode' : '410603',
            'areaName' : '山城区',
            'py' : 'dongcheng',
            'spy' : 'dc'
        }, {
            'areaCode' : '410611',
            'areaName' : '淇滨区',
            'py' : 'qibin',
            'spy' : 'qb'
        }, {
        'areaCode' : '410621',
        'areaName' : '浚县',
        'py' : 'junxian',
        'spy' : 'jx'
    }, {
        'areaCode' : '410622',
        'areaName' : '淇县',
        'py' : 'qixian',
        'spy' : 'qx'
    }]
    }, {
        'areaCode' : '410700',
        'areaName' : '新乡市',
        'py' : 'xinxiang',
        'spy' : 'xx',
        'datas' : [{
            'areaCode' : '410702',
            'areaName' : '红旗区',
            'py' : 'hongqi',
            'spy' : 'hq'
        }, {
            'areaCode' : '410703',
            'areaName' : '卫滨区',
            'py' : 'weibin',
            'spy' : 'wb'
        }, {
        'areaCode' : '410704',
        'areaName' : '凤泉区',
        'py' : 'fengquan',
        'spy' : 'fq'
    }, {
        'areaCode' : '410711',
        'areaName' : '牧野区',
        'py' : 'muye',
        'spy' : 'my'
    }, {
        'areaCode' : '410721',
        'areaName' : '新乡县',
        'py' : 'xinxiang',
        'spy' : 'xx'
    }, {
        'areaCode' : '410724',
        'areaName' : '获嘉县',
        'py' : 'huojia',
        'spy' : 'hj'
    }, {
        'areaCode' : '410725',
        'areaName' : '原阳县',
        'py' : 'yuanyang',
        'spy' : 'yy'
    }, {
        'areaCode' : '410726',
        'areaName' : '延津县',
        'py' : 'yanjin',
        'spy' : 'yj'
    }, {
        'areaCode' : '410727',
        'areaName' : '封丘县',
        'py' : 'fengqiu',
        'spy' : 'fq'
    }, {
    'areaCode' : '410728',
    'areaName' : '长垣县',
    'py' : 'changyuan',
    'spy' : 'cy'
}, {
    'areaCode' : '410781',
    'areaName' : '卫辉市',
    'py' : 'weihui',
    'spy' : 'wh'
}, {
    'areaCode' : '410782',
    'areaName' : '辉县市',
    'py' : 'huixian',
    'spy' : 'hx'
}]
    }, {
        'areaCode' : '410800',
        'areaName' : '焦作市',
        'py' : 'jiaozuo',
        'spy' : 'jz',
        'datas' : [{
            'areaCode' : '410802',
            'areaName' : '解放区',
            'py' : 'jiefang',
            'spy' : 'jf'
        }, {
        'areaCode' : '410803',
        'areaName' : '中站区',
        'py' : 'zhongzhan',
        'spy' : 'zz'
    }, {
        'areaCode' : '410804',
        'areaName' : '马村区',
        'py' : 'macun',
        'spy' : 'mc'
    }, {
        'areaCode' : '410811',
        'areaName' : '山阳区',
        'py' : 'shanyang',
        'spy' : 'sy'
    }, {
        'areaCode' : '410821',
        'areaName' : '修武县',
        'py' : 'xiuwu',
        'spy' : 'xw'
    }, {
        'areaCode' : '410822',
        'areaName' : '博爱县',
        'py' : 'boai',
        'spy' : 'ba'
    }, {
        'areaCode' : '410823',
        'areaName' : '武陟县',
        'py' : 'wuzhi',
        'spy' : 'wz'
    }, {
        'areaCode' : '410825',
        'areaName' : '温县',
        'py' : 'wenxian',
        'spy' : 'wx'
    }, {
        'areaCode' : '410882',
        'areaName' : '沁阳市',
        'py' : 'qinyang',
        'spy' : 'qy'
    }, {
    'areaCode' : '410883',
    'areaName' : '孟州市',
    'py' : 'mengzhou',
    'spy' : 'mz'
}]
    }, {
        'areaCode' : '410900',
        'areaName' : '濮阳市',
        'py' : 'puyang',
        'spy' : 'py',
        'datas' : [{
        'areaCode' : '410902',
        'areaName' : '华龙区',
        'py' : 'hualong',
        'spy' : 'hl'
    }, {
        'areaCode' : '410922',
        'areaName' : '清丰县',
        'py' : 'qingfeng',
        'spy' : 'qf'
    }, {
        'areaCode' : '410923',
        'areaName' : '南乐县',
        'py' : 'nanle',
        'spy' : 'nl'
    }, {
        'areaCode' : '410926',
        'areaName' : '范县',
        'py' : 'fanxian',
        'spy' : 'fx'
    }, {
        'areaCode' : '410927',
        'areaName' : '台前县',
        'py' : 'taiqian',
        'spy' : 'tq'
    }, {
        'areaCode' : '410928',
        'areaName' : '濮阳县',
        'py' : 'puyang',
        'spy' : 'py'
    }]
    }, {
    'areaCode' : '411000',
    'areaName' : '许昌市',
    'py' : 'xuchang',
    'spy' : 'xc',
    'datas' : [{
        'areaCode' : '411002',
        'areaName' : '魏都区',
        'py' : 'weidu',
        'spy' : 'wd'
    }, {
        'areaCode' : '411023',
        'areaName' : '许昌县',
        'py' : 'xuchang',
        'spy' : 'xc'
    }, {
        'areaCode' : '411024',
        'areaName' : '鄢陵县',
        'py' : 'yanling',
        'spy' : 'yl'
    }, {
        'areaCode' : '411025',
        'areaName' : '襄城县',
        'py' : 'xiangcheng',
        'spy' : 'xc'
    }, {
        'areaCode' : '411081',
        'areaName' : '禹州市',
        'py' : 'yuzhou',
        'spy' : 'yz'
    }, {
        'areaCode' : '411082',
        'areaName' : '长葛市',
        'py' : 'changge',
        'spy' : 'cg'
    }]
}, {
    'areaCode' : '411100',
    'areaName' : '漯河市',
    'py' : 'luohe',
    'spy' : 'lh',
    'datas' : [{
        'areaCode' : '411102',
        'areaName' : '源汇区',
        'py' : 'yuanhui',
        'spy' : 'yh'
    }, {
        'areaCode' : '411103',
        'areaName' : '郾城区',
        'py' : 'yancheng',
        'spy' : 'yc'
    }, {
        'areaCode' : '411104',
        'areaName' : '召陵区',
        'py' : 'zhaoling',
        'spy' : 'zl'
    }, {
        'areaCode' : '411121',
        'areaName' : '舞阳县',
        'py' : 'wuyang',
        'spy' : 'wy'
    }, {
        'areaCode' : '411122',
        'areaName' : '临颍县',
        'py' : 'linying',
        'spy' : 'ly'
    }]
}, {
    'areaCode' : '411200',
    'areaName' : '三门峡市',
    'py' : 'sanmenxia',
    'spy' : 'smx',
    'datas' : [{
        'areaCode' : '411202',
        'areaName' : '湖滨区',
        'py' : 'hubin',
        'spy' : 'hb'
    }, {
        'areaCode' : '411221',
        'areaName' : '渑池县',
        'py' : 'shengchi',
        'spy' : 'sc'
    }, {
        'areaCode' : '411222',
        'areaName' : '陕县',
        'py' : 'shanxian',
        'spy' : 'sx'
    }, {
        'areaCode' : '411224',
        'areaName' : '卢氏县',
        'py' : 'lushi',
        'spy' : 'ls'
    }, {
        'areaCode' : '411281',
        'areaName' : '义马市',
        'py' : 'yima',
        'spy' : 'ym'
    }, {
        'areaCode' : '411282',
        'areaName' : '灵宝市',
        'py' : 'lingbao',
        'spy' : 'lb'
    }]
}, {
    'areaCode' : '411300',
    'areaName' : '南阳市',
    'py' : 'nanyang',
    'spy' : 'ny',
    'datas' : [{
        'areaCode' : '411302',
        'areaName' : '宛城区',
        'py' : 'yuancheng',
        'spy' : 'yc'
    }, {
        'areaCode' : '411303',
        'areaName' : '卧龙区',
        'py' : 'wolong',
        'spy' : 'wl'
    }, {
        'areaCode' : '411321',
        'areaName' : '南召县',
        'py' : 'nanzhao',
        'spy' : 'nz'
    }, {
        'areaCode' : '411322',
        'areaName' : '方城县',
        'py' : 'fangcheng',
        'spy' : 'fc'
    }, {
        'areaCode' : '411323',
        'areaName' : '西峡县',
        'py' : 'xixia',
        'spy' : 'xx'
    }, {
        'areaCode' : '411324',
        'areaName' : '镇平县',
        'py' : 'zhenping',
        'spy' : 'zp'
    }, {
        'areaCode' : '411325',
        'areaName' : '内乡县',
        'py' : 'neixiang',
        'spy' : 'nx'
    }, {
        'areaCode' : '411326',
        'areaName' : '淅川县',
        'py' : 'xichuan',
        'spy' : 'xc'
    }, {
        'areaCode' : '411327',
        'areaName' : '社旗县',
        'py' : 'duqi',
        'spy' : 'dq'
    }, {
    'areaCode' : '411328',
    'areaName' : '唐河县',
    'py' : 'tanghe',
    'spy' : 'th'
}, {
    'areaCode' : '411329',
    'areaName' : '新野县',
    'py' : 'xinye',
    'spy' : 'xy'
}, {
    'areaCode' : '411330',
    'areaName' : '桐柏县',
    'py' : 'tongbo',
    'spy' : 'tb'
}, {
    'areaCode' : '411381',
    'areaName' : '邓州市',
    'py' : 'dengzhou',
    'spy' : 'dz'
}]
}, {
    'areaCode' : '411400',
    'areaName' : '商丘市',
    'py' : 'shangqiu',
    'spy' : 'sq',
    'datas' : [{
        'areaCode' : '411402',
        'areaName' : '梁园区',
        'py' : 'liangyuan',
        'spy' : 'ly'
    }, {
        'areaCode' : '411403',
        'areaName' : '睢阳区',
        'py' : 'suiyang',
        'spy' : 'sy'
    }, {
        'areaCode' : '411421',
        'areaName' : '民权县',
        'py' : 'minquan',
        'spy' : 'mq'
    }, {
        'areaCode' : '411422',
        'areaName' : '睢县',
        'py' : 'suixian',
        'spy' : 'sx'
    }, {
        'areaCode' : '411423',
        'areaName' : '宁陵县',
        'py' : 'ningling',
        'spy' : 'nl'
    }, {
        'areaCode' : '411424',
        'areaName' : '柘城县',
        'py' : 'zhecheng',
        'spy' : 'zc'
    }, {
        'areaCode' : '411425',
        'areaName' : '虞城县',
        'py' : 'yucheng',
        'spy' : 'yc'
    }, {
        'areaCode' : '411426',
        'areaName' : '夏邑县',
        'py' : 'xiayi',
        'spy' : 'xy'
    }, {
        'areaCode' : '411481',
        'areaName' : '永城市',
        'py' : 'yongcheng',
        'spy' : 'yc'
    }]
}, {
    'areaCode' : '411500',
    'areaName' : '信阳市',
    'py' : 'xinyang',
    'spy' : 'xy',
    'datas' : [{
        'areaCode' : '411502',
        'areaName' : '浉河区',
        'py' : 'shihe',
        'spy' : 'sh'
    }, {
        'areaCode' : '411503',
        'areaName' : '平桥区',
        'py' : 'pingqiao',
        'spy' : 'pq'
    }, {
        'areaCode' : '411521',
        'areaName' : '罗山县',
        'py' : 'luoshan',
        'spy' : 'ls'
    }, {
        'areaCode' : '411522',
        'areaName' : '光山县',
        'py' : 'guangshan',
        'spy' : 'gs'
    }, {
        'areaCode' : '411523',
        'areaName' : '新县',
        'py' : 'xinxian',
        'spy' : 'xx'
    }, {
        'areaCode' : '411524',
        'areaName' : '商城县',
        'py' : 'shangcheng',
        'spy' : 'sc'
    }, {
        'areaCode' : '411525',
        'areaName' : '固始县',
        'py' : 'gushi',
        'spy' : 'gs'
    }, {
        'areaCode' : '411526',
        'areaName' : '潢川县',
        'py' : 'huangchuan',
        'spy' : 'hc'
    }, {
        'areaCode' : '411527',
        'areaName' : '淮滨县',
        'py' : 'huaibin',
        'spy' : 'hb'
    }, {
    'areaCode' : '411528',
    'areaName' : '息县',
    'py' : 'xixian',
    'spy' : 'xx'
}]
}, {
    'areaCode' : '411600',
    'areaName' : '周口市',
    'py' : 'zhoukou',
    'spy' : 'zk',
    'datas' : [{
        'areaCode' : '411602',
        'areaName' : '川汇区',
        'py' : 'chuanhui',
        'spy' : 'ch'
    }, {
        'areaCode' : '411621',
        'areaName' : '扶沟县',
        'py' : 'fugou',
        'spy' : 'fg'
    }, {
        'areaCode' : '411622',
        'areaName' : '西华县',
        'py' : 'xihua',
        'spy' : 'xh'
    }, {
        'areaCode' : '411623',
        'areaName' : '商水县',
        'py' : 'shangshui',
        'spy' : 'ss'
    }, {
        'areaCode' : '411624',
        'areaName' : '沈丘县',
        'py' : 'shenqiu',
        'spy' : 'sq'
    }, {
        'areaCode' : '411625',
        'areaName' : '郸城县',
        'py' : 'dancheng',
        'spy' : 'dc'
    }, {
        'areaCode' : '411626',
        'areaName' : '淮阳县',
        'py' : 'huaiyang',
        'spy' : 'hy'
    }, {
        'areaCode' : '411627',
        'areaName' : '太康县',
        'py' : 'taikang',
        'spy' : 'tk'
    }, {
        'areaCode' : '411628',
        'areaName' : '鹿邑县',
        'py' : 'luyi',
        'spy' : 'ly'
    }, {
    'areaCode' : '411681',
    'areaName' : '项城市',
    'py' : 'xiangcheng',
    'spy' : 'xc'
}]
}, {
    'areaCode' : '411700',
    'areaName' : '驻马店市',
    'py' : 'zhumadian',
    'spy' : 'zmd',
    'datas' : [{
        'areaCode' : '411702',
        'areaName' : '驿城区',
        'py' : 'yicheng',
        'spy' : 'yc'
    }, {
        'areaCode' : '411721',
        'areaName' : '西平县',
        'py' : 'xiping',
        'spy' : 'xp'
    }, {
        'areaCode' : '411722',
        'areaName' : '上蔡县',
        'py' : 'shangcai',
        'spy' : 'sc'
    }, {
        'areaCode' : '411723',
        'areaName' : '平舆县',
        'py' : 'pingyu',
        'spy' : 'py'
    }, {
        'areaCode' : '411724',
        'areaName' : '正阳县',
        'py' : 'zhengyang',
        'spy' : 'zy'
    }, {
        'areaCode' : '411725',
        'areaName' : '确山县',
        'py' : 'queshan',
        'spy' : 'qs'
    }, {
        'areaCode' : '411726',
        'areaName' : '泌阳县',
        'py' : 'biyang',
        'spy' : 'by'
    }, {
        'areaCode' : '411727',
        'areaName' : '汝南县',
        'py' : 'runan',
        'spy' : 'rn'
    }, {
        'areaCode' : '411728',
        'areaName' : '遂平县',
        'py' : 'suiping',
        'spy' : 'sp'
    }, {
    'areaCode' : '411729',
    'areaName' : '新蔡县',
    'py' : 'xincai',
    'spy' : 'xc'
}]
}, {
    'areaCode' : '419000',
    'areaName' : '省直辖县级行政区划',
    'py' : 'shengzhixiaxianjixingzhengquhua',
    'spy' : 'szxxjxzqh',
    'datas' : [{
        'areaCode' : '419001',
        'areaName' : '济源市',
        'py' : 'jiyuan',
        'spy' : 'jy'
    }]
}]
}, {
    'areaCode' : '420000',
    'areaName' : '湖北省',
    'py' : 'hubei',
    'spy' : 'hb',
    'datas' : [{
        'areaCode' : '420100',
        'areaName' : '武汉市',
        'py' : 'wuhan',
        'spy' : 'wh',
        'datas' : [{
            'areaCode' : '420102',
            'areaName' : '江岸区',
            'py' : 'jiangan',
            'spy' : 'ja'
        }, {
            'areaCode' : '420103',
            'areaName' : '江汉区',
            'py' : 'jianghan',
            'spy' : 'jh'
        }, {
            'areaCode' : '420104',
            'areaName' : '硚口区',
            'py' : 'qiaokou',
            'spy' : 'qk'
        }, {
            'areaCode' : '420105',
            'areaName' : '汉阳区',
            'py' : 'hanyang',
            'spy' : 'hy'
        }, {
            'areaCode' : '420106',
            'areaName' : '武昌区',
            'py' : 'wuchang',
            'spy' : 'wc'
        }, {
            'areaCode' : '420107',
            'areaName' : '青山区',
            'py' : 'qingshan',
            'spy' : 'qs'
        }, {
            'areaCode' : '420111',
            'areaName' : '洪山区',
            'py' : 'hongshan',
            'spy' : 'hs'
        }, {
            'areaCode' : '420112',
            'areaName' : '东西湖区',
            'py' : 'dongxihu',
            'spy' : 'dxh'
        }, {
        'areaCode' : '420113',
        'areaName' : '汉南区',
        'py' : 'hannan',
        'spy' : 'hn'
    }, {
    'areaCode' : '420114',
    'areaName' : '蔡甸区',
    'py' : 'caidian',
    'spy' : 'cd'
}, {
    'areaCode' : '420115',
    'areaName' : '江夏区',
    'py' : 'jiangxia',
    'spy' : 'jx'
}, {
    'areaCode' : '420116',
    'areaName' : '黄陂区',
    'py' : 'huangpi',
    'spy' : 'hp'
}, {
    'areaCode' : '420117',
    'areaName' : '新洲区',
    'py' : 'xinzhou',
    'spy' : 'xz'
}]
    }, {
        'areaCode' : '420200',
        'areaName' : '黄石市',
        'py' : 'huangshi',
        'spy' : 'hs',
        'datas' : [{
            'areaCode' : '420202',
            'areaName' : '黄石港区',
            'py' : 'huangshigang',
            'spy' : 'hsg'
        }, {
            'areaCode' : '420203',
            'areaName' : '西塞山区',
            'py' : 'xisai',
            'spy' : 'xs'
        }, {
            'areaCode' : '420204',
            'areaName' : '下陆区',
            'py' : 'xialu',
            'spy' : 'xl'
        }, {
            'areaCode' : '420205',
            'areaName' : '铁山区',
            'py' : 'tieshan',
            'spy' : 'ts'
        }, {
            'areaCode' : '420222',
            'areaName' : '阳新县',
            'py' : 'yangxin',
            'spy' : 'yx'
        }, {
            'areaCode' : '420281',
            'areaName' : '大冶市',
            'py' : 'daye',
            'spy' : 'dy'
        }]
    }, {
        'areaCode' : '420300',
        'areaName' : '十堰市',
        'py' : 'shiyan',
        'spy' : 'sy',
        'datas' : [{
            'areaCode' : '420302',
            'areaName' : '茅箭区',
            'py' : 'maojian',
            'spy' : 'mj'
        }, {
            'areaCode' : '420303',
            'areaName' : '张湾区',
            'py' : 'zhangwan',
            'spy' : 'zw'
        }, {
            'areaCode' : '420321',
            'areaName' : '郧县',
            'py' : 'yunxian',
            'spy' : 'yx'
        }, {
            'areaCode' : '420322',
            'areaName' : '郧西县',
            'py' : 'yunxi',
            'spy' : 'yx'
        }, {
            'areaCode' : '420323',
            'areaName' : '竹山县',
            'py' : 'zhushan',
            'spy' : 'zs'
        }, {
            'areaCode' : '420324',
            'areaName' : '竹溪县',
            'py' : 'zhuxi',
            'spy' : 'zx'
        }, {
        'areaCode' : '420325',
        'areaName' : '房县',
        'py' : 'fangxian',
        'spy' : 'fx'
    }, {
        'areaCode' : '420381',
        'areaName' : '丹江口市',
        'py' : 'danjiangkou',
        'spy' : 'djk'
    }]
    }, {
        'areaCode' : '420500',
        'areaName' : '宜昌市',
        'py' : 'yichang',
        'spy' : 'yc',
        'datas' : [{
            'areaCode' : '420502',
            'areaName' : '西陵区',
            'py' : 'xiling',
            'spy' : 'xl'
        }, {
            'areaCode' : '420503',
            'areaName' : '伍家岗区',
            'py' : 'wujiagang',
            'spy' : 'wjg'
        }, {
            'areaCode' : '420504',
            'areaName' : '点军区',
            'py' : 'dianjun',
            'spy' : 'dj'
        }, {
            'areaCode' : '420505',
            'areaName' : '猇亭区',
            'py' : 'xiating',
            'spy' : 'xt'
        }, {
            'areaCode' : '420506',
            'areaName' : '夷陵区',
            'py' : 'yiling',
            'spy' : 'yl'
        }, {
        'areaCode' : '420525',
        'areaName' : '远安县',
        'py' : 'yuanan',
        'spy' : 'ya'
    }, {
        'areaCode' : '420526',
        'areaName' : '兴山县',
        'py' : 'xingshan',
        'spy' : 'xs'
    }, {
        'areaCode' : '420527',
        'areaName' : '秭归县',
        'py' : 'zigui',
        'spy' : 'zg'
    }, {
        'areaCode' : '420528',
        'areaName' : '长阳土家族自治县',
        'py' : 'changyangtujiazuzizhi',
        'spy' : 'cytjzzz'
    }, {
    'areaCode' : '420529',
    'areaName' : '五峰土家族自治县',
    'py' : 'wufengtujiazuzizhi',
    'spy' : 'wftjzzz'
}, {
    'areaCode' : '420581',
    'areaName' : '宜都市',
    'py' : 'yidu',
    'spy' : 'yd'
}, {
    'areaCode' : '420582',
    'areaName' : '当阳市',
    'py' : 'dangyang',
    'spy' : 'dy'
}, {
    'areaCode' : '420583',
    'areaName' : '枝江市',
    'py' : 'zhijiang',
    'spy' : 'zj'
}]
    }, {
        'areaCode' : '420600',
        'areaName' : '襄阳市',
        'py' : 'xiangyang',
        'spy' : 'xy',
        'datas' : [{
            'areaCode' : '420602',
            'areaName' : '襄城区',
            'py' : 'xiangcheng',
            'spy' : 'xc'
        }, {
            'areaCode' : '420606',
            'areaName' : '樊城区',
            'py' : 'fancheng',
            'spy' : 'fc'
        }, {
            'areaCode' : '420607',
            'areaName' : '襄州区',
            'py' : 'xiangzhou',
            'spy' : 'xz'
        }, {
            'areaCode' : '420624',
            'areaName' : '南漳县',
            'py' : 'nanzhang',
            'spy' : 'nz'
        }, {
        'areaCode' : '420625',
        'areaName' : '谷城县',
        'py' : 'gucheng',
        'spy' : 'gc'
    }, {
        'areaCode' : '420626',
        'areaName' : '保康县',
        'py' : 'baokang',
        'spy' : 'bk'
    }, {
        'areaCode' : '420682',
        'areaName' : '老河口市',
        'py' : 'laohekou',
        'spy' : 'lhk'
    }, {
        'areaCode' : '420683',
        'areaName' : '枣阳市',
        'py' : 'zaoyang',
        'spy' : 'zy'
    }, {
        'areaCode' : '420684',
        'areaName' : '宜城市',
        'py' : 'yicheng',
        'spy' : 'yc'
    }]
    }, {
        'areaCode' : '420700',
        'areaName' : '鄂州市',
        'py' : 'ezhou',
        'spy' : 'ez',
        'datas' : [{
            'areaCode' : '420702',
            'areaName' : '梁子湖区',
            'py' : 'liangzihu',
            'spy' : 'lzh'
        }, {
            'areaCode' : '420703',
            'areaName' : '华容区',
            'py' : 'huarong',
            'spy' : 'hr'
        }, {
            'areaCode' : '420704',
            'areaName' : '鄂城区',
            'py' : 'echeng',
            'spy' : 'ec'
        }]
    }, {
        'areaCode' : '420800',
        'areaName' : '荆门市',
        'py' : 'jingmen',
        'spy' : 'jm',
        'datas' : [{
            'areaCode' : '420802',
            'areaName' : '东宝区',
            'py' : 'dongbao',
            'spy' : 'db'
        }, {
            'areaCode' : '420804',
            'areaName' : '掇刀区',
            'py' : 'duodao',
            'spy' : 'dd'
        }, {
        'areaCode' : '420821',
        'areaName' : '京山县',
        'py' : 'jingshan',
        'spy' : 'js'
    }, {
        'areaCode' : '420822',
        'areaName' : '沙洋县',
        'py' : 'shayang',
        'spy' : 'sy'
    }, {
        'areaCode' : '420881',
        'areaName' : '钟祥市',
        'py' : 'zhongxiang',
        'spy' : 'zx'
    }]
    }, {
        'areaCode' : '420900',
        'areaName' : '孝感市',
        'py' : 'xiaogan',
        'spy' : 'xg',
        'datas' : [{
            'areaCode' : '420902',
            'areaName' : '孝南区',
            'py' : 'xiaonan',
            'spy' : 'xn'
        }, {
        'areaCode' : '420921',
        'areaName' : '孝昌县',
        'py' : 'kaochang',
        'spy' : 'kc'
    }, {
        'areaCode' : '420922',
        'areaName' : '大悟县',
        'py' : 'dawu',
        'spy' : 'dw'
    }, {
        'areaCode' : '420923',
        'areaName' : '云梦县',
        'py' : 'yunmeng',
        'spy' : 'ym'
    }, {
        'areaCode' : '420981',
        'areaName' : '应城市',
        'py' : 'yingcheng',
        'spy' : 'yc'
    }, {
        'areaCode' : '420982',
        'areaName' : '安陆市',
        'py' : 'anlu',
        'spy' : 'al'
    }, {
        'areaCode' : '420984',
        'areaName' : '汉川市',
        'py' : 'hanchuan',
        'spy' : 'hc'
    }]
    }, {
        'areaCode' : '421000',
        'areaName' : '荆州市',
        'py' : 'jingzhou',
        'spy' : 'jz',
        'datas' : [{
        'areaCode' : '421002',
        'areaName' : '沙市区',
        'py' : 'shashi',
        'spy' : 'ss'
    }, {
        'areaCode' : '421003',
        'areaName' : '荆州区',
        'py' : 'jingzhou',
        'spy' : 'jz'
    }, {
        'areaCode' : '421022',
        'areaName' : '公安县',
        'py' : 'gongan',
        'spy' : 'ga'
    }, {
        'areaCode' : '421023',
        'areaName' : '监利县',
        'py' : 'jianli',
        'spy' : 'jl'
    }, {
        'areaCode' : '421024',
        'areaName' : '江陵县',
        'py' : 'jiangling',
        'spy' : ''
    }, {
        'areaCode' : '421081',
        'areaName' : '石首市',
        'py' : 'shishou',
        'spy' : 'ss'
    }, {
        'areaCode' : '421083',
        'areaName' : '洪湖市',
        'py' : 'honghu',
        'spy' : 'hh'
    }, {
        'areaCode' : '421087',
        'areaName' : '松滋市',
        'py' : 'songzi',
        'spy' : 'sz'
    }]
    }, {
    'areaCode' : '421100',
    'areaName' : '黄冈市',
    'py' : 'huanggang',
    'spy' : 'hg',
    'datas' : [{
        'areaCode' : '421102',
        'areaName' : '黄州区',
        'py' : 'huangzhou',
        'spy' : 'hz'
    }, {
        'areaCode' : '421121',
        'areaName' : '团风县',
        'py' : 'tuanfeng',
        'spy' : 'tf'
    }, {
        'areaCode' : '421122',
        'areaName' : '红安县',
        'py' : 'hongan',
        'spy' : 'ha'
    }, {
        'areaCode' : '421123',
        'areaName' : '罗田县',
        'py' : 'luotan',
        'spy' : 'lt'
    }, {
        'areaCode' : '421124',
        'areaName' : '英山县',
        'py' : 'yingshan',
        'spy' : 'ys'
    }, {
        'areaCode' : '421125',
        'areaName' : '浠水县',
        'py' : 'xishui',
        'spy' : 'xs'
    }, {
        'areaCode' : '421126',
        'areaName' : '蕲春县',
        'py' : 'qichun',
        'spy' : 'qc'
    }, {
        'areaCode' : '421127',
        'areaName' : '黄梅县',
        'py' : 'huangmei',
        'spy' : 'hm'
    }, {
        'areaCode' : '421181',
        'areaName' : '麻城市',
        'py' : 'macheng',
        'spy' : 'mc'
    }, {
    'areaCode' : '421182',
    'areaName' : '武穴市',
    'py' : 'wuxue',
    'spy' : 'wx'
}]
}, {
    'areaCode' : '421200',
    'areaName' : '咸宁市',
    'py' : 'xianning',
    'spy' : 'xn',
    'datas' : [{
        'areaCode' : '421202',
        'areaName' : '咸安区',
        'py' : 'xianan',
        'spy' : 'xa'
    }, {
        'areaCode' : '421221',
        'areaName' : '嘉鱼县',
        'py' : 'jiayu',
        'spy' : 'jy'
    }, {
        'areaCode' : '421222',
        'areaName' : '通城县',
        'py' : 'tongcheng',
        'spy' : 'tc'
    }, {
        'areaCode' : '421223',
        'areaName' : '崇阳县',
        'py' : 'chongyang',
        'spy' : 'cy'
    }, {
        'areaCode' : '421224',
        'areaName' : '通山县',
        'py' : 'tongshan',
        'spy' : 'ts'
    }, {
        'areaCode' : '421281',
        'areaName' : '赤壁市',
        'py' : 'chibi',
        'spy' : 'cb'
    }]
}, {
    'areaCode' : '421300',
    'areaName' : '随州市',
    'py' : 'suizhou',
    'spy' : 'sz',
    'datas' : [{
        'areaCode' : '421302',
        'areaName' : '曾都区',
        'py' : 'zengdou',
        'spy' : 'zd'
    }, {
        'areaCode' : '421321',
        'areaName' : '随县',
        'py' : 'suixian',
        'spy' : 'sx'
    }, {
        'areaCode' : '421381',
        'areaName' : '广水市',
        'py' : 'guangshui',
        'spy' : 'gs'
    }]
}, {
    'areaCode' : '422800',
    'areaName' : '恩施土家族苗族自治州',
    'py' : 'enshitujiazumiaozuzizhizhou',
    'spy' : 'estjzmzzzz',
    'datas' : [{
        'areaCode' : '422801',
        'areaName' : '恩施市',
        'py' : 'enshi',
        'spy' : 'es'
    }, {
        'areaCode' : '422802',
        'areaName' : '利川市',
        'py' : 'lichuan',
        'spy' : 'lc'
    }, {
        'areaCode' : '422822',
        'areaName' : '建始县',
        'py' : 'jianshi',
        'spy' : 'js'
    }, {
        'areaCode' : '422823',
        'areaName' : '巴东县',
        'py' : 'badong',
        'spy' : 'bd'
    }, {
        'areaCode' : '422825',
        'areaName' : '宣恩县',
        'py' : 'xuanen',
        'spy' : 'xe'
    }, {
        'areaCode' : '422826',
        'areaName' : '咸丰县',
        'py' : 'xianfeng',
        'spy' : 'xf'
    }, {
        'areaCode' : '422827',
        'areaName' : '来凤县',
        'py' : 'laifeng',
        'spy' : 'lf'
    }, {
        'areaCode' : '422828',
        'areaName' : '鹤峰县',
        'py' : 'hefeng',
        'spy' : 'hf'
    }]
}, {
    'areaCode' : '429000',
    'areaName' : '省直辖县级行政区划',
    'py' : 'shengzhixiaxianjixingzhengquhua',
    'spy' : 'szxxjxzqh',
    'datas' : [{
        'areaCode' : '429004',
        'areaName' : '仙桃市',
        'py' : 'xiantao',
        'spy' : 'xt'
    }, {
        'areaCode' : '429005',
        'areaName' : '潜江市',
        'py' : 'qianjiang',
        'spy' : 'qj'
    }, {
        'areaCode' : '429006',
        'areaName' : '天门市',
        'py' : 'tianmen',
        'spy' : 'tm'
    }, {
        'areaCode' : '429021',
        'areaName' : '神农架林区',
        'py' : 'shennongjialin',
        'spy' : 'snjl'
    }]
}]
}, {
    'areaCode' : '430000',
    'areaName' : '湖南省',
    'py' : 'hunan',
    'spy' : 'hn',
    'datas' : [{
        'areaCode' : '430100',
        'areaName' : '长沙市',
        'py' : 'changsha',
        'spy' : 'cs',
        'datas' : [{
            'areaCode' : '430102',
            'areaName' : '芙蓉区',
            'py' : 'furong',
            'spy' : 'fr'
        }, {
            'areaCode' : '430103',
            'areaName' : '天心区',
            'py' : 'tianxin',
            'spy' : 'tx'
        }, {
            'areaCode' : '430104',
            'areaName' : '岳麓区',
            'py' : 'yuelu',
            'spy' : 'yl'
        }, {
            'areaCode' : '430105',
            'areaName' : '开福区',
            'py' : 'kaifu',
            'spy' : 'kf'
        }, {
            'areaCode' : '430111',
            'areaName' : '雨花区',
            'py' : 'yuhua',
            'spy' : 'yh'
        }, {
            'areaCode' : '430121',
            'areaName' : '长沙县',
            'py' : 'changsha',
            'spy' : 'cs'
        }, {
            'areaCode' : '430122',
            'areaName' : '望城区',
            'py' : 'wangcheng',
            'spy' : 'wc'
        }, {
            'areaCode' : '430124',
            'areaName' : '宁乡县',
            'py' : 'ningxiang',
            'spy' : 'nx'
        }, {
        'areaCode' : '430181',
        'areaName' : '浏阳市',
        'py' : 'liuyang',
        'spy' : 'ly'
    }]
    }, {
        'areaCode' : '430200',
        'areaName' : '株洲市',
        'py' : 'zhuzhou',
        'spy' : 'zz',
        'datas' : [{
            'areaCode' : '430202',
            'areaName' : '荷塘区',
            'py' : 'hetang',
            'spy' : 'ht'
        }, {
            'areaCode' : '430203',
            'areaName' : '芦淞区',
            'py' : 'lusong',
            'spy' : 'ls'
        }, {
            'areaCode' : '430204',
            'areaName' : '石峰区',
            'py' : 'shifeng',
            'spy' : 'sf'
        }, {
            'areaCode' : '430211',
            'areaName' : '天元区',
            'py' : 'tianyuan',
            'spy' : 'ty'
        }, {
            'areaCode' : '430221',
            'areaName' : '株洲县',
            'py' : 'zhuzhou',
            'spy' : 'zz'
        }, {
            'areaCode' : '430223',
            'areaName' : '攸县',
            'py' : 'youxian',
            'spy' : 'yx'
        }, {
            'areaCode' : '430224',
            'areaName' : '茶陵县',
            'py' : 'chaling',
            'spy' : 'cl'
        }, {
        'areaCode' : '430225',
        'areaName' : '炎陵县',
        'py' : 'yanling',
        'spy' : 'yl'
    }, {
        'areaCode' : '430281',
        'areaName' : '醴陵市',
        'py' : 'liling',
        'spy' : 'll'
    }]
    }, {
        'areaCode' : '430300',
        'areaName' : '湘潭市',
        'py' : 'xiangtan',
        'spy' : 'xt',
        'datas' : [{
            'areaCode' : '430302',
            'areaName' : '雨湖区',
            'py' : 'yuhu',
            'spy' : 'yh'
        }, {
            'areaCode' : '430304',
            'areaName' : '岳塘区',
            'py' : 'yuetang',
            'spy' : 'yt'
        }, {
            'areaCode' : '430321',
            'areaName' : '湘潭县',
            'py' : 'xiangtan',
            'spy' : 'xt'
        }, {
            'areaCode' : '430381',
            'areaName' : '湘乡市',
            'py' : 'xiangxiagn',
            'spy' : 'xx'
        }, {
            'areaCode' : '430382',
            'areaName' : '韶山市',
            'py' : 'shaoshan',
            'spy' : 'ss'
        }]
    }, {
        'areaCode' : '430400',
        'areaName' : '衡阳市',
        'py' : 'hengyang',
        'spy' : 'hy',
        'datas' : [{
            'areaCode' : '430405',
            'areaName' : '珠晖区',
            'py' : 'zhuhu',
            'spy' : 'zh'
        }, {
            'areaCode' : '430406',
            'areaName' : '雁峰区',
            'py' : 'yanfeng',
            'spy' : 'yf'
        }, {
            'areaCode' : '430407',
            'areaName' : '石鼓区',
            'py' : 'shigu',
            'spy' : 'sg'
        }, {
            'areaCode' : '430408',
            'areaName' : '蒸湘区',
            'py' : 'zhengxiang',
            'spy' : 'zx'
        }, {
            'areaCode' : '430412',
            'areaName' : '南岳区',
            'py' : 'nanyue',
            'spy' : 'ny'
        }, {
        'areaCode' : '430421',
        'areaName' : '衡阳县',
        'py' : 'hengyang',
        'spy' : 'hy'
    }, {
        'areaCode' : '430422',
        'areaName' : '衡南县',
        'py' : 'hengnan',
        'spy' : 'hn'
    }, {
        'areaCode' : '430423',
        'areaName' : '衡山县',
        'py' : 'hengshan',
        'spy' : 'hs'
    }, {
        'areaCode' : '430424',
        'areaName' : '衡东县',
        'py' : 'hengdong',
        'spy' : 'hd'
    }, {
    'areaCode' : '430426',
    'areaName' : '祁东县',
    'py' : 'qidong',
    'spy' : 'qd'
}, {
    'areaCode' : '430481',
    'areaName' : '耒阳市',
    'py' : 'laiyang',
    'spy' : 'ly'
}, {
    'areaCode' : '430482',
    'areaName' : '常宁市',
    'py' : 'changning',
    'spy' : 'cn'
}]
    }, {
        'areaCode' : '430500',
        'areaName' : '邵阳市',
        'py' : 'shaoyang',
        'spy' : 'sy',
        'datas' : [{
            'areaCode' : '430502',
            'areaName' : '双清区',
            'py' : 'shuangqing',
            'spy' : 'sq'
        }, {
            'areaCode' : '430503',
            'areaName' : '大祥区',
            'py' : 'daxiang',
            'spy' : 'dx'
        }, {
            'areaCode' : '430511',
            'areaName' : '北塔区',
            'py' : 'beita',
            'spy' : 'bt'
        }, {
            'areaCode' : '430521',
            'areaName' : '邵东县',
            'py' : 'shaodong',
            'spy' : 'sd'
        }, {
        'areaCode' : '430522',
        'areaName' : '新邵县',
        'py' : 'xinshao',
        'spy' : 'xs'
    }, {
        'areaCode' : '430523',
        'areaName' : '邵阳县',
        'py' : 'shaoyang',
        'spy' : 'sy'
    }, {
        'areaCode' : '430524',
        'areaName' : '隆回县',
        'py' : 'longhui',
        'spy' : 'lh'
    }, {
        'areaCode' : '430525',
        'areaName' : '洞口县',
        'py' : 'dongkou',
        'spy' : 'dk'
    }, {
        'areaCode' : '430527',
        'areaName' : '绥宁县',
        'py' : 'suining',
        'spy' : 'sn'
    }, {
    'areaCode' : '430528',
    'areaName' : '新宁县',
    'py' : 'xinning',
    'spy' : 'xn'
}, {
    'areaCode' : '430529',
    'areaName' : '城步苗族自治县',
    'py' : 'chengbumiaozuzizhixian',
    'spy' : 'cbmzzzx'
}, {
    'areaCode' : '430581',
    'areaName' : '武冈市',
    'py' : 'wugang',
    'spy' : 'wg'
}]
    }, {
        'areaCode' : '430600',
        'areaName' : '岳阳市',
        'py' : 'yueyang',
        'spy' : 'yy',
        'datas' : [{
            'areaCode' : '430602',
            'areaName' : '岳阳楼区',
            'py' : 'yueyanglou',
            'spy' : 'yyl'
        }, {
            'areaCode' : '430603',
            'areaName' : '云溪区',
            'py' : 'yunxi',
            'spy' : 'yx'
        }, {
            'areaCode' : '430611',
            'areaName' : '君山区',
            'py' : 'junshan',
            'spy' : 'js'
        }, {
        'areaCode' : '430621',
        'areaName' : '岳阳县',
        'py' : 'yueyang',
        'spy' : 'yy'
    }, {
        'areaCode' : '430623',
        'areaName' : '华容县',
        'py' : 'huarong',
        'spy' : 'hr'
    }, {
        'areaCode' : '430624',
        'areaName' : '湘阴县',
        'py' : 'xiangyin',
        'spy' : 'xy'
    }, {
        'areaCode' : '430626',
        'areaName' : '平江县',
        'py' : 'pingjiang',
        'spy' : 'pj'
    }, {
        'areaCode' : '430681',
        'areaName' : '汨罗市',
        'py' : 'leiluo',
        'spy' : 'll'
    }, {
        'areaCode' : '430682',
        'areaName' : '临湘市',
        'py' : 'linxiang',
        'spy' : 'lx'
    }]
    }, {
        'areaCode' : '430700',
        'areaName' : '常德市',
        'py' : 'changde',
        'spy' : 'cd',
        'datas' : [{
            'areaCode' : '430702',
            'areaName' : '武陵区',
            'py' : 'wuling',
            'spy' : 'wl'
        }, {
            'areaCode' : '430703',
            'areaName' : '鼎城区',
            'py' : 'dingcheng',
            'spy' : 'dc'
        }, {
        'areaCode' : '430721',
        'areaName' : '安乡县',
        'py' : 'anxiang',
        'spy' : 'ax'
    }, {
        'areaCode' : '430722',
        'areaName' : '汉寿县',
        'py' : 'hanshou',
        'spy' : 'hs'
    }, {
        'areaCode' : '430723',
        'areaName' : '澧县',
        'py' : 'lixian',
        'spy' : 'lx'
    }, {
        'areaCode' : '430724',
        'areaName' : '临澧县',
        'py' : 'linli',
        'spy' : 'll'
    }, {
        'areaCode' : '430725',
        'areaName' : '桃源县',
        'py' : 'taoyuan',
        'spy' : 'ty'
    }, {
        'areaCode' : '430726',
        'areaName' : '石门县',
        'py' : 'shimen',
        'spy' : 'sm'
    }, {
        'areaCode' : '430781',
        'areaName' : '津市市',
        'py' : 'jinshi',
        'spy' : 'js'
    }]
    }, {
        'areaCode' : '430800',
        'areaName' : '张家界市',
        'py' : 'zhangjiajie',
        'spy' : 'zjj',
        'datas' : [{
            'areaCode' : '430802',
            'areaName' : '永定区',
            'py' : 'yongding',
            'spy' : 'yd'
        }, {
        'areaCode' : '430811',
        'areaName' : '武陵源区',
        'py' : 'wulingyuan',
        'spy' : 'wly'
    }, {
        'areaCode' : '430821',
        'areaName' : '慈利县',
        'py' : 'cili',
        'spy' : 'cl'
    }, {
        'areaCode' : '430822',
        'areaName' : '桑植县',
        'py' : 'sangzhi',
        'spy' : 'sz'
    }]
    }, {
        'areaCode' : '430900',
        'areaName' : '益阳市',
        'py' : 'yiyang',
        'spy' : 'yy',
        'datas' : [{
        'areaCode' : '430902',
        'areaName' : '资阳区',
        'py' : 'ziyang',
        'spy' : 'zy'
    }, {
        'areaCode' : '430903',
        'areaName' : '赫山区',
        'py' : 'heshan',
        'spy' : 'hs'
    }, {
        'areaCode' : '430921',
        'areaName' : '南县',
        'py' : 'nanxian',
        'spy' : 'nx'
    }, {
        'areaCode' : '430922',
        'areaName' : '桃江县',
        'py' : 'taojiang',
        'spy' : 'tj'
    }, {
        'areaCode' : '430923',
        'areaName' : '安化县',
        'py' : 'anhua',
        'spy' : 'ah'
    }, {
        'areaCode' : '430981',
        'areaName' : '沅江市',
        'py' : 'yuanjiang',
        'spy' : 'yj'
    }]
    }, {
    'areaCode' : '431000',
    'areaName' : '郴州市',
    'py' : 'chenzhou',
    'spy' : 'cz',
    'datas' : [{
        'areaCode' : '431002',
        'areaName' : '北湖区',
        'py' : 'beihu',
        'spy' : 'bh'
    }, {
        'areaCode' : '431003',
        'areaName' : '苏仙区',
        'py' : 'suxian',
        'spy' : 'sx'
    }, {
        'areaCode' : '431021',
        'areaName' : '桂阳县',
        'py' : 'guiyang',
        'spy' : 'gy'
    }, {
        'areaCode' : '431022',
        'areaName' : '宜章县',
        'py' : 'yizhang',
        'spy' : 'yz'
    }, {
        'areaCode' : '431023',
        'areaName' : '永兴县',
        'py' : 'yongxing',
        'spy' : 'yx'
    }, {
        'areaCode' : '431024',
        'areaName' : '嘉禾县',
        'py' : 'jiahe',
        'spy' : 'jh'
    }, {
        'areaCode' : '431025',
        'areaName' : '临武县',
        'py' : 'linwu',
        'spy' : 'lw'
    }, {
        'areaCode' : '431026',
        'areaName' : '汝城县',
        'py' : 'rucheng',
        'spy' : 'rc'
    }, {
        'areaCode' : '431027',
        'areaName' : '桂东县',
        'py' : 'guidong',
        'spy' : 'gd'
    }, {
    'areaCode' : '431028',
    'areaName' : '安仁县',
    'py' : 'anren',
    'spy' : 'ar'
}, {
    'areaCode' : '431081',
    'areaName' : '资兴市',
    'py' : 'zixing',
    'spy' : 'zx'
}]
}, {
    'areaCode' : '431100',
    'areaName' : '永州市',
    'py' : 'yongzhou',
    'spy' : 'yz',
    'datas' : [{
        'areaCode' : '431102',
        'areaName' : '零陵区',
        'py' : 'lingling',
        'spy' : 'll'
    }, {
        'areaCode' : '431103',
        'areaName' : '冷水滩区',
        'py' : 'lengshuitan',
        'spy' : 'lst'
    }, {
        'areaCode' : '431121',
        'areaName' : '祁阳县',
        'py' : 'qiyang',
        'spy' : 'qy'
    }, {
        'areaCode' : '431122',
        'areaName' : '东安县',
        'py' : 'dongan',
        'spy' : 'da'
    }, {
        'areaCode' : '431123',
        'areaName' : '双牌县',
        'py' : 'shuangpai',
        'spy' : 'sp'
    }, {
        'areaCode' : '431124',
        'areaName' : '道县',
        'py' : 'daoxian',
        'spy' : 'dx'
    }, {
        'areaCode' : '431125',
        'areaName' : '江永县',
        'py' : 'jiangyong',
        'spy' : 'jy'
    }, {
        'areaCode' : '431126',
        'areaName' : '宁远县',
        'py' : 'ningyuan',
        'spy' : 'ny'
    }, {
        'areaCode' : '431127',
        'areaName' : '蓝山县',
        'py' : 'lanshan',
        'spy' : 'ls'
    }, {
    'areaCode' : '431128',
    'areaName' : '新田县',
    'py' : 'xintian',
    'spy' : 'xt'
}, {
    'areaCode' : '431129',
    'areaName' : '江华瑶族自治县',
    'py' : 'jianghuayaozuzizhixian',
    'spy' : 'jhyzzzx'
}]
}, {
    'areaCode' : '431200',
    'areaName' : '怀化市',
    'py' : 'huaihua',
    'spy' : 'hh',
    'datas' : [{
        'areaCode' : '431202',
        'areaName' : '鹤城区',
        'py' : 'hecheng',
        'spy' : 'hc'
    }, {
        'areaCode' : '431221',
        'areaName' : '中方县',
        'py' : 'zhongfang',
        'spy' : 'zf'
    }, {
        'areaCode' : '431222',
        'areaName' : '沅陵县',
        'py' : 'yuanling',
        'spy' : 'yl'
    }, {
        'areaCode' : '431223',
        'areaName' : '辰溪县',
        'py' : 'chenxi',
        'spy' : 'cx'
    }, {
        'areaCode' : '431224',
        'areaName' : '溆浦县',
        'py' : 'xupu',
        'spy' : 'xp'
    }, {
        'areaCode' : '431225',
        'areaName' : '会同县',
        'py' : 'hetong',
        'spy' : 'ht'
    }, {
        'areaCode' : '431226',
        'areaName' : '麻阳苗族自治县',
        'py' : 'mayangmiaozuzizhixian',
        'spy' : 'mymzzzx'
    }, {
        'areaCode' : '431227',
        'areaName' : '新晃侗族自治县',
        'py' : 'xinhuangtongzuzizhixian',
        'spy' : 'xhtzzzx'
    }, {
        'areaCode' : '431228',
        'areaName' : '芷江侗族自治县',
        'py' : 'zhijiangtongzuzizhixian',
        'spy' : 'zjtzzzx'
    }, {
    'areaCode' : '431229',
    'areaName' : '靖州苗族侗族自治县',
    'py' : 'jingzhoumiaozutongzuzizhixian',
    'spy' : 'jzmztzzzx'
}, {
    'areaCode' : '431230',
    'areaName' : '通道侗族自治县',
    'py' : 'tongdaotongzuzizhixian',
    'spy' : 'tdtzzzx'
}, {
    'areaCode' : '431281',
    'areaName' : '洪江市',
    'py' : 'hongjiang',
    'spy' : 'hj'
}]
}, {
    'areaCode' : '431300',
    'areaName' : '娄底市',
    'py' : 'loudi',
    'spy' : 'ld',
    'datas' : [{
        'areaCode' : '431302',
        'areaName' : '娄星区',
        'py' : 'louxing',
        'spy' : 'lx'
    }, {
        'areaCode' : '431321',
        'areaName' : '双峰县',
        'py' : 'shuangfeng',
        'spy' : 'sf'
    }, {
        'areaCode' : '431322',
        'areaName' : '新化县',
        'py' : 'xinhua',
        'spy' : 'xh'
    }, {
        'areaCode' : '431381',
        'areaName' : '冷水江市',
        'py' : 'lengshuijiang',
        'spy' : 'lsj'
    }, {
        'areaCode' : '431382',
        'areaName' : '涟源市',
        'py' : 'lianyuan',
        'spy' : 'ly'
    }]
}, {
    'areaCode' : '433100',
    'areaName' : '湘西土家族苗族自治州',
    'py' : 'xiangxitujiazumiaozuzizhizhou',
    'spy' : 'xxtjzmzzzz',
    'datas' : [{
        'areaCode' : '433101',
        'areaName' : '吉首市',
        'py' : 'jishou',
        'spy' : 'js'
    }, {
        'areaCode' : '433122',
        'areaName' : '泸溪县',
        'py' : 'luxi',
        'spy' : 'lx'
    }, {
        'areaCode' : '433123',
        'areaName' : '凤凰县',
        'py' : 'fenghuang',
        'spy' : 'fh'
    }, {
        'areaCode' : '433124',
        'areaName' : '花垣县',
        'py' : 'huayuan',
        'spy' : 'hy'
    }, {
        'areaCode' : '433125',
        'areaName' : '保靖县',
        'py' : 'baojing',
        'spy' : 'bj'
    }, {
        'areaCode' : '433126',
        'areaName' : '古丈县',
        'py' : 'guzhang',
        'spy' : 'gz'
    }, {
        'areaCode' : '433127',
        'areaName' : '永顺县',
        'py' : 'yongfeng',
        'spy' : 'yf'
    }, {
        'areaCode' : '433130',
        'areaName' : '龙山县',
        'py' : 'longshan',
        'spy' : 'ls'
    }]
}]
}, {
    'areaCode' : '440000',
    'areaName' : '广东省',
    'py' : 'guangdong',
    'spy' : 'gd',
    'datas' : [{
        'areaCode' : '440100',
        'areaName' : '广州市',
        'py' : 'guangzhou',
        'spy' : 'gz',
        'datas' : [{
            'areaCode' : '440102',
            'areaName' : '东山区',
            'py' : 'dongshan',
            'spy' : 'ds'
        }, {
            'areaCode' : '440103',
            'areaName' : '荔湾区',
            'py' : 'liwan',
            'spy' : 'lw'
        }, {
            'areaCode' : '440104',
            'areaName' : '越秀区',
            'py' : 'yuexiu',
            'spy' : 'yx'
        }, {
            'areaCode' : '440105',
            'areaName' : '海珠区',
            'py' : 'haizhu',
            'spy' : 'hz'
        }, {
            'areaCode' : '440106',
            'areaName' : '天河区',
            'py' : 'tianhe',
            'spy' : 'th'
        }, {
            'areaCode' : '440107',
            'areaName' : '芳村区',
            'py' : 'fangcun',
            'spy' : 'fc'
        }, {
            'areaCode' : '440111',
            'areaName' : '白云区',
            'py' : 'baiyun',
            'spy' : 'by'
        }, {
            'areaCode' : '440112',
            'areaName' : '黄埔区',
            'py' : 'huangpu',
            'spy' : 'hp'
        }, {
        'areaCode' : '440113',
        'areaName' : '番禺区',
        'py' : 'panyu',
        'spy' : 'py'
    }, {
    'areaCode' : '440114',
    'areaName' : '花都区',
    'py' : 'huadu',
    'spy' : 'hd'
}, {
    'areaCode' : '440115',
    'areaName' : '南沙区',
    'py' : 'nansha',
    'spy' : 'ns'
}, {
    'areaCode' : '440116',
    'areaName' : '萝岗区',
    'py' : 'luogang',
    'spy' : 'lg'
}, {
    'areaCode' : '440183',
    'areaName' : '增城市',
    'py' : 'zengcheng',
    'spy' : 'zc'
}, {
    'areaCode' : '440184',
    'areaName' : '从化市',
    'py' : 'conghua',
    'spy' : 'ch'
}]
    }, {
        'areaCode' : '440200',
        'areaName' : '韶关市',
        'py' : 'shaoguan',
        'spy' : 'sg',
        'datas' : [{
            'areaCode' : '440203',
            'areaName' : '武江区',
            'py' : 'wujiang',
            'spy' : 'wj'
        }, {
            'areaCode' : '440204',
            'areaName' : '浈江区',
            'py' : 'zhenjiang',
            'spy' : 'zj'
        }, {
            'areaCode' : '440205',
            'areaName' : '曲江区',
            'py' : 'qujiang',
            'spy' : 'qj'
        }, {
            'areaCode' : '440222',
            'areaName' : '始兴县',
            'py' : 'shixing',
            'spy' : 'sx'
        }, {
            'areaCode' : '440224',
            'areaName' : '仁化县',
            'py' : 'renhua',
            'spy' : 'rh'
        }, {
            'areaCode' : '440229',
            'areaName' : '翁源县',
            'py' : 'wengyuan',
            'spy' : 'wy'
        }, {
            'areaCode' : '440232',
            'areaName' : '乳源瑶族自治县',
            'py' : 'ruyuanyaozuzizhixian',
            'spy' : 'ryyzzzx'
        }, {
        'areaCode' : '440233',
        'areaName' : '新丰县',
        'py' : 'xinfeng',
        'spy' : 'xf'
    }, {
        'areaCode' : '440281',
        'areaName' : '乐昌市',
        'py' : 'lecang',
        'spy' : 'lc'
    }, {
    'areaCode' : '440282',
    'areaName' : '南雄市',
    'py' : 'nanxiong',
    'spy' : 'nx'
}]
    }, {
        'areaCode' : '440300',
        'areaName' : '深圳市',
        'py' : 'shenzhen',
        'spy' : 'sz',
        'datas' : [{
            'areaCode' : '440303',
            'areaName' : '罗湖区',
            'py' : 'luohu',
            'spy' : 'lh'
        }, {
            'areaCode' : '440304',
            'areaName' : '福田区',
            'py' : 'futian',
            'spy' : 'ft'
        }, {
            'areaCode' : '440305',
            'areaName' : '南山区',
            'py' : 'nanshan',
            'spy' : 'ns'
        }, {
            'areaCode' : '440306',
            'areaName' : '宝安区',
            'py' : 'baoan',
            'spy' : 'ba'
        }, {
            'areaCode' : '440307',
            'areaName' : '龙岗区',
            'py' : 'longgang',
            'spy' : 'lg'
        }, {
            'areaCode' : '440308',
            'areaName' : '盐田区',
            'py' : 'yantian',
            'spy' : 'yt'
        }]
    }, {
        'areaCode' : '440400',
        'areaName' : '珠海市',
        'py' : 'zhuhai',
        'spy' : 'zh',
        'datas' : [{
            'areaCode' : '440402',
            'areaName' : '香洲区',
            'py' : 'xiangzhou',
            'spy' : 'xz'
        }, {
            'areaCode' : '440403',
            'areaName' : '斗门区',
            'py' : 'doumen',
            'spy' : 'dm'
        }, {
            'areaCode' : '440404',
            'areaName' : '金湾区',
            'py' : 'jinwan',
            'spy' : 'jw'
        }]
    }, {
        'areaCode' : '440500',
        'areaName' : '汕头市',
        'py' : 'shantou',
        'spy' : 'st',
        'datas' : [{
            'areaCode' : '440507',
            'areaName' : '龙湖区',
            'py' : 'longhu',
            'spy' : 'lh'
        }, {
            'areaCode' : '440511',
            'areaName' : '金平区',
            'py' : 'jinping',
            'spy' : 'jp'
        }, {
            'areaCode' : '440512',
            'areaName' : '濠江区',
            'py' : 'haojang',
            'spy' : 'hj'
        }, {
            'areaCode' : '440513',
            'areaName' : '潮阳区',
            'py' : 'chaoyang',
            'spy' : 'cy'
        }, {
        'areaCode' : '440514',
        'areaName' : '潮南区',
        'py' : 'chaonan',
        'spy' : 'cn'
    }, {
        'areaCode' : '440515',
        'areaName' : '澄海区',
        'py' : 'chenghai',
        'spy' : 'ch'
    }, {
        'areaCode' : '440523',
        'areaName' : '南澳县',
        'py' : 'nanao',
        'spy' : 'na'
    }]
    }, {
        'areaCode' : '440600',
        'areaName' : '佛山市',
        'py' : 'foushan',
        'spy' : 'fs',
        'datas' : [{
            'areaCode' : '440604',
            'areaName' : '禅城区',
            'py' : 'chancheng',
            'spy' : 'cc'
        }, {
            'areaCode' : '440605',
            'areaName' : '南海区',
            'py' : 'nanwan',
            'spy' : 'nw'
        }, {
            'areaCode' : '440606',
            'areaName' : '顺德区',
            'py' : 'shunde',
            'spy' : 'sd'
        }, {
        'areaCode' : '440607',
        'areaName' : '三水区',
        'py' : 'sanshui',
        'spy' : 'ss'
    }, {
        'areaCode' : '440608',
        'areaName' : '高明区',
        'py' : 'gaoming',
        'spy' : 'gm'
    }]
    }, {
        'areaCode' : '440700',
        'areaName' : '江门市',
        'py' : 'jiangmen',
        'spy' : 'jm',
        'datas' : [{
            'areaCode' : '440703',
            'areaName' : '蓬江区',
            'py' : 'pengjiang',
            'spy' : 'pj'
        }, {
            'areaCode' : '440704',
            'areaName' : '江海区',
            'py' : 'jianghai',
            'spy' : 'jh'
        }, {
        'areaCode' : '440705',
        'areaName' : '新会区',
        'py' : 'xinhui',
        'spy' : 'xh'
    }, {
        'areaCode' : '440781',
        'areaName' : '台山市',
        'py' : 'taishan',
        'spy' : 'ts'
    }, {
        'areaCode' : '440783',
        'areaName' : '开平市',
        'py' : 'kaiping',
        'spy' : 'kp'
    }, {
        'areaCode' : '440784',
        'areaName' : '鹤山市',
        'py' : 'heshan',
        'spy' : 'hs'
    }, {
        'areaCode' : '440785',
        'areaName' : '恩平市',
        'py' : 'enping',
        'spy' : 'ep'
    }]
    }, {
        'areaCode' : '440800',
        'areaName' : '湛江市',
        'py' : 'zhanjiang',
        'spy' : 'zj',
        'datas' : [{
            'areaCode' : '440802',
            'areaName' : '赤坎区',
            'py' : 'chikan',
            'spy' : 'ck'
        }, {
        'areaCode' : '440803',
        'areaName' : '霞山区',
        'py' : 'xiashan',
        'spy' : 'xs'
    }, {
        'areaCode' : '440804',
        'areaName' : '坡头区',
        'py' : 'potou',
        'spy' : 'pt'
    }, {
        'areaCode' : '440811',
        'areaName' : '麻章区',
        'py' : 'mazhang',
        'spy' : 'mz'
    }, {
        'areaCode' : '440823',
        'areaName' : '遂溪县',
        'py' : 'suixi',
        'spy' : 'sx'
    }, {
        'areaCode' : '440825',
        'areaName' : '徐闻县',
        'py' : 'xuwen',
        'spy' : 'xw'
    }, {
        'areaCode' : '440881',
        'areaName' : '廉江市',
        'py' : 'lianjiang',
        'spy' : 'lj'
    }, {
        'areaCode' : '440882',
        'areaName' : '雷州市',
        'py' : 'leizhou',
        'spy' : 'lz'
    }, {
        'areaCode' : '440883',
        'areaName' : '吴川市',
        'py' : 'wuchuan',
        'spy' : 'wc'
    }]
    }, {
        'areaCode' : '440900',
        'areaName' : '茂名市',
        'py' : 'maoming',
        'spy' : 'mm',
        'datas' : [{
        'areaCode' : '440902',
        'areaName' : '茂南区',
        'py' : 'maonan',
        'spy' : 'mn'
    }, {
        'areaCode' : '440903',
        'areaName' : '茂港区',
        'py' : 'maogang',
        'spy' : 'mg'
    }, {
        'areaCode' : '440923',
        'areaName' : '电白县',
        'py' : 'dianbai',
        'spy' : 'db'
    }, {
        'areaCode' : '440981',
        'areaName' : '高州市',
        'py' : 'gaozhou',
        'spy' : 'gz'
    }, {
        'areaCode' : '440982',
        'areaName' : '化州市',
        'py' : 'huazhou',
        'spy' : 'hz'
    }, {
        'areaCode' : '440983',
        'areaName' : '信宜市',
        'py' : 'xinyi',
        'spy' : 'xy'
    }]
    }, {
    'areaCode' : '441200',
    'areaName' : '肇庆市',
    'py' : 'zhaoqing',
    'spy' : 'zq',
    'datas' : [{
        'areaCode' : '441202',
        'areaName' : '端州区',
        'py' : 'duanzhou',
        'spy' : 'dz'
    }, {
        'areaCode' : '441203',
        'areaName' : '鼎湖区',
        'py' : 'dinghu',
        'spy' : 'dh'
    }, {
        'areaCode' : '441223',
        'areaName' : '广宁县',
        'py' : 'guangning',
        'spy' : 'gn'
    }, {
        'areaCode' : '441224',
        'areaName' : '怀集县',
        'py' : 'huaiji',
        'spy' : 'hj'
    }, {
        'areaCode' : '441225',
        'areaName' : '封开县',
        'py' : 'fengkai',
        'spy' : 'fk'
    }, {
        'areaCode' : '441226',
        'areaName' : '德庆县',
        'py' : 'deqing',
        'spy' : 'dq'
    }, {
        'areaCode' : '441283',
        'areaName' : '高要市',
        'py' : 'gaoyao',
        'spy' : 'gy'
    }, {
        'areaCode' : '441284',
        'areaName' : '四会市',
        'py' : 'sihui',
        'spy' : 'sh'
    }]
}, {
    'areaCode' : '441300',
    'areaName' : '惠州市',
    'py' : 'huizhou',
    'spy' : 'hz',
    'datas' : [{
        'areaCode' : '441302',
        'areaName' : '惠城区',
        'py' : 'huicheng',
        'spy' : 'hc'
    }, {
        'areaCode' : '441303',
        'areaName' : '惠阳区',
        'py' : 'huiyang',
        'spy' : 'hy'
    }, {
        'areaCode' : '441322',
        'areaName' : '博罗县',
        'py' : 'boluo',
        'spy' : 'bl'
    }, {
        'areaCode' : '441323',
        'areaName' : '惠东县',
        'py' : 'huidong',
        'spy' : 'hd'
    }, {
        'areaCode' : '441324',
        'areaName' : '龙门县',
        'py' : 'longmen',
        'spy' : 'lm'
    }]
}, {
    'areaCode' : '441400',
    'areaName' : '梅州市',
    'py' : 'meizhou',
    'spy' : 'mz',
    'datas' : [{
        'areaCode' : '441402',
        'areaName' : '梅江区',
        'py' : 'meijiang',
        'spy' : 'mj'
    }, {
        'areaCode' : '441421',
        'areaName' : '梅县',
        'py' : 'meixian',
        'spy' : 'mx'
    }, {
        'areaCode' : '441422',
        'areaName' : '大埔县',
        'py' : 'dapu',
        'spy' : 'dp'
    }, {
        'areaCode' : '441423',
        'areaName' : '丰顺县',
        'py' : 'fengshun',
        'spy' : 'fs'
    }, {
        'areaCode' : '441424',
        'areaName' : '五华县',
        'py' : 'wuhua',
        'spy' : 'wh'
    }, {
        'areaCode' : '441426',
        'areaName' : '平远县',
        'py' : 'pingyuan',
        'spy' : 'py'
    }, {
        'areaCode' : '441427',
        'areaName' : '蕉岭县',
        'py' : 'jiaoling',
        'spy' : 'jl'
    }, {
        'areaCode' : '441481',
        'areaName' : '兴宁市',
        'py' : 'xingning',
        'spy' : 'xn'
    }]
}, {
    'areaCode' : '441500',
    'areaName' : '汕尾市',
    'py' : 'shanwei',
    'spy' : 'sw',
    'datas' : [{
        'areaCode' : '441502',
        'areaName' : '城区',
        'py' : 'chengqu',
        'spy' : 'cq'
    }, {
        'areaCode' : '441521',
        'areaName' : '海丰县',
        'py' : 'haifeng',
        'spy' : 'hf'
    }, {
        'areaCode' : '441523',
        'areaName' : '陆河县',
        'py' : 'luhe',
        'spy' : 'lh'
    }, {
        'areaCode' : '441581',
        'areaName' : '陆丰市',
        'py' : 'lufeng',
        'spy' : 'lf'
    }]
}, {
    'areaCode' : '441600',
    'areaName' : '河源市',
    'py' : 'heyuan',
    'spy' : 'hy',
    'datas' : [{
        'areaCode' : '441602',
        'areaName' : '源城区',
        'py' : 'yuancheng',
        'spy' : 'yc'
    }, {
        'areaCode' : '441621',
        'areaName' : '紫金县',
        'py' : 'zijin',
        'spy' : 'zj'
    }, {
        'areaCode' : '441622',
        'areaName' : '龙川县',
        'py' : 'longchuan',
        'spy' : 'lc'
    }, {
        'areaCode' : '441623',
        'areaName' : '连平县',
        'py' : 'lianping',
        'spy' : 'lp'
    }, {
        'areaCode' : '441624',
        'areaName' : '和平县',
        'py' : 'heping',
        'spy' : 'hp'
    }, {
        'areaCode' : '441625',
        'areaName' : '东源县',
        'py' : 'dongyuan',
        'spy' : 'dy'
    }]
}, {
    'areaCode' : '441700',
    'areaName' : '阳江市',
    'py' : 'yangjiang',
    'spy' : 'yj',
    'datas' : [{
        'areaCode' : '441702',
        'areaName' : '江城区',
        'py' : 'jiangcheng',
        'spy' : 'jc'
    }, {
        'areaCode' : '441721',
        'areaName' : '阳西县',
        'py' : 'yangxi',
        'spy' : 'yx'
    }, {
        'areaCode' : '441723',
        'areaName' : '阳东县',
        'py' : 'yangdong',
        'spy' : 'yd'
    }, {
        'areaCode' : '441781',
        'areaName' : '阳春市',
        'py' : 'yangchun',
        'spy' : 'yc'
    }]
}, {
    'areaCode' : '441800',
    'areaName' : '清远市',
    'py' : 'qingyuan',
    'spy' : 'qy',
    'datas' : [{
        'areaCode' : '441802',
        'areaName' : '清城区',
        'py' : 'qingcheng',
        'spy' : 'qc'
    }, {
        'areaCode' : '441821',
        'areaName' : '佛冈县',
        'py' : 'fougang',
        'spy' : 'fg'
    }, {
        'areaCode' : '441823',
        'areaName' : '阳山县',
        'py' : 'yangshan',
        'spy' : 'ys'
    }, {
        'areaCode' : '441825',
        'areaName' : '连山壮族瑶族自治县',
        'py' : 'lianshanzhuangzuyaozuzizhixian',
        'spy' : 'lszzyzzzx'
    }, {
        'areaCode' : '441826',
        'areaName' : '连南瑶族自治县',
        'py' : 'liannanyaozuzizhixian',
        'spy' : 'lnyzzzx'
    }, {
        'areaCode' : '441827',
        'areaName' : '清新县',
        'py' : 'qingxin',
        'spy' : 'qx'
    }, {
        'areaCode' : '441881',
        'areaName' : '英德市',
        'py' : 'yingde',
        'spy' : 'yd'
    }, {
        'areaCode' : '441882',
        'areaName' : '连州市',
        'py' : 'lianzhou',
        'spy' : 'lz'
    }]
}, {
    'areaCode' : '441900',
    'areaName' : '东莞市',
    'py' : 'donguan',
    'spy' : 'dg',
    'datas' : [{
        'areaCode' : '441901',
        'areaName' : '南城区',
        'py' : 'nancheng',
        'spy' : 'nc'
    }, {
        'areaCode' : '441902',
        'areaName' : '茶山镇',
        'py' : 'chashanzhen',
        'spy' : 'csz'
    }, {
        'areaCode' : '441903',
        'areaName' : '万江区',
        'py' : 'wanjiang',
        'spy' : 'wj'
    }, {
        'areaCode' : '441904',
        'areaName' : '石龙镇',
        'py' : 'shilongzhen',
        'spy' : 'slz'
    }, {
        'areaCode' : '441905',
        'areaName' : '虎门镇',
        'py' : 'humenzhen',
        'spy' : 'hmz'
    }, {
        'areaCode' : '441906',
        'areaName' : '中堂镇',
        'py' : 'zhongtangzhen',
        'spy' : 'ztz'
    }, {
        'areaCode' : '441907',
        'areaName' : '麻涌镇',
        'py' : 'mayongz',
        'spy' : 'myz'
    }, {
        'areaCode' : '441908',
        'areaName' : '石碣镇',
        'py' : 'shijiezhen',
        'spy' : 'sjz'
    }, {
        'areaCode' : '441909',
        'areaName' : '高埗镇',
        'py' : 'gaobuzhen',
        'spy' : 'gbz'
    }, {
    'areaCode' : '441910',
    'areaName' : '洪梅镇',
    'py' : 'hongmeizhen',
    'spy' : 'hmz'
}, {
    'areaCode' : '441911',
    'areaName' : '望牛墩镇',
    'py' : 'wangniudunzhen',
    'spy' : 'wndz'
}, {
    'areaCode' : '441912',
    'areaName' : '道滘镇',
    'py' : 'daojiaozhen',
    'spy' : 'djz'
}, {
    'areaCode' : '441913',
    'areaName' : '厚街镇',
    'py' : 'houjiezhen',
    'spy' : 'hjz'
}, {
    'areaCode' : '441914',
    'areaName' : '沙田镇',
    'py' : 'shatianzhen',
    'spy' : 'stz'
}, {
    'areaCode' : '441915',
    'areaName' : '长安镇',
    'py' : 'changanzhen',
    'spy' : 'caz'
}, {
    'areaCode' : '441916',
    'areaName' : '寮步镇',
    'py' : 'liaobuzhen',
    'spy' : 'lbz'
}, {
    'areaCode' : '441917',
    'areaName' : '大朗镇',
    'py' : 'dalangzhen',
    'spy' : 'dlz'
}, {
    'areaCode' : '441918',
    'areaName' : '黄江镇',
    'py' : 'huangjiangzhen',
    'spy' : 'hjz'
}, {
    'areaCode' : '441919',
    'areaName' : '大岭山镇',
    'py' : 'dalingshan镇',
    'spy' : 'dlsz'
}, {
    'areaCode' : '441920',
    'areaName' : '凤岗镇',
    'py' : 'fenggangzhen',
    'spy' : 'fgz'
}, {
    'areaCode' : '441921',
    'areaName' : '塘厦镇',
    'py' : 'tangxiazhen',
    'spy' : 'txz'
}, {
    'areaCode' : '441922',
    'areaName' : '谢岗镇',
    'py' : 'xiegangzhen',
    'spy' : 'xgz'
}, {
    'areaCode' : '441923',
    'areaName' : '清溪镇',
    'py' : 'qingxizhen',
    'spy' : 'qxz'
}, {
    'areaCode' : '441924',
    'areaName' : '常平镇',
    'py' : 'changpingzhen',
    'spy' : 'cpz'
}, {
    'areaCode' : '441925',
    'areaName' : '桥头镇',
    'py' : 'qiaotouzhen',
    'spy' : 'qtz'
}, {
    'areaCode' : '441926',
    'areaName' : '横沥镇',
    'py' : 'henglizhen',
    'spy' : 'hlz'
}, {
    'areaCode' : '441927',
    'areaName' : '樟木头镇',
    'py' : 'zhangmutouzhen',
    'spy' : 'zmtz'
}, {
    'areaCode' : '441928',
    'areaName' : '东坑镇',
    'py' : 'dongkengzhen',
    'spy' : 'dkz'
}, {
    'areaCode' : '441929',
    'areaName' : '企石镇',
    'py' : 'qishizhen',
    'spy' : 'qsz'
}, {
    'areaCode' : '441930',
    'areaName' : '石排镇',
    'py' : 'shipaizhen',
    'spy' : 'spz'
}, {
    'areaCode' : '441931',
    'areaName' : '莞城区',
    'py' : 'guancheng',
    'spy' : 'gc'
}, {
    'areaCode' : '441932',
    'areaName' : '东城区',
    'py' : 'dongcheng',
    'spy' : 'dc'
}]
}, {
    'areaCode' : '442000',
    'areaName' : '中山市',
    'py' : 'zhongshan',
    'spy' : 'zs',
    'datas' : [{
        'areaCode' : '442001',
        'areaName' : '南区',
        'py' : 'nanqu',
        'spy' : 'nq'
    }, {
        'areaCode' : '442002',
        'areaName' : '石岐区',
        'py' : 'shiqi',
        'spy' : 'sq'
    }, {
        'areaCode' : '442003',
        'areaName' : '中山火炬高技术产业开发区',
        'py' : 'zhongshanhuojugaojishuchanyekaifaqu',
        'spy' : 'zshjgjscykfq'
    }, {
        'areaCode' : '442004',
        'areaName' : '五桂山区',
        'py' : 'wuguishan',
        'spy' : 'wgs'
    }, {
        'areaCode' : '442005',
        'areaName' : '南朗镇',
        'py' : 'nanlangzhen',
        'spy' : 'nlz'
    }, {
        'areaCode' : '442006',
        'areaName' : '三乡镇',
        'py' : 'sanxiangzhen',
        'spy' : 'sxz'
    }, {
        'areaCode' : '442007',
        'areaName' : '民众镇',
        'py' : 'minzhongzhen',
        'spy' : 'mzz'
    }, {
        'areaCode' : '442008',
        'areaName' : '神湾镇',
        'py' : 'shenwanzhen',
        'spy' : 'swz'
    }, {
        'areaCode' : '442009',
        'areaName' : '板芙镇',
        'py' : 'banfuzhen',
        'spy' : 'bfz'
    }, {
    'areaCode' : '442010',
    'areaName' : '大涌镇',
    'py' : 'dayongzhen',
    'spy' : 'dyz'
}, {
    'areaCode' : '442011',
    'areaName' : '沙溪镇',
    'py' : 'shaxizhen',
    'spy' : 'sxz'
}, {
    'areaCode' : '442012',
    'areaName' : '横栏镇',
    'py' : 'henlanzhen',
    'spy' : 'hlz'
}, {
    'areaCode' : '442013',
    'areaName' : '古镇镇',
    'py' : 'guzhenzhen',
    'spy' : 'gzz'
}, {
    'areaCode' : '442014',
    'areaName' : '小榄镇',
    'py' : 'xiaolanzhen',
    'spy' : 'xlz'
}, {
    'areaCode' : '442015',
    'areaName' : '东凤镇',
    'py' : 'dongfengzhen',
    'spy' : 'dfz'
}, {
    'areaCode' : '442016',
    'areaName' : '南头镇',
    'py' : 'nantouzhen',
    'spy' : 'ntz'
}, {
    'areaCode' : '442017',
    'areaName' : '黄圃镇',
    'py' : 'huangpuzhen',
    'spy' : 'hpz'
}, {
    'areaCode' : '442018',
    'areaName' : '三角镇',
    'py' : 'sanjiaozhen',
    'spy' : 'sjz'
}, {
    'areaCode' : '442019',
    'areaName' : '港口镇',
    'py' : 'gangkouzhen',
    'spy' : 'gkz'
}, {
    'areaCode' : '442020',
    'areaName' : '阜沙镇',
    'py' : 'fushazhen',
    'spy' : 'fsz'
}, {
    'areaCode' : '442021',
    'areaName' : '东升镇',
    'py' : 'dongshengzhen',
    'spy' : 'dsz'
}, {
    'areaCode' : '442022',
    'areaName' : '坦洲镇',
    'py' : 'tanzhouzhen',
    'spy' : 'tzz'
}, {
    'areaCode' : '442023',
    'areaName' : '东区',
    'py' : 'dongqu',
    'spy' : 'dq'
}, {
    'areaCode' : '442024',
    'areaName' : '西区',
    'py' : 'xiqu',
    'spy' : 'xq'
}]
}, {
    'areaCode' : '445100',
    'areaName' : '潮州市',
    'py' : 'chaozhou',
    'spy' : 'cz',
    'datas' : [{
        'areaCode' : '445102',
        'areaName' : '湘桥区',
        'py' : 'xiangqiao',
        'spy' : 'xq'
    }, {
        'areaCode' : '445121',
        'areaName' : '潮安县',
        'py' : 'chaoan',
        'spy' : 'ca'
    }, {
        'areaCode' : '445122',
        'areaName' : '饶平县',
        'py' : 'raoping',
        'spy' : 'rp'
    }]
}, {
    'areaCode' : '445200',
    'areaName' : '揭阳市',
    'py' : 'jieyang',
    'spy' : 'jy',
    'datas' : [{
        'areaCode' : '445202',
        'areaName' : '榕城区',
        'py' : 'rongcheng',
        'spy' : 'rc'
    }, {
        'areaCode' : '445221',
        'areaName' : '揭东县',
        'py' : 'jiedong',
        'spy' : 'jd'
    }, {
        'areaCode' : '445222',
        'areaName' : '揭西县',
        'py' : 'jiexi',
        'spy' : 'jx'
    }, {
        'areaCode' : '445224',
        'areaName' : '惠来县',
        'py' : 'huilai',
        'spy' : 'hl'
    }, {
        'areaCode' : '445281',
        'areaName' : '普宁市',
        'py' : 'puning',
        'spy' : 'pn'
    }]
}, {
    'areaCode' : '445300',
    'areaName' : '云浮市',
    'py' : 'yunfu',
    'spy' : 'yf',
    'datas' : [{
        'areaCode' : '445302',
        'areaName' : '云城区',
        'py' : 'yuncheng',
        'spy' : 'yc'
    }, {
        'areaCode' : '445321',
        'areaName' : '新兴县',
        'py' : 'xinxing',
        'spy' : 'xx'
    }, {
        'areaCode' : '445322',
        'areaName' : '郁南县',
        'py' : 'yunan',
        'spy' : 'yn'
    }, {
        'areaCode' : '445323',
        'areaName' : '云安县',
        'py' : 'yunan',
        'spy' : 'ya'
    }, {
        'areaCode' : '445381',
        'areaName' : '罗定市',
        'py' : 'luoding',
        'spy' : 'ld'
    }]
}]
}, {
    'areaCode' : '450000',
    'areaName' : '广西壮族自治区',
    'py' : 'guangxizhuagnzuzhizhiqu',
    'spy' : 'gxzzzzq',
    'datas' : [{
        'areaCode' : '450100',
        'areaName' : '南宁市',
        'py' : 'nanning',
        'spy' : 'nn',
        'datas' : [{
            'areaCode' : '450102',
            'areaName' : '兴宁区',
            'py' : 'xingning',
            'spy' : 'xn'
        }, {
            'areaCode' : '450103',
            'areaName' : '青秀区',
            'py' : 'qingxiu',
            'spy' : 'qx'
        }, {
            'areaCode' : '450105',
            'areaName' : '江南区',
            'py' : 'jiangnan',
            'spy' : 'jn'
        }, {
            'areaCode' : '450107',
            'areaName' : '西乡塘区',
            'py' : 'xixiangtang',
            'spy' : 'xxt'
        }, {
            'areaCode' : '450108',
            'areaName' : '良庆区',
            'py' : 'liangqing',
            'spy' : 'lq'
        }, {
            'areaCode' : '450109',
            'areaName' : '邕宁区',
            'py' : 'yongning',
            'spy' : 'yn'
        }, {
            'areaCode' : '450122',
            'areaName' : '武鸣县',
            'py' : 'wuming',
            'spy' : 'wm'
        }, {
            'areaCode' : '450123',
            'areaName' : '隆安县',
            'py' : 'longan',
            'spy' : 'la'
        }, {
        'areaCode' : '450124',
        'areaName' : '马山县',
        'py' : 'mashan',
        'spy' : 'ms'
    }, {
    'areaCode' : '450125',
    'areaName' : '上林县',
    'py' : 'shanglin',
    'spy' : 'sl'
}, {
    'areaCode' : '450126',
    'areaName' : '宾阳县',
    'py' : 'binyang',
    'spy' : 'by'
}, {
    'areaCode' : '450127',
    'areaName' : '横县',
    'py' : 'hengxian',
    'spy' : 'hx'
}]
    }, {
        'areaCode' : '450200',
        'areaName' : '柳州市',
        'py' : 'liuzhou',
        'spy' : 'lz',
        'datas' : [{
            'areaCode' : '450202',
            'areaName' : '城中区',
            'py' : 'chengzhong',
            'spy' : 'cz'
        }, {
            'areaCode' : '450203',
            'areaName' : '鱼峰区',
            'py' : 'yufeng',
            'spy' : 'yf'
        }, {
            'areaCode' : '450204',
            'areaName' : '柳南区',
            'py' : 'liunan',
            'spy' : 'ln'
        }, {
            'areaCode' : '450205',
            'areaName' : '柳北区',
            'py' : 'liubei',
            'spy' : 'lb'
        }, {
            'areaCode' : '450221',
            'areaName' : '柳江县',
            'py' : 'liujiang',
            'spy' : 'lj'
        }, {
            'areaCode' : '450222',
            'areaName' : '柳城县',
            'py' : 'liucheng',
            'spy' : 'lc'
        }, {
            'areaCode' : '450223',
            'areaName' : '鹿寨县',
            'py' : 'luzhai',
            'spy' : 'lz'
        }, {
        'areaCode' : '450224',
        'areaName' : '融安县',
        'py' : 'rongan',
        'spy' : 'ra'
    }, {
        'areaCode' : '450225',
        'areaName' : '融水苗族自治县',
        'py' : 'rongshuimiaozuzizhixian',
        'spy' : 'rsmzzzx'
    }, {
    'areaCode' : '450226',
    'areaName' : '三江侗族自治县',
    'py' : 'sanjiangtongzuzizhixian',
    'spy' : 'sjtzzzx'
}]
    }, {
        'areaCode' : '450300',
        'areaName' : '桂林市',
        'py' : 'guilin',
        'spy' : 'gl',
        'datas' : [{
            'areaCode' : '450302',
            'areaName' : '秀峰区',
            'py' : 'xiufeng',
            'spy' : 'xf'
        }, {
            'areaCode' : '450303',
            'areaName' : '叠彩区',
            'py' : 'diecai',
            'spy' : 'dc'
        }, {
            'areaCode' : '450304',
            'areaName' : '象山区',
            'py' : 'xiangshan',
            'spy' : 'xs'
        }, {
            'areaCode' : '450305',
            'areaName' : '七星区',
            'py' : 'qixing',
            'spy' : 'qx'
        }, {
            'areaCode' : '450311',
            'areaName' : '雁山区',
            'py' : 'yanshan',
            'spy' : 'ys'
        }, {
            'areaCode' : '450321',
            'areaName' : '阳朔县',
            'py' : 'yangsuo',
            'spy' : 'ys'
        }, {
        'areaCode' : '450322',
        'areaName' : '临桂县',
        'py' : 'lingui',
        'spy' : 'lg'
    }, {
        'areaCode' : '450323',
        'areaName' : '灵川县',
        'py' : 'lingchuan',
        'spy' : 'lc'
    }, {
        'areaCode' : '450324',
        'areaName' : '全州县',
        'py' : 'quanzhou',
        'spy' : 'qz'
    }, {
    'areaCode' : '450325',
    'areaName' : '兴安县',
    'py' : 'xingan',
    'spy' : 'xa'
}, {
    'areaCode' : '450326',
    'areaName' : '永福县',
    'py' : 'yongfu',
    'spy' : 'yf'
}, {
    'areaCode' : '450327',
    'areaName' : '灌阳县',
    'py' : 'guanyang',
    'spy' : 'gy'
}, {
    'areaCode' : '450328',
    'areaName' : '龙胜各族自治县',
    'py' : 'longshenggezuzizhixian',
    'spy' : 'lsgzzzx'
}, {
    'areaCode' : '450329',
    'areaName' : '资源县',
    'py' : 'ziyuan',
    'spy' : 'zy'
}, {
    'areaCode' : '450330',
    'areaName' : '平乐县',
    'py' : 'pingle',
    'spy' : 'pl'
}, {
    'areaCode' : '450331',
    'areaName' : '荔蒲县',
    'py' : 'jiapu',
    'spy' : 'jp'
}, {
    'areaCode' : '450332',
    'areaName' : '恭城瑶族自治县',
    'py' : 'gongchengyaozuzizhixian',
    'spy' : 'gcyzzzx'
}]
    }, {
        'areaCode' : '450400',
        'areaName' : '梧州市',
        'py' : 'wuzhou',
        'spy' : 'wz',
        'datas' : [{
            'areaCode' : '450403',
            'areaName' : '万秀区',
            'py' : 'wanxiu',
            'spy' : 'wx'
        }, {
            'areaCode' : '450404',
            'areaName' : '蝶山区',
            'py' : 'dieshan',
            'spy' : 'ds'
        }, {
            'areaCode' : '450405',
            'areaName' : '长洲区',
            'py' : 'changzhou',
            'spy' : 'cz'
        }, {
            'areaCode' : '450421',
            'areaName' : '苍梧县',
            'py' : 'cangwu',
            'spy' : 'cw'
        }, {
            'areaCode' : '450422',
            'areaName' : '藤县',
            'py' : 'tengxian',
            'spy' : 'tx'
        }, {
        'areaCode' : '450423',
        'areaName' : '蒙山县',
        'py' : 'mengshan',
        'spy' : 'ms'
    }, {
        'areaCode' : '450481',
        'areaName' : '岑溪市',
        'py' : 'cenxi',
        'spy' : 'cx'
    }]
    }, {
        'areaCode' : '450500',
        'areaName' : '北海市',
        'py' : 'beihai',
        'spy' : 'bh',
        'datas' : [{
            'areaCode' : '450502',
            'areaName' : '海城区',
            'py' : 'haicheng',
            'spy' : 'hc'
        }, {
            'areaCode' : '450503',
            'areaName' : '银海区',
            'py' : 'yinhai',
            'spy' : 'yh'
        }, {
            'areaCode' : '450512',
            'areaName' : '铁山港区',
            'py' : 'tieshangang',
            'spy' : 'tsg'
        }, {
            'areaCode' : '450521',
            'areaName' : '合浦县',
            'py' : 'taipu',
            'spy' : 'tp'
        }]
    }, {
        'areaCode' : '450600',
        'areaName' : '防城港市',
        'py' : 'fangchenggang',
        'spy' : 'fcg',
        'datas' : [{
            'areaCode' : '450602',
            'areaName' : '港口区',
            'py' : 'gangkou',
            'spy' : 'gk'
        }, {
            'areaCode' : '450603',
            'areaName' : '防城区',
            'py' : 'fangcheng',
            'spy' : 'fc'
        }, {
            'areaCode' : '450621',
            'areaName' : '上思县',
            'py' : 'shangsi',
            'spy' : 'ss'
        }, {
        'areaCode' : '450681',
        'areaName' : '东兴市',
        'py' : 'dongxing',
        'spy' : 'dx'
    }]
    }, {
        'areaCode' : '450700',
        'areaName' : '钦州市',
        'py' : 'qinzhou',
        'spy' : 'qz',
        'datas' : [{
            'areaCode' : '450702',
            'areaName' : '钦南区',
            'py' : 'qinnan',
            'spy' : 'qn'
        }, {
            'areaCode' : '450703',
            'areaName' : '钦北区',
            'py' : 'qinbei',
            'spy' : 'qb'
        }, {
        'areaCode' : '450721',
        'areaName' : '灵山县',
        'py' : 'lingshan',
        'spy' : 'ls'
    }, {
        'areaCode' : '450722',
        'areaName' : '浦北县',
        'py' : 'pubei',
        'spy' : 'pb'
    }]
    }, {
        'areaCode' : '450800',
        'areaName' : '贵港市',
        'py' : 'guigang',
        'spy' : 'gg',
        'datas' : [{
            'areaCode' : '450802',
            'areaName' : '港北区',
            'py' : 'gangbei',
            'spy' : 'gb'
        }, {
        'areaCode' : '450803',
        'areaName' : '港南区',
        'py' : 'gangnan',
        'spy' : 'gn'
    }, {
        'areaCode' : '450804',
        'areaName' : '覃塘区',
        'py' : 'qintang',
        'spy' : 'qt'
    }, {
        'areaCode' : '450821',
        'areaName' : '平南县',
        'py' : 'pingnan',
        'spy' : 'pn'
    }, {
        'areaCode' : '450881',
        'areaName' : '桂平市',
        'py' : 'guiping',
        'spy' : 'gp'
    }]
    }, {
        'areaCode' : '450900',
        'areaName' : '玉林市',
        'py' : 'yulin',
        'spy' : 'yl',
        'datas' : [{
        'areaCode' : '450902',
        'areaName' : '玉州区',
        'py' : 'yuzhou',
        'spy' : 'yz'
    }, {
        'areaCode' : '450921',
        'areaName' : '容县',
        'py' : 'rongxian',
        'spy' : 'rx'
    }, {
        'areaCode' : '450922',
        'areaName' : '陆川县',
        'py' : 'luchuan',
        'spy' : 'lc'
    }, {
        'areaCode' : '450923',
        'areaName' : '博白县',
        'py' : 'bobai',
        'spy' : 'bb'
    }, {
        'areaCode' : '450924',
        'areaName' : '兴业县',
        'py' : 'xingye',
        'spy' : 'xy'
    }, {
        'areaCode' : '450981',
        'areaName' : '北流市',
        'py' : 'beiliu',
        'spy' : 'bl'
    }]
    }, {
    'areaCode' : '451000',
    'areaName' : '百色市',
    'py' : 'baise',
    'spy' : 'bs',
    'datas' : [{
        'areaCode' : '451002',
        'areaName' : '右江区',
        'py' : 'youjiang',
        'spy' : 'yj'
    }, {
        'areaCode' : '451021',
        'areaName' : '田阳县',
        'py' : 'tianyang',
        'spy' : 'ty'
    }, {
        'areaCode' : '451022',
        'areaName' : '田东县',
        'py' : 'tiandong',
        'spy' : 'td'
    }, {
        'areaCode' : '451023',
        'areaName' : '平果县',
        'py' : 'pingguo',
        'spy' : 'pg'
    }, {
        'areaCode' : '451024',
        'areaName' : '德保县',
        'py' : 'debao',
        'spy' : 'db'
    }, {
        'areaCode' : '451025',
        'areaName' : '靖西县',
        'py' : 'jingxi',
        'spy' : 'jx'
    }, {
        'areaCode' : '451026',
        'areaName' : '那坡县',
        'py' : 'napo',
        'spy' : 'np'
    }, {
        'areaCode' : '451027',
        'areaName' : '凌云县',
        'py' : 'lingyun',
        'spy' : 'ly'
    }, {
        'areaCode' : '451028',
        'areaName' : '乐业县',
        'py' : 'leye',
        'spy' : 'ly'
    }, {
    'areaCode' : '451029',
    'areaName' : '田林县',
    'py' : 'tianlin',
    'spy' : 'tl'
}, {
    'areaCode' : '451030',
    'areaName' : '西林县',
    'py' : 'xilin',
    'spy' : 'xl'
}, {
    'areaCode' : '451031',
    'areaName' : '隆林各族自治县',
    'py' : 'longlingezuzizhixian',
    'spy' : 'llgzzzx'
}]
}, {
    'areaCode' : '451100',
    'areaName' : '贺州市',
    'py' : 'hezhou',
    'spy' : 'hz',
    'datas' : [{
        'areaCode' : '451102',
        'areaName' : '八步区',
        'py' : 'babu',
        'spy' : 'bb'
    }, {
        'areaCode' : '451121',
        'areaName' : '昭平县',
        'py' : 'zhaoping',
        'spy' : 'zp'
    }, {
        'areaCode' : '451122',
        'areaName' : '钟山县',
        'py' : 'zhongshan',
        'spy' : 'zs'
    }, {
        'areaCode' : '451123',
        'areaName' : '富川瑶族自治县',
        'py' : 'fuchuanyaozuzizhixian',
        'spy' : 'fcyzzzx'
    }]
}, {
    'areaCode' : '451200',
    'areaName' : '河池市',
    'py' : 'hechi',
    'spy' : 'hc',
    'datas' : [{
        'areaCode' : '451202',
        'areaName' : '金城江区',
        'py' : 'jinchengjiang',
        'spy' : 'jcj'
    }, {
        'areaCode' : '451221',
        'areaName' : '南丹县',
        'py' : 'nandan',
        'spy' : 'nd'
    }, {
        'areaCode' : '451222',
        'areaName' : '天峨县',
        'py' : 'tiane',
        'spy' : ' te'
    }, {
        'areaCode' : '451223',
        'areaName' : '凤山县',
        'py' : 'fengshan',
        'spy' : 'fs'
    }, {
        'areaCode' : '451224',
        'areaName' : '东兰县',
        'py' : 'donglan',
        'spy' : 'dl'
    }, {
        'areaCode' : '451225',
        'areaName' : '罗城仫佬族自治县',
        'py' : 'luochengmulaozuzizhixian',
        'spy' : 'lcmlzzzx'
    }, {
        'areaCode' : '451226',
        'areaName' : '环江毛南族自治县',
        'py' : 'huanjangmaonanzuzizhixian',
        'spy' : 'hjmnzzzx'
    }, {
        'areaCode' : '451227',
        'areaName' : '巴马瑶族自治县',
        'py' : 'bamayaozuzizhixian',
        'spy' : 'bmyzzzx'
    }, {
        'areaCode' : '451228',
        'areaName' : '都安瑶族自治县',
        'py' : 'duanyaozuzizhixian',
        'spy' : 'dayzzzx'
    }, {
    'areaCode' : '451229',
    'areaName' : '大化瑶族自治县',
    'py' : 'dahuayaozuzizhixian',
    'spy' : 'dhyzzzx'
}, {
    'areaCode' : '451281',
    'areaName' : '宜州市',
    'py' : 'yizhou',
    'spy' : 'yz'
}]
}, {
    'areaCode' : '451300',
    'areaName' : '来宾市',
    'py' : 'laibin',
    'spy' : 'lb',
    'datas' : [{
        'areaCode' : '451302',
        'areaName' : '兴宾区',
        'py' : 'xingbin',
        'spy' : 'xb'
    }, {
        'areaCode' : '451321',
        'areaName' : '忻城县',
        'py' : 'xincheng',
        'spy' : 'xc'
    }, {
        'areaCode' : '451322',
        'areaName' : '象州县',
        'py' : 'xiangzhou',
        'spy' : 'xz'
    }, {
        'areaCode' : '451323',
        'areaName' : '武宣县',
        'py' : 'wuxuan',
        'spy' : 'wx'
    }, {
        'areaCode' : '451324',
        'areaName' : '金秀瑶族自治县',
        'py' : 'jinxiuyaozuzizhixian',
        'spy' : 'jxyzzzx'
    }, {
        'areaCode' : '451381',
        'areaName' : '合山市',
        'py' : 'heshan',
        'spy' : 'hs'
    }]
}, {
    'areaCode' : '451400',
    'areaName' : '崇左市',
    'py' : 'chongzuo',
    'spy' : 'cz',
    'datas' : [{
        'areaCode' : '451402',
        'areaName' : '江洲区',
        'py' : 'jiangzhou',
        'spy' : 'jz'
    }, {
        'areaCode' : '451421',
        'areaName' : '扶绥县',
        'py' : 'fusui',
        'spy' : 'fs'
    }, {
        'areaCode' : '451422',
        'areaName' : '宁明县',
        'py' : 'ningming',
        'spy' : 'nm'
    }, {
        'areaCode' : '451423',
        'areaName' : '龙州县',
        'py' : 'longzhou',
        'spy' : 'lz'
    }, {
        'areaCode' : '451424',
        'areaName' : '大新县',
        'py' : 'daxin',
        'spy' : 'dx'
    }, {
        'areaCode' : '451425',
        'areaName' : '天等县',
        'py' : 'tiandeng',
        'spy' : 'td'
    }, {
        'areaCode' : '451481',
        'areaName' : '凭祥市',
        'py' : 'pingxiang',
        'spy' : 'px'
    }]
}]
}, {
    'areaCode' : '460000',
    'areaName' : '海南省',
    'py' : 'hainan',
    'spy' : 'hn',
    'datas' : [{
        'areaCode' : '460100',
        'areaName' : '海口市',
        'py' : 'haikou',
        'spy' : 'hk',
        'datas' : [{
            'areaCode' : '460105',
            'areaName' : '秀英区',
            'py' : 'xiuying',
            'spy' : 'xy'
        }, {
            'areaCode' : '460106',
            'areaName' : '龙华区',
            'py' : 'longhua',
            'spy' : 'lh'
        }, {
            'areaCode' : '460107',
            'areaName' : '琼山区',
            'py' : 'qiongshan',
            'spy' : 'qs'
        }, {
            'areaCode' : '460108',
            'areaName' : '美兰区',
            'py' : 'meilan',
            'spy' : 'ml'
        }]
    }, {
        'areaCode' : '460200',
        'areaName' : '三亚市',
        'py' : 'sanya',
        'spy' : 'sy',
        'datas' : [{
            'areaCode' : '460202',
            'areaName' : '海棠湾镇',
            'py' : 'haitangwan',
            'spy' : 'htw'
        }, {
            'areaCode' : '460203',
            'areaName' : '吉阳镇',
            'py' : 'jiyang',
            'spy' : 'jy'
        }, {
            'areaCode' : '460204',
            'areaName' : '凤凰镇',
            'py' : 'fenghuang',
            'spy' : 'fh'
        }, {
            'areaCode' : '460205',
            'areaName' : '国营农场',
            'py' : 'guoyingnongchang',
            'spy' : 'gync'
        }, {
            'areaCode' : '460206',
            'areaName' : '天涯镇',
            'py' : 'tianya',
            'spy' : 'ty'
        }, {
            'areaCode' : '460207',
            'areaName' : '育才镇',
            'py' : 'yucai',
            'spy' : 'yc'
        }, {
            'areaCode' : '460208',
            'areaName' : '崖城镇',
            'py' : 'yacheng',
            'spy' : 'yc'
        }, {
        'areaCode' : '460209',
        'areaName' : '河东区',
        'py' : 'hedong',
        'spy' : 'hd'
    }, {
        'areaCode' : '460210',
        'areaName' : '河西区',
        'py' : 'hexi',
        'spy' : 'hx'
    }]
    }, {
        'areaCode' : '460300',
        'areaName' : '三沙市',
        'py' : 'sansha',
        'spy' : 'ss',
        'datas' : []
    }, {
        'areaCode' : '469000',
        'areaName' : '省直辖县级行政区划',
        'py' : 'shengzhixiaxianjixingzhengquhua',
        'spy' : 'szxxjxzqh',
        'datas' : [{
            'areaCode' : '460321',
            'areaName' : '西沙群岛',
            'py' : 'xishaqundao',
            'spy' : 'xsqd'
        }, {
            'areaCode' : '460322',
            'areaName' : '南沙群岛',
            'py' : 'nanshaqundao',
            'spy' : 'nsqd'
        }, {
            'areaCode' : '460323',
            'areaName' : '中沙群岛的岛礁及其海域',
            'py' : 'zhongshaqundao',
            'spy' : 'zsqd'
        }, {
            'areaCode' : '469001',
            'areaName' : '五指山市',
            'py' : 'wuzhishan',
            'spy' : 'wzs'
        }, {
            'areaCode' : '469002',
            'areaName' : '琼海市',
            'py' : 'qionghai',
            'spy' : 'qh'
        }, {
        'areaCode' : '469003',
        'areaName' : '儋州市',
        'py' : 'danzhou',
        'spy' : 'dz'
    }, {
        'areaCode' : '469005',
        'areaName' : '文昌市',
        'py' : 'wenchang',
        'spy' : 'wc'
    }, {
        'areaCode' : '469006',
        'areaName' : '万宁市',
        'py' : 'wanning',
        'spy' : 'wn'
    }, {
        'areaCode' : '469007',
        'areaName' : '东方市',
        'py' : 'dongfang',
        'spy' : 'df'
    }, {
    'areaCode' : '469021',
    'areaName' : '定安县',
    'py' : 'dingan',
    'spy' : 'da'
}, {
    'areaCode' : '469022',
    'areaName' : '屯昌县',
    'py' : 'tunchang',
    'spy' : 'tc'
}, {
    'areaCode' : '469023',
    'areaName' : '澄迈县',
    'py' : 'chengmai',
    'spy' : 'cm'
}, {
    'areaCode' : '469024',
    'areaName' : '临高县',
    'py' : 'lingao',
    'spy' : 'lg'
}, {
    'areaCode' : '469025',
    'areaName' : '白沙黎族自治县',
    'py' : 'baishalizuzizhixian',
    'spy' : 'bslzzzx'
}, {
    'areaCode' : '469026',
    'areaName' : '昌江黎族自治县',
    'py' : 'changjianglizuzizhixian',
    'spy' : 'cjlzzzx'
}, {
    'areaCode' : '469027',
    'areaName' : '乐东黎族自治县',
    'py' : 'ledonglizuzizhixian',
    'spy' : 'ldlzzzx'
}, {
    'areaCode' : '469028',
    'areaName' : '陵水黎族自治县',
    'py' : 'lingshuilizuzizhixian',
    'spy' : 'lslzzzx'
}, {
    'areaCode' : '469029',
    'areaName' : '保亭黎族苗族自治县',
    'py' : 'baotinglizumiaozuzizhixian',
    'spy' : 'btlzmzzzx'
}, {
    'areaCode' : '469030',
    'areaName' : '琼中黎族苗族自治县',
    'py' : 'qiongzhonglizumiaozuzizhixian',
    'spy' : 'qzlzmzzzx'
}]
    }]
}, {
    'areaCode' : '500000',
    'areaName' : '重庆市',
    'py' : 'chongqing',
    'spy' : 'cq',
    'datas' : [{
        'areaCode' : '500100',
        'areaName' : '重庆市',
        'py' : 'chongqing',
        'spy' : 'cq',
        'datas' : [{
            'areaCode' : '500101',
            'areaName' : '万州区',
            'py' : 'wanzhou',
            'spy' : 'wz'
        }, {
            'areaCode' : '500102',
            'areaName' : '涪陵区',
            'py' : 'fuling',
            'spy' : 'fl'
        }, {
            'areaCode' : '500103',
            'areaName' : '渝中区',
            'py' : 'yuzhong',
            'spy' : 'yz'
        }, {
            'areaCode' : '500104',
            'areaName' : '大渡口区',
            'py' : 'dadukou',
            'spy' : 'ddk'
        }, {
            'areaCode' : '500105',
            'areaName' : '江北区',
            'py' : 'jiangbei',
            'spy' : 'jb'
        }, {
            'areaCode' : '500106',
            'areaName' : '沙坪坝区',
            'py' : 'shapingba',
            'spy' : 'spb'
        }, {
            'areaCode' : '500107',
            'areaName' : '九龙坡区',
            'py' : 'jiulongpo',
            'spy' : 'jlp'
        }, {
            'areaCode' : '500108',
            'areaName' : '南岸区',
            'py' : 'nanan',
            'spy' : 'na'
        }, {
        'areaCode' : '500109',
        'areaName' : '北碚区',
        'py' : 'beibei',
        'spy' : 'bb'
    }, {
    'areaCode' : '500110',
    'areaName' : '綦江区',
    'py' : 'qijiang',
    'spy' : 'qj'
}, {
    'areaCode' : '500111',
    'areaName' : '大足区',
    'py' : 'dazu',
    'spy' : 'dz'
}, {
    'areaCode' : '500112',
    'areaName' : '渝北区',
    'py' : 'yubei',
    'spy' : 'yb'
}, {
    'areaCode' : '500113',
    'areaName' : '巴南区',
    'py' : 'banan',
    'spy' : 'bn'
}, {
    'areaCode' : '500114',
    'areaName' : '黔江区',
    'py' : 'qianjiang',
    'spy' : 'qj'
}, {
    'areaCode' : '500115',
    'areaName' : '长寿区',
    'py' : 'changshou',
    'spy' : 'cs'
}, {
    'areaCode' : '500116',
    'areaName' : '江津区',
    'py' : 'jiangjin',
    'spy' : 'jj'
}, {
    'areaCode' : '500117',
    'areaName' : '合川区',
    'py' : 'hechuan',
    'spy' : 'hc'
}, {
    'areaCode' : '500118',
    'areaName' : '永川区',
    'py' : 'yongchuan',
    'spy' : 'yc'
}, {
    'areaCode' : '500119',
    'areaName' : '南川区',
    'py' : 'nanchuan',
    'spy' : 'nc'
}, {
    'areaCode' : '500223',
    'areaName' : '潼南县',
    'py' : 'tongnan',
    'spy' : 'tn'
}, {
    'areaCode' : '500224',
    'areaName' : '铜梁县',
    'py' : 'tongliang',
    'spy' : 'tl'
}, {
    'areaCode' : '500226',
    'areaName' : '荣昌县',
    'py' : 'rongchang',
    'spy' : 'rc'
}, {
    'areaCode' : '500227',
    'areaName' : '璧山县',
    'py' : 'bishan',
    'spy' : 'bs'
}, {
    'areaCode' : '500228',
    'areaName' : '梁平县',
    'py' : 'liangping',
    'spy' : 'lp'
}, {
    'areaCode' : '500229',
    'areaName' : '城口县',
    'py' : 'chengkou',
    'spy' : 'ck'
}, {
    'areaCode' : '500230',
    'areaName' : '丰都县',
    'py' : 'fengdu',
    'spy' : 'fd'
}, {
    'areaCode' : '500231',
    'areaName' : '垫江县',
    'py' : 'dianjiang',
    'spy' : 'dj'
}, {
    'areaCode' : '500232',
    'areaName' : '武隆县',
    'py' : 'wulong',
    'spy' : 'wl'
}, {
    'areaCode' : '500233',
    'areaName' : '忠县',
    'py' : 'zhongxian',
    'spy' : 'zx'
}, {
    'areaCode' : '500234',
    'areaName' : '开县',
    'py' : 'kaixian',
    'spy' : 'kx'
}, {
    'areaCode' : '500235',
    'areaName' : '云阳县',
    'py' : 'yunyang',
    'spy' : 'yy'
}, {
    'areaCode' : '500236',
    'areaName' : '奉节县',
    'py' : 'fengjie',
    'spy' : 'fj'
}, {
    'areaCode' : '500237',
    'areaName' : '巫山县',
    'py' : 'wushan',
    'spy' : 'ws'
}, {
    'areaCode' : '500238',
    'areaName' : '巫溪县',
    'py' : 'wuxi',
    'spy' : 'wx'
}, {
    'areaCode' : '500240',
    'areaName' : '石柱土家族自治县',
    'py' : 'shizhutujiazuzizhixian',
    'spy' : 'sztjzzzx'
}, {
    'areaCode' : '500241',
    'areaName' : '秀山土家族苗族自治县',
    'py' : 'xiushantujiazumiaozuzizhixian',
    'spy' : 'xstjzmzzzx'
}, {
    'areaCode' : '500242',
    'areaName' : '酉阳土家族苗族自治县',
    'py' : 'youyangtujiazumiaozuzizhixian',
    'spy' : 'yytjzmzzzx'
}, {
    'areaCode' : '500243',
    'areaName' : '彭水苗族土家族自治县',
    'py' : 'pengshuimiaozutujiaozuzizhixian',
    'spy' : 'psmztjzzzx'
}]
    }]
}, {
    'areaCode' : '510000',
    'areaName' : '四川省',
    'py' : 'sichuan',
    'spy' : 'sc',
    'datas' : [{
        'areaCode' : '510100',
        'areaName' : '成都市',
        'py' : 'chengdu',
        'spy' : 'cd',
        'datas' : [{
            'areaCode' : '510104',
            'areaName' : '锦江区',
            'py' : 'jingjiang',
            'spy' : 'jj'
        }, {
            'areaCode' : '510105',
            'areaName' : '青羊区',
            'py' : 'qingyang',
            'spy' : 'qy'
        }, {
            'areaCode' : '510106',
            'areaName' : '金牛区',
            'py' : 'jinniu',
            'spy' : 'jn'
        }, {
            'areaCode' : '510107',
            'areaName' : '武侯区',
            'py' : 'wuhou',
            'spy' : 'wh'
        }, {
            'areaCode' : '510108',
            'areaName' : '成华区',
            'py' : 'chenghua',
            'spy' : 'ch'
        }, {
            'areaCode' : '510112',
            'areaName' : '龙泉驿区',
            'py' : 'longquanyi',
            'spy' : 'lqy'
        }, {
            'areaCode' : '510113',
            'areaName' : '青白江区',
            'py' : 'qingbaijiang',
            'spy' : 'qbj'
        }, {
            'areaCode' : '510114',
            'areaName' : '新都区',
            'py' : 'xindu',
            'spy' : 'xd'
        }, {
        'areaCode' : '510115',
        'areaName' : '温江区',
        'py' : 'wenjiang',
        'spy' : 'wj'
    }, {
    'areaCode' : '510121',
    'areaName' : '金堂县',
    'py' : 'jintang',
    'spy' : 'jt'
}, {
    'areaCode' : '510122',
    'areaName' : '双流县',
    'py' : 'shuangliu',
    'spy' : 'sl'
}, {
    'areaCode' : '510124',
    'areaName' : '郫县',
    'py' : 'pixian',
    'spy' : 'px'
}, {
    'areaCode' : '510129',
    'areaName' : '大邑县',
    'py' : 'dayi',
    'spy' : 'dy'
}, {
    'areaCode' : '510131',
    'areaName' : '蒲江县',
    'py' : 'pujiang',
    'spy' : 'pj'
}, {
    'areaCode' : '510132',
    'areaName' : '新津县',
    'py' : 'xinjin',
    'spy' : 'xj'
}, {
    'areaCode' : '510181',
    'areaName' : '都江堰市',
    'py' : 'dujiangyan',
    'spy' : 'djy'
}, {
    'areaCode' : '510182',
    'areaName' : '彭州市',
    'py' : 'pengzhou',
    'spy' : 'pz'
}, {
    'areaCode' : '510183',
    'areaName' : '邛崃市',
    'py' : 'qionglai',
    'spy' : 'ql'
}, {
    'areaCode' : '510184',
    'areaName' : '崇州市',
    'py' : 'chongzhou',
    'spy' : 'cz'
}]
    }, {
        'areaCode' : '510300',
        'areaName' : '自贡市',
        'py' : 'zigong',
        'spy' : 'zg',
        'datas' : [{
            'areaCode' : '510302',
            'areaName' : '自流井区',
            'py' : 'ziliujing',
            'spy' : 'zlj'
        }, {
            'areaCode' : '510303',
            'areaName' : '贡井区',
            'py' : 'gongjing',
            'spy' : 'gj'
        }, {
            'areaCode' : '510304',
            'areaName' : '大安区',
            'py' : 'daan',
            'spy' : 'da'
        }, {
            'areaCode' : '510311',
            'areaName' : '沿滩区',
            'py' : 'yantan',
            'spy' : 'yt'
        }, {
            'areaCode' : '510321',
            'areaName' : '荣县',
            'py' : 'rongxian',
            'spy' : 'rx'
        }, {
            'areaCode' : '510322',
            'areaName' : '富顺县',
            'py' : 'fushun',
            'spy' : 'fs'
        }]
    }, {
        'areaCode' : '510400',
        'areaName' : '攀枝花市',
        'py' : 'panzhihua',
        'spy' : 'pzh',
        'datas' : [{
            'areaCode' : '510402',
            'areaName' : '东区',
            'py' : 'dongqu',
            'spy' : 'dq'
        }, {
            'areaCode' : '510403',
            'areaName' : '西区',
            'py' : 'xiqu',
            'spy' : 'xq'
        }, {
            'areaCode' : '510411',
            'areaName' : '仁和区',
            'py' : 'renhe',
            'spy' : 'rh'
        }, {
            'areaCode' : '510421',
            'areaName' : '米易县',
            'py' : 'miyi',
            'spy' : 'my'
        }, {
            'areaCode' : '510422',
            'areaName' : '盐边县',
            'py' : 'yanbian',
            'spy' : 'yb'
        }]
    }, {
        'areaCode' : '510500',
        'areaName' : '泸州市',
        'py' : 'luzhou',
        'spy' : 'lz',
        'datas' : [{
            'areaCode' : '510502',
            'areaName' : '江阳区',
            'py' : 'jiangyang',
            'spy' : 'jy'
        }, {
            'areaCode' : '510503',
            'areaName' : '纳溪区',
            'py' : 'naxi',
            'spy' : 'nx'
        }, {
            'areaCode' : '510504',
            'areaName' : '龙马潭区',
            'py' : 'longmatan',
            'spy' : 'lmt'
        }, {
            'areaCode' : '510521',
            'areaName' : '泸县',
            'py' : 'luxian',
            'spy' : 'lx'
        }, {
            'areaCode' : '510522',
            'areaName' : '合江县',
            'py' : 'hejiang',
            'spy' : 'hj'
        }, {
        'areaCode' : '510524',
        'areaName' : '叙永县',
        'py' : 'xuyong',
        'spy' : 'xy'
    }, {
        'areaCode' : '510525',
        'areaName' : '古蔺县',
        'py' : 'gulin',
        'spy' : 'gl'
    }]
    }, {
        'areaCode' : '510600',
        'areaName' : '德阳市',
        'py' : 'deyang',
        'spy' : 'dy',
        'datas' : [{
            'areaCode' : '510603',
            'areaName' : '旌阳区',
            'py' : 'jingyang',
            'spy' : 'jy'
        }, {
            'areaCode' : '510623',
            'areaName' : '中江县',
            'py' : 'zhongjiang',
            'spy' : 'zj'
        }, {
            'areaCode' : '510626',
            'areaName' : '罗江县',
            'py' : 'luojiang',
            'spy' : 'lj'
        }, {
            'areaCode' : '510681',
            'areaName' : '广汉市',
            'py' : 'guanghan',
            'spy' : 'gh'
        }, {
        'areaCode' : '510682',
        'areaName' : '什邡市',
        'py' : 'shifang',
        'spy' : 'sf'
    }, {
        'areaCode' : '510683',
        'areaName' : '绵竹市',
        'py' : 'mianzhu',
        'spy' : 'mz'
    }]
    }, {
        'areaCode' : '510700',
        'areaName' : '绵阳市',
        'py' : 'mianyang',
        'spy' : 'my',
        'datas' : [{
            'areaCode' : '510703',
            'areaName' : '涪城区',
            'py' : 'fucheng',
            'spy' : 'fc'
        }, {
            'areaCode' : '510704',
            'areaName' : '游仙区',
            'py' : 'youxian',
            'spy' : 'yx'
        }, {
            'areaCode' : '510722',
            'areaName' : '三台县',
            'py' : 'santai',
            'spy' : 'st'
        }, {
        'areaCode' : '510723',
        'areaName' : '盐亭县',
        'py' : 'yantin',
        'spy' : 'yt'
    }, {
        'areaCode' : '510724',
        'areaName' : '安县',
        'py' : 'anxian',
        'spy' : 'ax'
    }, {
        'areaCode' : '510725',
        'areaName' : '梓潼县',
        'py' : 'zitong',
        'spy' : 'zt'
    }, {
        'areaCode' : '510726',
        'areaName' : '北川羌族自治县',
        'py' : 'beichuanqiangzuzizhixian',
        'spy' : 'bcqzzzx'
    }, {
        'areaCode' : '510727',
        'areaName' : '平武县',
        'py' : 'pingwu',
        'spy' : 'pw'
    }, {
        'areaCode' : '510781',
        'areaName' : '江油市',
        'py' : 'jiangyou',
        'spy' : 'jy'
    }]
    }, {
        'areaCode' : '510800',
        'areaName' : '广元市',
        'py' : 'guangyuan',
        'spy' : 'gy',
        'datas' : [{
            'areaCode' : '510802',
            'areaName' : '利州区',
            'py' : 'lizhou',
            'spy' : 'lz'
        }, {
            'areaCode' : '510803',
            'areaName' : '市中区',
            'py' : 'shizhong',
            'spy' : 'sz'
        }, {
        'areaCode' : '510811',
        'areaName' : '元坝区',
        'py' : 'yuanba',
        'spy' : 'yb'
    }, {
        'areaCode' : '510812',
        'areaName' : '朝天区',
        'py' : 'chaotian',
        'spy' : 'ct'
    }, {
        'areaCode' : '510821',
        'areaName' : '旺苍县',
        'py' : 'wancang',
        'spy' : 'wc'
    }, {
        'areaCode' : '510822',
        'areaName' : '青川县',
        'py' : 'qingchuan',
        'spy' : 'qc'
    }, {
        'areaCode' : '510823',
        'areaName' : '剑阁县',
        'py' : 'jiange',
        'spy' : 'jg'
    }, {
        'areaCode' : '510824',
        'areaName' : '苍溪县',
        'py' : 'cangxi',
        'spy' : 'cx'
    }]
    }, {
        'areaCode' : '510900',
        'areaName' : '遂宁市',
        'py' : 'suining',
        'spy' : 'sn',
        'datas' : [{
            'areaCode' : '510903',
            'areaName' : '船山区',
            'py' : 'chuanshan',
            'spy' : 'cs'
        }, {
        'areaCode' : '510904',
        'areaName' : '安居区',
        'py' : 'anju',
        'spy' : 'aj'
    }, {
        'areaCode' : '510921',
        'areaName' : '蓬溪县',
        'py' : 'pengxi',
        'spy' : 'px'
    }, {
        'areaCode' : '510922',
        'areaName' : '射洪县',
        'py' : 'shehong',
        'spy' : 'sh'
    }, {
        'areaCode' : '510923',
        'areaName' : '大英县',
        'py' : 'daying',
        'spy' : 'dy'
    }]
    }, {
        'areaCode' : '511000',
        'areaName' : '内江市',
        'py' : 'neijiang',
        'spy' : 'nj',
        'datas' : [{
        'areaCode' : '511002',
        'areaName' : '市中区',
        'py' : 'shizhong',
        'spy' : 'sz'
    }, {
        'areaCode' : '511011',
        'areaName' : '东兴区',
        'py' : 'dongxing',
        'spy' : 'dx'
    }, {
        'areaCode' : '511024',
        'areaName' : '威远县',
        'py' : 'weiyuan',
        'spy' : 'wy'
    }, {
        'areaCode' : '511025',
        'areaName' : '资中县',
        'py' : 'zizhong',
        'spy' : 'zz'
    }, {
        'areaCode' : '511028',
        'areaName' : '隆昌县',
        'py' : 'longchang',
        'spy' : 'lc'
    }]
    }, {
    'areaCode' : '511100',
    'areaName' : '乐山市',
    'py' : 'leshan',
    'spy' : 'ls',
    'datas' : [{
        'areaCode' : '511102',
        'areaName' : '市中区',
        'py' : 'shizhong',
        'spy' : 'sz'
    }, {
        'areaCode' : '511111',
        'areaName' : '沙湾区',
        'py' : 'shawan',
        'spy' : 'sw'
    }, {
        'areaCode' : '511112',
        'areaName' : '五通桥区',
        'py' : 'wutongqiao',
        'spy' : 'wtq'
    }, {
        'areaCode' : '511113',
        'areaName' : '金口河区',
        'py' : 'jinkouhe',
        'spy' : 'jkh'
    }, {
        'areaCode' : '511123',
        'areaName' : '犍为县',
        'py' : 'qianwei',
        'spy' : 'qw'
    }, {
        'areaCode' : '511124',
        'areaName' : '井研县',
        'py' : 'jingyan',
        'spy' : 'jy'
    }, {
        'areaCode' : '511126',
        'areaName' : '夹江县',
        'py' : 'jiajiang',
        'spy' : 'jj'
    }, {
        'areaCode' : '511129',
        'areaName' : '沐川县',
        'py' : 'muchuan',
        'spy' : 'mc'
    }, {
        'areaCode' : '511132',
        'areaName' : '峨边彝族自治县',
        'py' : 'ebianyizuzizhixian',
        'spy' : 'ebyzzzx'
    }, {
    'areaCode' : '511133',
    'areaName' : '马边彝族自治县',
    'py' : 'mabianyizuzizhixian',
    'spy' : 'mbyzzzx'
}, {
    'areaCode' : '511181',
    'areaName' : '峨眉山市',
    'py' : 'emeishan',
    'spy' : 'ems'
}]
}, {
    'areaCode' : '511300',
    'areaName' : '南充市',
    'py' : 'nanchong',
    'spy' : 'nc',
    'datas' : [{
        'areaCode' : '511302',
        'areaName' : '顺庆区',
        'py' : 'shunqing',
        'spy' : 'sq'
    }, {
        'areaCode' : '511303',
        'areaName' : '高坪区',
        'py' : 'gaoping',
        'spy' : 'gp'
    }, {
        'areaCode' : '511304',
        'areaName' : '嘉陵区',
        'py' : 'jialing',
        'spy' : 'jl'
    }, {
        'areaCode' : '511321',
        'areaName' : '南部县',
        'py' : 'nanbu',
        'spy' : 'nb'
    }, {
        'areaCode' : '511322',
        'areaName' : '营山县',
        'py' : 'yingshan',
        'spy' : 'ys'
    }, {
        'areaCode' : '511323',
        'areaName' : '蓬安县',
        'py' : 'pengan',
        'spy' : 'pa'
    }, {
        'areaCode' : '511324',
        'areaName' : '仪陇县',
        'py' : 'yilong',
        'spy' : 'yl'
    }, {
        'areaCode' : '511325',
        'areaName' : '西充县',
        'py' : 'xichong',
        'spy' : 'xc'
    }, {
        'areaCode' : '511381',
        'areaName' : '阆中市',
        'py' : 'langzhong',
        'spy' : 'lz'
    }]
}, {
    'areaCode' : '511400',
    'areaName' : '眉山市',
    'py' : 'meishan',
    'spy' : 'ms',
    'datas' : [{
        'areaCode' : '511402',
        'areaName' : '东坡区',
        'py' : 'dongpo',
        'spy' : 'dp'
    }, {
        'areaCode' : '511421',
        'areaName' : '仁寿县',
        'py' : 'renshou',
        'spy' : 'rs'
    }, {
        'areaCode' : '511422',
        'areaName' : '彭山县',
        'py' : 'pengshan',
        'spy' : 'ps'
    }, {
        'areaCode' : '511423',
        'areaName' : '洪雅县',
        'py' : 'hongya',
        'spy' : 'hy'
    }, {
        'areaCode' : '511424',
        'areaName' : '丹棱县',
        'py' : 'danling',
        'spy' : 'dl'
    }, {
        'areaCode' : '511425',
        'areaName' : '青神县',
        'py' : 'qingshen',
        'spy' : 'qs'
    }]
}, {
    'areaCode' : '511500',
    'areaName' : '宜宾市',
    'py' : 'yibin',
    'spy' : 'yb',
    'datas' : [{
        'areaCode' : '511502',
        'areaName' : '翠屏区',
        'py' : 'cuiping',
        'spy' : 'cp'
    }, {
        'areaCode' : '511503',
        'areaName' : '南溪区',
        'py' : 'nanxi',
        'spy' : 'nx'
    }, {
        'areaCode' : '511521',
        'areaName' : '宜宾县',
        'py' : 'yibin',
        'spy' : 'yb'
    }, {
        'areaCode' : '511523',
        'areaName' : '江安县',
        'py' : 'jiangan',
        'spy' : 'ja'
    }, {
        'areaCode' : '511524',
        'areaName' : '长宁县',
        'py' : 'changning',
        'spy' : 'cn'
    }, {
        'areaCode' : '511525',
        'areaName' : '高县',
        'py' : 'gaoxian',
        'spy' : 'gx'
    }, {
        'areaCode' : '511526',
        'areaName' : '珙县',
        'py' : 'gongxian',
        'spy' : 'gx'
    }, {
        'areaCode' : '511527',
        'areaName' : '筠连县',
        'py' : 'junlian',
        'spy' : 'jl'
    }, {
        'areaCode' : '511528',
        'areaName' : '兴文县',
        'py' : 'xingwen',
        'spy' : 'xw'
    }, {
    'areaCode' : '511529',
    'areaName' : '屏山县',
    'py' : 'pingshan',
    'spy' : 'ps'
}]
}, {
    'areaCode' : '511600',
    'areaName' : '广安市',
    'py' : 'guangan',
    'spy' : 'ga',
    'datas' : [{
        'areaCode' : '511602',
        'areaName' : '广安区',
        'py' : 'guangan',
        'spy' : 'ga'
    }, {
        'areaCode' : '511621',
        'areaName' : '岳池县',
        'py' : 'yuechi',
        'spy' : 'yc'
    }, {
        'areaCode' : '511622',
        'areaName' : '武胜县',
        'py' : 'wusheng',
        'spy' : 'ws'
    }, {
        'areaCode' : '511623',
        'areaName' : '邻水县',
        'py' : 'lingshui',
        'spy' : 'ls'
    }, {
        'areaCode' : '511681',
        'areaName' : '华蓥市',
        'py' : 'huaying',
        'spy' : 'hy'
    }]
}, {
    'areaCode' : '511700',
    'areaName' : '达州市',
    'py' : 'dazhou',
    'spy' : 'dz',
    'datas' : [{
        'areaCode' : '511702',
        'areaName' : '通川区',
        'py' : 'tongchuan',
        'spy' : 'tc'
    }, {
        'areaCode' : '511721',
        'areaName' : '达县',
        'py' : 'daxian',
        'spy' : 'dx'
    }, {
        'areaCode' : '511722',
        'areaName' : '宣汉县',
        'py' : 'xuanhan',
        'spy' : 'xh'
    }, {
        'areaCode' : '511723',
        'areaName' : '开江县',
        'py' : 'kaijiang',
        'spy' : 'kj'
    }, {
        'areaCode' : '511724',
        'areaName' : '大竹县',
        'py' : 'dazhu',
        'spy' : 'dz'
    }, {
        'areaCode' : '511725',
        'areaName' : '渠县',
        'py' : 'quxian',
        'spy' : 'qx'
    }, {
        'areaCode' : '511781',
        'areaName' : '万源市',
        'py' : 'wanyuan',
        'spy' : 'wy'
    }]
}, {
    'areaCode' : '511800',
    'areaName' : '雅安市',
    'py' : 'yaan',
    'spy' : 'ya',
    'datas' : [{
        'areaCode' : '511802',
        'areaName' : '雨城区',
        'py' : 'yucheng',
        'spy' : 'yc'
    }, {
        'areaCode' : '511821',
        'areaName' : '名山县',
        'py' : 'mingshan',
        'spy' : 'ms'
    }, {
        'areaCode' : '511822',
        'areaName' : '荥经县',
        'py' : 'yingjing',
        'spy' : 'yj'
    }, {
        'areaCode' : '511823',
        'areaName' : '汉源县',
        'py' : 'hanyuan',
        'spy' : 'hy'
    }, {
        'areaCode' : '511824',
        'areaName' : '石棉县',
        'py' : 'shimian',
        'spy' : 'sm'
    }, {
        'areaCode' : '511825',
        'areaName' : '天全县',
        'py' : 'tianquan',
        'spy' : 'tq'
    }, {
        'areaCode' : '511826',
        'areaName' : '芦山县',
        'py' : 'lushan',
        'spy' : 'ls'
    }, {
        'areaCode' : '511827',
        'areaName' : '宝兴县',
        'py' : 'baoxing',
        'spy' : 'bx'
    }]
}, {
    'areaCode' : '511900',
    'areaName' : '巴中市',
    'py' : 'bazong',
    'spy' : 'bz',
    'datas' : [{
        'areaCode' : '511902',
        'areaName' : '巴州区',
        'py' : 'bazhou',
        'spy' : 'bz'
    }, {
        'areaCode' : '511921',
        'areaName' : '通江县',
        'py' : 'tongjiang',
        'spy' : 'tj'
    }, {
        'areaCode' : '511922',
        'areaName' : '南江县',
        'py' : 'nanjiang',
        'spy' : 'nj'
    }, {
        'areaCode' : '511923',
        'areaName' : '平昌县',
        'py' : 'pingchang',
        'spy' : 'pc'
    }]
}, {
    'areaCode' : '512000',
    'areaName' : '资阳市',
    'py' : 'ziyang',
    'spy' : 'zy',
    'datas' : [{
        'areaCode' : '512002',
        'areaName' : '雁江区',
        'py' : 'yanjiang',
        'spy' : 'yj'
    }, {
        'areaCode' : '512021',
        'areaName' : '安岳县',
        'py' : 'anyue',
        'spy' : 'ay'
    }, {
        'areaCode' : '512022',
        'areaName' : '乐至县',
        'py' : 'lezhi',
        'spy' : 'lz'
    }, {
        'areaCode' : '512081',
        'areaName' : '简阳市',
        'py' : 'jianyang',
        'spy' : 'jy'
    }]
}, {
    'areaCode' : '513200',
    'areaName' : '阿坝藏族羌族自治州',
    'py' : 'abazangzuqiangzuzizhizhou',
    'spy' : 'zbzzqzzzz',
    'datas' : [{
        'areaCode' : '513221',
        'areaName' : '汶川县',
        'py' : 'wenchuan',
        'spy' : 'wc'
    }, {
        'areaCode' : '513222',
        'areaName' : '理县',
        'py' : 'lixian',
        'spy' : 'lx'
    }, {
        'areaCode' : '513223',
        'areaName' : '茂县',
        'py' : 'maoxian',
        'spy' : 'mx'
    }, {
        'areaCode' : '513224',
        'areaName' : '松潘县',
        'py' : 'songpan',
        'spy' : 'sp'
    }, {
        'areaCode' : '513225',
        'areaName' : '九寨沟县',
        'py' : 'jiuzhaigou',
        'spy' : 'jzg'
    }, {
        'areaCode' : '513226',
        'areaName' : '金川县',
        'py' : 'jinchuan',
        'spy' : 'jc'
    }, {
        'areaCode' : '513227',
        'areaName' : '小金县',
        'py' : 'xiaojin',
        'spy' : 'xj'
    }, {
        'areaCode' : '513228',
        'areaName' : '黑水县',
        'py' : 'heishui',
        'spy' : 'hs'
    }, {
        'areaCode' : '513229',
        'areaName' : '马尔康县',
        'py' : 'maerkang',
        'spy' : 'mek'
    }, {
    'areaCode' : '513230',
    'areaName' : '壤塘县',
    'py' : 'xiangtang',
    'spy' : 'xt'
}, {
    'areaCode' : '513231',
    'areaName' : '阿坝县',
    'py' : 'aba',
    'spy' : 'ab'
}, {
    'areaCode' : '513232',
    'areaName' : '若尔盖县',
    'py' : 'ruoergai',
    'spy' : 'reg'
}, {
    'areaCode' : '513233',
    'areaName' : '红原县',
    'py' : 'hongyuan',
    'spy' : 'hy'
}]
}, {
    'areaCode' : '513300',
    'areaName' : '甘孜藏族自治州',
    'py' : 'ganzizangzuzizhizhou',
    'spy' : 'gzzzzzz',
    'datas' : [{
        'areaCode' : '513321',
        'areaName' : '康定县',
        'py' : 'kangding',
        'spy' : 'kd'
    }, {
        'areaCode' : '513322',
        'areaName' : '泸定县',
        'py' : 'luding',
        'spy' : 'ld'
    }, {
        'areaCode' : '513323',
        'areaName' : '丹巴县',
        'py' : 'danba',
        'spy' : 'db'
    }, {
        'areaCode' : '513324',
        'areaName' : '九龙县',
        'py' : 'jiulong',
        'spy' : 'jl'
    }, {
        'areaCode' : '513325',
        'areaName' : '雅江县',
        'py' : 'yajiang',
        'spy' : 'yj'
    }, {
        'areaCode' : '513326',
        'areaName' : '道孚县',
        'py' : 'daofu',
        'spy' : 'df'
    }, {
        'areaCode' : '513327',
        'areaName' : '炉霍县',
        'py' : 'luhuo',
        'spy' : 'lh'
    }, {
        'areaCode' : '513328',
        'areaName' : '甘孜县',
        'py' : 'ganzi',
        'spy' : 'gz'
    }, {
        'areaCode' : '513329',
        'areaName' : '新龙县',
        'py' : 'xinlong',
        'spy' : 'xl'
    }, {
    'areaCode' : '513330',
    'areaName' : '德格县',
    'py' : 'dege',
    'spy' : 'dg'
}, {
    'areaCode' : '513331',
    'areaName' : '白玉县',
    'py' : 'baiyu',
    'spy' : 'by'
}, {
    'areaCode' : '513332',
    'areaName' : '石渠县',
    'py' : 'shiqu',
    'spy' : 'sq'
}, {
    'areaCode' : '513333',
    'areaName' : '色达县',
    'py' : 'seda',
    'spy' : 'sd'
}, {
    'areaCode' : '513334',
    'areaName' : '理塘县',
    'py' : 'litang',
    'spy' : 'lt'
}, {
    'areaCode' : '513335',
    'areaName' : '巴塘县',
    'py' : 'batang',
    'spy' : 'bt'
}, {
    'areaCode' : '513336',
    'areaName' : '乡城县',
    'py' : 'xiangcheng',
    'spy' : 'xc'
}, {
    'areaCode' : '513337',
    'areaName' : '稻城县',
    'py' : 'daocheng',
    'spy' : 'dc'
}, {
    'areaCode' : '513338',
    'areaName' : '得荣县',
    'py' : 'derong',
    'spy' : 'dr'
}]
}, {
    'areaCode' : '513400',
    'areaName' : '凉山彝族自治州',
    'py' : 'liangshanyizuzizhizhou',
    'spy' : 'lsyzzzz',
    'datas' : [{
        'areaCode' : '513401',
        'areaName' : '西昌市',
        'py' : 'xichang',
        'spy' : 'xc'
    }, {
        'areaCode' : '513422',
        'areaName' : '木里藏族自治县',
        'py' : 'mulizangzuzizhixian',
        'spy' : 'mlzzzzx'
    }, {
        'areaCode' : '513423',
        'areaName' : '盐源县',
        'py' : 'yanyuan',
        'spy' : 'yy'
    }, {
        'areaCode' : '513424',
        'areaName' : '德昌县',
        'py' : 'dechang',
        'spy' : 'dc'
    }, {
        'areaCode' : '513425',
        'areaName' : '会理县',
        'py' : 'huili',
        'spy' : 'hl'
    }, {
        'areaCode' : '513426',
        'areaName' : '会东县',
        'py' : 'huidong',
        'spy' : 'hd'
    }, {
        'areaCode' : '513427',
        'areaName' : '宁南县',
        'py' : 'ningnan',
        'spy' : 'nn'
    }, {
        'areaCode' : '513428',
        'areaName' : '普格县',
        'py' : 'puge',
        'spy' : 'pg'
    }, {
        'areaCode' : '513429',
        'areaName' : '布拖县',
        'py' : 'butuo',
        'spy' : 'bt'
    }, {
    'areaCode' : '513430',
    'areaName' : '金阳县',
    'py' : 'jinyang',
    'spy' : 'jy'
}, {
    'areaCode' : '513431',
    'areaName' : '昭觉县',
    'py' : 'zhaojue',
    'spy' : 'zj'
}, {
    'areaCode' : '513432',
    'areaName' : '喜德县',
    'py' : 'xide',
    'spy' : 'xd'
}, {
    'areaCode' : '513433',
    'areaName' : '冕宁县',
    'py' : 'mianning',
    'spy' : 'mn'
}, {
    'areaCode' : '513434',
    'areaName' : '越西县',
    'py' : 'yuexi',
    'spy' : 'yx'
}, {
    'areaCode' : '513435',
    'areaName' : '甘洛县',
    'py' : 'ganluo',
    'spy' : 'gl'
}, {
    'areaCode' : '513436',
    'areaName' : '美姑县',
    'py' : 'meigu',
    'spy' : 'mg'
}, {
    'areaCode' : '513437',
    'areaName' : '雷波县',
    'py' : 'leibo',
    'spy' : 'lb'
}]
}]
}, {
    'areaCode' : '520000',
    'areaName' : '贵州省',
    'py' : 'guizhou',
    'spy' : 'gz',
    'datas' : [{
        'areaCode' : '520100',
        'areaName' : '贵阳市',
        'py' : 'guiyang',
        'spy' : 'gy',
        'datas' : [{
            'areaCode' : '520102',
            'areaName' : '南明区',
            'py' : 'nanming',
            'spy' : 'nm'
        }, {
            'areaCode' : '520103',
            'areaName' : '云岩区',
            'py' : 'yunyan',
            'spy' : 'yy'
        }, {
            'areaCode' : '520111',
            'areaName' : '花溪区',
            'py' : 'huaxi',
            'spy' : 'hx'
        }, {
            'areaCode' : '520112',
            'areaName' : '乌当区',
            'py' : 'wudang',
            'spy' : 'wd'
        }, {
            'areaCode' : '520113',
            'areaName' : '白云区',
            'py' : 'baiyun',
            'spy' : 'by'
        }, {
            'areaCode' : '520114',
            'areaName' : '小河区',
            'py' : 'xiaohe',
            'spy' : 'xh'
        }, {
            'areaCode' : '520115',
            'areaName' : '观山湖区',
            'py' : 'guanshanhu',
            'spy' : 'gsh'
        }, {
            'areaCode' : '520121',
            'areaName' : '开阳县',
            'py' : 'kaiyang',
            'spy' : 'ky'
        }, {
        'areaCode' : '520122',
        'areaName' : '息烽县',
        'py' : 'xifeng',
        'spy' : 'xf'
    }, {
    'areaCode' : '520123',
    'areaName' : '修文县',
    'py' : 'xiuwen',
    'spy' : 'xw'
}, {
    'areaCode' : '520181',
    'areaName' : '清镇市',
    'py' : 'qingzhen',
    'spy' : 'qz'
}, {
    'areaCode' : '520182',
    'areaName' : '贵安新区',
    'py' : 'guianxinqu',
    'spy' : 'gaxq'
}]
    }, {
        'areaCode' : '520200',
        'areaName' : '六盘水市',
        'py' : 'liupanshui',
        'spy' : 'lps',
        'datas' : [{
            'areaCode' : '520201',
            'areaName' : '钟山区',
            'py' : 'zhongshan',
            'spy' : 'zs'
        }, {
            'areaCode' : '520203',
            'areaName' : '六枝特区',
            'py' : 'liuzhitequ',
            'spy' : 'lztq'
        }, {
            'areaCode' : '520221',
            'areaName' : '水城县',
            'py' : 'shuicheng',
            'spy' : 'sc'
        }, {
            'areaCode' : '520222',
            'areaName' : '盘县',
            'py' : 'panxian',
            'spy' : 'px'
        }]
    }, {
        'areaCode' : '520300',
        'areaName' : '遵义市',
        'py' : 'zunyi',
        'spy' : 'zy',
        'datas' : [{
            'areaCode' : '520302',
            'areaName' : '红花岗区',
            'py' : 'honghuagang',
            'spy' : 'hhg'
        }, {
            'areaCode' : '520303',
            'areaName' : '汇川区',
            'py' : 'huichuan',
            'spy' : 'hc'
        }, {
            'areaCode' : '520321',
            'areaName' : '遵义县',
            'py' : 'zunyi',
            'spy' : 'zy'
        }, {
            'areaCode' : '520322',
            'areaName' : '桐梓县',
            'py' : 'tongzi',
            'spy' : 'tz'
        }, {
            'areaCode' : '520323',
            'areaName' : '绥阳县',
            'py' : 'suiyang',
            'spy' : 'sy'
        }, {
            'areaCode' : '520324',
            'areaName' : '正安县',
            'py' : 'zhengan',
            'spy' : 'za'
        }, {
        'areaCode' : '520325',
        'areaName' : '道真仡佬族苗族自治县',
        'py' : 'daozhengelaozumiaozuzizhixian',
        'spy' : 'dzglzmzzzx'
    }, {
        'areaCode' : '520326',
        'areaName' : '务川仡佬族苗族自治县',
        'py' : 'wuchuangelaozumiaozuzizhixian',
        'spy' : 'wcglzmzzzx'
    }, {
        'areaCode' : '520327',
        'areaName' : '凤冈县',
        'py' : 'fenggang',
        'spy' : 'fg'
    }, {
    'areaCode' : '520328',
    'areaName' : '湄潭县',
    'py' : 'meitan',
    'spy' : 'mt'
}, {
    'areaCode' : '520329',
    'areaName' : '余庆县',
    'py' : 'yuqing',
    'spy' : 'yq'
}, {
    'areaCode' : '520330',
    'areaName' : '习水县',
    'py' : 'xishui',
    'spy' : 'xs'
}, {
    'areaCode' : '520381',
    'areaName' : '赤水市',
    'py' : 'chishui',
    'spy' : 'cs'
}, {
    'areaCode' : '520382',
    'areaName' : '仁怀市',
    'py' : 'renhuai',
    'spy' : 'rh'
}]
    }, {
        'areaCode' : '520400',
        'areaName' : '安顺市',
        'py' : 'anshun',
        'spy' : 'as',
        'datas' : [{
            'areaCode' : '520402',
            'areaName' : '西秀区',
            'py' : 'xixiu',
            'spy' : 'xx'
        }, {
            'areaCode' : '520421',
            'areaName' : '平坝县',
            'py' : 'pingba',
            'spy' : 'pb'
        }, {
            'areaCode' : '520422',
            'areaName' : '普定县',
            'py' : 'puding',
            'spy' : 'pd'
        }, {
            'areaCode' : '520423',
            'areaName' : '镇宁布依族苗族自治县',
            'py' : 'zhenningbuyizumiaozuzizhixian',
            'spy' : 'znbyzmzzzx'
        }, {
            'areaCode' : '520424',
            'areaName' : '关岭布依族苗族自治县',
            'py' : 'guanlingbuyizumiaozuzizhixian',
            'spy' : 'glbyzmzzzx'
        }, {
        'areaCode' : '520425',
        'areaName' : '紫云苗族布依族自治县',
        'py' : 'ziyunmiaozubuyizuzizhixian',
        'spy' : 'zymzbyzzzx'
    }]
    }, {
        'areaCode' : '520500',
        'areaName' : '毕节市',
        'py' : 'bijie',
        'spy' : 'bj',
        'datas' : [{
            'areaCode' : '520502',
            'areaName' : '七星关区',
            'py' : 'qixingguan',
            'spy' : 'qxg'
        }, {
            'areaCode' : '520521',
            'areaName' : '大方县',
            'py' : 'dafang',
            'spy' : 'df'
        }, {
            'areaCode' : '520522',
            'areaName' : '黔西县',
            'py' : 'qianxi',
            'spy' : 'qx'
        }, {
            'areaCode' : '520523',
            'areaName' : '金沙县',
            'py' : 'jinsha',
            'spy' : 'js'
        }, {
        'areaCode' : '520524',
        'areaName' : '织金县',
        'py' : 'zhijin',
        'spy' : 'zj'
    }, {
        'areaCode' : '520525',
        'areaName' : '纳雍县',
        'py' : 'nayong',
        'spy' : 'ny'
    }, {
        'areaCode' : '520526',
        'areaName' : '威宁彝族回族苗族自治县',
        'py' : 'weiningyizuhuizumiaozuzizhixian',
        'spy' : 'wnyzhzmzzzx'
    }, {
        'areaCode' : '520527',
        'areaName' : '赫章县',
        'py' : 'hezhang',
        'spy' : 'hz'
    }]
    }, {
        'areaCode' : '520600',
        'areaName' : '铜仁市',
        'py' : 'tongren',
        'spy' : 'tr',
        'datas' : [{
            'areaCode' : '520602',
            'areaName' : '碧江区',
            'py' : 'bijiang',
            'spy' : 'bj'
        }, {
            'areaCode' : '520603',
            'areaName' : '万山区',
            'py' : 'wanshan',
            'spy' : 'ws'
        }, {
            'areaCode' : '520621',
            'areaName' : '江口县',
            'py' : 'jiangkou',
            'spy' : 'jk'
        }, {
        'areaCode' : '520622',
        'areaName' : '玉屏侗族自治县',
        'py' : 'yupingdongzuzizhixian',
        'spy' : 'ypdzzzx'
    }, {
        'areaCode' : '520623',
        'areaName' : '石阡县',
        'py' : 'shiqian',
        'spy' : 'sq'
    }, {
        'areaCode' : '520624',
        'areaName' : '思南县',
        'py' : 'sinan',
        'spy' : 'sn'
    }, {
        'areaCode' : '520625',
        'areaName' : '印江土家族苗族自治县',
        'py' : 'yinjiangtujiazumiaozuzizhixian',
        'spy' : 'yjtjzmzzzx'
    }, {
        'areaCode' : '520626',
        'areaName' : '德江县',
        'py' : 'dejiang',
        'spy' : 'dj'
    }, {
        'areaCode' : '520627',
        'areaName' : '沿河土家族自治县',
        'py' : 'yanhetujiazuzizhixian',
        'spy' : 'yhtjzzzx'
    }, {
    'areaCode' : '520628',
    'areaName' : '松桃苗族自治县',
    'py' : 'songtaomiaozuzizhixian',
    'spy' : 'stmzzzx'
}]
    }, {
        'areaCode' : '522300',
        'areaName' : '黔西南布依族苗族自治州',
        'py' : 'qianxinanbuyizumiaozuzizhizhou',
        'spy' : 'qxnbyzmzzzz',
        'datas' : [{
            'areaCode' : '522301',
            'areaName' : '兴义市',
            'py' : 'xingyi',
            'spy' : 'xy'
        }, {
            'areaCode' : '522322',
            'areaName' : '兴仁县',
            'py' : 'xingren',
            'spy' : 'xr'
        }, {
        'areaCode' : '522323',
        'areaName' : '普安县',
        'py' : 'puan',
        'spy' : 'pa'
    }, {
        'areaCode' : '522324',
        'areaName' : '晴隆县',
        'py' : 'qinglong',
        'spy' : 'ql'
    }, {
        'areaCode' : '522325',
        'areaName' : '贞丰县',
        'py' : 'zhenfeng',
        'spy' : 'zf'
    }, {
        'areaCode' : '522326',
        'areaName' : '望谟县',
        'py' : 'wangmo',
        'spy' : 'wm'
    }, {
        'areaCode' : '522327',
        'areaName' : '册亨县',
        'py' : 'cexiang',
        'spy' : 'cx'
    }, {
        'areaCode' : '522328',
        'areaName' : '安龙县',
        'py' : 'anlong',
        'spy' : 'al'
    }]
    }, {
        'areaCode' : '522600',
        'areaName' : '黔东南苗族侗族自治州',
        'py' : 'qiandongnanmiaozudongzuzizhizhou',
        'spy' : 'qdnmzdzzzz',
        'datas' : [{
            'areaCode' : '522601',
            'areaName' : '凯里市',
            'py' : 'kaili',
            'spy' : 'kl'
        }, {
        'areaCode' : '522622',
        'areaName' : '黄平县',
        'py' : 'huangping',
        'spy' : 'hp'
    }, {
        'areaCode' : '522623',
        'areaName' : '施秉县',
        'py' : 'shibing',
        'spy' : 'sb'
    }, {
        'areaCode' : '522624',
        'areaName' : '三穗县',
        'py' : 'sansui',
        'spy' : 'ss'
    }, {
        'areaCode' : '522625',
        'areaName' : '镇远县',
        'py' : 'zhenyuan',
        'spy' : 'zy'
    }, {
        'areaCode' : '522626',
        'areaName' : '岑巩县',
        'py' : 'cengong',
        'spy' : 'cg'
    }, {
        'areaCode' : '522627',
        'areaName' : '天柱县',
        'py' : 'tianzhu',
        'spy' : 'tz'
    }, {
        'areaCode' : '522628',
        'areaName' : '锦屏县',
        'py' : 'jinping',
        'spy' : 'jp'
    }, {
        'areaCode' : '522629',
        'areaName' : '剑河县',
        'py' : 'jianhe',
        'spy' : 'jh'
    }, {
    'areaCode' : '522630',
    'areaName' : '台江县',
    'py' : 'taijiang',
    'spy' : 'tj'
}, {
    'areaCode' : '522631',
    'areaName' : '黎平县',
    'py' : 'liping',
    'spy' : 'lp'
}, {
    'areaCode' : '522632',
    'areaName' : '榕江县',
    'py' : 'rongjiang',
    'spy' : 'rj'
}, {
    'areaCode' : '522633',
    'areaName' : '从江县',
    'py' : 'congjiang',
    'spy' : 'cj'
}, {
    'areaCode' : '522634',
    'areaName' : '雷山县',
    'py' : 'leishan',
    'spy' : 'ls'
}, {
    'areaCode' : '522635',
    'areaName' : '麻江县',
    'py' : 'majiang',
    'spy' : 'mj'
}, {
    'areaCode' : '522636',
    'areaName' : '丹寨县',
    'py' : 'danzhai',
    'spy' : 'dz'
}]
    }, {
        'areaCode' : '522700',
        'areaName' : '黔南布依族苗族自治州',
        'py' : 'qiannanbuyizumiaozuzizhizhou',
        'spy' : 'qnbyzmzzzz',
        'datas' : [{
        'areaCode' : '522701',
        'areaName' : '都匀市',
        'py' : 'duyun',
        'spy' : 'dy'
    }, {
        'areaCode' : '522702',
        'areaName' : '福泉市',
        'py' : 'fuquan',
        'spy' : 'fq'
    }, {
        'areaCode' : '522722',
        'areaName' : '荔波县',
        'py' : 'libo',
        'spy' : 'lb'
    }, {
        'areaCode' : '522723',
        'areaName' : '贵定县',
        'py' : 'guiding',
        'spy' : 'gd'
    }, {
        'areaCode' : '522725',
        'areaName' : '瓮安县',
        'py' : 'wengan',
        'spy' : 'wa'
    }, {
        'areaCode' : '522726',
        'areaName' : '独山县',
        'py' : 'dushan',
        'spy' : 'ds'
    }, {
        'areaCode' : '522727',
        'areaName' : '平塘县',
        'py' : 'pingtang',
        'spy' : 'pt'
    }, {
        'areaCode' : '522728',
        'areaName' : '罗甸县',
        'py' : 'luodian',
        'spy' : 'ld'
    }, {
        'areaCode' : '522729',
        'areaName' : '长顺县',
        'py' : 'changshun',
        'spy' : 'cs'
    }, {
    'areaCode' : '522730',
    'areaName' : '龙里县',
    'py' : 'longli',
    'spy' : 'll'
}, {
    'areaCode' : '522731',
    'areaName' : '惠水县',
    'py' : 'huishui',
    'spy' : 'hs'
}, {
    'areaCode' : '522732',
    'areaName' : '三都水族自治县',
    'py' : 'sandushuizuzizhixian',
    'spy' : 'sdszzzx'
}]
    }]
}, {
    'areaCode' : '530000',
    'areaName' : '云南省',
    'py' : 'yunnan',
    'spy' : 'yn',
    'datas' : [{
        'areaCode' : '530100',
        'areaName' : '昆明市',
        'py' : 'kunming',
        'spy' : 'km',
        'datas' : [{
            'areaCode' : '530102',
            'areaName' : '五华区',
            'py' : 'wuhua',
            'spy' : 'wh'
        }, {
            'areaCode' : '530103',
            'areaName' : '盘龙区',
            'py' : 'panlong',
            'spy' : 'pl'
        }, {
            'areaCode' : '530111',
            'areaName' : '官渡区',
            'py' : 'guandu',
            'spy' : 'gd'
        }, {
            'areaCode' : '530112',
            'areaName' : '西山区',
            'py' : 'xishan',
            'spy' : 'xs'
        }, {
            'areaCode' : '530113',
            'areaName' : '东川区',
            'py' : 'dongchuan',
            'spy' : 'dc'
        }, {
            'areaCode' : '530114',
            'areaName' : '呈贡区',
            'py' : 'chenggong',
            'spy' : 'cg'
        }, {
            'areaCode' : '530122',
            'areaName' : '晋宁县',
            'py' : 'puning',
            'spy' : 'pn'
        }, {
            'areaCode' : '530124',
            'areaName' : '富民县',
            'py' : 'fumin',
            'spy' : 'fm'
        }, {
        'areaCode' : '530125',
        'areaName' : '宜良县',
        'py' : 'yiliang',
        'spy' : 'yl'
    }, {
    'areaCode' : '530126',
    'areaName' : '石林彝族自治县',
    'py' : 'shilinyizuzizhixian',
    'spy' : 'slyzzzx'
}, {
    'areaCode' : '530127',
    'areaName' : '嵩明县',
    'py' : 'songming',
    'spy' : 'sm'
}, {
    'areaCode' : '530128',
    'areaName' : '禄劝彝族苗族自治县',
    'py' : 'luquanyizumiaozuzizhixian',
    'spy' : 'lqyzmzzzx'
}, {
    'areaCode' : '530129',
    'areaName' : '寻甸回族彝族自治县',
    'py' : 'xundianhuizuyizuzizhixian',
    'spy' : 'xdhzyzzzx'
}, {
    'areaCode' : '530181',
    'areaName' : '安宁市',
    'py' : 'anning',
    'spy' : 'an'
}]
    }, {
        'areaCode' : '530300',
        'areaName' : '曲靖市',
        'py' : 'qujing',
        'spy' : 'qj',
        'datas' : [{
            'areaCode' : '530302',
            'areaName' : '麒麟区',
            'py' : 'qilin',
            'spy' : 'ql'
        }, {
            'areaCode' : '530321',
            'areaName' : '马龙县',
            'py' : 'malong',
            'spy' : 'ml'
        }, {
            'areaCode' : '530322',
            'areaName' : '陆良县',
            'py' : 'luliang',
            'spy' : 'll'
        }, {
            'areaCode' : '530323',
            'areaName' : '师宗县',
            'py' : 'shizong',
            'spy' : 'sz'
        }, {
            'areaCode' : '530324',
            'areaName' : '罗平县',
            'py' : 'luoping',
            'spy' : 'lp'
        }, {
            'areaCode' : '530325',
            'areaName' : '富源县',
            'py' : 'fuyuan',
            'spy' : 'fy'
        }, {
            'areaCode' : '530326',
            'areaName' : '会泽县',
            'py' : 'huize',
            'spy' : 'hz'
        }, {
        'areaCode' : '530328',
        'areaName' : '沾益县',
        'py' : 'zhanyi',
        'spy' : 'zy'
    }, {
        'areaCode' : '530381',
        'areaName' : '宣威市',
        'py' : 'xuanwei',
        'spy' : 'xw'
    }]
    }, {
        'areaCode' : '530400',
        'areaName' : '玉溪市',
        'py' : 'yuxi',
        'spy' : 'yx',
        'datas' : [{
            'areaCode' : '530402',
            'areaName' : '红塔区',
            'py' : 'hongta',
            'spy' : 'ht'
        }, {
            'areaCode' : '530421',
            'areaName' : '江川县',
            'py' : 'jiangchuan',
            'spy' : 'jc'
        }, {
            'areaCode' : '530422',
            'areaName' : '澄江县',
            'py' : 'chengjiang',
            'spy' : 'cj'
        }, {
            'areaCode' : '530423',
            'areaName' : '通海县',
            'py' : 'tonghai',
            'spy' : 'th'
        }, {
            'areaCode' : '530424',
            'areaName' : '华宁县',
            'py' : 'huaning',
            'spy' : 'hn'
        }, {
            'areaCode' : '530425',
            'areaName' : '易门县',
            'py' : 'yimen',
            'spy' : 'ym'
        }, {
        'areaCode' : '530426',
        'areaName' : '峨山彝族自治县',
        'py' : 'eshanyizuzizhixian',
        'spy' : 'esyzzzx'
    }, {
        'areaCode' : '530427',
        'areaName' : '新平彝族傣族自治县',
        'py' : 'xinpingyizudaizuzizhixian',
        'spy' : 'xpyzdzzzx'
    }, {
        'areaCode' : '530428',
        'areaName' : '元江哈尼族彝族傣族自治县',
        'py' : 'yuanjianghanizuyizudaizuzizhixian',
        'spy' : 'yjhnzdzzzx'
    }]
    }, {
        'areaCode' : '530500',
        'areaName' : '保山市',
        'py' : 'baoshan',
        'spy' : 'bs',
        'datas' : [{
            'areaCode' : '530502',
            'areaName' : '隆阳区',
            'py' : 'longyang',
            'spy' : 'ly'
        }, {
            'areaCode' : '530521',
            'areaName' : '施甸县',
            'py' : 'shidian',
            'spy' : 'sd'
        }, {
            'areaCode' : '530522',
            'areaName' : '腾冲县',
            'py' : 'tengchong',
            'spy' : 'tc'
        }, {
            'areaCode' : '530523',
            'areaName' : '龙陵县',
            'py' : 'longling',
            'spy' : 'll'
        }, {
            'areaCode' : '530524',
            'areaName' : '昌宁县',
            'py' : 'changning',
            'spy' : 'cn'
        }]
    }, {
        'areaCode' : '530600',
        'areaName' : '昭通市',
        'py' : 'zhaotong',
        'spy' : 'zt',
        'datas' : [{
            'areaCode' : '530602',
            'areaName' : '昭阳区',
            'py' : 'zhaoyang',
            'spy' : 'zy'
        }, {
            'areaCode' : '530621',
            'areaName' : '鲁甸县',
            'py' : 'ludian',
            'spy' : 'ld'
        }, {
            'areaCode' : '530622',
            'areaName' : '巧家县',
            'py' : 'qiaojia',
            'spy' : 'qj'
        }, {
            'areaCode' : '530623',
            'areaName' : '盐津县',
            'py' : 'yanjin',
            'spy' : 'yj'
        }, {
        'areaCode' : '530624',
        'areaName' : '大关县',
        'py' : 'daguan',
        'spy' : 'dg'
    }, {
        'areaCode' : '530625',
        'areaName' : '永善县',
        'py' : 'yongshan',
        'spy' : 'ys'
    }, {
        'areaCode' : '530626',
        'areaName' : '绥江县',
        'py' : 'suijiang',
        'spy' : 'sj'
    }, {
        'areaCode' : '530627',
        'areaName' : '镇雄县',
        'py' : 'zhenxiong',
        'spy' : 'zx'
    }, {
        'areaCode' : '530628',
        'areaName' : '彝良县',
        'py' : 'yiliang',
        'spy' : 'yl'
    }, {
    'areaCode' : '530629',
    'areaName' : '威信县',
    'py' : 'weixin',
    'spy' : 'wx'
}, {
    'areaCode' : '530630',
    'areaName' : '水富县',
    'py' : 'shuifu',
    'spy' : 'sf'
}]
    }, {
        'areaCode' : '530700',
        'areaName' : '丽江市',
        'py' : 'lijiang',
        'spy' : 'lj',
        'datas' : [{
            'areaCode' : '530702',
            'areaName' : '古城区',
            'py' : 'gucheng',
            'spy' : 'gc'
        }, {
            'areaCode' : '530721',
            'areaName' : '玉龙纳西族自治县',
            'py' : 'yulongnaxizuzizhixian',
            'spy' : 'ylnxzzzx'
        }, {
            'areaCode' : '530722',
            'areaName' : '永胜县',
            'py' : 'yongsheng',
            'spy' : 'ys'
        }, {
        'areaCode' : '530723',
        'areaName' : '华坪县',
        'py' : 'huaping',
        'spy' : 'hp'
    }, {
        'areaCode' : '530724',
        'areaName' : '宁蒗彝族自治县',
        'py' : 'ninglangyizuzizhixian',
        'spy' : 'nlyzzzx'
    }]
    }, {
        'areaCode' : '530800',
        'areaName' : '普洱市',
        'py' : 'puer',
        'spy' : 'pe',
        'datas' : [{
            'areaCode' : '530802',
            'areaName' : '思茅区',
            'py' : 'simao',
            'spy' : 'sm'
        }, {
            'areaCode' : '530821',
            'areaName' : '宁洱哈尼族彝族自治县',
            'py' : 'ningerhanizuyizuzizhixian',
            'spy' : 'nehnzyzzzx'
        }, {
        'areaCode' : '530822',
        'areaName' : '墨江哈尼族自治县',
        'py' : 'mojianghanizuzizhixian',
        'spy' : 'mjhnzzzx'
    }, {
        'areaCode' : '530823',
        'areaName' : '景东彝族自治县',
        'py' : 'jingdongyizuzizhixian',
        'spy' : 'jdyzzzx'
    }, {
        'areaCode' : '530824',
        'areaName' : '景谷傣族彝族自治县',
        'py' : 'jinggudaizuyizuzizhixian',
        'spy' : 'jgdzyzzzx'
    }, {
        'areaCode' : '530825',
        'areaName' : '镇沅彝族哈尼族拉祜族自治县',
        'py' : 'zhenyuanyizuhanizulahuzuzizhixian',
        'spy' : 'zyyzhnzlhzzzx'
    }, {
        'areaCode' : '530826',
        'areaName' : '江城哈尼族彝族自治县',
        'py' : 'jiangchenghanizuyizuzizhixian',
        'spy' : 'jchnzyzzzx'
    }, {
        'areaCode' : '530827',
        'areaName' : '孟连傣族拉祜族佤族自治县',
        'py' : 'mengliandaizulahuzuwazuzizhixian',
        'spy' : 'mldzlhzwzzzx'
    }, {
        'areaCode' : '530828',
        'areaName' : '澜沧拉祜族自治县',
        'py' : 'lancanglahuzuzizhixian',
        'spy' : 'lclhzzzx'
    }, {
    'areaCode' : '530829',
    'areaName' : '西盟佤族自治县',
    'py' : 'ximengwazuzizhixian',
    'spy' : 'xmwzzzx'
}]
    }, {
        'areaCode' : '530900',
        'areaName' : '临沧市',
        'py' : 'lincang',
        'spy' : 'lc',
        'datas' : [{
            'areaCode' : '530902',
            'areaName' : '临翔区',
            'py' : 'linxiang',
            'spy' : 'lx'
        }, {
        'areaCode' : '530921',
        'areaName' : '凤庆县',
        'py' : 'fengqing',
        'spy' : 'fq'
    }, {
        'areaCode' : '530922',
        'areaName' : '云县',
        'py' : 'yunxian',
        'spy' : 'yx'
    }, {
        'areaCode' : '530923',
        'areaName' : '永德县',
        'py' : 'yongde',
        'spy' : 'yd'
    }, {
        'areaCode' : '530924',
        'areaName' : '镇康县',
        'py' : 'zhenkang',
        'spy' : 'zk'
    }, {
        'areaCode' : '530925',
        'areaName' : '双江拉祜族佤族布朗族傣族自治县',
        'py' : 'shuangjianglahuzuwazubulangzudaizuzizhixian',
        'spy' : 'sjlhzwzblzdzzzx'
    }, {
        'areaCode' : '530926',
        'areaName' : '耿马傣族佤族自治县',
        'py' : 'genmadaizuwazuzizhixian',
        'spy' : 'gmdzwzzzx'
    }, {
        'areaCode' : '530927',
        'areaName' : '沧源佤族自治县',
        'py' : 'cangyuanwazuzizhixian',
        'spy' : 'cywzzzx'
    }]
    }, {
        'areaCode' : '532300',
        'areaName' : '楚雄彝族自治州',
        'py' : 'chuxiongyizuzizhizhou',
        'spy' : 'cxyzzzz',
        'datas' : [{
        'areaCode' : '532301',
        'areaName' : '楚雄市',
        'py' : 'chuxiong',
        'spy' : 'cx'
    }, {
        'areaCode' : '532322',
        'areaName' : '双柏县',
        'py' : 'shuangbai',
        'spy' : 'sb'
    }, {
        'areaCode' : '532323',
        'areaName' : '牟定县',
        'py' : 'mouding',
        'spy' : 'md'
    }, {
        'areaCode' : '532324',
        'areaName' : '南华县',
        'py' : 'nanhua',
        'spy' : 'nh'
    }, {
        'areaCode' : '532325',
        'areaName' : '姚安县',
        'py' : 'yaoan',
        'spy' : 'ya'
    }, {
        'areaCode' : '532326',
        'areaName' : '大姚县',
        'py' : 'dayao',
        'spy' : 'dy'
    }, {
        'areaCode' : '532327',
        'areaName' : '永仁县',
        'py' : 'yongren',
        'spy' : 'yr'
    }, {
        'areaCode' : '532328',
        'areaName' : '元谋县',
        'py' : 'yuanmou',
        'spy' : 'ym'
    }, {
        'areaCode' : '532329',
        'areaName' : '武定县',
        'py' : 'wuding',
        'spy' : 'wd'
    }, {
    'areaCode' : '532331',
    'areaName' : '禄丰县',
    'py' : 'lufeng',
    'spy' : 'lf'
}]
    }, {
    'areaCode' : '532500',
    'areaName' : '红河哈尼族彝族自治州',
    'py' : 'honghehanizuyizuzizhizhou',
    'spy' : 'hhhnzyzzzz',
    'datas' : [{
        'areaCode' : '532501',
        'areaName' : '个旧市',
        'py' : 'gejiu',
        'spy' : 'gj'
    }, {
        'areaCode' : '532502',
        'areaName' : '开远市',
        'py' : 'kaiyuan',
        'spy' : 'ky'
    }, {
        'areaCode' : '532522',
        'areaName' : '蒙自市',
        'py' : 'mengzi',
        'spy' : 'mz'
    }, {
        'areaCode' : '532523',
        'areaName' : '屏边苗族自治县',
        'py' : 'pingbianmiaozuzizhixian',
        'spy' : 'pbmzzzx'
    }, {
        'areaCode' : '532524',
        'areaName' : '建水县',
        'py' : 'jianshui',
        'spy' : 'js'
    }, {
        'areaCode' : '532525',
        'areaName' : '石屏县',
        'py' : 'shiping',
        'spy' : 'sp'
    }, {
        'areaCode' : '532526',
        'areaName' : '弥勒县',
        'py' : 'mile',
        'spy' : 'ml'
    }, {
        'areaCode' : '532527',
        'areaName' : '泸西县',
        'py' : 'luxi',
        'spy' : 'lx'
    }, {
        'areaCode' : '532528',
        'areaName' : '元阳县',
        'py' : 'yuanyang',
        'spy' : 'yy'
    }, {
    'areaCode' : '532529',
    'areaName' : '红河县',
    'py' : 'honghe',
    'spy' : 'hh'
}, {
    'areaCode' : '532530',
    'areaName' : '金平苗族瑶族傣族自治县',
    'py' : 'jinpingmiaozuyaozudaizuzizhixian',
    'spy' : 'jpmzyzdzzzx'
}, {
    'areaCode' : '532531',
    'areaName' : '绿春县',
    'py' : 'lvchun',
    'spy' : 'lc'
}, {
    'areaCode' : '532532',
    'areaName' : '河口瑶族自治县',
    'py' : 'hekouyaozuzizhixian',
    'spy' : 'hkyzzzx'
}]
}, {
    'areaCode' : '532600',
    'areaName' : '文山壮族苗族自治州',
    'py' : 'wenshanzhuangzumiaozuzizhizhou',
    'spy' : 'wszzmzzzz',
    'datas' : [{
        'areaCode' : '532601',
        'areaName' : '文山市',
        'py' : 'wenshan',
        'spy' : 'ws'
    }, {
        'areaCode' : '532622',
        'areaName' : '砚山县',
        'py' : 'yanshan',
        'spy' : 'ys'
    }, {
        'areaCode' : '532623',
        'areaName' : '西畴县',
        'py' : 'xichou',
        'spy' : 'xc'
    }, {
        'areaCode' : '532624',
        'areaName' : '麻栗坡县',
        'py' : 'malipo',
        'spy' : 'mlp'
    }, {
        'areaCode' : '532625',
        'areaName' : '马关县',
        'py' : 'maguan',
        'spy' : 'mg'
    }, {
        'areaCode' : '532626',
        'areaName' : '丘北县',
        'py' : 'qiubei',
        'spy' : 'qb'
    }, {
        'areaCode' : '532627',
        'areaName' : '广南县',
        'py' : 'guangnan',
        'spy' : 'gn'
    }, {
        'areaCode' : '532628',
        'areaName' : '富宁县',
        'py' : 'funing',
        'spy' : 'fn'
    }]
}, {
    'areaCode' : '532800',
    'areaName' : '西双版纳傣族自治州',
    'py' : 'xishuangbannadaizuzizhizhou',
    'spy' : 'xsbndzzzz',
    'datas' : [{
        'areaCode' : '532801',
        'areaName' : '景洪市',
        'py' : 'jinghong',
        'spy' : 'jh'
    }, {
        'areaCode' : '532822',
        'areaName' : '勐海县',
        'py' : 'menghai',
        'spy' : 'mh'
    }, {
        'areaCode' : '532823',
        'areaName' : '勐腊县',
        'py' : 'mengla',
        'spy' : 'ml'
    }]
}, {
    'areaCode' : '532900',
    'areaName' : '大理白族自治州',
    'py' : 'dalibaizuzizhizhou',
    'spy' : 'dlbzzzz',
    'datas' : [{
        'areaCode' : '532901',
        'areaName' : '大理市',
        'py' : 'dali',
        'spy' : 'dl'
    }, {
        'areaCode' : '532922',
        'areaName' : '漾濞彝族自治县',
        'py' : 'yangbiyizuzizhizhou',
        'spy' : 'ybyzzzz'
    }, {
        'areaCode' : '532923',
        'areaName' : '祥云县',
        'py' : 'xiangyun',
        'spy' : 'xy'
    }, {
        'areaCode' : '532924',
        'areaName' : '宾川县',
        'py' : 'binchuan',
        'spy' : 'bc'
    }, {
        'areaCode' : '532925',
        'areaName' : '弥渡县',
        'py' : 'midu',
        'spy' : 'md'
    }, {
        'areaCode' : '532926',
        'areaName' : '南涧彝族自治县',
        'py' : 'nanjianyizuzizhixian',
        'spy' : 'njyzzzx'
    }, {
        'areaCode' : '532927',
        'areaName' : '巍山彝族回族自治县',
        'py' : 'weishanyizuhuizuzizhixian',
        'spy' : 'wsyzhzzzx'
    }, {
        'areaCode' : '532928',
        'areaName' : '永平县',
        'py' : 'yongping',
        'spy' : 'yp'
    }, {
        'areaCode' : '532929',
        'areaName' : '云龙县',
        'py' : 'yunlong',
        'spy' : 'yl'
    }, {
    'areaCode' : '532930',
    'areaName' : '洱源县',
    'py' : 'eryuan',
    'spy' : 'ey'
}, {
    'areaCode' : '532931',
    'areaName' : '剑川县',
    'py' : 'jianchuan',
    'spy' : 'jc'
}, {
    'areaCode' : '532932',
    'areaName' : '鹤庆县',
    'py' : 'heqing',
    'spy' : 'hq'
}]
}, {
    'areaCode' : '533100',
    'areaName' : '德宏傣族景颇族自治州',
    'py' : 'dehongdaizujingpozuzizhizhou',
    'spy' : 'dhdzjpzzzz',
    'datas' : [{
        'areaCode' : '533102',
        'areaName' : '瑞丽市',
        'py' : 'ruili',
        'spy' : 'rl'
    }, {
        'areaCode' : '533103',
        'areaName' : '芒市',
        'py' : 'mangshi',
        'spy' : 'ms'
    }, {
        'areaCode' : '533104',
        'areaName' : '潞西市',
        'py' : 'luxi',
        'spy' : 'lx'
    }, {
        'areaCode' : '533122',
        'areaName' : '梁河县',
        'py' : 'lianghe',
        'spy' : 'lh'
    }, {
        'areaCode' : '533123',
        'areaName' : '盈江县',
        'py' : 'yingjiang',
        'spy' : 'yj'
    }, {
        'areaCode' : '533124',
        'areaName' : '陇川县',
        'py' : 'longchuan',
        'spy' : 'lc'
    }]
}, {
    'areaCode' : '533300',
    'areaName' : '怒江傈僳族自治州',
    'py' : 'nujianglisuzuzizhizhou',
    'spy' : 'njlszzzz',
    'datas' : [{
        'areaCode' : '533321',
        'areaName' : '泸水县',
        'py' : 'lushui',
        'spy' : 'ls'
    }, {
        'areaCode' : '533323',
        'areaName' : '福贡县',
        'py' : 'fugong',
        'spy' : 'fg'
    }, {
        'areaCode' : '533324',
        'areaName' : '贡山独龙族怒族自治县',
        'py' : 'gongshandulongzunuzuzizhixian',
        'spy' : 'gsdlznzzzx'
    }, {
        'areaCode' : '533325',
        'areaName' : '兰坪白族普米族自治县',
        'py' : 'lanpingbaizupumizuzizhixian',
        'spy' : 'lpbzpmzzzx'
    }]
}, {
    'areaCode' : '533400',
    'areaName' : '迪庆藏族自治州',
    'py' : 'diqingzangzuzizhizhou',
    'spy' : 'dqzzzzz',
    'datas' : [{
        'areaCode' : '533421',
        'areaName' : '香格里拉县',
        'py' : 'xianggelila',
        'spy' : 'xgll'
    }, {
        'areaCode' : '533422',
        'areaName' : '德钦县',
        'py' : 'deqin',
        'spy' : 'dq'
    }, {
        'areaCode' : '533423',
        'areaName' : '维西傈僳族自治县',
        'py' : 'weixlisuzuzizhixian',
        'spy' : 'wxlszzx'
    }]
}]
}, {
    'areaCode' : '540000',
    'areaName' : '西藏自治区',
    'py' : 'xizangzizhiqu',
    'spy' : 'xzzzq',
    'datas' : [{
        'areaCode' : '540100',
        'areaName' : '拉萨市',
        'py' : 'lasa',
        'spy' : 'ls',
        'datas' : [{
            'areaCode' : '540102',
            'areaName' : '城关区',
            'py' : 'chengguan',
            'spy' : 'cg'
        }, {
            'areaCode' : '540121',
            'areaName' : '林周县',
            'py' : 'linzhou',
            'spy' : 'lz'
        }, {
            'areaCode' : '540122',
            'areaName' : '当雄县',
            'py' : 'dangxiong',
            'spy' : 'dx'
        }, {
            'areaCode' : '540123',
            'areaName' : '尼木县',
            'py' : 'nimu',
            'spy' : 'nm'
        }, {
            'areaCode' : '540124',
            'areaName' : '曲水县',
            'py' : 'qushui',
            'spy' : 'qs'
        }, {
            'areaCode' : '540125',
            'areaName' : '堆龙德庆县',
            'py' : 'duilongdeqing',
            'spy' : 'dldq'
        }, {
            'areaCode' : '540126',
            'areaName' : '达孜县',
            'py' : 'dazi',
            'spy' : 'dz'
        }, {
            'areaCode' : '540127',
            'areaName' : '墨竹工卡县',
            'py' : 'mozhugongka',
            'spy' : 'mzgk'
        }]
    }, {
        'areaCode' : '542100',
        'areaName' : '昌都地区',
        'py' : 'changdudiqu',
        'spy' : 'cddq',
        'datas' : [{
            'areaCode' : '542121',
            'areaName' : '昌都县',
            'py' : 'changdu',
            'spy' : 'cd'
        }, {
            'areaCode' : '542122',
            'areaName' : '江达县',
            'py' : 'jiangda',
            'spy' : 'jd'
        }, {
            'areaCode' : '542123',
            'areaName' : '贡觉县',
            'py' : 'gongjue',
            'spy' : 'gj'
        }, {
            'areaCode' : '542124',
            'areaName' : '类乌齐县',
            'py' : 'leiwuqi',
            'spy' : 'lwq'
        }, {
            'areaCode' : '542125',
            'areaName' : '丁青县',
            'py' : 'dingqing',
            'spy' : 'dq'
        }, {
            'areaCode' : '542126',
            'areaName' : '察雅县',
            'py' : 'chaya',
            'spy' : 'cy'
        }, {
            'areaCode' : '542127',
            'areaName' : '八宿县',
            'py' : 'basu',
            'spy' : 'bs'
        }, {
        'areaCode' : '542128',
        'areaName' : '左贡县',
        'py' : 'zuogong',
        'spy' : 'zg'
    }, {
        'areaCode' : '542129',
        'areaName' : '芒康县',
        'py' : 'mangkang',
        'spy' : 'mk'
    }, {
    'areaCode' : '542132',
    'areaName' : '洛隆县',
    'py' : 'luolong',
    'spy' : 'll'
}, {
    'areaCode' : '542133',
    'areaName' : '边坝县',
    'py' : 'bianba',
    'spy' : 'bb'
}]
    }, {
        'areaCode' : '542200',
        'areaName' : '山南地区',
        'py' : 'shannandiqu',
        'spy' : 'sndq',
        'datas' : [{
            'areaCode' : '542221',
            'areaName' : '乃东县',
            'py' : 'naidong',
            'spy' : 'nd'
        }, {
            'areaCode' : '542222',
            'areaName' : '扎囊县',
            'py' : 'zhanang',
            'spy' : 'zn'
        }, {
            'areaCode' : '542223',
            'areaName' : '贡嘎县',
            'py' : 'gongga',
            'spy' : 'gg'
        }, {
            'areaCode' : '542224',
            'areaName' : '桑日县',
            'py' : 'sangri',
            'spy' : 'sr'
        }, {
            'areaCode' : '542225',
            'areaName' : '琼结县',
            'py' : 'qiongjie',
            'spy' : 'qj'
        }, {
            'areaCode' : '542226',
            'areaName' : '曲松县',
            'py' : 'qusong',
            'spy' : 'qs'
        }, {
        'areaCode' : '542227',
        'areaName' : '措美县',
        'py' : 'cuomei',
        'spy' : 'cm'
    }, {
        'areaCode' : '542228',
        'areaName' : '洛扎县',
        'py' : 'luozha',
        'spy' : 'lz'
    }, {
        'areaCode' : '542229',
        'areaName' : '加查县',
        'py' : 'jiacha',
        'spy' : 'jc'
    }, {
    'areaCode' : '542231',
    'areaName' : '隆子县',
    'py' : 'longzi',
    'spy' : 'lz'
}, {
    'areaCode' : '542232',
    'areaName' : '错那县',
    'py' : 'cuona',
    'spy' : 'cn'
}, {
    'areaCode' : '542233',
    'areaName' : '浪卡子县',
    'py' : 'langkazi',
    'spy' : 'lkz'
}]
    }, {
        'areaCode' : '542300',
        'areaName' : '日喀则地区',
        'py' : 'rikezediqu',
        'spy' : 'rkzdq',
        'datas' : [{
            'areaCode' : '542301',
            'areaName' : '日喀则市',
            'py' : 'rikeze',
            'spy' : 'rkz'
        }, {
            'areaCode' : '542322',
            'areaName' : '南木林县',
            'py' : 'nanmulin',
            'spy' : 'nml'
        }, {
            'areaCode' : '542323',
            'areaName' : '江孜县',
            'py' : 'jiangzi',
            'spy' : 'jz'
        }, {
            'areaCode' : '542324',
            'areaName' : '定日县',
            'py' : 'dingri',
            'spy' : 'dr'
        }, {
            'areaCode' : '542325',
            'areaName' : '萨迦县',
            'py' : 'sajia',
            'spy' : 'sj'
        }, {
        'areaCode' : '542326',
        'areaName' : '拉孜县',
        'py' : 'lazi',
        'spy' : 'lz'
    }, {
        'areaCode' : '542327',
        'areaName' : '昂仁县',
        'py' : 'angren',
        'spy' : 'ar'
    }, {
        'areaCode' : '542328',
        'areaName' : '谢通门县',
        'py' : 'xietongmen',
        'spy' : 'xtm'
    }, {
        'areaCode' : '542329',
        'areaName' : '白朗县',
        'py' : 'bailang',
        'spy' : 'bl'
    }, {
    'areaCode' : '542330',
    'areaName' : '仁布县',
    'py' : 'renbu',
    'spy' : 'rb'
}, {
    'areaCode' : '542331',
    'areaName' : '康马县',
    'py' : 'kangma',
    'spy' : 'km'
}, {
    'areaCode' : '542332',
    'areaName' : '定结县',
    'py' : 'dingjie',
    'spy' : 'dj'
}, {
    'areaCode' : '542333',
    'areaName' : '仲巴县',
    'py' : 'zhongba',
    'spy' : 'zb'
}, {
    'areaCode' : '542334',
    'areaName' : '亚东县',
    'py' : 'yadong',
    'spy' : 'yd'
}, {
    'areaCode' : '542335',
    'areaName' : '吉隆县',
    'py' : 'jilong',
    'spy' : 'jl'
}, {
    'areaCode' : '542336',
    'areaName' : '聂拉木县',
    'py' : 'nielamu',
    'spy' : 'nll'
}, {
    'areaCode' : '542337',
    'areaName' : '萨嘎县',
    'py' : 'saga',
    'spy' : 'sg'
}, {
    'areaCode' : '542338',
    'areaName' : '岗巴县',
    'py' : 'gangba',
    'spy' : 'gb'
}]
    }, {
        'areaCode' : '542400',
        'areaName' : '那曲地区',
        'py' : 'naqudiqu',
        'spy' : 'nqdq',
        'datas' : [{
            'areaCode' : '542421',
            'areaName' : '那曲县',
            'py' : 'naqu',
            'spy' : 'nq'
        }, {
            'areaCode' : '542422',
            'areaName' : '嘉黎县',
            'py' : 'jiali',
            'spy' : 'jl'
        }, {
            'areaCode' : '542423',
            'areaName' : '比如县',
            'py' : 'biru',
            'spy' : 'br'
        }, {
            'areaCode' : '542424',
            'areaName' : '聂荣县',
            'py' : 'nierong',
            'spy' : 'nr'
        }, {
        'areaCode' : '542425',
        'areaName' : '安多县',
        'py' : 'anduo',
        'spy' : 'ad'
    }, {
        'areaCode' : '542426',
        'areaName' : '申扎县',
        'py' : 'shenzha',
        'spy' : 'sz'
    }, {
        'areaCode' : '542427',
        'areaName' : '索县',
        'py' : 'suoxian',
        'spy' : 'sx'
    }, {
        'areaCode' : '542428',
        'areaName' : '班戈县',
        'py' : 'bange',
        'spy' : 'bg'
    }, {
        'areaCode' : '542429',
        'areaName' : '巴青县',
        'py' : 'baqing',
        'spy' : 'bq'
    }, {
    'areaCode' : '542430',
    'areaName' : '尼玛县',
    'py' : 'nima',
    'spy' : 'nm'
}]
    }, {
        'areaCode' : '542500',
        'areaName' : '阿里地区',
        'py' : 'alidiqu',
        'spy' : 'aldq',
        'datas' : [{
            'areaCode' : '542521',
            'areaName' : '普兰县',
            'py' : 'suolan',
            'spy' : 'sl'
        }, {
            'areaCode' : '542522',
            'areaName' : '札达县',
            'py' : 'zhada',
            'spy' : 'zd'
        }, {
            'areaCode' : '542523',
            'areaName' : '噶尔县',
            'py' : 'geer',
            'spy' : 'ge'
        }, {
        'areaCode' : '542524',
        'areaName' : '日土县',
        'py' : 'ritu',
        'spy' : 'rt'
    }, {
        'areaCode' : '542525',
        'areaName' : '革吉县',
        'py' : 'geji',
        'spy' : 'gj'
    }, {
        'areaCode' : '542526',
        'areaName' : '改则县',
        'py' : 'gaize',
        'spy' : 'gz'
    }, {
        'areaCode' : '542527',
        'areaName' : '措勤县',
        'py' : 'cuoqin',
        'spy' : 'cq'
    }]
    }, {
        'areaCode' : '542600',
        'areaName' : '林芝地区',
        'py' : 'linzhidiqu',
        'spy' : 'lzdq',
        'datas' : [{
            'areaCode' : '542621',
            'areaName' : '林芝县',
            'py' : 'linzhi',
            'spy' : 'lz'
        }, {
            'areaCode' : '542622',
            'areaName' : '工布江达县',
            'py' : 'gongbujiangda',
            'spy' : 'gbjd'
        }, {
        'areaCode' : '542623',
        'areaName' : '米林县',
        'py' : 'milin',
        'spy' : 'ml'
    }, {
        'areaCode' : '542624',
        'areaName' : '墨脱县',
        'py' : 'motuo',
        'spy' : 'mt'
    }, {
        'areaCode' : '542625',
        'areaName' : '波密县',
        'py' : 'bomi',
        'spy' : ''
    }, {
        'areaCode' : '542626',
        'areaName' : '察隅县',
        'py' : 'chayu',
        'spy' : 'cy'
    }, {
        'areaCode' : '542627',
        'areaName' : '朗县',
        'py' : 'langxian',
        'spy' : 'lx'
    }]
    }]
}, {
    'areaCode' : '610000',
    'areaName' : '陕西省',
    'py' : 'shanxi',
    'spy' : 'sx',
    'datas' : [{
        'areaCode' : '610100',
        'areaName' : '西安市',
        'py' : 'xian',
        'spy' : 'xa',
        'datas' : [{
            'areaCode' : '610102',
            'areaName' : '新城区',
            'py' : 'xincheng',
            'spy' : 'xc'
        }, {
            'areaCode' : '610103',
            'areaName' : '碑林区',
            'py' : 'beilin',
            'spy' : 'bl'
        }, {
            'areaCode' : '610104',
            'areaName' : '莲湖区',
            'py' : 'lianhu',
            'spy' : 'lh'
        }, {
            'areaCode' : '610111',
            'areaName' : '灞桥区',
            'py' : 'baqiao',
            'spy' : 'bq'
        }, {
            'areaCode' : '610112',
            'areaName' : '未央区',
            'py' : 'weiyang',
            'spy' : 'wy'
        }, {
            'areaCode' : '610113',
            'areaName' : '雁塔区',
            'py' : 'yanta',
            'spy' : 'yt'
        }, {
            'areaCode' : '610114',
            'areaName' : '阎良区',
            'py' : 'yanliang',
            'spy' : 'yl'
        }, {
            'areaCode' : '610115',
            'areaName' : '临潼区',
            'py' : 'lintong',
            'spy' : 'lt'
        }, {
        'areaCode' : '610116',
        'areaName' : '长安区',
        'py' : 'changan',
        'spy' : 'ca'
    }, {
    'areaCode' : '610122',
    'areaName' : '蓝田县',
    'py' : 'lantian',
    'spy' : 'lt'
}, {
    'areaCode' : '610124',
    'areaName' : '周至县',
    'py' : 'zhouzhi',
    'spy' : 'zz'
}, {
    'areaCode' : '610125',
    'areaName' : '户县',
    'py' : 'huxian',
    'spy' : 'hx'
}, {
    'areaCode' : '610126',
    'areaName' : '高陵县',
    'py' : 'gaoling',
    'spy' : 'gl'
}]
    }, {
        'areaCode' : '610200',
        'areaName' : '铜川市',
        'py' : 'tongchuan',
        'spy' : 'tc',
        'datas' : [{
            'areaCode' : '610202',
            'areaName' : '王益区',
            'py' : 'wangyi',
            'spy' : 'wy'
        }, {
            'areaCode' : '610203',
            'areaName' : '印台区',
            'py' : 'yintai',
            'spy' : 'yt'
        }, {
            'areaCode' : '610204',
            'areaName' : '耀州区',
            'py' : 'yaozhou',
            'spy' : 'yz'
        }, {
            'areaCode' : '610222',
            'areaName' : '宜君县',
            'py' : 'yijun',
            'spy' : 'yj'
        }]
    }, {
        'areaCode' : '610300',
        'areaName' : '宝鸡市',
        'py' : 'baoji',
        'spy' : 'bj',
        'datas' : [{
            'areaCode' : '610302',
            'areaName' : '渭滨区',
            'py' : 'weibin',
            'spy' : 'wb'
        }, {
            'areaCode' : '610303',
            'areaName' : '金台区',
            'py' : 'jintai',
            'spy' : 'jt'
        }, {
            'areaCode' : '610304',
            'areaName' : '陈仓区',
            'py' : 'chencang',
            'spy' : 'cc'
        }, {
            'areaCode' : '610322',
            'areaName' : '凤翔县',
            'py' : 'fengxiang',
            'spy' : 'fx'
        }, {
            'areaCode' : '610323',
            'areaName' : '岐山县',
            'py' : 'qishan',
            'spy' : 'qs'
        }, {
            'areaCode' : '610324',
            'areaName' : '扶风县',
            'py' : 'fufeng',
            'spy' : 'ff'
        }, {
        'areaCode' : '610326',
        'areaName' : '眉县',
        'py' : 'meixian',
        'spy' : 'mx'
    }, {
        'areaCode' : '610327',
        'areaName' : '陇县',
        'py' : 'longxian',
        'spy' : 'lx'
    }, {
        'areaCode' : '610328',
        'areaName' : '千阳县',
        'py' : 'qianyang',
        'spy' : 'qy'
    }, {
    'areaCode' : '610329',
    'areaName' : '麟游县',
    'py' : 'linyou',
    'spy' : 'ly'
}, {
    'areaCode' : '610330',
    'areaName' : '凤县',
    'py' : 'fengxian',
    'spy' : 'fx'
}, {
    'areaCode' : '610331',
    'areaName' : '太白县',
    'py' : 'taibai',
    'spy' : 'tb'
}]
    }, {
        'areaCode' : '610400',
        'areaName' : '咸阳市',
        'py' : 'xianyang',
        'spy' : 'xy',
        'datas' : [{
            'areaCode' : '610402',
            'areaName' : '秦都区',
            'py' : 'qindu',
            'spy' : 'qd'
        }, {
            'areaCode' : '610403',
            'areaName' : '杨陵区',
            'py' : 'yangling',
            'spy' : 'yl'
        }, {
            'areaCode' : '610404',
            'areaName' : '渭城区',
            'py' : 'weicheng',
            'spy' : 'wc'
        }, {
            'areaCode' : '610422',
            'areaName' : '三原县',
            'py' : 'sanyuan',
            'spy' : 'sy'
        }, {
            'areaCode' : '610423',
            'areaName' : '泾阳县',
            'py' : 'jingyang',
            'spy' : 'jy'
        }, {
        'areaCode' : '610424',
        'areaName' : '乾县',
        'py' : 'qianxian',
        'spy' : 'qx'
    }, {
        'areaCode' : '610425',
        'areaName' : '礼泉县',
        'py' : 'liquan',
        'spy' : 'lq'
    }, {
        'areaCode' : '610426',
        'areaName' : '永寿县',
        'py' : 'yongshou',
        'spy' : 'ys'
    }, {
        'areaCode' : '610427',
        'areaName' : '彬县',
        'py' : 'binxian',
        'spy' : 'bx'
    }, {
    'areaCode' : '610428',
    'areaName' : '长武县',
    'py' : 'changwu',
    'spy' : 'cw'
}, {
    'areaCode' : '610429',
    'areaName' : '旬邑县',
    'py' : 'xunyi',
    'spy' : 'xy'
}, {
    'areaCode' : '610430',
    'areaName' : '淳化县',
    'py' : 'cunhua',
    'spy' : 'ch'
}, {
    'areaCode' : '610431',
    'areaName' : '武功县',
    'py' : 'wugong',
    'spy' : 'wg'
}, {
    'areaCode' : '610481',
    'areaName' : '兴平市',
    'py' : 'xingping',
    'spy' : 'xp'
}]
    }, {
        'areaCode' : '610500',
        'areaName' : '渭南市',
        'py' : 'weinan',
        'spy' : 'wn',
        'datas' : [{
            'areaCode' : '610502',
            'areaName' : '临渭区',
            'py' : 'linwei',
            'spy' : 'lw'
        }, {
            'areaCode' : '610521',
            'areaName' : '华县',
            'py' : 'huaxian',
            'spy' : 'hx'
        }, {
            'areaCode' : '610522',
            'areaName' : '潼关县',
            'py' : 'tongguan',
            'spy' : 'tg'
        }, {
            'areaCode' : '610523',
            'areaName' : '大荔县',
            'py' : 'dali',
            'spy' : 'dl'
        }, {
        'areaCode' : '610524',
        'areaName' : '合阳县',
        'py' : 'heyang',
        'spy' : 'hy'
    }, {
        'areaCode' : '610525',
        'areaName' : '澄城县',
        'py' : 'chengcheng',
        'spy' : 'cc'
    }, {
        'areaCode' : '610526',
        'areaName' : '蒲城县',
        'py' : 'pucheng',
        'spy' : 'pc'
    }, {
        'areaCode' : '610527',
        'areaName' : '白水县',
        'py' : 'baishui',
        'spy' : 'bs'
    }, {
        'areaCode' : '610528',
        'areaName' : '富平县',
        'py' : 'fuping',
        'spy' : 'fp'
    }, {
    'areaCode' : '610581',
    'areaName' : '韩城市',
    'py' : 'hancheng',
    'spy' : 'hc'
}, {
    'areaCode' : '610582',
    'areaName' : '华阴市',
    'py' : 'huayin',
    'spy' : 'hy'
}]
    }, {
        'areaCode' : '610600',
        'areaName' : '延安市',
        'py' : 'yanan',
        'spy' : 'ya',
        'datas' : [{
            'areaCode' : '610602',
            'areaName' : '宝塔区',
            'py' : 'baota',
            'spy' : 'bt'
        }, {
            'areaCode' : '610621',
            'areaName' : '延长县',
            'py' : 'yanchang',
            'spy' : 'yc'
        }, {
            'areaCode' : '610622',
            'areaName' : '延川县',
            'py' : 'yanchuan',
            'spy' : 'yc'
        }, {
        'areaCode' : '610623',
        'areaName' : '子长县',
        'py' : 'zichang',
        'spy' : 'zc'
    }, {
        'areaCode' : '610624',
        'areaName' : '安塞县',
        'py' : 'ansai',
        'spy' : 'as'
    }, {
        'areaCode' : '610625',
        'areaName' : '志丹县',
        'py' : 'zhidan',
        'spy' : 'zd'
    }, {
        'areaCode' : '610626',
        'areaName' : '吴起县',
        'py' : 'wuqi',
        'spy' : 'wq'
    }, {
        'areaCode' : '610627',
        'areaName' : '甘泉县',
        'py' : 'ganquan',
        'spy' : 'gq'
    }, {
        'areaCode' : '610628',
        'areaName' : '富县',
        'py' : 'fuxian',
        'spy' : 'fx'
    }, {
    'areaCode' : '610629',
    'areaName' : '洛川县',
    'py' : 'luochuan',
    'spy' : 'lc'
}, {
    'areaCode' : '610630',
    'areaName' : '宜川县',
    'py' : 'yichuan',
    'spy' : 'yc'
}, {
    'areaCode' : '610631',
    'areaName' : '黄龙县',
    'py' : 'huanglong',
    'spy' : 'hl'
}, {
    'areaCode' : '610632',
    'areaName' : '黄陵县',
    'py' : 'huangling',
    'spy' : 'hl'
}]
    }, {
        'areaCode' : '610700',
        'areaName' : '汉中市',
        'py' : 'hanzhong',
        'spy' : 'hz',
        'datas' : [{
            'areaCode' : '610702',
            'areaName' : '汉台区',
            'py' : 'hantai',
            'spy' : 'ht'
        }, {
            'areaCode' : '610721',
            'areaName' : '南郑县',
            'py' : 'nanzheng',
            'spy' : 'nz'
        }, {
        'areaCode' : '610722',
        'areaName' : '城固县',
        'py' : 'chenggu',
        'spy' : 'cg'
    }, {
        'areaCode' : '610723',
        'areaName' : '洋县',
        'py' : 'yangxian',
        'spy' : 'yx'
    }, {
        'areaCode' : '610724',
        'areaName' : '西乡县',
        'py' : 'xixiang',
        'spy' : 'xx'
    }, {
        'areaCode' : '610725',
        'areaName' : '勉县',
        'py' : 'mianxian',
        'spy' : 'mx'
    }, {
        'areaCode' : '610726',
        'areaName' : '宁强县',
        'py' : 'ningqiang',
        'spy' : 'nq'
    }, {
        'areaCode' : '610727',
        'areaName' : '略阳县',
        'py' : 'lvyang',
        'spy' : 'ly'
    }, {
        'areaCode' : '610728',
        'areaName' : '镇巴县',
        'py' : 'zhenba',
        'spy' : 'zb'
    }, {
    'areaCode' : '610729',
    'areaName' : '留坝县',
    'py' : 'liuba',
    'spy' : 'lb'
}, {
    'areaCode' : '610730',
    'areaName' : '佛坪县',
    'py' : 'foping',
    'spy' : 'fp'
}]
    }, {
        'areaCode' : '610800',
        'areaName' : '榆林市',
        'py' : 'yulin',
        'spy' : 'yl',
        'datas' : [{
            'areaCode' : '610802',
            'areaName' : '榆阳区',
            'py' : 'yuyang',
            'spy' : 'yy'
        }, {
        'areaCode' : '610821',
        'areaName' : '神木县',
        'py' : 'shenmu',
        'spy' : 'sm'
    }, {
        'areaCode' : '610822',
        'areaName' : '府谷县',
        'py' : 'fugu',
        'spy' : 'fg'
    }, {
        'areaCode' : '610823',
        'areaName' : '横山县',
        'py' : 'hengshan',
        'spy' : 'hs'
    }, {
        'areaCode' : '610824',
        'areaName' : '靖边县',
        'py' : 'jingbian',
        'spy' : 'jb'
    }, {
        'areaCode' : '610825',
        'areaName' : '定边县',
        'py' : 'dingbian',
        'spy' : 'db'
    }, {
        'areaCode' : '610826',
        'areaName' : '绥德县',
        'py' : 'suide',
        'spy' : 'sd'
    }, {
        'areaCode' : '610827',
        'areaName' : '米脂县',
        'py' : 'mizhi',
        'spy' : 'mz'
    }, {
        'areaCode' : '610828',
        'areaName' : '佳县',
        'py' : 'jiaxian',
        'spy' : 'jx'
    }, {
    'areaCode' : '610829',
    'areaName' : '吴堡县',
    'py' : 'wubao',
    'spy' : 'wb'
}, {
    'areaCode' : '610830',
    'areaName' : '清涧县',
    'py' : 'qingjian',
    'spy' : 'qj'
}, {
    'areaCode' : '610831',
    'areaName' : '子洲县',
    'py' : 'zizhou',
    'spy' : 'zz'
}]
    }, {
        'areaCode' : '610900',
        'areaName' : '安康市',
        'py' : 'ankang',
        'spy' : 'ak',
        'datas' : [{
        'areaCode' : '610902',
        'areaName' : '汉滨区',
        'py' : 'hanbin',
        'spy' : 'hb'
    }, {
        'areaCode' : '610921',
        'areaName' : '汉阴县',
        'py' : 'hanyin',
        'spy' : 'hy'
    }, {
        'areaCode' : '610922',
        'areaName' : '石泉县',
        'py' : 'shiquan',
        'spy' : 'sq'
    }, {
        'areaCode' : '610923',
        'areaName' : '宁陕县',
        'py' : 'ningshan',
        'spy' : 'ns'
    }, {
        'areaCode' : '610924',
        'areaName' : '紫阳县',
        'py' : 'ziyang',
        'spy' : 'zy'
    }, {
        'areaCode' : '610925',
        'areaName' : '岚皋县',
        'py' : 'langao',
        'spy' : 'lg'
    }, {
        'areaCode' : '610926',
        'areaName' : '平利县',
        'py' : 'pingli',
        'spy' : 'pl'
    }, {
        'areaCode' : '610927',
        'areaName' : '镇坪县',
        'py' : 'zhenping',
        'spy' : 'zp'
    }, {
        'areaCode' : '610928',
        'areaName' : '旬阳县',
        'py' : 'xunyang',
        'spy' : 'xy'
    }, {
    'areaCode' : '610929',
    'areaName' : '白河县',
    'py' : 'baihe',
    'spy' : ''
}]
    }, {
    'areaCode' : '611000',
    'areaName' : '商洛市',
    'py' : 'shangluo',
    'spy' : 'sl',
    'datas' : [{
        'areaCode' : '611002',
        'areaName' : '商州区',
        'py' : 'shangzhou',
        'spy' : 'sz'
    }, {
        'areaCode' : '611021',
        'areaName' : '洛南县',
        'py' : 'luonan',
        'spy' : 'ln'
    }, {
        'areaCode' : '611022',
        'areaName' : '丹凤县',
        'py' : 'danfeng',
        'spy' : 'df'
    }, {
        'areaCode' : '611023',
        'areaName' : '商南县',
        'py' : 'shangnan',
        'spy' : 'sn'
    }, {
        'areaCode' : '611024',
        'areaName' : '山阳县',
        'py' : 'shanyin',
        'spy' : 'sy'
    }, {
        'areaCode' : '611025',
        'areaName' : '镇安县',
        'py' : 'zhenan',
        'spy' : 'za'
    }, {
        'areaCode' : '611026',
        'areaName' : '柞水县',
        'py' : 'zhashui',
        'spy' : 'zs'
    }]
}]
}, {
    'areaCode' : '620000',
    'areaName' : '甘肃省',
    'py' : 'gansu',
    'spy' : 'gs',
    'datas' : [{
        'areaCode' : '620100',
        'areaName' : '兰州市',
        'py' : 'lanzhou',
        'spy' : 'lz',
        'datas' : [{
            'areaCode' : '620102',
            'areaName' : '城关区',
            'py' : 'chengguan',
            'spy' : 'cg'
        }, {
            'areaCode' : '620103',
            'areaName' : '七里河区',
            'py' : 'qilihe',
            'spy' : 'qlh'
        }, {
            'areaCode' : '620104',
            'areaName' : '西固区',
            'py' : 'xigu',
            'spy' : 'xg'
        }, {
            'areaCode' : '620105',
            'areaName' : '安宁区',
            'py' : 'anning',
            'spy' : 'an'
        }, {
            'areaCode' : '620111',
            'areaName' : '红古区',
            'py' : 'honggu',
            'spy' : 'hg'
        }, {
            'areaCode' : '620121',
            'areaName' : '永登县',
            'py' : 'yongdeng',
            'spy' : 'yd'
        }, {
            'areaCode' : '620122',
            'areaName' : '皋兰县',
            'py' : 'gaolan',
            'spy' : 'gl'
        }, {
            'areaCode' : '620123',
            'areaName' : '榆中县',
            'py' : 'yuzhong',
            'spy' : 'yz'
        }]
    }, {
        'areaCode' : '620200',
        'areaName' : '嘉峪关市',
        'py' : 'jiayuguan',
        'spy' : 'jyg',
        'datas' : [{
            'areaCode' : '620202',
            'areaName' : '长城区',
            'py' : 'changcheng',
            'spy' : 'cc'
        }, {
            'areaCode' : '620203',
            'areaName' : '镜铁区',
            'py' : 'jingtie',
            'spy' : 'jt'
        }, {
            'areaCode' : '620204',
            'areaName' : '雄关区',
            'py' : 'xiongguan',
            'spy' : 'xg'
        }]
    }, {
        'areaCode' : '620300',
        'areaName' : '金昌市',
        'py' : 'jinchang',
        'spy' : 'jc',
        'datas' : [{
            'areaCode' : '620302',
            'areaName' : '金川区',
            'py' : 'jinchuan',
            'spy' : 'jc'
        }, {
            'areaCode' : '620321',
            'areaName' : '永昌县',
            'py' : 'yongchang',
            'spy' : 'yc'
        }]
    }, {
        'areaCode' : '620400',
        'areaName' : '白银市',
        'py' : 'baiyin',
        'spy' : 'by',
        'datas' : [{
            'areaCode' : '620402',
            'areaName' : '白银区',
            'py' : 'baiyin',
            'spy' : 'by'
        }, {
            'areaCode' : '620403',
            'areaName' : '平川区',
            'py' : 'pingchuan',
            'spy' : 'pc'
        }, {
            'areaCode' : '620421',
            'areaName' : '靖远县',
            'py' : 'jingyuan',
            'spy' : 'jy'
        }, {
            'areaCode' : '620422',
            'areaName' : '会宁县',
            'py' : 'huining',
            'spy' : 'hn'
        }, {
            'areaCode' : '620423',
            'areaName' : '景泰县',
            'py' : 'jingtai',
            'spy' : 'jt'
        }]
    }, {
        'areaCode' : '620500',
        'areaName' : '天水市',
        'py' : 'tianshui',
        'spy' : 'ts',
        'datas' : [{
            'areaCode' : '620502',
            'areaName' : '秦州区',
            'py' : 'qinzhou',
            'spy' : 'qz'
        }, {
            'areaCode' : '620503',
            'areaName' : '麦积区',
            'py' : 'maiji',
            'spy' : 'mj'
        }, {
            'areaCode' : '620521',
            'areaName' : '清水县',
            'py' : 'qingshui',
            'spy' : 'qs'
        }, {
            'areaCode' : '620522',
            'areaName' : '秦安县',
            'py' : 'qinan',
            'spy' : 'qa'
        }, {
        'areaCode' : '620523',
        'areaName' : '甘谷县',
        'py' : 'gangu',
        'spy' : 'gg'
    }, {
        'areaCode' : '620524',
        'areaName' : '武山县',
        'py' : 'wushan',
        'spy' : 'ws'
    }, {
        'areaCode' : '620525',
        'areaName' : '张家川回族自治县',
        'py' : 'zhangjiachuanhuizuzizhixian',
        'spy' : 'zjchzzzx'
    }]
    }, {
        'areaCode' : '620600',
        'areaName' : '武威市',
        'py' : 'wuwei',
        'spy' : 'ww',
        'datas' : [{
            'areaCode' : '620602',
            'areaName' : '凉州区',
            'py' : 'liangzhou',
            'spy' : 'lz'
        }, {
            'areaCode' : '620621',
            'areaName' : '民勤县',
            'py' : 'minqin',
            'spy' : 'mq'
        }, {
            'areaCode' : '620622',
            'areaName' : '古浪县',
            'py' : 'gulang',
            'spy' : 'gl'
        }, {
        'areaCode' : '620623',
        'areaName' : '天祝藏族自治县',
        'py' : 'tianzhuzangzuzizhixian',
        'spy' : 'tzzzzzx'
    }]
    }, {
        'areaCode' : '620700',
        'areaName' : '张掖市',
        'py' : 'zhangye',
        'spy' : 'zy',
        'datas' : [{
            'areaCode' : '620702',
            'areaName' : '甘州区',
            'py' : 'ganzhou',
            'spy' : 'gz'
        }, {
            'areaCode' : '620721',
            'areaName' : '肃南裕固族自治县',
            'py' : 'sunanyuguzuzizhixian',
            'spy' : 'snygzzzx'
        }, {
        'areaCode' : '620722',
        'areaName' : '民乐县',
        'py' : 'minle',
        'spy' : 'ml'
    }, {
        'areaCode' : '620723',
        'areaName' : '临泽县',
        'py' : 'linze',
        'spy' : 'lz'
    }, {
        'areaCode' : '620724',
        'areaName' : '高台县',
        'py' : 'gaotai',
        'spy' : 'gt'
    }, {
        'areaCode' : '620725',
        'areaName' : '山丹县',
        'py' : 'shandan',
        'spy' : 'sd'
    }]
    }, {
        'areaCode' : '620800',
        'areaName' : '平凉市',
        'py' : 'pingliang',
        'spy' : 'pl',
        'datas' : [{
            'areaCode' : '620802',
            'areaName' : '崆峒区',
            'py' : 'kongdong',
            'spy' : 'kd'
        }, {
        'areaCode' : '620821',
        'areaName' : '泾川县',
        'py' : 'jingchuan',
        'spy' : 'jc'
    }, {
        'areaCode' : '620822',
        'areaName' : '灵台县',
        'py' : 'lingtai',
        'spy' : 'lt'
    }, {
        'areaCode' : '620823',
        'areaName' : '崇信县',
        'py' : 'chongxin',
        'spy' : 'cx'
    }, {
        'areaCode' : '620824',
        'areaName' : '华亭县',
        'py' : 'huating',
        'spy' : 'ht'
    }, {
        'areaCode' : '620825',
        'areaName' : '庄浪县',
        'py' : 'zhuanglang',
        'spy' : 'zl'
    }, {
        'areaCode' : '620826',
        'areaName' : '静宁县',
        'py' : 'jingning',
        'spy' : 'jn'
    }]
    }, {
        'areaCode' : '620900',
        'areaName' : '酒泉市',
        'py' : 'jiuquan',
        'spy' : 'jq',
        'datas' : [{
        'areaCode' : '620902',
        'areaName' : '肃州区',
        'py' : 'suzhou',
        'spy' : 'sz'
    }, {
        'areaCode' : '620921',
        'areaName' : '金塔县',
        'py' : 'jinta',
        'spy' : 'jt'
    }, {
        'areaCode' : '620922',
        'areaName' : '瓜州县',
        'py' : 'guazhou',
        'spy' : 'gz'
    }, {
        'areaCode' : '620923',
        'areaName' : '肃北蒙古族自治县',
        'py' : 'subeimengguzuzizhixian',
        'spy' : 'sbmgzzzx'
    }, {
        'areaCode' : '620924',
        'areaName' : '阿克塞哈萨克族自治县',
        'py' : 'akesaihasakezuzizhixian',
        'spy' : 'akshskzzzx'
    }, {
        'areaCode' : '620981',
        'areaName' : '玉门市',
        'py' : 'yumen',
        'spy' : 'ym'
    }, {
        'areaCode' : '620982',
        'areaName' : '敦煌市',
        'py' : 'dunhuang',
        'spy' : 'dh'
    }]
    }, {
    'areaCode' : '621000',
    'areaName' : '庆阳市',
    'py' : 'qingyang',
    'spy' : 'qy',
    'datas' : [{
        'areaCode' : '621002',
        'areaName' : '西峰区',
        'py' : 'xifeng',
        'spy' : 'xf'
    }, {
        'areaCode' : '621021',
        'areaName' : '庆城县',
        'py' : 'qingcheng',
        'spy' : 'qc'
    }, {
        'areaCode' : '621022',
        'areaName' : '环县',
        'py' : 'huanxian',
        'spy' : 'hx'
    }, {
        'areaCode' : '621023',
        'areaName' : '华池县',
        'py' : 'huachi',
        'spy' : 'hc'
    }, {
        'areaCode' : '621024',
        'areaName' : '合水县',
        'py' : 'heshui',
        'spy' : 'hs'
    }, {
        'areaCode' : '621025',
        'areaName' : '正宁县',
        'py' : 'zhengning',
        'spy' : 'zn'
    }, {
        'areaCode' : '621026',
        'areaName' : '宁县',
        'py' : 'ningxian',
        'spy' : 'nx'
    }, {
        'areaCode' : '621027',
        'areaName' : '镇原县',
        'py' : 'zhenyuan',
        'spy' : 'zy'
    }]
}, {
    'areaCode' : '621100',
    'areaName' : '定西市',
    'py' : 'dingxi',
    'spy' : 'dx',
    'datas' : [{
        'areaCode' : '621102',
        'areaName' : '安定区',
        'py' : 'anning',
        'spy' : 'an'
    }, {
        'areaCode' : '621121',
        'areaName' : '通渭县',
        'py' : 'tongwei',
        'spy' : 'tw'
    }, {
        'areaCode' : '621122',
        'areaName' : '陇西县',
        'py' : 'longxi',
        'spy' : 'lx'
    }, {
        'areaCode' : '621123',
        'areaName' : '渭源县',
        'py' : 'weiyuan',
        'spy' : 'wy'
    }, {
        'areaCode' : '621124',
        'areaName' : '临洮县',
        'py' : 'lintao',
        'spy' : 'lt'
    }, {
        'areaCode' : '621125',
        'areaName' : '漳县',
        'py' : 'zhangxian',
        'spy' : 'zx'
    }, {
        'areaCode' : '621126',
        'areaName' : '岷县',
        'py' : 'minxian',
        'spy' : 'mx'
    }]
}, {
    'areaCode' : '621200',
    'areaName' : '陇南市',
    'py' : 'longnan',
    'spy' : 'ln',
    'datas' : [{
        'areaCode' : '621202',
        'areaName' : '武都区',
        'py' : 'wudu',
        'spy' : 'wd'
    }, {
        'areaCode' : '621221',
        'areaName' : '成县',
        'py' : 'chengxian',
        'spy' : 'cx'
    }, {
        'areaCode' : '621222',
        'areaName' : '文县',
        'py' : 'wenxian',
        'spy' : 'wx'
    }, {
        'areaCode' : '621223',
        'areaName' : '宕昌县',
        'py' : 'dangchang',
        'spy' : 'dc'
    }, {
        'areaCode' : '621224',
        'areaName' : '康县',
        'py' : 'kangxian',
        'spy' : 'kx'
    }, {
        'areaCode' : '621225',
        'areaName' : '西和县',
        'py' : 'xihe',
        'spy' : 'xh'
    }, {
        'areaCode' : '621226',
        'areaName' : '礼县',
        'py' : 'lixian',
        'spy' : 'lx'
    }, {
        'areaCode' : '621227',
        'areaName' : '徽县',
        'py' : 'heixian',
        'spy' : 'hx'
    }, {
        'areaCode' : '621228',
        'areaName' : '两当县',
        'py' : 'liangdang',
        'spy' : 'ld'
    }]
}, {
    'areaCode' : '622900',
    'areaName' : '临夏回族自治州',
    'py' : 'linxiahuizuzizhizhou',
    'spy' : 'lxhzzzz',
    'datas' : [{
        'areaCode' : '622901',
        'areaName' : '临夏市',
        'py' : 'linxia',
        'spy' : 'lx'
    }, {
        'areaCode' : '622921',
        'areaName' : '临夏县',
        'py' : 'linxia',
        'spy' : 'lx'
    }, {
        'areaCode' : '622922',
        'areaName' : '康乐县',
        'py' : 'kangle',
        'spy' : 'kl'
    }, {
        'areaCode' : '622923',
        'areaName' : '永靖县',
        'py' : 'yongjing',
        'spy' : 'yj'
    }, {
        'areaCode' : '622924',
        'areaName' : '广河县',
        'py' : 'guanghe',
        'spy' : 'gh'
    }, {
        'areaCode' : '622925',
        'areaName' : '和政县',
        'py' : 'hezheng',
        'spy' : 'hz'
    }, {
        'areaCode' : '622926',
        'areaName' : '东乡族自治县',
        'py' : 'dongxiangzuzizhixian',
        'spy' : 'dxzzzx'
    }, {
        'areaCode' : '622927',
        'areaName' : '积石山保安族东乡族撒拉族自治县',
        'py' : 'jishishanbaoanzudongxiangzusalazuzizhixian',
        'spy' : 'jssbazdxzslzzzx'
    }]
}, {
    'areaCode' : '623000',
    'areaName' : '甘南藏族自治州',
    'py' : 'gannanzangzuzizhizhou',
    'spy' : 'gnzzzzz',
    'datas' : [{
        'areaCode' : '623001',
        'areaName' : '合作市',
        'py' : 'hezuo',
        'spy' : 'hz'
    }, {
        'areaCode' : '623021',
        'areaName' : '临潭县',
        'py' : 'lintan',
        'spy' : 'lt'
    }, {
        'areaCode' : '623022',
        'areaName' : '卓尼县',
        'py' : 'zhuoni',
        'spy' : 'zn'
    }, {
        'areaCode' : '623023',
        'areaName' : '舟曲县',
        'py' : 'zhouqu',
        'spy' : 'zq'
    }, {
        'areaCode' : '623024',
        'areaName' : '迭部县',
        'py' : 'diebu',
        'spy' : 'db'
    }, {
        'areaCode' : '623025',
        'areaName' : '玛曲县',
        'py' : 'maqu',
        'spy' : 'mq'
    }, {
        'areaCode' : '623026',
        'areaName' : '碌曲县',
        'py' : 'luqu',
        'spy' : 'lq'
    }, {
        'areaCode' : '623027',
        'areaName' : '夏河县',
        'py' : 'xiahe',
        'spy' : 'xh'
    }]
}]
}, {
    'areaCode' : '630000',
    'areaName' : '青海省',
    'py' : 'qinghai',
    'spy' : 'qh',
    'datas' : [{
        'areaCode' : '630100',
        'areaName' : '西宁市',
        'py' : 'xining',
        'spy' : 'xn',
        'datas' : [{
            'areaCode' : '630102',
            'areaName' : '城东区',
            'py' : 'chengdong',
            'spy' : 'cd'
        }, {
            'areaCode' : '630103',
            'areaName' : '城中区',
            'py' : 'chengzhong',
            'spy' : 'cz'
        }, {
            'areaCode' : '630104',
            'areaName' : '城西区',
            'py' : 'chengxi',
            'spy' : 'cx'
        }, {
            'areaCode' : '630105',
            'areaName' : '城北区',
            'py' : 'chengbei',
            'spy' : 'cb'
        }, {
            'areaCode' : '630121',
            'areaName' : '大通回族土族自治县',
            'py' : 'datonghuizutuzuzizhixian',
            'spy' : 'dthztzzzx'
        }, {
            'areaCode' : '630122',
            'areaName' : '湟中县',
            'py' : 'huangzhong',
            'spy' : 'hz'
        }, {
            'areaCode' : '630123',
            'areaName' : '湟源县',
            'py' : 'huangyuan',
            'spy' : 'hy'
        }]
    }, {
        'areaCode' : '632100',
        'areaName' : '海东地区',
        'py' : 'haidongdiqu',
        'spy' : 'hddq',
        'datas' : [{
            'areaCode' : '632121',
            'areaName' : '平安县',
            'py' : 'pingan',
            'spy' : 'pa'
        }, {
            'areaCode' : '632122',
            'areaName' : '民和回族土族自治县',
            'py' : 'minhehuizutuzuzizhixian',
            'spy' : 'mhhztzzzx'
        }, {
            'areaCode' : '632123',
            'areaName' : '乐都县',
            'py' : 'ledu',
            'spy' : 'ld'
        }, {
            'areaCode' : '632126',
            'areaName' : '互助土族自治县',
            'py' : 'huzutuzuzizhixian',
            'spy' : 'hztzzzx'
        }, {
            'areaCode' : '632127',
            'areaName' : '化隆回族自治县',
            'py' : 'hualonghuizuzizhixian',
            'spy' : 'hlhzzzx'
        }, {
            'areaCode' : '632128',
            'areaName' : '循化撒拉族自治县',
            'py' : 'xunhuasalazuzizhixian',
            'spy' : 'xhslzzzx'
        }]
    }, {
        'areaCode' : '632200',
        'areaName' : '海北藏族自治州',
        'py' : 'haibeizangzuzizhizhou',
        'spy' : 'hbzzzzz',
        'datas' : [{
            'areaCode' : '632221',
            'areaName' : '门源回族自治县',
            'py' : 'menyuanhuizuzizhixian',
            'spy' : 'myhzzzx'
        }, {
            'areaCode' : '632222',
            'areaName' : '祁连县',
            'py' : 'qilian',
            'spy' : 'ql'
        }, {
            'areaCode' : '632223',
            'areaName' : '海晏县',
            'py' : 'haiyan',
            'spy' : 'hy'
        }, {
            'areaCode' : '632224',
            'areaName' : '刚察县',
            'py' : 'gangcha',
            'spy' : 'gc'
        }]
    }, {
        'areaCode' : '632300',
        'areaName' : '黄南藏族自治州',
        'py' : 'huangnanzangzuzizhizhou',
        'spy' : 'hnzzzzz',
        'datas' : [{
            'areaCode' : '632321',
            'areaName' : '同仁县',
            'py' : 'tongren',
            'spy' : 'tr'
        }, {
            'areaCode' : '632322',
            'areaName' : '尖扎县',
            'py' : 'jianzha',
            'spy' : 'jz'
        }, {
            'areaCode' : '632323',
            'areaName' : '泽库县',
            'py' : 'zeku',
            'spy' : 'zk'
        }, {
            'areaCode' : '632324',
            'areaName' : '河南蒙古族自治县',
            'py' : 'henanmengguzuzizhixian',
            'spy' : 'hnmgzzzx'
        }]
    }, {
        'areaCode' : '632500',
        'areaName' : '海南藏族自治州',
        'py' : 'hainanzangzuzizhizhou',
        'spy' : 'hnzzzzz',
        'datas' : [{
            'areaCode' : '632521',
            'areaName' : '共和县',
            'py' : 'gonghe',
            'spy' : 'gh'
        }, {
            'areaCode' : '632522',
            'areaName' : '同德县',
            'py' : 'tongde',
            'spy' : 'td'
        }, {
            'areaCode' : '632523',
            'areaName' : '贵德县',
            'py' : 'guide',
            'spy' : 'gd'
        }, {
            'areaCode' : '632524',
            'areaName' : '兴海县',
            'py' : 'xinghai',
            'spy' : 'xh'
        }, {
        'areaCode' : '632525',
        'areaName' : '贵南县',
        'py' : 'guinan',
        'spy' : 'gn'
    }]
    }, {
        'areaCode' : '632600',
        'areaName' : '果洛藏族自治州',
        'py' : 'guoluozangzuzizhizhou',
        'spy' : 'glzzzzz',
        'datas' : [{
            'areaCode' : '632621',
            'areaName' : '玛沁县',
            'py' : 'maqin',
            'spy' : 'mq'
        }, {
            'areaCode' : '632622',
            'areaName' : '班玛县',
            'py' : 'banma',
            'spy' : 'bm'
        }, {
            'areaCode' : '632623',
            'areaName' : '甘德县',
            'py' : 'gande',
            'spy' : 'gd'
        }, {
        'areaCode' : '632624',
        'areaName' : '达日县',
        'py' : 'dari',
        'spy' : 'dr'
    }, {
        'areaCode' : '632625',
        'areaName' : '久治县',
        'py' : 'jiuzhi',
        'spy' : 'jz'
    }, {
        'areaCode' : '632626',
        'areaName' : '玛多县',
        'py' : 'maduo',
        'spy' : 'md'
    }]
    }, {
        'areaCode' : '632700',
        'areaName' : '玉树藏族自治州',
        'py' : 'yushuzangzuzizhizhou',
        'spy' : 'yszzzzz',
        'datas' : [{
            'areaCode' : '632721',
            'areaName' : '玉树县',
            'py' : 'yushu',
            'spy' : 'ys'
        }, {
            'areaCode' : '632722',
            'areaName' : '杂多县',
            'py' : 'zaduo',
            'spy' : 'zd'
        }, {
        'areaCode' : '632723',
        'areaName' : '称多县',
        'py' : 'chengduo',
        'spy' : 'cd'
    }, {
        'areaCode' : '632724',
        'areaName' : '治多县',
        'py' : 'zhiduo',
        'spy' : 'zd'
    }, {
        'areaCode' : '632725',
        'areaName' : '囊谦县',
        'py' : 'nangqian',
        'spy' : 'nq'
    }, {
        'areaCode' : '632726',
        'areaName' : '曲麻莱县',
        'py' : 'qumalai',
        'spy' : 'qml'
    }]
    }, {
        'areaCode' : '632800',
        'areaName' : '海西蒙古族藏族自治州',
        'py' : 'haiximengguzuzangzuzizhizhou',
        'spy' : 'hxmgzzzzzz',
        'datas' : [{
            'areaCode' : '632801',
            'areaName' : '格尔木市',
            'py' : 'geermu',
            'spy' : 'gem'
        }, {
        'areaCode' : '632802',
        'areaName' : '德令哈市',
        'py' : 'delingha',
        'spy' : 'dlh'
    }, {
        'areaCode' : '632821',
        'areaName' : '乌兰县',
        'py' : 'wulan',
        'spy' : ''
    }, {
        'areaCode' : '632822',
        'areaName' : '都兰县',
        'py' : 'dulan',
        'spy' : 'dl'
    }, {
        'areaCode' : '632823',
        'areaName' : '天峻县',
        'py' : 'tianjun',
        'spy' : 'tj'
    }]
    }]
}, {
    'areaCode' : '640000',
    'areaName' : '宁夏回族自治区',
    'py' : 'ningxiahuizuzizhiqu',
    'spy' : 'nxhzzzq',
    'datas' : [{
        'areaCode' : '640100',
        'areaName' : '银川市',
        'py' : 'yinchuan',
        'spy' : 'yc',
        'datas' : [{
            'areaCode' : '640104',
            'areaName' : '兴庆区',
            'py' : 'xingqing',
            'spy' : 'xq'
        }, {
            'areaCode' : '640105',
            'areaName' : '西夏区',
            'py' : 'xixia',
            'spy' : 'xx'
        }, {
            'areaCode' : '640106',
            'areaName' : '金凤区',
            'py' : 'jinfeng',
            'spy' : 'jf'
        }, {
            'areaCode' : '640121',
            'areaName' : '永宁县',
            'py' : 'yongning',
            'spy' : 'yn'
        }, {
            'areaCode' : '640122',
            'areaName' : '贺兰县',
            'py' : 'helan',
            'spy' : 'hl'
        }, {
            'areaCode' : '640181',
            'areaName' : '灵武市',
            'py' : 'lingwu',
            'spy' : 'lw'
        }]
    }, {
        'areaCode' : '640200',
        'areaName' : '石嘴山市',
        'py' : 'shizuishan',
        'spy' : 'szs',
        'datas' : [{
            'areaCode' : '640202',
            'areaName' : '大武口区',
            'py' : 'dawukou',
            'spy' : 'dwk'
        }, {
            'areaCode' : '640205',
            'areaName' : '惠农区',
            'py' : 'huinong',
            'spy' : 'hn'
        }, {
            'areaCode' : '640221',
            'areaName' : '平罗县',
            'py' : 'pingluo',
            'spy' : 'pl'
        }]
    }, {
        'areaCode' : '640300',
        'areaName' : '吴忠市',
        'py' : 'wuzhong',
        'spy' : 'wz',
        'datas' : [{
            'areaCode' : '640302',
            'areaName' : '利通区',
            'py' : 'litong',
            'spy' : 'lt'
        }, {
            'areaCode' : '640303',
            'areaName' : '红寺堡区',
            'py' : 'hongsibao',
            'spy' : 'hsb'
        }, {
            'areaCode' : '640323',
            'areaName' : '盐池县',
            'py' : 'yanchi',
            'spy' : 'yc'
        }, {
            'areaCode' : '640324',
            'areaName' : '同心县',
            'py' : 'tongxin',
            'spy' : 'tx'
        }, {
            'areaCode' : '640381',
            'areaName' : '青铜峡市',
            'py' : 'qingtongxia',
            'spy' : 'qtx'
        }]
    }, {
        'areaCode' : '640400',
        'areaName' : '固原市',
        'py' : 'guyuan',
        'spy' : 'gy',
        'datas' : [{
            'areaCode' : '640402',
            'areaName' : '原州区',
            'py' : 'yuanzhou',
            'spy' : 'yz'
        }, {
            'areaCode' : '640422',
            'areaName' : '西吉县',
            'py' : 'xiji',
            'spy' : 'xj'
        }, {
            'areaCode' : '640423',
            'areaName' : '隆德县',
            'py' : 'longde',
            'spy' : 'ld'
        }, {
            'areaCode' : '640424',
            'areaName' : '泾源县',
            'py' : 'jingyuan',
            'spy' : 'jy'
        }, {
            'areaCode' : '640425',
            'areaName' : '彭阳县',
            'py' : 'pengyang',
            'spy' : 'py'
        }]
    }, {
        'areaCode' : '640500',
        'areaName' : '中卫市',
        'py' : 'zhongwei',
        'spy' : 'zw',
        'datas' : [{
            'areaCode' : '640502',
            'areaName' : '沙坡头区',
            'py' : 'shapotou',
            'spy' : 'spt'
        }, {
            'areaCode' : '640521',
            'areaName' : '中宁县',
            'py' : 'zhongning',
            'spy' : 'zn'
        }, {
            'areaCode' : '640522',
            'areaName' : '海原县',
            'py' : 'haiyuan',
            'spy' : 'hy'
        }]
    }]
}, {
    'areaCode' : '650000',
    'areaName' : '新疆维吾尔自治区',
    'py' : 'xinjiangweiwuerzizhiqu',
    'spy' : 'xjwwezzq',
    'datas' : [{
        'areaCode' : '650100',
        'areaName' : '乌鲁木齐市',
        'py' : 'wulumuqi',
        'spy' : 'wlmq',
        'datas' : [{
            'areaCode' : '650102',
            'areaName' : '天山区',
            'py' : 'tianshan',
            'spy' : 'ts'
        }, {
            'areaCode' : '650103',
            'areaName' : '沙依巴克区',
            'py' : 'shayibake',
            'spy' : 'sybk'
        }, {
            'areaCode' : '650104',
            'areaName' : '新市区',
            'py' : 'xinshi',
            'spy' : 'xs'
        }, {
            'areaCode' : '650105',
            'areaName' : '水磨沟区',
            'py' : 'shuimogou',
            'spy' : 'smg'
        }, {
            'areaCode' : '650106',
            'areaName' : '头屯河区',
            'py' : 'toutunhe',
            'spy' : 'tth'
        }, {
            'areaCode' : '650107',
            'areaName' : '达坂城区',
            'py' : 'dabancheng',
            'spy' : 'dbc'
        }, {
            'areaCode' : '650108',
            'areaName' : '东山区',
            'py' : 'dongshan',
            'spy' : 'ds'
        }, {
            'areaCode' : '650109',
            'areaName' : '米东区',
            'py' : 'midong',
            'spy' : 'md'
        }, {
        'areaCode' : '650121',
        'areaName' : '乌鲁木齐县',
        'py' : 'wulumuqi',
        'spy' : 'wlmq'
    }]
    }, {
        'areaCode' : '650200',
        'areaName' : '克拉玛依市',
        'py' : 'kelamayi',
        'spy' : 'klmy',
        'datas' : [{
            'areaCode' : '650202',
            'areaName' : '独山子区',
            'py' : 'dushanzi',
            'spy' : 'dsz'
        }, {
            'areaCode' : '650203',
            'areaName' : '克拉玛依区',
            'py' : 'kelamayi',
            'spy' : 'klmy'
        }, {
            'areaCode' : '650204',
            'areaName' : '白碱滩区',
            'py' : 'baijiantan',
            'spy' : 'bjt'
        }, {
            'areaCode' : '650205',
            'areaName' : '乌尔禾区',
            'py' : 'wuerhe',
            'spy' : 'weh'
        }]
    }, {
        'areaCode' : '652100',
        'areaName' : '吐鲁番地区',
        'py' : 'tulufandiqu',
        'spy' : 'tlfdq',
        'datas' : [{
            'areaCode' : '652101',
            'areaName' : '吐鲁番市',
            'py' : 'tulufan',
            'spy' : 'tlf'
        }, {
            'areaCode' : '652122',
            'areaName' : '鄯善县',
            'py' : 'shanshan',
            'spy' : 'ss'
        }, {
            'areaCode' : '652123',
            'areaName' : '托克逊县',
            'py' : 'tuokexun',
            'spy' : 'tkx'
        }]
    }, {
        'areaCode' : '652200',
        'areaName' : '哈密地区',
        'py' : 'hamidiqu',
        'spy' : 'hmdq',
        'datas' : [{
            'areaCode' : '652201',
            'areaName' : '哈密市',
            'py' : 'hami',
            'spy' : 'hm'
        }, {
            'areaCode' : '652222',
            'areaName' : '巴里坤哈萨克自治县',
            'py' : 'balikunhasakezizhixian',
            'spy' : 'blkhskzzx'
        }, {
            'areaCode' : '652223',
            'areaName' : '伊吾县',
            'py' : 'yiwu',
            'spy' : 'yw'
        }]
    }, {
        'areaCode' : '652300',
        'areaName' : '昌吉回族自治州',
        'py' : 'changjihuizuzizhizhou',
        'spy' : 'cjhzzzz',
        'datas' : [{
            'areaCode' : '652301',
            'areaName' : '昌吉市',
            'py' : 'changji',
            'spy' : 'cj'
        }, {
            'areaCode' : '652302',
            'areaName' : '阜康市',
            'py' : 'fukang',
            'spy' : 'fk'
        }, {
            'areaCode' : '652303',
            'areaName' : '米泉市',
            'py' : 'miquan',
            'spy' : 'mq'
        }, {
            'areaCode' : '652323',
            'areaName' : '呼图壁县',
            'py' : 'hutubi',
            'spy' : 'htb'
        }, {
        'areaCode' : '652324',
        'areaName' : '玛纳斯县',
        'py' : 'manasi',
        'spy' : 'mns'
    }, {
        'areaCode' : '652325',
        'areaName' : '奇台县',
        'py' : 'qitai',
        'spy' : 'qt'
    }, {
        'areaCode' : '652327',
        'areaName' : '吉木萨尔县',
        'py' : 'jimusaer',
        'spy' : 'jmse'
    }, {
        'areaCode' : '652328',
        'areaName' : '木垒哈萨克自治县',
        'py' : 'muleihasakezizhixian',
        'spy' : 'mlhskzzx'
    }]
    }, {
        'areaCode' : '652700',
        'areaName' : '博尔塔拉蒙古自治州',
        'py' : 'boertalamengguzizhizhou',
        'spy' : 'betlmgzzz',
        'datas' : [{
            'areaCode' : '652701',
            'areaName' : '博乐市',
            'py' : 'bole',
            'spy' : 'bl'
        }, {
            'areaCode' : '652722',
            'areaName' : '精河县',
            'py' : 'jinghe',
            'spy' : 'jh'
        }, {
            'areaCode' : '652723',
            'areaName' : '温泉县',
            'py' : 'wenquan',
            'spy' : 'wq'
        }]
    }, {
        'areaCode' : '652800',
        'areaName' : '巴音郭楞蒙古自治州',
        'py' : 'bayinguolengmengguzizhizhou',
        'spy' : 'byglmgzzz',
        'datas' : [{
            'areaCode' : '652801',
            'areaName' : '库尔勒市',
            'py' : 'kuerle',
            'spy' : 'kel'
        }, {
            'areaCode' : '652822',
            'areaName' : '轮台县',
            'py' : 'luntai',
            'spy' : 'lt'
        }, {
        'areaCode' : '652823',
        'areaName' : '尉犁县',
        'py' : 'yuli',
        'spy' : 'yl'
    }, {
        'areaCode' : '652824',
        'areaName' : '若羌县',
        'py' : 'ruoqiang',
        'spy' : 'rq'
    }, {
        'areaCode' : '652825',
        'areaName' : '且末县',
        'py' : 'qiemo',
        'spy' : 'qm'
    }, {
        'areaCode' : '652826',
        'areaName' : '焉耆回族自治县',
        'py' : 'yanqihuizuzizhixian',
        'spy' : 'yqhzzzx'
    }, {
        'areaCode' : '652827',
        'areaName' : '和静县',
        'py' : 'hejing',
        'spy' : 'hj'
    }, {
        'areaCode' : '652828',
        'areaName' : '和硕县',
        'py' : 'hesuo',
        'spy' : 'hs'
    }, {
        'areaCode' : '652829',
        'areaName' : '博湖县',
        'py' : 'bohu',
        'spy' : 'bh'
    }]
    }, {
        'areaCode' : '652900',
        'areaName' : '阿克苏地区',
        'py' : 'akesudiqu',
        'spy' : 'aksdq',
        'datas' : [{
            'areaCode' : '652901',
            'areaName' : '阿克苏市',
            'py' : 'akesu',
            'spy' : 'aks'
        }, {
        'areaCode' : '652922',
        'areaName' : '温宿县',
        'py' : 'wensu',
        'spy' : 'ws'
    }, {
        'areaCode' : '652923',
        'areaName' : '库车县',
        'py' : 'kuche',
        'spy' : 'kc'
    }, {
        'areaCode' : '652924',
        'areaName' : '沙雅县',
        'py' : 'shaya',
        'spy' : 'sy'
    }, {
        'areaCode' : '652925',
        'areaName' : '新和县',
        'py' : 'xinhe',
        'spy' : 'xh'
    }, {
        'areaCode' : '652926',
        'areaName' : '拜城县',
        'py' : 'baicheng',
        'spy' : 'bc'
    }, {
        'areaCode' : '652927',
        'areaName' : '乌什县',
        'py' : 'wushi',
        'spy' : 'ws'
    }, {
        'areaCode' : '652928',
        'areaName' : '阿瓦提县',
        'py' : 'awati',
        'spy' : 'awt'
    }, {
        'areaCode' : '652929',
        'areaName' : '柯坪县',
        'py' : 'keping',
        'spy' : 'kp'
    }]
    }, {
        'areaCode' : '653000',
        'areaName' : '克孜勒苏柯尔克孜自治州',
        'py' : 'kezilesukeerkezizizhizhou',
        'spy' : 'kzlskekzzzz',
        'datas' : [{
        'areaCode' : '653001',
        'areaName' : '阿图什市',
        'py' : 'atushi',
        'spy' : 'ats'
    }, {
        'areaCode' : '653022',
        'areaName' : '阿克陶县',
        'py' : 'aketao',
        'spy' : 'akt'
    }, {
        'areaCode' : '653023',
        'areaName' : '阿合奇县',
        'py' : 'aheqi',
        'spy' : 'ahq'
    }, {
        'areaCode' : '653024',
        'areaName' : '乌恰县',
        'py' : 'wuqia',
        'spy' : 'wq'
    }]
    }, {
    'areaCode' : '653100',
    'areaName' : '喀什地区',
    'py' : 'kashidiqu',
    'spy' : 'ksdq',
    'datas' : [{
        'areaCode' : '653101',
        'areaName' : '喀什市',
        'py' : 'kashi',
        'spy' : 'ks'
    }, {
        'areaCode' : '653121',
        'areaName' : '疏附县',
        'py' : 'shufu',
        'spy' : 'sf'
    }, {
        'areaCode' : '653122',
        'areaName' : '疏勒县',
        'py' : 'shule',
        'spy' : 'sl'
    }, {
        'areaCode' : '653123',
        'areaName' : '英吉沙县',
        'py' : 'yingjisha',
        'spy' : 'yjs'
    }, {
        'areaCode' : '653124',
        'areaName' : '泽普县',
        'py' : 'zepu',
        'spy' : 'zp'
    }, {
        'areaCode' : '653125',
        'areaName' : '莎车县',
        'py' : 'shache',
        'spy' : 'sc'
    }, {
        'areaCode' : '653126',
        'areaName' : '叶城县',
        'py' : 'yecheng',
        'spy' : 'yc'
    }, {
        'areaCode' : '653127',
        'areaName' : '麦盖提县',
        'py' : 'maigaiti',
        'spy' : 'mgt'
    }, {
        'areaCode' : '653128',
        'areaName' : '岳普湖县',
        'py' : 'yuepuhu',
        'spy' : 'yph'
    }, {
    'areaCode' : '653129',
    'areaName' : '伽师县',
    'py' : 'jiashi',
    'spy' : 'js'
}, {
    'areaCode' : '653130',
    'areaName' : '巴楚县',
    'py' : 'bachu',
    'spy' : 'bc'
}, {
    'areaCode' : '653131',
    'areaName' : '塔什库尔干塔吉克自治县',
    'py' : 'tashikuergantajikezizhixian',
    'spy' : 'tskegtjkzzx'
}]
}, {
    'areaCode' : '653200',
    'areaName' : '和田地区',
    'py' : 'hetiandiqu',
    'spy' : 'htdq',
    'datas' : [{
        'areaCode' : '653201',
        'areaName' : '和田市',
        'py' : 'hetian',
        'spy' : 'ht'
    }, {
        'areaCode' : '653221',
        'areaName' : '和田县',
        'py' : 'hetian',
        'spy' : 'ht'
    }, {
        'areaCode' : '653222',
        'areaName' : '墨玉县',
        'py' : 'moyu',
        'spy' : 'my'
    }, {
        'areaCode' : '653223',
        'areaName' : '皮山县',
        'py' : 'pishan',
        'spy' : 'ps'
    }, {
        'areaCode' : '653224',
        'areaName' : '洛浦县',
        'py' : 'luopu',
        'spy' : 'lp'
    }, {
        'areaCode' : '653225',
        'areaName' : '策勒县',
        'py' : 'cele',
        'spy' : 'cl'
    }, {
        'areaCode' : '653226',
        'areaName' : '于田县',
        'py' : 'yutian',
        'spy' : 'yt'
    }, {
        'areaCode' : '653227',
        'areaName' : '民丰县',
        'py' : 'minfeng',
        'spy' : 'mf'
    }]
}, {
    'areaCode' : '654000',
    'areaName' : '伊犁哈萨克自治州',
    'py' : 'yilihasakezizhizhou',
    'spy' : 'ylhskzzz',
    'datas' : [{
        'areaCode' : '654002',
        'areaName' : '伊宁市',
        'py' : 'yining',
        'spy' : 'yn'
    }, {
        'areaCode' : '654003',
        'areaName' : '奎屯市',
        'py' : 'kuitun',
        'spy' : 'kt'
    }, {
        'areaCode' : '654021',
        'areaName' : '伊宁县',
        'py' : 'yining',
        'spy' : 'yn'
    }, {
        'areaCode' : '654022',
        'areaName' : '察布查尔锡伯自治县',
        'py' : 'chabuchaerxibozizhixian',
        'spy' : 'cbcexbzzx'
    }, {
        'areaCode' : '654023',
        'areaName' : '霍城县',
        'py' : 'huocheng',
        'spy' : 'hc'
    }, {
        'areaCode' : '654024',
        'areaName' : '巩留县',
        'py' : 'gongliu',
        'spy' : 'gl'
    }, {
        'areaCode' : '654025',
        'areaName' : '新源县',
        'py' : 'xinyuan',
        'spy' : 'xy'
    }, {
        'areaCode' : '654026',
        'areaName' : '昭苏县',
        'py' : 'zhaosu',
        'spy' : 'zs'
    }, {
        'areaCode' : '654027',
        'areaName' : '特克斯县',
        'py' : 'tekesi',
        'spy' : 'tks'
    }, {
    'areaCode' : '654028',
    'areaName' : '尼勒克县',
    'py' : 'nileke',
    'spy' : 'nlk'
}]
}, {
    'areaCode' : '654200',
    'areaName' : '塔城地区',
    'py' : 'tachengdiqu',
    'spy' : 'tcdq',
    'datas' : [{
        'areaCode' : '654201',
        'areaName' : '塔城市',
        'py' : 'tacheng',
        'spy' : 'tc'
    }, {
        'areaCode' : '654202',
        'areaName' : '乌苏市',
        'py' : 'wusu',
        'spy' : 'ws'
    }, {
        'areaCode' : '654221',
        'areaName' : '额敏县',
        'py' : 'emin',
        'spy' : 'em'
    }, {
        'areaCode' : '654223',
        'areaName' : '沙湾县',
        'py' : 'shawan',
        'spy' : 'sw'
    }, {
        'areaCode' : '654224',
        'areaName' : '托里县',
        'py' : 'tuoli',
        'spy' : 'tl'
    }, {
        'areaCode' : '654225',
        'areaName' : '裕民县',
        'py' : 'yumin',
        'spy' : 'ym'
    }, {
        'areaCode' : '654226',
        'areaName' : '和布克赛尔蒙古自治县',
        'py' : 'hebukesaiermengguzizhixian',
        'spy' : 'hbksemgzzx'
    }]
}, {
    'areaCode' : '654300',
    'areaName' : '阿勒泰地区',
    'py' : 'aletaidiqu',
    'spy' : 'altdq',
    'datas' : [{
        'areaCode' : '654301',
        'areaName' : '阿勒泰市',
        'py' : 'aletai',
        'spy' : 'alt'
    }, {
        'areaCode' : '654321',
        'areaName' : '布尔津县',
        'py' : 'buerjin',
        'spy' : 'bej'
    }, {
        'areaCode' : '654322',
        'areaName' : '富蕴县',
        'py' : 'fuyun',
        'spy' : 'fy'
    }, {
        'areaCode' : '654323',
        'areaName' : '福海县',
        'py' : 'fuhai',
        'spy' : 'fh'
    }, {
        'areaCode' : '654324',
        'areaName' : '哈巴河县',
        'py' : 'habahe',
        'spy' : 'hbh'
    }, {
        'areaCode' : '654325',
        'areaName' : '青河县',
        'py' : 'qinghe',
        'spy' : 'qh'
    }, {
        'areaCode' : '654326',
        'areaName' : '吉木乃县',
        'py' : 'jimunai',
        'spy' : 'jmn'
    }]
}, {
    'areaCode' : '659000',
    'areaName' : '自治区直辖县级行政区划',
    'py' : 'zizhiquzhixiaxianjixingzhengquhua',
    'spy' : 'zzqzxxjxzqh',
    'datas' : [{
        'areaCode' : '659001',
        'areaName' : '石河子市',
        'py' : 'shihezi',
        'spy' : 'shz'
    }, {
        'areaCode' : '659002',
        'areaName' : '阿拉尔市',
        'py' : 'alabu',
        'spy' : 'alb'
    }, {
        'areaCode' : '659003',
        'areaName' : '图木舒克市',
        'py' : 'tumushuke',
        'spy' : 'tmsk'
    }, {
        'areaCode' : '659004',
        'areaName' : '五家渠市',
        'py' : 'wujiaqu',
        'spy' : 'wjq'
    }, {
        'areaCode' : '659005',
        'areaName' : '北屯市',
        'py' : 'beitun',
        'spy' : 'bt'
    }, {
        'areaCode' : '659006',
        'areaName' : '铁门关市',
        'py' : 'tiemenguan',
        'spy' : 'tmg'
    }]
}]
}, {
    'areaCode' : '710000',
    'areaName' : '台湾省',
    'py' : 'taiwan',
    'spy' : 'tw',
    'datas' : [{
        'areaCode' : '710100',
        'areaName' : '省辖市县级行政区划',
        'py' : 'shengxiashixianjixingzhengquhua',
        'spy' : 'sxsxjxzqh',
        'datas' : [{
            'areaCode' : '710200',
            'areaName' : '基隆市',
            'py' : 'jilong',
            'spy' : 'jl'
        }, {
            'areaCode' : '710210',
            'areaName' : '台东县',
            'py' : 'taidong',
            'spy' : 'td'
        }, {
            'areaCode' : '710260',
            'areaName' : '宜兰县',
            'py' : 'yilan',
            'spy' : 'yl'
        }, {
            'areaCode' : '710300',
            'areaName' : '新竹市',
            'py' : 'xinzhu',
            'spy' : 'xz'
        }, {
            'areaCode' : '710330',
            'areaName' : '桃园县',
            'py' : 'taoyuan',
            'spy' : 'ty'
        }, {
            'areaCode' : '710360',
            'areaName' : '苗粟县',
            'py' : 'miaosu',
            'spy' : 'ms'
        }, {
            'areaCode' : '710500',
            'areaName' : '彰化县',
            'py' : 'zhanghua',
            'spy' : 'zh'
        }, {
            'areaCode' : '710540',
            'areaName' : '南投县',
            'py' : 'nantou',
            'spy' : 'nt'
        }, {
        'areaCode' : '710591',
        'areaName' : '连江县',
        'py' : 'lianjiang',
        'spy' : 'lj'
    }, {
    'areaCode' : '710600',
    'areaName' : '嘉义市',
    'py' : 'jiayi',
    'spy' : 'jy'
}, {
    'areaCode' : '710640',
    'areaName' : '云林县',
    'py' : 'yunlin',
    'spy' : 'yl'
}, {
    'areaCode' : '710880',
    'areaName' : '彭湖县',
    'py' : 'penghu',
    'spy' : 'ph'
}, {
    'areaCode' : '710890',
    'areaName' : '金门县',
    'py' : 'jinmen',
    'spy' : 'jm'
}, {
    'areaCode' : '710900',
    'areaName' : '屏东县',
    'py' : 'pingdong',
    'spy' : 'pd'
}, {
    'areaCode' : '710950',
    'areaName' : '花莲县',
    'py' : 'hualian',
    'spy' : 'hl'
}]
    }, {
        'areaCode' : '710220',
        'areaName' : '新北市',
        'py' : 'xinbei',
        'spy' : 'xb',
        'datas' : [{
            'areaCode' : '710221',
            'areaName' : '市辖区',
            'py' : 'shixiaqu',
            'spy' : 'sxq'
        }]
    }, {
        'areaCode' : '710222',
        'areaName' : '台北市',
        'py' : 'taibei',
        'spy' : 'tb',
        'datas' : [{
            'areaCode' : '710212',
            'areaName' : '市辖区',
            'py' : 'shixiaqu',
            'spy' : 'sxq'
        }, {
            'areaCode' : '710213',
            'areaName' : '大同区',
            'py' : 'datong',
            'spy' : 'dt'
        }, {
            'areaCode' : '710214',
            'areaName' : '中山区',
            'py' : 'zhongshan',
            'spy' : 'zs'
        }]
    }, {
        'areaCode' : '710400',
        'areaName' : '台中市',
        'py' : 'taizhong',
        'spy' : 'tz',
        'datas' : [{
            'areaCode' : '710401',
            'areaName' : '市辖区',
            'py' : 'shixiaqu',
            'spy' : 'sxq'
        }]
    }, {
        'areaCode' : '710700',
        'areaName' : '台南市',
        'py' : 'tainan',
        'spy' : 'tn',
        'datas' : [{
            'areaCode' : '710701',
            'areaName' : '市辖区',
            'py' : 'shixiaqu',
            'spy' : 'sxq'
        }]
    }, {
        'areaCode' : '710800',
        'areaName' : '高雄市',
        'py' : 'gaoxiong',
        'spy' : '',
        'datas' : [{
            'areaCode' : '710801',
            'areaName' : '市辖区',
            'py' : 'shixiaqu',
            'spy' : 'sxq'
        }]
    }]
}, {
    'areaCode' : '810000',
    'areaName' : '香港特别行政区',
    'py' : 'xianggangtebiexingzhengqu',
    'spy' : 'xgtbxzq',
    'datas' : [{
        'areaCode' : '810100',
        'areaName' : '香港',
        'py' : 'xianggang',
        'spy' : 'xg',
        'datas' : [{
            'areaCode' : '810110',
            'areaName' : '香港',
            'py' : 'xianggang',
            'spy' : 'xg'
        }]
    }]
}, {
    'areaCode' : '820000',
    'areaName' : '澳门特别行政区',
    'py' : 'aomentebiexingzhengqu',
    'spy' : 'amtbxzq',
    'datas' : [{
        'areaCode' : '820100',
        'areaName' : '澳门',
        'py' : 'aomen',
        'spy' : 'am',
        'datas' : [{
            'areaCode' : '820110',
            'areaName' : '澳门',
            'py' : 'aomen',
            'spy' : 'am'
        }]
    }]
}];

